import React from 'react';
import { ModuleType, TEST } from '../constants';
import { BookingServices, RoutesTabType } from '../models/permission';
import FlightOrderAddBaggage from '../modules/order/flight/baggage/pages/FlightOrderAddBaggage';
import FlightOrderBaggagePay from '../modules/order/flight/baggagePay/pages/FlightOrderBaggagePay';
import FlightOrderDetail from '../modules/order/flight/detail/pages/FlightOrderDetail';
import FlightOrderHoldingPay from '../modules/order/flight/holdingPay/pages/FlightOrderHoldingPay';
import FlightOrder from '../modules/order/flight/result/pages/FlightOrder';
import { retry } from './utils';

const NotFoundBox = React.lazy(() =>
  retry(() => import('../modules/common/components/NotFoundBox')),
);

/* -------------Flight Booking----------------- */
const Flight = React.lazy(() => import('../modules/booking/flight/default/pages/Flight'));

const FlightResult = React.lazy(() =>
  retry(() => import('../modules/booking/flight/result/pages/FlightResult')),
);
const FlightBookingInfo = React.lazy(() =>
  retry(() => import('../modules/booking/flight/booking/pages/FlightBookingInfo')),
);
const FlightReview = React.lazy(() =>
  retry(() => import('../modules/booking/flight/review/pages/FlightReview')),
);

const FlightPay = React.lazy(() =>
  retry(() => import('../modules/booking/flight/payment/pages/FlightPay')),
);

/* -------------Checkout----------------- */
const PartnerPaymentSuccess = React.lazy(() =>
  retry(() => import('../modules/booking/checkout/paymentSuccess/pages/PartnerPaymentSuccess')),
);

export const BOOKING_URL = '/';
export const ORDER_URL = '/account-info';
export const CHECKOUT_URL = '/checkout';
export const PAYMENT_URL = '/payment';

function buildRoutePathBooking(moduleName: BookingServices, path: string) {
  return `${BOOKING_URL}${moduleName}${path}`;
}
function buildRoutePathManager(moduleName: BookingServices, path: string) {
  return `${ORDER_URL}/${moduleName}${path}`;
}

export const ROUTES = {
  terms: '/terms',
  notFound: '/404',
  booking: {
    flight: {
      default: buildRoutePathBooking('', ''),
      result: buildRoutePathBooking('', 'result'),
      bookingInfo: buildRoutePathBooking('', 'bookingInfo'),
      review: buildRoutePathBooking('', 'review'),
      pay: buildRoutePathBooking('', 'pay'),
    },
    hotel: {
      default: buildRoutePathBooking('hotel', '/default'),
      result: buildRoutePathBooking('hotel', '/result'),
      detail: buildRoutePathBooking('hotel', '/detail'),
      bookingInfo: buildRoutePathBooking('hotel', '/bookingInfo'),
      pay: buildRoutePathBooking('hotel', '/pay'),
    },
    checkout: {
      paymentSuccess: buildRoutePathBooking('payment', '/partnerSuccess/:moduleType/:bookingId'),
      success: buildRoutePathBooking('checkout', '/partnerSuccess/:moduleType/:bookingId'),
      paySuccess: {
        value: buildRoutePathBooking('checkout', '/success/:moduleType/:bookingId'),
        gen: (moduleType: ModuleType, bookingID: string) =>
          `/checkout/success/${moduleType}/${bookingID}`,
      },
      payFailure: {
        value: buildRoutePathBooking('checkout', '/fail/:moduleType'),
        gen: (moduleType: ModuleType) => `/checkout/fail/${moduleType}`,
      },
    },
  },
  order: {
    flight: {
      result: buildRoutePathManager('order', '/flight'),
      detail: {
        value: buildRoutePathManager('order', '/flight/detail/:bookingId'),
        gen: (bookingID: string) => buildRoutePathManager('order', `/flight/detail/${bookingID}`),
      },
      baggage: {
        value: buildRoutePathManager('order', '/flight/baggage/:bookingId'),
        gen: (bookingID: string) => buildRoutePathManager('order', `/flight/baggage/${bookingID}`),
      },
      baggagePay: buildRoutePathManager('order', '/flight/baggagePay'),
      holdingPay: buildRoutePathManager('order', '/flight/holdingPay'),
      holdingPayNow: buildRoutePathManager('order', '/flight/holdingPayNow'),
    },

    notFound: buildRoutePathManager('order', '/404'),
  },
};

export const ROUTES_TAB: RoutesTabType[] = [];

export const ROUTES_BOOKING: RoutesTabType[] = [
  {
    name: 'notFound',
    isModule: true,
    path: ROUTES.notFound,
    component: NotFoundBox,
    disableBreadcrumb: true,
    hidden: true,
  },
  {
    name: 'flight.home',
    isModule: true,
    path: ROUTES.booking.flight.default,
    component: Flight,
    disableBreadcrumb: true,
    exact: true,
    hiddenMenu: [
      {
        name: 'flight.flightResult',
        isModule: true,
        path: ROUTES.booking.flight.result,
        component: FlightResult,
        disableBreadcrumb: true,
        exact: true,
        hiddenMenu: [
          {
            name: 'flight.flightBookingInfo',
            path: ROUTES.booking.flight.bookingInfo,
            component: FlightBookingInfo,
            exact: true,
            hiddenMenu: [
              {
                name: 'flight.flightReview',
                path: ROUTES.booking.flight.review,
                component: FlightReview,
                exact: true,
                hiddenMenu: [
                  {
                    title: 'flight.payTitle',
                    name: 'flight.pay',
                    path: ROUTES.booking.flight.pay,
                    component: FlightPay,
                    exact: true,
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
];

export const ROUTES_CHECKOUT: RoutesTabType[] = [
  {
    name: 'notFound',
    isModule: true,
    path: ROUTES.notFound,
    component: NotFoundBox,
    disableBreadcrumb: true,
    hidden: true,
  },
  {
    name: 'checkout.success',
    isModule: true,
    path: ROUTES.booking.checkout.paymentSuccess,
    component: PartnerPaymentSuccess,
    disableBreadcrumb: true,
  },
  {
    name: 'checkout.success',
    isModule: true,
    path: ROUTES.booking.checkout.success,
    component: PartnerPaymentSuccess,
    disableBreadcrumb: true,
  },
  {
    name: 'checkout.paySuccess',
    isModule: true,
    path: ROUTES.booking.checkout.paySuccess.value,
    component: PartnerPaymentSuccess,
    disableBreadcrumb: true,
  },
  {
    name: 'checkout.payFailure',
    isModule: true,
    path: ROUTES.booking.checkout.payFailure.value,
    component: PartnerPaymentSuccess,
    disableBreadcrumb: true,
  },
];

export const ROUTES_ORDER: RoutesTabType[] = [
  {
    name: 'notFound',
    isModule: true,
    path: ROUTES.order.notFound,
    component: NotFoundBox,
    disableBreadcrumb: true,
    hidden: true,
    exact: true,
  },
  {
    name: 'order',
    isModule: true,
    exact: true,
    subMenu: [
      {
        name: 'order.flight',
        path: ROUTES.order.flight.result,
        component: FlightOrder,
        exact: true,
        hiddenMenu: [
          {
            name: 'order.flight.holding.pay',
            path: ROUTES.order.flight.holdingPayNow,
            component: FlightOrderHoldingPay,
            exact: true,
          },
          {
            name: 'order.flight.detail',
            path: ROUTES.order.flight.detail.value,
            component: FlightOrderDetail,
            exact: true,
            hiddenMenu: [
              {
                name: 'order.flight.addMoreCheckIn',
                path: ROUTES.order.flight.baggage.value,
                component: FlightOrderAddBaggage,
                exact: true,
                hiddenMenu: [
                  {
                    name: 'order.flight.baggage.pay',
                    path: ROUTES.order.flight.baggagePay,
                    component: FlightOrderBaggagePay,
                    exact: true,
                  },
                ],
              },
              {
                name: 'order.flight.holding.pay',
                path: ROUTES.order.flight.holdingPay,
                component: FlightOrderHoldingPay,
                exact: true,
              },
            ],
          },
        ],
      },
      {
        name: 'aside.bookingHotel',
        ref: TEST
          ? 'https://ver3s.mytour.vn/account-info/order/hotel'
          : 'https://mytour.vn/account-info/order/hotel',
      },
      {
        name: 'aside.bookingTour',
        ref: TEST
          ? 'https://ver3s.mytour.vn/account-info/order/tour'
          : 'https://mytour.vn/account-info/order/tour',
      },
      {
        name: 'aside.reviewHotel',
        ref: TEST
          ? 'https://ver3s.mytour.vn/account-info/order/hotel-review'
          : 'https://mytour.vn/account-info/order/hotel-review',
      },
    ],
  },
  {
    name: 'aside.profileInfo',
    isModule: true,
    ref: TEST
      ? 'https://ver3s.mytour.vn/account-info/profile-info'
      : 'https://mytour.vn/account-info/profile-info',
  },
  {
    name: 'aside.member',
    isModule: true,
    subMenu: [
      {
        name: 'aside.promotion',
        ref: TEST
          ? 'https://ver3s.mytour.vn/account-info/member/promotion'
          : 'https://mytour.vn/account-info/member/promotion',
      },
      {
        name: 'aside.point',
        ref: TEST
          ? 'https://ver3s.mytour.vn/account-info/member/vpoint'
          : 'https://mytour.vn/account-info/member/vpoint',
      },
    ],
  },
  {
    name: 'aside.favoriteHotel',
    ref: TEST
      ? 'https://ver3s.mytour.vn/account-info/favorite-hotels'
      : 'https://mytour.vn/account-info/favorite-hotels',
    isModule: true,
  },
];
