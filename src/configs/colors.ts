import colors from '../scss/colors.module.scss';
import colorsMyTour from '../scss/colors.myTour.module.scss';
import { CA_ID, MT_ID } from '../constants';

export const {
  TEAL,
  TEAL_300,
  TEAL_200,
  TEAL_100,
  TEAL_50,

  PURPLE,
  PURPLE_300,
  PURPLE_200,
  PURPLE_100,
  PURPLE_50,

  BLACK,
  GREY,
  GREY_900,
  GREY_700,
  GREY_500,
  GREY_300,
  GREY_100,
  GREY_50,
  WHITE,

  BLUE,
  BLUE_300,
  BLUE_200,
  BLUE_100,
  BLUE_50,

  GREEN,
  GREEN_300,
  GREEN_200,
  GREEN_100,
  GREEN_50,

  RED,
  RED_300,
  RED_200,
  RED_100,
  RED_50,

  BROWN,
  BROWN_300,
  BROWN_200,
  BROWN_100,
  BROWN_50,

  ORANGE,
  ORANGE_500,
  ORANGE_300,
  ORANGE_200,
  ORANGE_100,
  ORANGE_50,

  YELLOW,
  YELLOW_300,
  YELLOW_200,
  YELLOW_100,
  YELLOW_50,

  PINK,
  PINK_300,
  PINK_200,
  PINK_100,
  PINK_50,
} = CA_ID === MT_ID ? colorsMyTour : colors;

// export const TEAL = '#00aca0';
// export const TEAL_300 = '#4db6ac';
// export const TEAL_200 = '#80cbc4';
// export const TEAL_100 = '#b2dfdb';
// export const TEAL_50 = '#e0f2f1';

// export const PURPLE = '#7e57c2';
// export const PURPLE_300 = '#9575cd';
// export const PURPLE_200 = '#b39ddb';
// export const PURPLE_100 = '#d1c4e9';
// export const PURPLE_50 = '#ede7f6';

// export const BLACK = '#000000';
// export const GREY = '#212121';
// export const GREY_900 = '#212121';
// export const GREY_700 = '#616161';
// export const GREY_500 = '#9e9e9e';
// export const GREY_300 = '#e0e0e0';
// export const GREY_100 = '#f5f5f5';
// export const GREY_50 = '#fafafa';
// export const WHITE = '#ffffff';

// export const BLUE = '#276ef1';
// export const BLUE_300 = '#5a90f4';
// export const BLUE_200 = '#9fbff8';
// export const BLUE_100 = '#d4e2fc';
// export const BLUE_50 = '#edf3fd';

// export const GREEN = '#00ad50';
// export const GREEN_300 = '#43bf75';
// export const GREEN_200 = '#9ee2b8';
// export const GREEN_100 = '#cff3dd';
// export const GREEN_50 = '#f0faf3';

// export const RED = '#f44336';
// export const RED_300 = '#e57373';
// export const RED_200 = '#ef9a9a';
// export const RED_100 = '#ffcdd2';
// export const RED_50 = '#ffebee';

// export const BROWN = '#653818';
// export const BROWN_300 = '#b18976';
// export const BROWN_200 = '#d2bab0';
// export const BROWN_100 = '#ebe0db';
// export const BROWN_50 = '#f6f2f0';

// export const ORANGE = '#ff6a39';
// export const ORANGE_300 = '#f19063';
// export const ORANGE_200 = '#f7bfa5';
// export const ORANGE_100 = '#fbe2d6';
// export const ORANGE_50 = '#fdf3ee';

// export const YELLOW = '#ffb822';
// export const YELLOW_300 = '#ffcf70';
// export const YELLOW_200 = '#ffe3ac';
// export const YELLOW_100 = '#fff2d9';
// export const YELLOW_50 = '#fff9ef';

// export const PINK = '#cc0066';
// export const PINK_300 = '#f06292';
// export const PINK_200 = '#f48fb1';
// export const PINK_100 = '#f8bbd0';
// export const PINK_50 = '#fce4ec';

export const BACK_GROUND = BLUE_50;
export const PRIMARY = CA_ID === 17 ? BLUE_300 : TEAL;
export const SECONDARY = CA_ID === 17 ? RED : PURPLE;
