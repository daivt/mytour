import { DEV, TEST, STAGING } from '../constants';

enum APIServices {
  account,
  flight,
  hotel,
  general,
}

function getBaseUrl(service: APIServices) {
  if (service === APIServices.account) {
    return DEV
      ? '/api/account/'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://flightapi.tripi.vn/v3';
  }
  if (service === APIServices.general) {
    return DEV
      ? '/api/general/'
      : TEST
      ? 'https://dev-api.tripi.vn'
      : STAGING
      ? 'https://stageapi.tripi.vn'
      : 'https://flightapi.tripi.vn/v3';
  }

  if (service === APIServices.flight) {
    if (DEV) {
      return '/api/flight/';
    }
    if (TEST) {
      return 'https://dev-api.tripi.vn';
    }
    if (STAGING) {
      return 'https://stageapi.tripi.vn';
    }
    return 'https://flightapi.tripi.vn/v3';
  }
  if (service === APIServices.hotel) {
    if (DEV) {
      return '/api/hotel/';
    }
    if (TEST) {
      return 'https://dev-api.tripi.vn';
    }
    if (STAGING) {
      return 'https://stageapi.tripi.vn';
    }

    return 'https://hotelapi.tripi.vn/v3';
  }
  return null;
}

export const API_PATHS = {
  searchAirports: (term: string) =>
    `${getBaseUrl(
      APIServices.flight,
    )}/flights/searchAirports?num_items=10&term=${encodeURIComponent(term)}`,
  searchAirlines: (term: string) =>
    `${getBaseUrl(APIServices.flight)}/flight/searchAirline?term=${encodeURIComponent(term)}`,
  login: `${getBaseUrl(APIServices.flight)}/account/loginV2`,
  validateAccessToken: `${getBaseUrl(APIServices.flight)}/account/login-with-oauth`,
  // Flight
  getListLocationsInformation: `${getBaseUrl(APIServices.flight)}/getListLocationsInformation`,
  searchOneWayTickets: `${getBaseUrl(APIServices.flight)}/flights/searchOneDirectionTickets`,
  searchTwoWayTickets: `${getBaseUrl(APIServices.flight)}/flights/searchRoundTripTickets`,
  flightGeneralInfo: `${getBaseUrl(APIServices.flight)}/flights/getGeneralInformation`,
  getTicketDetail: `${getBaseUrl(APIServices.flight)}/flights/getTicketDetail`,
  getInsurancePackage: `${getBaseUrl(APIServices.flight)}/insurance/getInsurancePackage`,
  validateInsuranceInfo: `${getBaseUrl(APIServices.flight)}/insurance/validateInsuranceInfo`,
  getFlightPaymentMethods: `${getBaseUrl(APIServices.flight)}/flight/getPaymentMethods`,
  bookTicket: `${getBaseUrl(APIServices.flight)}/flight/bookTicket`,
  getAllCountries: `${getBaseUrl(APIServices.flight)}/account/getAllCountries`,
  checkBookingVoidability: `${getBaseUrl(APIServices.flight)}/flights/checkBookingVoidability`,
  checkDividable: `${getBaseUrl(APIServices.flight)}/flight/checkDividable`,
  divideBooking: `${getBaseUrl(APIServices.flight)}/flight/divideBooking`,
  refundBooking: `${getBaseUrl(APIServices.flight)}/flights/refundBooking`,
  getTopDestinations: `${getBaseUrl(APIServices.flight)}/flight/getTopDestinations`,
  getTopHotelLocation: (size: number) =>
    `${getBaseUrl(APIServices.hotel)}/homepage/topHotelLocations?size=${size}`,
  suggestLocation: (term: string) =>
    `${getBaseUrl(APIServices.hotel)}/hotels/suggest?maxItems=5&term=${encodeURIComponent(term)}`,
  getFlightBookingDetail: `${getBaseUrl(APIServices.flight)}/account/getFlightBookingDetail`,
  getBaggageForFlightBooking: `${getBaseUrl(APIServices.flight)}/booker/getBaggageForFlightBooking`,
  getPaymentMethodsForAddingBaggages: `${getBaseUrl(
    APIServices.flight,
  )}/booker/getPaymentMethodsForAddingBaggages`,
  addBaggagesForFlightBooking: `${getBaseUrl(
    APIServices.flight,
  )}/booker/addBaggagesForFlightBooking`,
  getPaymentMethodOfHoldingBooking: `${getBaseUrl(
    APIServices.flight,
  )}/booker/getPaymentMethodOfHoldingBooking`,
  paymentForHoldingBooking: `${getBaseUrl(APIServices.flight)}/booker/paymentForHoldingBooking`,
  getEnterpriseInfo: `${getBaseUrl(APIServices.flight)}/utils/getEnterpriseInfo`,

  // Hotel
  searchHotel: `${getBaseUrl(APIServices.hotel)}/hotels/search`,
  hotelDetail: `${getBaseUrl(APIServices.hotel)}/hotels/details`,
  hotelPrice: `${getBaseUrl(APIServices.hotel)}/hotels/prices`,
  bookHotel: `${getBaseUrl(APIServices.hotel)}/hotels/bookHotelRoom`,
  getHotelReviews: `${getBaseUrl(APIServices.hotel)}/hotels/getReviews`,
  getHotelPaymentMethods: `${getBaseUrl(APIServices.hotel)}/checkout/getPaymentMethods`,

  getPointPayment: `${getBaseUrl(APIServices.hotel)}/payment/getPointPayment`,
  getRewardsHistory: `${getBaseUrl(APIServices.flight)}/account/getRewardsHistory`,
  checkPromotion: `${getBaseUrl(APIServices.flight)}/promotion/checkCode`,

  ticketAlertGeneralInformation: `${getBaseUrl(APIServices.flight)}/tpa/personalInformation`,
  ticketAlert: `${getBaseUrl(APIServices.flight)}/tpa`,
  searchHotTickets: `${getBaseUrl(APIServices.flight)}/hotFne/searchHotTickets`,

  sendChangeCreditPasswordOtp: `${getBaseUrl(APIServices.flight)}/sendChangeCreditPasswordOtp`,
  sendCreateCreditPasswordOtp: `${getBaseUrl(APIServices.flight)}/sendCreateCreditPasswordOtp`,
  changeCreditPassword: `${getBaseUrl(APIServices.flight)}/changeCreditPassword`,
  createNewCreditPassword: `${getBaseUrl(APIServices.flight)}/createNewCreditPassword`,
  validateCreditPassword: `${getBaseUrl(APIServices.flight)}/validateCreditPassword`,

  // Checkout
  checkoutSuccess: `${getBaseUrl(APIServices.general)}/smartlink/displayResult`,
  getFlightTask: `${getBaseUrl(APIServices.flight)}/booker/getFlightTask`,
  getBookingInformation: `${getBaseUrl(APIServices.flight)}/flight/getBookingInformation`,

  // Order
  getFlightBookings: `${getBaseUrl(APIServices.flight)}/account/getFlightBookings`,
};
