import { combineReducers } from 'redux';
import flightReducer, { FlightBookingState } from '../flight/redux/flightBookingReducer';

export interface BookingState {
  flight: FlightBookingState;
}

export default combineReducers({
  flight: flightReducer,
});
