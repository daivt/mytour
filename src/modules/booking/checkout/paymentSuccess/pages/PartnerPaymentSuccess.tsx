import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { ModuleType, some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { fetchThunk } from '../../../../common/redux/thunk';
import PaymentSuccessDesktop from '../components/PaymentSuccessDesktop';

const mapStateToProps = (state: AppState) => {
  return {
    stateRouter: state.router.location.state,
    router: state.router,
  };
};

interface Props extends RouteComponentProps<{ moduleType: ModuleType; bookingId: string }> {
  dispatch: ThunkDispatch<AppState, null, Action<string>>;
}

const PartnerPaymentSuccess: React.FC<Props> = props => {
  const { dispatch, match } = props;

  const { bookingId, moduleType } = match.params;
  const [loading, setLoading] = React.useState(false);
  const [messageJson, setMessageJson] = React.useState<some>();
  const [bookingInfo, setBookingInfo] = React.useState<some | undefined>(undefined);
  const [baggagesInfo, setBaggagesInfo] = React.useState<some | undefined>(undefined);

  const fetchData = React.useCallback(async () => {
    setLoading(true);
    await dispatch(
      fetchThunk(API_PATHS.checkoutSuccess, 'post', { url: window.location.href }),
    ).then(async value => {
      let fetchBookingInfo;
      if (moduleType === 'baggages') {
        await dispatch(fetchThunk(API_PATHS.getFlightTask, 'post', { bookingId })).then(async v => {
          fetchBookingInfo = await dispatch(
            fetchThunk(API_PATHS.getBookingInformation, 'post', {
              bookingId: v.data.bookingInfo.bookingEncrypted,
            }),
          );
          setBaggagesInfo(v.data);
        });
      } else {
        fetchBookingInfo = await dispatch(
          fetchThunk(API_PATHS.getBookingInformation, 'post', { bookingId }),
        );
      }

      if (fetchBookingInfo?.data) {
        setBookingInfo(fetchBookingInfo.data);
      }
      if (fetchBookingInfo?.data?.paymentStatus === 'pending') {
        await new Promise(resolve => setTimeout(resolve, 2500));
      }
      setMessageJson(value);
      setLoading(false);
    });
  }, [dispatch, moduleType, bookingId]);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <PaymentSuccessDesktop
      loading={loading}
      moduleType={moduleType}
      messageJson={messageJson}
      bookingInfo={bookingInfo}
      baggagesInfo={baggagesInfo}
    />
  );
};

export default connect(mapStateToProps)(PartnerPaymentSuccess);
