import { some } from "../../../../constants";
import { PRIMARY, SECONDARY } from "../../../../configs/colors";

export function getMainColor(data: some) {
  return {
    primary: data?.mainColorCode ?? PRIMARY,
    secondary: SECONDARY,
  };
}
