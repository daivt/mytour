import { Divider, Paper, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import styled from 'styled-components';
import { BLUE_200 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconAirplane } from '../../../../../svg/booking/ic_airplane_horizontal.svg';
import { ReactComponent as IconCoin } from '../../../../../svg/booking/ic_coin.svg';
import { Col, Row } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';

const Line = styled.div`
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

function renderFlightItineraryItem(info: some, airlineInfo: some, inbound?: boolean) {
  return (
    <>
      <Row>
        <IconAirplane style={{ width: 16, height: 16 }} />
        <Typography variant="subtitle2" style={{ marginLeft: '10px' }}>
          <FormattedMessage id={inbound ? 'flight.inbound' : 'flight.outbound'} />
          &nbsp;-&nbsp;
          {moment(info?.departureDayStr, DATE_FORMAT_BACK_END).format(DATE_FORMAT)}
        </Typography>
      </Row>
      <Row
        className="card-background"
        style={{
          padding: 8,
          marginTop: 8,
        }}
      >
        <Col style={{ flex: 1 }}>
          <Row style={{ flexWrap: 'wrap' }}>
            <Typography variant="body2" style={{ minWidth: 50, marginRight: 16 }}>
              {info.departureTimeStr}&nbsp;
              {moment(info?.departureDayStr, DATE_FORMAT_BACK_END).format(DATE_FORMAT)}
            </Typography>
            <Typography variant="body2">
              {info.departureAirportName} ({info.departureAirport})
            </Typography>
          </Row>
          <Row style={{ marginTop: 8, flexWrap: 'wrap' }}>
            <Typography variant="body2" style={{ minWidth: 50, marginRight: 16 }}>
              {info.arrivalTimeStr}&nbsp;
              {moment(info?.arrivalDayStr, DATE_FORMAT_BACK_END).format(DATE_FORMAT)}
            </Typography>
            <Typography variant="body2">
              {info.arrivalAirportName} ({info.arrivalAirport})
            </Typography>
          </Row>
        </Col>
        <Row>
          {airlineInfo && <img style={{ maxWidth: '48px' }} src={airlineInfo.logo} alt="" />}
        </Row>
      </Row>
    </>
  );
}

interface Props {
  bookingInfo?: some;
}

const BookingInfoBox: React.FC<Props> = props => {
  const { bookingInfo } = props;
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight, shallowEqual);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );

  const outboundTicket = bookingInfo?.tickets?.outbound;
  const inboundTicket = bookingInfo?.tickets?.inbound.aid
    ? bookingInfo?.tickets?.inbound
    : undefined;

  const getPrice = React.useMemo(() => {
    return [
      {
        type: 'payment.adultPrice',
        num: bookingInfo?.numAdults || 0,
        price: (outboundTicket?.priceAdult || 0) + (inboundTicket?.priceAdult || 0),
        priceBadge: (outboundTicket?.priceAdult || 0) + (inboundTicket?.priceAdult || 0),
      },
      {
        type: 'payment.childPrice',
        num: bookingInfo?.numChildren || 0,
        price: (outboundTicket?.priceChild || 0) + (inboundTicket?.priceChild || 0),
      },
      {
        type: 'payment.babyPrice',
        num: bookingInfo?.numInfants || 0,
        price: (outboundTicket?.priceInfant || 0) + (inboundTicket?.priceInfant || 0),
      },
    ];
  }, [bookingInfo, inboundTicket, outboundTicket]);
  const getPriceBaggage = React.useMemo(() => {
    let price = 0;
    bookingInfo?.guests.forEach((element: some) => {
      price += (element?.outboundBaggage?.price || 0) + (element?.inboundBaggage?.price || 0);
    });
    return price;
  }, [bookingInfo]);

  const getDetail = React.useMemo(() => {
    const list = [];

    if (getPriceBaggage > 0) {
      list.push(
        <Line key={1}>
          <Typography variant="body2">
            <FormattedMessage id="payment.baggages" />
          </Typography>
          <Typography variant="body2">
            <FormattedNumber value={getPriceBaggage} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>,
      );
    }
    if (bookingInfo?.insuranceAmount > 0) {
      list.push(
        <Line key={2}>
          <Typography variant="body2">
            <FormattedMessage id="payment.insurance" />
          </Typography>
          <Typography variant="body2">
            <FormattedNumber value={bookingInfo?.insuranceAmount} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>,
      );
    }
    if (bookingInfo?.discount > 0) {
      list.push(
        <Line key={3}>
          <Typography variant="body2">
            <FormattedMessage id="payment.promotion" />
          </Typography>
          <Typography variant="body2">
            <FormattedNumber value={bookingInfo?.discount} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>,
      );
    }
    if (bookingInfo?.paymentMethodFee > 0) {
      list.push(
        <Line key={4}>
          <Typography variant="body2">
            <FormattedMessage id="payment.paymentMethodFee" />
          </Typography>
          <Typography variant="body2">
            <FormattedNumber value={bookingInfo?.paymentMethodFee} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>,
      );
    }
    return list;
  }, [bookingInfo, getPriceBaggage]);

  if (!bookingInfo) {
    return <LoadingIcon style={{ height: '100vh' }} />;
  }

  return (
    <Paper variant="outlined" style={{ marginTop: 40 }}>
      <Row style={{ padding: 24 }}>
        {bookingInfo?.orderCode && (
          <Col style={{ marginRight: 24 }}>
            <Typography variant={matches ? 'subtitle1' : 'h6'}>
              <FormattedMessage id="payment.orderCode" />
            </Typography>
            <Paper
              elevation={0}
              style={{
                padding: '8px 16px',
                background: BLUE_200,
                marginTop: 12,
                textAlign: 'center',
              }}
            >
              <Typography variant={matches ? 'subtitle2' : 'subtitle1'}>
                {bookingInfo?.orderCode}
              </Typography>
            </Paper>
          </Col>
        )}
        {bookingInfo?.pnr && (
          <Col
            style={{
              borderLeft: '2px dashed #00adef4d',
              paddingLeft: 24,
              position: 'relative',
            }}
          >
            <Typography variant={matches ? 'subtitle1' : 'h6'}>
              <FormattedMessage id="payment.prnCode" />
            </Typography>
            <Paper
              variant="outlined"
              style={{
                padding: '8px 16px',
                marginTop: 12,
                textAlign: 'center',
              }}
            >
              <Typography variant={matches ? 'subtitle2' : 'subtitle1'}>
                {bookingInfo?.pnr}
              </Typography>
            </Paper>
          </Col>
        )}
      </Row>
      <Row>
        <Col style={{ flex: 1 }}>
          <Row
            style={{
              flex: 1,
              alignItems: 'flex-start',
              flexWrap: 'wrap',
            }}
          >
            <Col style={{ flex: 1, margin: '12px 24px', minWidth: 200 }}>
              <Typography variant="h6">
                <FormattedMessage id="payment.flightInfo" />
              </Typography>
              <Paper variant="outlined" style={{ padding: 16, marginTop: 16 }}>
                {outboundTicket &&
                  renderFlightItineraryItem(
                    outboundTicket,
                    getFlightGeneralInfo(outboundTicket.aid),
                  )}

                {inboundTicket && (
                  <div style={{ marginTop: 16 }}>
                    {renderFlightItineraryItem(
                      inboundTicket,
                      getFlightGeneralInfo(inboundTicket.aid),
                      true,
                    )}
                  </div>
                )}
              </Paper>
            </Col>
            <Col style={{ flex: 1, margin: '12px 24px', minWidth: 200 }}>
              <Typography variant="h6">
                <FormattedMessage id="payment.flightBookingInfo" />
              </Typography>
              <Paper variant="outlined" style={{ overflow: 'hidden', marginTop: 16, padding: 16 }}>
                {bookingInfo.guests.map((one: some, i: number) => (
                  <Line key={i}>
                    <Typography variant="body2">
                      <FormattedMessage id={`payment.${one.ageCategory}.${one.gender}`} />
                      &nbsp;
                      {one.fullName}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      <FormattedMessage id={`flight.${one.ageCategory}`} values={{ num: '' }} />
                    </Typography>
                  </Line>
                ))}
              </Paper>
            </Col>
          </Row>
          <Row
            style={{
              flex: 1,
              alignItems: 'flex-start',
              flexWrap: 'wrap',
            }}
          >
            <Col style={{ flex: 1, margin: '12px 24px', minWidth: 200 }}>
              <Typography variant="h6">
                <FormattedMessage id="payment.contactInfo" />
              </Typography>
              <Paper variant="outlined" style={{ padding: 16, marginTop: 16 }}>
                {bookingInfo.contactInfo && (
                  <Col>
                    <Line>
                      <Typography variant="body2">
                        <FormattedMessage id="fullName" />
                      </Typography>
                      <Typography variant="body2">
                        {bookingInfo?.contactInfo?.fullName?.toUpperCase()}
                      </Typography>
                    </Line>
                    <Line>
                      <Typography variant="body2">
                        <FormattedMessage id="gender" />
                      </Typography>
                      <Typography variant="body2">
                        <FormattedMessage
                          id={bookingInfo.contactInfo?.title === 'Mr' ? 'male' : 'female'}
                        />
                      </Typography>
                    </Line>
                    <Line>
                      <Typography variant="body2">
                        <FormattedMessage id="phoneNumber" />
                      </Typography>
                      <Typography variant="body2">{bookingInfo.contactInfo.phone1}</Typography>
                    </Line>
                    <Line>
                      <Typography variant="body2">
                        <FormattedMessage id="email" />:
                      </Typography>
                      <Typography variant="body2">{bookingInfo.contactInfo.email}</Typography>
                    </Line>
                    {bookingInfo.contactInfo.addr1 && (
                      <Line>
                        <Typography variant="body2">
                          <FormattedMessage id="address" />
                        </Typography>
                        <Typography variant="body2">{bookingInfo.contactInfo.addr1}</Typography>
                      </Line>
                    )}
                  </Col>
                )}
              </Paper>
            </Col>
            <Col style={{ flex: 1, margin: '12px 24px', minWidth: 200 }}>
              <Typography variant="h6">
                <FormattedMessage id="payment.detailInfo" />
              </Typography>
              <Paper variant="outlined" style={{ overflow: 'hidden', marginTop: 16 }}>
                <div style={{ padding: '8px 16px' }}>
                  {getPrice.map(
                    (one: some, i: number) =>
                      one.num > 0 && (
                        <Line key={i}>
                          <Typography variant="body2">
                            <FormattedMessage id={one.type} />
                          </Typography>
                          <Typography variant="body2">
                            {one.num} x <FormattedNumber value={one.price} />
                            &nbsp;
                            <FormattedMessage id="currency" />
                          </Typography>
                        </Line>
                      ),
                  )}
                </div>

                {getDetail.length > 0 && (
                  <>
                    <Divider /> <div style={{ padding: '8px 16px' }}>{getDetail}</div>
                  </>
                )}
                <div className="card-background" style={{ padding: '8px 16px' }}>
                  <Line>
                    <Typography variant="subtitle1">
                      <FormattedMessage id="payment.totalPrice" />
                    </Typography>
                    <Typography className="final-price" variant="subtitle1">
                      <FormattedNumber value={bookingInfo.finalPrice} />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </Typography>
                  </Line>
                  {bookingInfo.bonusPoint > 0 && (
                    <Row style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                      <IconCoin />
                      &nbsp;
                      <Typography className="point" variant="body2" style={{ textAlign: 'end' }}>
                        <FormattedNumber value={bookingInfo.bonusPoint} />
                        &nbsp;
                        <FormattedMessage id="point" />
                      </Typography>
                    </Row>
                  )}
                </div>
              </Paper>
            </Col>
          </Row>
        </Col>
      </Row>
    </Paper>
  );
};

export default BookingInfoBox;
