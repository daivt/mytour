import { Button, Container, Paper, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import CheckRoundedIcon from '@material-ui/icons/CheckRounded';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import React from 'react';
import { FormattedHTMLMessage, FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { GREEN_100, GREEN_200, RED_100, RED_200 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { ReactComponent as IconArrowRight } from '../../../../../svg/ic_arrow_right.svg';
import { Col, Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { ROUTES } from '../../../../../configs/routes';
import { AppState } from '../../../../../redux/reducers';

const Line2 = styled.div`
  min-height: 36px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
`;

interface Props {
  messageJson?: some;
  bookingInfo?: some;
  baggagesInfo?: some;
}

const BaggagesBookingInfo: React.FC<Props> = props => {
  const { messageJson, bookingInfo, baggagesInfo } = props;
  const auth = useSelector((state: AppState) => state.auth.auth);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));
  if (!bookingInfo || !baggagesInfo) {
    return <LoadingIcon style={{ height: '100vh' }} />;
  }

  return (
    <>
      <Col
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          padding: 42,
          background: messageJson?.paymentStatus === 'fail' ? RED_100 : GREEN_100,
        }}
      >
        {messageJson?.paymentStatus === 'fail' ? (
          <WarningRoundedIcon
            style={{
              height: 150,
              width: 'auto',
              color: RED_200,
            }}
          />
        ) : (
          <CheckRoundedIcon
            style={{
              height: 150,
              width: 'auto',
              color: GREEN_200,
            }}
          />
        )}
        <div style={{ position: 'absolute' }}>
          <Typography
            variant={matches ? 'h6' : 'h4'}
            style={{
              margin: '12px 0',
              textAlign: 'center',
            }}
          >
            <FormattedMessage
              id={
                messageJson?.paymentStatus === 'success'
                  ? 'payment.changeBaggages.success'
                  : 'payment.changeBaggages.fail'
              }
            />
          </Typography>
          <Typography variant="subtitle2" style={{ textAlign: 'center', marginTop: 16 }}>
            <FormattedHTMLMessage
              id="payment.tripiSupportText"
              values={{
                chanel: bookingInfo?.caAccount?.fullName,
                phone: bookingInfo?.caAccount?.hotline,
              }}
            />
          </Typography>
          <Typography variant="subtitle2" style={{ textAlign: 'center' }}>
            <FormattedHTMLMessage
              id="payment.tripiReadyServeYou"
              values={{
                chanel: bookingInfo?.caAccount?.fullName,
              }}
            />
          </Typography>
        </div>
      </Col>
      <Container style={{ maxWidth: 1024 }}>
        {baggagesInfo && (
          <Paper variant="outlined" style={{ padding: 24, marginTop: 40 }}>
            <Typography variant="h6">
              <FormattedMessage id="payment.baggagesInfo" />
            </Typography>

            {baggagesInfo.addedBaggages.map((v: some, i: number) => (
              <Col key={i}>
                <Line2>
                  <Typography variant="subtitle2">
                    <FormattedMessage id="payment.guest" />
                    &nbsp;{i + 1}
                    :&nbsp;
                  </Typography>
                  <Typography variant="body2">{v.guestName}</Typography>
                </Line2>
                {(v.fromOutboundBaggage || v.toOutboundBaggage) && (
                  <>
                    <Typography variant="subtitle2">
                      <FormattedMessage id="flight.outbound" />
                      :&nbsp;
                      {v.outboundAmount > 0 && (
                        <Typography variant="body2" component="span">
                          <FormattedMessage id="payment.payMore" />
                          &nbsp;-&nbsp;
                          <FormattedNumber value={v.outboundAmount} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      )}
                    </Typography>
                    <Line2>
                      <Typography variant="body2">
                        {v.fromOutboundBaggage?.price > 0 ? (
                          <>
                            {v.fromOutboundBaggage?.name}
                            &nbsp;-&nbsp;
                            <FormattedNumber value={v.fromOutboundBaggage?.price} />
                            &nbsp;
                            <FormattedMessage id="currency" />
                          </>
                        ) : (
                          <FormattedMessage id="payment.noBaggages" />
                        )}
                      </Typography>
                      <IconArrowRight style={{ margin: '0px 16px' }} />
                      <Typography variant="body2">
                        {v.toOutboundBaggage?.name}
                        &nbsp;-&nbsp;
                        {v.toOutboundBaggage?.price > 0 && (
                          <FormattedNumber value={v.toOutboundBaggage?.price} />
                        )}
                        &nbsp;
                        <FormattedMessage id="currency" />
                      </Typography>
                    </Line2>
                  </>
                )}
                {(v.fromInboundBaggage || v.toInboundBaggage) && (
                  <>
                    <Typography variant="subtitle2">
                      <FormattedMessage id="flight.inbound" />
                      :&nbsp;
                      {v.inboundAmount > 0 && (
                        <Typography variant="body2" component="span">
                          <FormattedMessage id="payment.payMore" />
                          &nbsp;-&nbsp;
                          <FormattedNumber value={v.inboundAmount} />
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      )}
                    </Typography>
                    <Line2>
                      <Typography variant="body2">
                        {v.fromInboundBaggage?.price > 0 ? (
                          <>
                            {v.fromInboundBaggage?.name}
                            &nbsp;-&nbsp;
                            <FormattedNumber value={v.fromInboundBaggage?.price} />
                            &nbsp;
                            <FormattedMessage id="currency" />
                          </>
                        ) : (
                          <FormattedMessage id="payment.noBaggages" />
                        )}
                      </Typography>
                      <IconArrowRight style={{ margin: '0px 16px' }} />
                      <Typography variant="body2">
                        {v.toInboundBaggage?.name}
                        &nbsp;-&nbsp;
                        {v.toInboundBaggage?.price > 0 && (
                          <FormattedNumber value={v.toInboundBaggage?.price} />
                        )}
                        &nbsp;
                        <FormattedMessage id="currency" />
                      </Typography>
                    </Line2>
                  </>
                )}
              </Col>
            ))}

            <Typography variant="h6" style={{ marginTop: 20 }}>
              <FormattedMessage id="payment.paymentInfo" />
            </Typography>
            <Line2>
              <Typography variant="subtitle2">
                <FormattedMessage id="payment.totalPrice" />
                :&nbsp;
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={baggagesInfo?.totalAmount || 0} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line2>
          </Paper>
        )}
        <Row style={{ justifyContent: 'center', marginTop: 32, marginBottom: 32 }}>
          <Link to="/">
            <Button variant="outlined" size="large" style={{ minWidth: 240 }}>
              <FormattedMessage id="backToHome" />
            </Button>
          </Link>
          {auth && (
            <Link to={ROUTES.order.flight.result} style={{ marginLeft: 24 }}>
              <Button
                variant="contained"
                color="secondary"
                disableElevation
                size="large"
                style={{ minWidth: 240 }}
              >
                <FormattedMessage id="backToOrder" />
              </Button>
            </Link>
          )}
        </Row>
      </Container>
    </>
  );
};

export default BaggagesBookingInfo;
