import { Divider, Typography } from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import React from 'react';
import { FormattedHTMLMessage, FormattedMessage, FormattedNumber } from 'react-intl';
import { some } from '../../../../../constants';
import { getMainColor } from '../utils';
import { RED, BLUE } from '../../../../../configs/colors';
import PassengerCount from '../../../common/PassengerCount';
import { Row } from '../../../../common/components/elements';

interface Props {
  messageJson?: some;
  bookingInfo?: some;
}

const HotelBookingInfo: React.FC<Props> = props => {
  const { messageJson, bookingInfo } = props;
  const colors = getMainColor(bookingInfo?.caAccount);

  return (
    <div
      style={{
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
      }}
    >
      <Typography
        variant="subtitle1"
        style={{
          margin: '12px 0',
          textAlign: 'center',
          lineHeight: '24px',
          color: messageJson?.code !== 200 ? RED : colors.primary,
        }}
      >
        {messageJson?.message}
      </Typography>

      <Typography variant="subtitle2" style={{ textAlign: 'center', color: colors.primary }}>
        <FormattedHTMLMessage
          id="payment.tripiSupportText"
          values={{
            chanel: bookingInfo?.caAccount?.fullName,
          }}
        />
      </Typography>
      <Typography variant="subtitle2" style={{ textAlign: 'center', color: colors.primary }}>
        <FormattedHTMLMessage
          id="payment.tripiReadyServeYou"
          values={{
            chanel: bookingInfo?.caAccount?.fullName,
          }}
        />
      </Typography>

      <Divider style={{ width: '80%', margin: '16px 0' }} />

      <Typography variant="h5" style={{ margin: '24px 0', textAlign: 'center' }}>
        <FormattedMessage id="payment.bookingHotelDetail" />
      </Typography>

      <div style={{ border: `1px solid ${colors.primary}`, width: '100%', maxWidth: '780px' }}>
        <div style={{ background: colors.primary, display: 'flex', justifyContent: 'center' }}>
          <Typography variant="button" style={{ color: 'white', margin: '6px 0' }}>
            <FormattedMessage id="result.hotelInfo" />
          </Typography>
        </div>
        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="m.bookingRoomCode" />
          </Typography>
          <Typography
            variant="body2"
            style={{ flex: 2, fontWeight: 'bold', color: colors.secondary }}
          >
            {bookingInfo?.bookingCode}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.hotelName" />
          </Typography>
          <Typography
            variant="body2"
            style={{
              flex: 2,
              fontWeight: 'bold',
              display: 'flex',
              alignItems: 'center',
              flexWrap: 'wrap',
              color: BLUE,
            }}
          >
            {bookingInfo?.hotelName}
            &nbsp;
            <Rating
              name="half-rating-read"
              value={bookingInfo?.starNum || 0}
              precision={0.5}
              readOnly
              size="small"
            />
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="address" />
          </Typography>
          <Typography variant="body2" style={{ flex: 2, fontWeight: 'bold' }}>
            {bookingInfo?.hotelAddress}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="m.roomCheckinDate" />
          </Typography>
          <Typography variant="body2" style={{ flex: 2, fontWeight: 'bold' }}>
            {bookingInfo?.checkinDate}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="m.roomCheckoutDate" />
          </Typography>
          <Typography variant="body2" style={{ flex: 2, fontWeight: 'bold' }}>
            {bookingInfo?.checkoutDate}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="m.roomType" />
          </Typography>
          <div style={{ flex: 2 }}>
            <Typography variant="body2" style={{ fontWeight: 'bold' }}>
              {bookingInfo?.roomTitle}
            </Typography>
            {bookingInfo?.freeBreakfast && (
              <Typography variant="caption" color="textSecondary">
                <FormattedMessage id="payment.freeBreakfast" />
              </Typography>
            )}
          </div>
        </Row>
        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="booking.hotel.roomInfo" />
          </Typography>
          <Typography variant="body2" style={{ flex: 2, fontWeight: 'bold' }}>
            {bookingInfo?.numRooms}
          </Typography>
        </Row>
        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="order.numPassenger" />
          </Typography>
          <Typography variant="body2" style={{ flex: 2, fontWeight: 'bold' }}>
            <PassengerCount adults={bookingInfo?.numAdults} child={bookingInfo?.numChildren} />
          </Typography>
        </Row>
        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.cancelPolicy" />
          </Typography>
          <div style={{ flex: 2 }}>
            {bookingInfo?.cancellationPolicies.map((val: string, index: number) => (
              <Typography variant="body2" key={index}>
                {val}
              </Typography>
            ))}
          </div>
        </Row>
      </div>

      <div
        style={{
          border: `1px solid ${colors.primary}`,
          width: '100%',
          maxWidth: '780px',
          margin: '24px 0 32px',
        }}
      >
        <div style={{ background: colors.primary, display: 'flex', justifyContent: 'center' }}>
          <Typography variant="button" style={{ color: 'white', margin: '6px 0' }}>
            <FormattedMessage id="payment.paymentInfo" />
          </Typography>
        </div>
        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.personPayment" />
          </Typography>
          <Typography variant="body2" style={{ flex: 1, fontWeight: 'bold' }}>
            {bookingInfo?.customerName}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="email" />
          </Typography>
          <Typography
            variant="body2"
            style={{
              flex: 1,
              fontWeight: 'bold',
              display: 'flex',
              alignItems: 'center',
              wordBreak: 'break-word',
            }}
          >
            {bookingInfo?.customerEmail}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="telephone" />
          </Typography>
          <Typography variant="body2" style={{ flex: 1, fontWeight: 'bold' }}>
            {bookingInfo?.customerPhone}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.bookingDate" />
          </Typography>
          <Typography variant="body2" style={{ flex: 1, fontWeight: 'bold' }}>
            {bookingInfo?.bookingDate}
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="payment.totalPrice" />
          </Typography>
          <Typography variant="body2" style={{ flex: 1, fontWeight: 'bold' }}>
            <FormattedNumber value={bookingInfo?.totalPrice || 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>

        <Row>
          <Typography variant="body2" style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}>
            <FormattedMessage id="mobile.promotion" />
          </Typography>
          <Typography variant="body2" style={{ flex: 1, fontWeight: 'bold' }}>
            <FormattedNumber value={bookingInfo?.discount || 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>

        <Row>
          <Typography
            variant="subtitle2"
            style={{ flex: 1, marginRight: '16px', fontWeight: 'bold' }}
          >
            <FormattedMessage id="payment.finalPrice" />
          </Typography>
          <Typography
            variant="subtitle2"
            style={{ flex: 1, fontWeight: 'bold', color: colors.secondary }}
          >
            <FormattedNumber value={bookingInfo?.finalPrice || 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>
      </div>
    </div>
  );
};

export default HotelBookingInfo;
