import { Button, Card, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ReactComponent as IconWarning } from '../../../../../svg/ic_warning.svg';
import Link from '../../../../common/components/Link';
import { ModuleType } from '../../../../../constants';

interface Props {
  message?: string;
  moduleType?: ModuleType | 'all';
}

const CheckoutFailDesktop: React.FC<Props> = props => {
  const { message, moduleType } = props;
  return (
    <div style={{ width: '700px', margin: 'auto' }}>
      <Card
        elevation={2}
        style={{
          padding: '32px',
          margin: '24px 0',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <IconWarning />
        </div>
        <div style={{ margin: '32px 0 12px' }}>
          <Typography variant="h6">
            <FormattedMessage id="booking.checkout.failed" />
          </Typography>
        </div>
        <div style={{ width: '400px', margin: 'auto' }}>
          <Typography variant="body2">
            {message || <FormattedMessage id="booking.checkout.failDescription" />}
          </Typography>
        </div>
        <div style={{ marginTop: '100px' }}>
          {/* <Link
            to={
              moduleType !== 'mobile_topup' && moduleType !== 'mobile_card'
                ? `${ROUTES.management}?${CURRENT_PARAM_NAME}=m.orders&tab=${
                    moduleType === 'baggages' || moduleType === 'itinerary_changing'
                      ? 'flight'
                      : moduleType
                  }`
                : ROUTES.mobile.mobileHistory
            }
          > */}
          <Button
            style={{ marginRight: '32px', width: '260px' }}
            size="large"
            variant="contained"
            color="secondary"
          >
            <FormattedMessage id="booking.checkout.orderManagement" />
          </Button>
          {/* </Link> */}
          <Link to="/">
            <Button style={{ width: '260px' }} size="large" variant="outlined">
              <FormattedMessage id="booking.checkout.backMainPage" />
            </Button>
          </Link>
        </div>
      </Card>
    </div>
  );
};

export default CheckoutFailDesktop;
