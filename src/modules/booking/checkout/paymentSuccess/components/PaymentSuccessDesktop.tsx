import React from 'react';
import { ModuleType, some } from '../../../../../constants';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import BaggagesBookingInfo from './BaggagesBookingInfo';
import FlightBookingInfo from './FlightBookingInfo';
import HotelBookingInfo from './HotelBookingInfo';

interface Props {
  loading: boolean;
  moduleType: ModuleType;
  messageJson?: some;
  bookingInfo?: some;
  baggagesInfo?: some;
}

const PaymentSuccessDesktop: React.FC<Props> = props => {
  const { loading, moduleType, messageJson, bookingInfo, baggagesInfo } = props;

  if (loading) {
    return <LoadingIcon style={{ flex: 1, height: '100vh' }} />;
  }
  return (
    <>
      {/* <div
        style={{
          padding: '16px 0',
        }}
      >
        <img src={bookingInfo?.caAccount?.logo} alt="" style={{ maxHeight: '32px' }} />
      </div> */}
      {moduleType === 'hotel' && (
        <HotelBookingInfo messageJson={messageJson} bookingInfo={bookingInfo} />
      )}
      {moduleType === 'baggages' && (
        <BaggagesBookingInfo
          messageJson={messageJson}
          bookingInfo={bookingInfo}
          baggagesInfo={baggagesInfo}
        />
      )}
      {moduleType === 'flight' && (
        <FlightBookingInfo messageJson={messageJson} bookingInfo={bookingInfo} />
      )}
    </>
  );
};

export default PaymentSuccessDesktop;
