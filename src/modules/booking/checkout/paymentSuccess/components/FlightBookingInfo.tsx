import { Button, Container, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import CheckRoundedIcon from '@material-ui/icons/CheckRounded';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import {
  BLUE_200,
  BLUE_300,
  GREEN_100,
  GREEN_200,
  RED_100,
  RED_200,
} from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { Col, Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import BookingInfoBox from './BookingInfoBox';
import { ROUTES } from '../../../../../configs/routes';
import { AppState } from '../../../../../redux/reducers';

interface Props {
  messageJson?: some;
  bookingInfo?: some;
}

const FlightBookingInfo: React.FC<Props> = props => {
  const { messageJson, bookingInfo } = props;
  const auth = useSelector((state: AppState) => state.auth.auth);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));
  if (!bookingInfo) {
    return <LoadingIcon style={{ height: '100vh' }} />;
  }

  return (
    <>
      <Col
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          padding: 42,
          background:
            messageJson?.paymentStatus === 'fail'
              ? RED_100
              : bookingInfo?.paymentStatus === 'holding'
              ? BLUE_200
              : GREEN_100,
        }}
      >
        {messageJson?.paymentStatus === 'fail' ? (
          <WarningRoundedIcon
            style={{
              height: 150,
              width: 'auto',
              color: RED_200,
            }}
          />
        ) : (
          <CheckRoundedIcon
            style={{
              height: 150,
              width: 'auto',
              color: bookingInfo?.paymentStatus === 'holding' ? BLUE_300 : GREEN_200,
            }}
          />
        )}
        <div style={{ position: 'absolute' }}>
          <Typography
            variant={matches ? 'h6' : 'h4'}
            style={{
              margin: '12px 0',
              textAlign: 'center',
            }}
          >
            {messageJson?.message}
          </Typography>
          <Typography variant="subtitle2" style={{ textAlign: 'center', marginTop: 16 }}>
            <FormattedHTMLMessage
              id="payment.tripiSupportText"
              values={{
                chanel: bookingInfo?.caAccount?.fullName,
                phone: bookingInfo?.caAccount?.hotline,
              }}
            />
          </Typography>
          <Typography variant="subtitle2" style={{ textAlign: 'center' }}>
            <FormattedHTMLMessage
              id="payment.tripiReadyServeYou"
              values={{
                chanel: bookingInfo?.caAccount?.fullName,
              }}
            />
          </Typography>
        </div>
      </Col>
      <Container style={{ maxWidth: 1024 }}>
        <BookingInfoBox bookingInfo={bookingInfo} />
        <Row style={{ justifyContent: 'center', marginTop: 32, marginBottom: 32 }}>
          <Link to="/">
            <Button variant="outlined" size="large" style={{ minWidth: 240 }}>
              <FormattedMessage id="backToHome" />
            </Button>
          </Link>
          {auth && (
            <Link to={ROUTES.order.flight.result} style={{ marginLeft: 24 }}>
              <Button
                variant="contained"
                color="secondary"
                disableElevation
                size="large"
                style={{ minWidth: 240 }}
              >
                <FormattedMessage id="backToOrder" />
              </Button>
            </Link>
          )}
        </Row>
      </Container>
    </>
  );
};

export default FlightBookingInfo;
