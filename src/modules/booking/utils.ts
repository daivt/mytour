import { some } from '../../constants';
import atm from '../../svg/booking/payment/atm_payments.svg';
import hold from '../../svg/booking/payment/hold_payment.svg';
import tc from '../../svg/booking/payment/tripi_credits.svg';
import visa from '../../svg/booking/payment/visa_logo.svg';
import vnpay from '../../svg/booking/payment/vnpay_qr.svg';
import {
  PAYMENT_ATM_CODE,
  PAYMENT_HOLDING_CODE,
  PAYMENT_TRIPI_CREDIT_CODE,
  PAYMENT_VISA_CODE,
  PAYMENT_VNPAY_CODE,
  TAX,
} from './constants';

export function roundUp(num: number, step: number) {
  return Math.ceil(num / step) * step;
}
export function calculateTax(num: number) {
  return Math.round((num * TAX) / (100 + TAX));
}

export function millisecondsToHour(milliseconds: number) {
  if (milliseconds <= 0) {
    return 0;
  }

  return Math.floor(milliseconds / 3600000);
}
export function scrollTo(id: string, offsetTop: number) {
  const el = document.getElementById(id);
  if (el) {
    window.scrollTo({ top: el.offsetTop - offsetTop, behavior: 'smooth' });
  }
}

export function durationMillisecondToHour(millisecond: number) {
  const second = millisecond / 1000;
  const hours = Math.floor(second / 3600);
  const minutes = Math.floor((second - hours * 3600) / 60);

  if (minutes) {
    return `${hours}h ${minutes}m`;
  }

  return `${hours}h`;
}

export function getScoreDescription(score: number) {
  if (score < 6) {
    return 'medium';
  }
  if (score < 7) {
    return 'satisfied';
  }
  if (score < 8) {
    return 'good';
  }
  if (score < 9) {
    return 'veryGood';
  }
  if (score <= 10) {
    return 'great';
  }

  return 'medium';
}

export interface PointPayment {
  availablePoint: number;
  min: number;
  max: number;
  paymentMethodCode: string;
  pointFactor: number;
  credit: number;
  maxPointOnModuleTag: string;
  usedPointOnModuleTag: string;
}

export const defaultPointPayment: PointPayment = {
  availablePoint: 0,
  min: 0,
  max: 0,
  paymentMethodCode: PAYMENT_TRIPI_CREDIT_CODE,
  pointFactor: 0,
  credit: 0,
  maxPointOnModuleTag: '',
  usedPointOnModuleTag: '',
};

export function getPointPaymentData(data: some): PointPayment {
  return (
    data.find((one: some) => one.paymentMethodCode === PAYMENT_TRIPI_CREDIT_CODE) ||
    defaultPointPayment
  );
}

export interface Promotions {
  list: some[];
  total: number;
  message: string;
}
export const defaultPromotions: Promotions = {
  list: [],
  total: 0,
  message: '',
};

export function getPaymentMethodIcon(code: string) {
  if (code === PAYMENT_TRIPI_CREDIT_CODE) {
    return tc;
  }
  if (code === PAYMENT_VNPAY_CODE) {
    return vnpay;
  }
  if (code === PAYMENT_ATM_CODE) {
    return atm;
  }
  if (code === PAYMENT_VISA_CODE) {
    return visa;
  }
  if (code === PAYMENT_HOLDING_CODE) {
    return hold;
  }
  return undefined;
}

export function computePaymentFees(method: some, amount: number) {
  let value = 0;
  value += method.fixedFee;
  value += (method.percentFee / 100) * amount;
  value = Math.round(value);
  return value;
}
