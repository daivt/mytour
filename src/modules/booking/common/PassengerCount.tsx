import React from 'react';
import { useIntl } from 'react-intl';

interface Props {
  adults?: number;
  child?: number;
  infants?: number;
  babies?: number;
}

const PassengerCount: React.FC<Props> = props => {
  const { adults, child, infants, babies } = props;
  const intl = useIntl();
  const passengers = [];

  if (adults) {
    passengers.push(`${adults} ${intl.formatMessage({ id: 'adult' }).toLowerCase()}`);
  }

  if (child) {
    passengers.push(`${child} ${intl.formatMessage({ id: 'children' }).toLowerCase()}`);
  }

  if (infants) {
    passengers.push(`${infants} ${intl.formatMessage({ id: 'infant' }).toLowerCase()}`);
  }

  if (babies) {
    passengers.push(`${babies} ${intl.formatMessage({ id: 'baby' }).toLowerCase()}`);
  }

  return <span>{passengers.join(', ')}</span>;
};

export default PassengerCount;
