import { Card, fade, IconButton, Paper } from '@material-ui/core';
import ArrowOpen from '@material-ui/icons/ChevronLeft';
import ArrowClose from '@material-ui/icons/ChevronRight';
import Close from '@material-ui/icons/Close';
import FilterList from '@material-ui/icons/FilterList';
import React from 'react';
import { BLACK, GREY_300 } from '../../../configs/colors';
import { HEADER_HEIGHT } from '../../../layout/constants';

const WIDTH = 60;
interface Props {
  isTablet?: boolean;
  direction?: 'left' | 'right' | 'up' | 'down';
  style?: React.CSSProperties;
  top?: number;
}

const AsideBound: React.FC<Props> = props => {
  const { isTablet, direction, style, top, children } = props;
  const [show, setShow] = React.useState(false);

  if (isTablet) {
    if (direction === 'right') {
      return (
        <>
          <IconButton
            style={{
              position: 'absolute',
              top: top || HEADER_HEIGHT,
              right: 0,
              padding: 28,
              zIndex: 1,
            }}
            onClick={() => setShow(true)}
          >
            <ArrowOpen />
          </IconButton>
          {show && (
            <Card
              style={{
                position: 'fixed',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                background: fade(BLACK, 0.5),
              }}
              onClick={() => setShow(false)}
            />
          )}
          <Paper
            elevation={0}
            style={{
              borderRadius: 0,
              minHeight: '100%',
              zIndex: 1200,
              position: 'fixed',
              right: 0,
              top: HEADER_HEIGHT,
              width: 320,
              transform: show ? `translateX(0)` : 'translateX(100%)',
              transition: 'all 300ms',
              borderLeft: `1px solid ${GREY_300}`,
              outline: 'none',
              ...style,
            }}
          >
            <div style={{ position: 'relative', minHeight: '100%' }}>
              <IconButton onClick={() => setShow(false)}>
                <ArrowClose />
              </IconButton>
              <div
                style={{
                  padding: '0px 12px',
                }}
              >
                {children}
              </div>
            </div>
          </Paper>
        </>
      );
    }
    return (
      <div
        style={{
          width: WIDTH,
          minHeight: '100%',
          zIndex: 100,
        }}
      >
        <Paper
          elevation={0}
          style={{
            borderRadius: 0,
            width: 320,
            transform: show ? 'translateX(0)' : `translateX(calc(-100% + ${WIDTH}px))`,
            borderRight: `1px solid ${GREY_300}`,
            transition: 'all 300ms',
            minHeight: '100%',
            ...style,
          }}
        >
          <div
            style={{
              position: 'sticky',
              top: top || HEADER_HEIGHT,
            }}
          >
            <div style={{ textAlign: 'end' }}>
              {show ? (
                <IconButton onClick={() => setShow(false)}>
                  <Close />
                </IconButton>
              ) : (
                <IconButton onClick={() => setShow(true)}>
                  <FilterList />
                </IconButton>
              )}
            </div>
            <div
              style={{
                opacity: show ? 1 : 0.4,
                position: 'relative',
                padding: '0px 12px',
              }}
            >
              {children}
              {!show && (
                <div
                  style={{
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                  }}
                />
              )}
            </div>
          </div>
        </Paper>
      </div>
    );
  }

  return (
    <div style={style}>
      <div style={{ position: 'sticky', top: top || HEADER_HEIGHT + 24 }}>{children}</div>
    </div>
  );
};

export default AsideBound;
