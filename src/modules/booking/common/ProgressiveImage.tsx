import * as React from 'react';
import RPI from 'react-progressive-image';
import styled, { keyframes } from 'styled-components';
import { GREEN_300 } from '../../../configs/colors';

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const FadeInImg = styled.img`
  animation-name: ${fadeIn};
  animation-duration: 0.3s;
  animation-timing-function: linear;
`;

interface IProgressiveImageProps extends React.ImgHTMLAttributes<HTMLImageElement> {}

const ProgressiveImage: React.FunctionComponent<IProgressiveImageProps> = props => {
  const { src, style, alt, ...rest } = props;
  return (
    <RPI src={src || ''} placeholder={src || ''}>
      {(srcTmp: string, loading: boolean) =>
        loading ? (
          <span
            style={{
              ...style,
              display: 'inline-block',
              backgroundColor: GREEN_300,
            }}
          />
        ) : (
          <FadeInImg
            {...props}
            alt={alt || ''}
            style={{
              ...style,
            }}
            {...rest}
          />
        )
      }
    </RPI>
  );
};

export default ProgressiveImage;
