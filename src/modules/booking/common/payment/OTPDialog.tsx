import { Button, Dialog, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect, shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../configs/API';
import { validNumberRegex } from '../../../../models/regex';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconWarning } from '../../../../svg/ic_warning.svg';
import { validateAccessToken } from '../../../auth/redux/authThunks';
import { Row } from '../../../common/components/elements';
import FormControlTextField from '../../../common/components/FormControlTextField';
import LoadingButton from '../../../common/components/LoadingButton';
import { fetchThunk } from '../../../common/redux/thunk';
import { SUCCESS_CODE } from '../../../../constants';

interface Props {
  show: boolean;
  pay(pass?: string): void;
  close(): void;
}

const OTPDialog: React.FunctionComponent<Props> = props => {
  const { show, pay, close } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();

  const userData = useSelector((state: AppState) => state.account.userData, shallowEqual);
  const [pass, setPass] = React.useState('');
  const [message, setMessage] = React.useState('');
  const [verifying, setVerifying] = React.useState(false);

  const validateCreditPassword = React.useCallback(async () => {
    setVerifying(true);
    const json = await dispatch(
      fetchThunk(API_PATHS.validateCreditPassword, 'post', {
        password: pass,
      }),
    );
    setVerifying(false);
    if (json.code === SUCCESS_CODE) {
      pay(pass);
      close();
    } else {
      setMessage(json.message);
    }
  }, [close, dispatch, pass, pay]);

  if (!userData) {
    return <></>;
  }

  return (
    <Dialog
      open={show}
      onExited={e => {
        setPass('');
      }}
      onEnter={() => dispatch(validateAccessToken())}
    >
      {userData.useCreditPassword ? (
        <form
          autoComplete="off"
          onSubmit={e => {
            e.preventDefault();
            validateCreditPassword();
          }}
        >
          <div style={{ padding: '24px', width: '444px' }}>
            <Typography variant="h6">
              <FormattedMessage id="pay.enterSmartOTP" />
            </Typography>
            <Typography variant="body1" color="textSecondary" style={{ marginTop: '16px' }}>
              <FormattedMessage id="pay.OTPDescription" />
            </Typography>
            <FormControlTextField
              value={pass}
              placeholder=""
              onChange={e => {
                if (validNumberRegex.test(e.target.value)) {
                  setPass(e.target.value);
                  if (message) {
                    setMessage('');
                  }
                }
              }}
              type="password"
              formControlStyle={{ marginTop: '24px', marginRight: '0px' }}
              autoFocus
            />

            {!!message && (
              <Typography variant="body2" color="error">
                {message}
              </Typography>
            )}

            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button>
                {/* <NewTabLink
                  href={`${ROUTES.management}?${CURRENT_PARAM_NAME}=m.profile&${ACTION_PARAM_NAME}=smartOTP`}
                > */}
                <Typography variant="body2">
                  <FormattedMessage id="pay.changeCode" />
                </Typography>
                {/* </NewTabLink> */}
              </Button>
            </div>
            <Row style={{ marginTop: '20px', justifyContent: 'center' }}>
              <LoadingButton
                loading={verifying}
                variant="contained"
                color="secondary"
                disabled={!pass}
                type="submit"
                style={{ minWidth: 120 }}
                size="large"
                disableElevation
              >
                <Typography variant="button">
                  <FormattedMessage id="pay" />
                </Typography>
              </LoadingButton>
              <Button
                variant="outlined"
                onClick={close}
                style={{ marginLeft: '16px', minWidth: 120 }}
                size="large"
              >
                <Typography variant="button" color="textSecondary">
                  <FormattedMessage id="reject" />
                </Typography>
              </Button>
            </Row>
          </div>
        </form>
      ) : (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            padding: '32px',
            flexDirection: 'column',
          }}
        >
          <IconWarning />
          <Typography variant="body1" style={{ paddingTop: '18px' }}>
            <FormattedMessage id="pay.youNeedCreateSmartOTP" />
          </Typography>

          <Button variant="outlined" onClick={close} style={{ marginTop: '32px' }}>
            <Typography variant="button" color="textSecondary">
              <FormattedMessage id="close" />
            </Typography>
          </Button>
        </div>
      )}
    </Dialog>
  );
};

function mapStateToProps(state: AppState) {
  return {
    userData: state.account.userData,
  };
}

export default connect(mapStateToProps)(OTPDialog);
