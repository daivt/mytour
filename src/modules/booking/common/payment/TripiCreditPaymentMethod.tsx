import {
  Button,
  Checkbox,
  Collapse,
  FormControlLabel,
  Paper,
  Popover,
  Slider,
  Tooltip,
  Typography,
} from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { BLUE, GREY_500 } from '../../../../configs/colors';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconLoyalty } from '../../../../svg/booking/payment/ic_loyalty.svg';
import { ReactComponent as IconInfo } from '../../../../svg/ic_info.svg';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { PointPayment } from '../../utils';

interface PropsLabel {
  children: React.ReactElement;
  open: boolean;
  value: number;
}

function PriceLabelComponent(props: PropsLabel) {
  const { children, open, value } = props;

  return (
    <Tooltip
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={
        <>
          <FormattedNumber value={value} />
          &nbsp;
          <FormattedMessage id="currency" />
        </>
      }
    >
      {children}
    </Tooltip>
  );
}

interface Props {
  pointPayment?: PointPayment;
  point: number;
  usingPoint?: boolean;
  onChange?: (point: number, usingPoint: boolean) => void;
  anchorEl?: Element | undefined;
}

const TripiCreditPaymentMethod: React.FC<Props> = props => {
  // state: State = {
  //   pointPayment: {
  //     ...initPointPayment,
  //     credit: this.props.userData ? this.props.userData.credit : 0,
  //   },
  //   point: 0,
  // };
  const { pointPayment, point, usingPoint, onChange, anchorEl } = props;

  const userData = useSelector((state: AppState) => state.account.userData, shallowEqual);
  const [anchor, setAnchor] = React.useState<Element | undefined>(anchorEl);
  if (!userData) {
    return null;
  }
  if (!pointPayment) {
    return (
      <Paper
        style={{
          width: '480px',
          marginBottom: '24px',
        }}
        variant="outlined"
      >
        <LoadingIcon />
      </Paper>
    );
  }

  return (
    <>
      <Paper
        style={{
          width: '480px',
          marginBottom: '24px',
        }}
        variant="outlined"
      >
        <div
          style={{
            borderBottom: `0.5px solid ${GREY_500}`,
            margin: '0 16px',
            padding: '3px 0',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <FormControlLabel
            style={{ outline: 'none', margin: '0' }}
            label={
              <Typography variant="body2">
                <FormattedMessage id="pay.payWithTripCredits" />
              </Typography>
            }
            labelPlacement="end"
            control={
              <Checkbox
                size="medium"
                checked={usingPoint || false}
                onChange={event => onChange && onChange(point, event.target.checked)}
                disabled={pointPayment.max === 0}
              />
            }
          />
          <Button
            style={{
              padding: '3px 10px',
            }}
            disableElevation
            onClick={event => setAnchor(event.currentTarget)}
          >
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Typography variant="body2" style={{ color: BLUE, paddingRight: '8px' }}>
                <FormattedMessage id="seeMore" />
              </Typography>
              <IconInfo />
            </div>
          </Button>

          <Popover
            open={Boolean(anchor)}
            anchorEl={anchor}
            onClose={() => setAnchor(undefined)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column',
                alignItems: 'center',
                textAlign: 'center',
                maxWidth: '420px',
                padding: '32px 0',
              }}
            >
              <IconLoyalty style={{ width: '140px', height: '116px' }} />
              <Typography variant="body2" style={{ padding: '16px 30px' }}>
                <FormattedMessage id="pay.tripiPointDescription" />
              </Typography>

              <Button
                variant="contained"
                color="secondary"
                size="large"
                style={{ marginTop: '16px' }}
                onClick={() => setAnchor(undefined)}
                disableElevation
              >
                <Typography variant="button" style={{ padding: '0 16px' }}>
                  <FormattedMessage id="close" />
                </Typography>
              </Button>
            </div>
          </Popover>
        </div>
        <div
          style={{
            padding: '10px 16px 16px',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              minHeight: '32px',
              alignItems: 'center',
            }}
          >
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Typography variant="body2" color={usingPoint ? 'textPrimary' : 'textSecondary'}>
                <FormattedMessage id="pay.accountBalance" />
                :&nbsp;
              </Typography>
              <Typography variant="body2" color={usingPoint ? 'secondary' : 'textSecondary'}>
                <FormattedNumber value={Math.round(pointPayment.credit)} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </div>

            {/* <Link target="blank" to={{ pathname: ROUTES.invoices }}> */}
            <Button
              disableElevation
              variant="contained"
              color="secondary"
              style={{ width: '115px' }}
            >
              <Typography variant="button">
                <FormattedMessage id="recharge" />
              </Typography>
            </Button>
            {/* </Link> */}
          </div>

          <div style={{ display: 'flex', alignItems: 'center', minHeight: '32px' }}>
            <Typography variant="body2" color={usingPoint ? 'textPrimary' : 'textSecondary'}>
              <FormattedMessage id="pay.usePoints" />
              :&nbsp;
              <FormattedNumber value={point} />
              &nbsp;
            </Typography>
            <Typography variant="body2" color={usingPoint ? 'textPrimary' : 'textSecondary'}>
              <FormattedMessage id="point" />
            </Typography>
          </div>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              minHeight: '32px',
            }}
          >
            <Typography variant="body2" color={usingPoint ? 'textPrimary' : 'textSecondary'}>
              <FormattedMessage id="pay.remainningPoints" />
              :&nbsp;
              <FormattedNumber value={(pointPayment.availablePoint || 0) - point} />
              &nbsp;
            </Typography>
            <Typography variant="body2" color={usingPoint ? 'textPrimary' : 'textSecondary'}>
              <FormattedMessage id="point" />
            </Typography>
          </div>
          <Collapse in={usingPoint}>
            {pointPayment.maxPointOnModuleTag && (
              <Typography variant="body2" style={{ minHeight: '32px' }} color="error">
                {pointPayment.maxPointOnModuleTag}
              </Typography>
            )}
          </Collapse>
          <div style={{ display: 'flex', alignItems: 'center', margin: '8px 0' }}>
            <div style={{ width: '100%' }}>
              <Slider
                key={JSON.stringify(point)}
                defaultValue={point}
                min={pointPayment.min}
                max={pointPayment.max}
                disabled={!usingPoint}
                onChangeCommitted={(e: any, value: any) =>
                  usingPoint && onChange && onChange(value, usingPoint)
                }
                ValueLabelComponent={PriceLabelComponent}
              />
            </div>
            <div style={{ paddingLeft: '16px' }}>
              <Typography variant="body2" color={usingPoint ? 'textPrimary' : 'textSecondary'}>
                &nbsp;
                <FormattedNumber value={point} />
                /
                <FormattedNumber value={pointPayment.max} />
              </Typography>
            </div>
          </div>
        </div>
      </Paper>
    </>
  );
};

export default TripiCreditPaymentMethod;
