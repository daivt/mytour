/* eslint-disable no-nested-ternary */
import {
  ButtonBase,
  Checkbox,
  Collapse,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  Typography,
} from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import IconCheck from '@material-ui/icons/CheckCircle';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import {
  BLUE,
  GREY_100,
  GREY_500,
  ORANGE,
  PRIMARY,
  WHITE,
  GREEN_300,
} from '../../../../configs/colors';
import { ROUTES } from '../../../../configs/routes';
import { some } from '../../../../constants';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import LoadingButton from '../../../common/components/LoadingButton';
import { NewTabLink } from '../../../common/components/NewTabLink';
import { PAYMENT_ATM_CODE, PAYMENT_HOLDING_CODE, PAYMENT_TRIPI_CREDIT_CODE } from '../../constants';
import { computePaymentFees, getPaymentMethodIcon, PointPayment } from '../../utils';
import OTPDialog from './OTPDialog';
import TripiCreditPaymentMethod from './TripiCreditPaymentMethod';

const ItemDiv = styled.div<{ current: boolean }>`
  display: flex;
  align-items: center;
  cursor: pointer;
  border-bottom: 1px solid ${props => (props.current ? PRIMARY : GREY_500)};
  flex-direction: column;
  :hover {
    background: ${props => (props.current ? '' : fade(PRIMARY, 0.025))};
  }
`;

const BankBox = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 60px;
  border-radius: 2px;
  margin: 4px;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 5px, rgba(0, 0, 0, 0.12) 0px 3px 4px,
    rgba(0, 0, 0, 0.14) 0px 2px 4px;
  :hover {
    background-color: ${GREY_100};
  }
`;

interface Props {
  paymentMethods: ReadonlyArray<some>;
  selectedMethod?: some;
  total: number;
  setSelectedMethod(method: some): void;
  priceAfter: number;
  promotionCode?: string;
  paying?: boolean;
  pay(pass?: string): void;
  pointPayment?: PointPayment;
  point?: number;
  usingPoint?: boolean;
  onChangePoint?: (point: number, usingPoint: boolean) => void;
}

const PaymentMethodsBox: React.FunctionComponent<Props> = props => {
  const {
    paymentMethods,
    total,
    setSelectedMethod,
    selectedMethod,
    priceAfter,
    promotionCode,
    pay,
    paying,
    pointPayment,
    point,
    usingPoint,
    onChangePoint,
  } = props;
  const [agreed, setAgreed] = React.useState(true);
  const [showConfirmDialog, setShowConfirmDialog] = React.useState(false);
  const [showHoldingConfirm, setShowHoldingConfirm] = React.useState(false);
  const [showOTPDialog, setShowOTPDialog] = React.useState(false);

  const paymentFee = selectedMethod ? computePaymentFees(selectedMethod, total) : 0;
  const totalPayable = priceAfter + paymentFee;

  return (
    <div style={{ paddingLeft: '6px', paddingRight: '10px' }}>
      {/* <NoOTPWarningBox /> */}
      <RadioGroup radioGroup="method">
        {paymentMethods.map(method => {
          const current = method.code === selectedMethod?.code;
          const fee = computePaymentFees(method, total);
          return (
            <ItemDiv current={current} key={method.id}>
              <ButtonBase
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  width: '100%',
                  background:
                    current &&
                    method.code !== PAYMENT_TRIPI_CREDIT_CODE &&
                    method.code !== PAYMENT_ATM_CODE
                      ? fade(PRIMARY, 0.05)
                      : undefined,
                }}
                onClick={() => setSelectedMethod(method)}
              >
                <img
                  src={getPaymentMethodIcon(method.code)}
                  style={{
                    width: '24px',
                    height: '24px',
                    marginRight: '8px',
                  }}
                  alt=""
                />
                <div
                  style={{
                    whiteSpace: 'nowrap',
                    display: 'flex',
                    alignItems: 'baseline',
                    flex: 1,
                  }}
                >
                  <Typography>
                    {method.name}
                    &nbsp;
                  </Typography>
                  {method.code === PAYMENT_HOLDING_CODE ? (
                    <Typography variant="body2" style={{ color: GREEN_300 }}>
                      <FormattedMessage id="pay.holdingTitle" />
                    </Typography>
                  ) : (
                    <Typography variant="body2" style={{ color: ORANGE }}>
                      {fee < 0 ? (
                        <FormattedMessage
                          id="pay.paymentFixedDiscount"
                          values={{
                            value: <FormattedNumber value={-fee} maximumFractionDigits={0} />,
                          }}
                        />
                      ) : fee > 0 ? (
                        <FormattedMessage
                          id="pay.paymentFixedFee"
                          values={{
                            value: <FormattedNumber value={fee} maximumFractionDigits={0} />,
                          }}
                        />
                      ) : null}
                    </Typography>
                  )}
                </div>
                <Radio value={method.code} checked={current} />
              </ButtonBase>
              {method.code === PAYMENT_TRIPI_CREDIT_CODE && (
                <Collapse in={selectedMethod?.code === PAYMENT_TRIPI_CREDIT_CODE}>
                  <TripiCreditPaymentMethod
                    pointPayment={pointPayment}
                    point={point || 0}
                    usingPoint={usingPoint}
                    onChange={onChangePoint}
                  />
                </Collapse>
              )}
              {method.code === PAYMENT_ATM_CODE && (
                <Collapse in={selectedMethod?.code === PAYMENT_ATM_CODE}>
                  <div
                    style={{
                      display: 'flex',
                      flexWrap: 'wrap',
                      border: `0.5px solid ${GREY_500}`,
                      borderRadius: '2px',
                      padding: '12px',
                      margin: '32px 44px',
                      width: '482px',
                    }}
                  >
                    <Grid container spacing={1}>
                      {method.bankList.map((bank: some) => (
                        <Grid item xs={3} key={bank.code}>
                          <BankBox
                            style={{
                              boxShadow:
                                selectedMethod?.bankId === bank.id
                                  ? `${PRIMARY} 0px 1px 5px, ${PRIMARY} 0px 3px 4px, ${PRIMARY} 0px 2px 4px`
                                  : undefined,
                              border:
                                selectedMethod?.bankId === bank.id
                                  ? `1px solid ${PRIMARY}`
                                  : undefined,
                            }}
                            onClick={() =>
                              setSelectedMethod({ ...selectedMethod, bankId: bank.id })
                            }
                          >
                            <img style={{ maxWidth: '106px' }} src={bank.logoUrl} alt={bank.name} />
                            {selectedMethod?.bankId === bank.id && (
                              <IconCheck
                                style={{
                                  position: 'absolute',
                                  transition: 'opacity 300ms',
                                  right: '4px',
                                  top: '4px',
                                  color: PRIMARY,
                                  width: '14px',
                                  height: '14px',
                                }}
                              />
                            )}
                          </BankBox>
                        </Grid>
                      ))}
                    </Grid>
                  </div>
                </Collapse>
              )}
            </ItemDiv>
          );
        })}
      </RadioGroup>
      <FormControlLabel
        style={{ marginBottom: '16px', marginTop: '16px' }}
        control={<Checkbox checked={agreed} />}
        onChange={(e, value) => setAgreed(value)}
        label={
          <Typography variant="body2" style={{ marginTop: '4px' }}>
            <FormattedMessage
              id="pay.agreeWithTerms"
              values={{
                link: (
                  <NewTabLink href={ROUTES.terms} style={{ color: BLUE }}>
                    <FormattedMessage id="pay.conditions" />
                  </NewTabLink>
                ),
              }}
            />
          </Typography>
        }
      />
      <div style={{ textAlign: 'center' }}>
        <LoadingButton
          color="secondary"
          variant="contained"
          disabled={
            paying ||
            !agreed ||
            (selectedMethod?.code === PAYMENT_ATM_CODE && selectedMethod?.bankId === undefined)
          }
          size="large"
          style={{ width: '170px' }}
          onClick={() =>
            promotionCode && selectedMethod && selectedMethod?.code === PAYMENT_HOLDING_CODE
              ? setShowHoldingConfirm(true)
              : setShowConfirmDialog(true)
          }
          disableElevation
          loading={paying}
        >
          <FormattedMessage id="pay" />
        </LoadingButton>
      </div>

      <ConfirmDialog
        closeId="flightPay.closeDialogHoldingNotice"
        rejectId="flightPay.cancelPaymentHoldingNotice"
        acceptId="flightPay.confirmPaymentHoldingNotice"
        titleLabel={
          <div
            style={{
              padding: '12px 16px',
              textAlign: 'center',
              backgroundColor: PRIMARY,
              color: 'white',
            }}
          >
            <Typography variant="h5" color="inherit">
              <FormattedMessage id="attention" />
            </Typography>
          </div>
        }
        styleCloseBtn={{ color: WHITE }}
        open={showHoldingConfirm}
        rejectLabel="cancel"
        acceptLabel="confirm"
        onClose={() => setShowHoldingConfirm(false)}
        onReject={() => setShowHoldingConfirm(false)}
        onAccept={() => {
          setShowHoldingConfirm(false);
          setShowConfirmDialog(true);
        }}
      >
        <div style={{ textAlign: 'center', padding: '16px 24px' }}>
          <Typography variant="body1">
            <FormattedMessage id="pay.holdingNotice" />
          </Typography>
        </div>
      </ConfirmDialog>

      <ConfirmDialog
        closeId="flightPay.closeDialogPaymentNotice"
        rejectId="flightPay.cancelPaymentPaymentNotice"
        acceptId="flightPay.confirmPaymentPaymentNotice"
        titleLabel={
          <div
            style={{
              padding: '12px 16px',
              textAlign: 'center',
              backgroundColor: PRIMARY,
              color: 'white',
            }}
          >
            <Typography variant="h5" color="inherit">
              <FormattedMessage id="attention" />
            </Typography>
          </div>
        }
        styleCloseBtn={{ color: WHITE }}
        open={showConfirmDialog}
        rejectLabel="cancel"
        acceptLabel="confirm"
        onClose={() => setShowConfirmDialog(false)}
        onReject={() => setShowConfirmDialog(false)}
        onAccept={() => {
          setShowConfirmDialog(false);
          if (selectedMethod?.code === PAYMENT_TRIPI_CREDIT_CODE) {
            setShowOTPDialog(true);
          } else {
            pay();
          }
        }}
      >
        <div style={{ textAlign: 'center', padding: '24px 16px', minHeight: 120 }}>
          <Typography variant="body1">
            <FormattedMessage
              id={`pay.paymentMessage.${selectedMethod?.code.toLowerCase()}`}
              values={{
                currency: (
                  <>
                    <br />
                    <span className="final-price text-bold" style={{ whiteSpace: 'nowrap' }}>
                      <FormattedNumber
                        value={totalPayable < 0 ? 0 : totalPayable}
                        maximumFractionDigits={0}
                      />
                      &nbsp;
                      <FormattedMessage id="currency" />
                    </span>
                    <br />
                    <FormattedMessage id="pay.confirmToContinue" />
                  </>
                ),
              }}
            />
          </Typography>
        </div>
      </ConfirmDialog>

      <OTPDialog show={showOTPDialog} pay={pay} close={() => setShowOTPDialog(false)} />
    </div>
  );
};
PaymentMethodsBox.defaultProps = {
  point: 0,
};
export default PaymentMethodsBox;
