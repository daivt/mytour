/* eslint-disable no-nested-ternary */
import { ButtonBase, IconButton, Paper } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import Radio from '@material-ui/core/Radio';
import Typography from '@material-ui/core/Typography';
import RadioButtonIcon from '@material-ui/icons/RadioButtonUnchecked';
import IconSearch from '@material-ui/icons/Search';
import { debounce } from 'lodash';
import moment from 'moment';
import * as React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import styled from 'styled-components';
import { GREY, GREY_500, PRIMARY } from '../../../../configs/colors';
import { some } from '../../../../constants';
import { Col, Row } from '../../../common/components/elements';
import FormControlTextField from '../../../common/components/FormControlTextField';
import LoadingIcon from '../../../common/components/LoadingIcon';
import { Promotions } from '../../utils';

const EllipseLeft = styled.div<{ top: number }>`
  box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 3px inset, rgba(0, 0, 0, 0.12) 0px 2px 2px inset,
    rgba(0, 0, 0, 0.14) 0px 0px 2px inset;
  position: absolute;
  width: 12px;
  height: 8px;
  top: ${props => props.top}%;
  left: -1.17%;
  background: rgb(255, 255, 255);
  border-radius: 50%;
`;

const EllipseRight = styled.div<{ top: number }>`
  box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 3px inset, rgba(0, 0, 0, 0.12) 0px 2px 2px inset,
    rgba(0, 0, 0, 0.14) 0px 0px 2px inset;
  position: absolute;
  width: 12px;
  height: 8px;
  top: ${props => props.top}%;
  right: -1.17%;
  background: rgb(255, 255, 255);
  border-radius: 50%;
`;

export interface Props {
  close(): void;
  selectCode(promotionCode?: string): void;
  fetchRewardsHistory(search: string, page: number): void;
  listPromotion?: Promotions;
  code?: string;
  loading?: boolean;
}

function drawEllipse(top: number) {
  return (
    <>
      <EllipseLeft top={top} />
      <EllipseRight top={top} />
    </>
  );
}

const PromotionListBox: React.FC<Props> = props => {
  const { listPromotion, close, fetchRewardsHistory, selectCode, code, loading } = props;
  const [page, setPage] = React.useState<number>(1);
  const [search, setSearch] = React.useState('');
  const [searchTmp, setSearchTmp] = React.useState('');
  const [promotionCode, setPromotionCode] = React.useState<string | undefined>(code);
  const intl = useIntl();

  const fetchData = React.useCallback(
    async (searchStr: string, pageNumber: number) => {
      fetchRewardsHistory(searchStr, pageNumber);
    },
    [fetchRewardsHistory],
  );

  const onSearch = React.useCallback(
    debounce(
      async (searchStr: string) => {
        setSearch(searchStr);
        setPage(1);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );
  React.useEffect(() => {
    setPromotionCode(code);
  }, [code]);

  React.useEffect(() => {
    fetchData(search, page);
  }, [fetchData, fetchRewardsHistory, page, search]);
  const scrollingDiv = React.createRef<HTMLDivElement>();

  return (
    <>
      <Col style={{ margin: '16px 24px 8px 16px' }}>
        <Typography variant="h5" style={{ fontWeight: 'normal' }}>
          <FormattedMessage id="pay.promotion.myPromotion" />
        </Typography>
        <Typography variant="body2" style={{ color: GREY_500, margin: '8px 0px' }}>
          <FormattedMessage id="pay.promotion.promotionDescription" />
        </Typography>
        <FormControlTextField
          onChange={e => {
            setSearchTmp(e.target.value);
            onSearch(e.target.value);
          }}
          margin="dense"
          style={{
            height: '40px',
          }}
          value={searchTmp || ''}
          fullWidth
          label={intl.formatMessage({ id: 'pay.promotion.whatYourPromotionCode' })}
          placeholder={intl.formatMessage({ id: 'pay.discountCodeExample' })}
          endAdornment={
            <InputAdornment position="end" variant="filled">
              <IconButton style={{ marginRight: 8, padding: 0 }}>
                <IconSearch />
              </IconButton>
            </InputAdornment>
          }
        />
        {listPromotion?.list && !listPromotion?.list?.length && (
          <Typography variant="subtitle1" style={{ paddingTop: '20px', paddingLeft: '12px' }}>
            <FormattedMessage id="pay.promotion.selectPromotionCode" />
          </Typography>
        )}
      </Col>
      <Col
        ref={scrollingDiv}
        style={{
          minWidth: '560px',
          maxHeight: '500px',
          overflowY: 'auto',
          overflowX: 'hidden',
          padding: '0px 16px',
        }}
      >
        {!listPromotion ? (
          <LoadingIcon  />
        ) : !listPromotion.list?.length ? (
          <Typography
            variant="body2"
            style={{ paddingTop: '20px', paddingLeft: '12px', textAlign: 'center', height: 320 }}
          >
            <FormattedMessage id="pay.promotion.noResult" />
          </Typography>
        ) : (
          <InfiniteScroll
            pageStart={1}
            initialLoad={false}
            loadMore={value => setPage(value)}
            hasMore={listPromotion.list.length < listPromotion.total && !loading}
            getScrollParent={() => scrollingDiv.current}
            loader={
              <LoadingIcon
                key="loader"
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              />
            }
            useWindow={false}
          >
            {listPromotion.list.map((promotion: some, index: number) => (
              <ButtonBase
                key={promotion.id}
                style={{
                  overflow: 'hidden',
                  margin: '4px 0px',
                  width: '100%',
                }}
                onClick={() => setPromotionCode(promotion.codeDetail)}
              >
                <Paper
                  style={{
                    display: 'flex',
                    borderRadius: '4px 0 0 4px',
                    minHeight: '90px',
                    marginLeft: '1px',
                    position: 'relative',
                    textAlign: 'start',
                    flex: 1,
                  }}
                  variant="outlined"
                >
                  <Row
                    style={{
                      width: '100%',
                      padding: '8px 16px',
                    }}
                  >
                    <div style={{ paddingTop: '12px', alignSelf: 'flex-start' }}>
                      <img style={{ width: '24px' }} src={promotion.iconType} alt="" />
                    </div>
                    <div
                      style={{
                        display: 'flex',
                        paddingLeft: '16px',
                        flexDirection: 'column',
                        flex: 1,
                      }}
                    >
                      <Typography
                        variant="subtitle2"
                        style={{
                          minHeight: '48px',
                        }}
                      >
                        {promotion.rewardProgram.title}
                      </Typography>
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <div
                          style={{
                            minWidth: '60px',
                            backgroundColor: PRIMARY,
                            borderRadius: '99px',
                            height: '16px',
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Typography
                            variant="body2"
                            style={{ fontSize: 10, color: '#fff', padding: '0 10px' }}
                          >
                            {promotion.typeDetail}
                          </Typography>
                        </div>
                        <div style={{ paddingLeft: '6px' }}>
                          <Typography
                            variant="body2"
                            style={{ fontSize: '12px', textTransform: 'uppercase' }}
                          >
                            {promotion.codeDetail}
                          </Typography>
                        </div>
                      </div>

                      <div
                        style={{
                          display: 'flex',
                        }}
                      >
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'column',
                            paddingRight: '8px',
                            minWidth: '80px',
                          }}
                        >
                          <Typography variant="caption" color="textSecondary">
                            <FormattedMessage id="pay.promotion.expired" />
                          </Typography>
                          <Typography variant="caption" color="error">
                            {moment(promotion.rewardProgram.toDate * 1000).format('L')}
                          </Typography>
                        </div>
                        <div style={{ borderRight: `1px solid ${GREY}` }} />
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'column',
                            paddingLeft: '8px',
                          }}
                        >
                          <Typography variant="caption" color="error">
                            <FormattedMessage id="pay.promotion.remainNumber" />
                          </Typography>
                          <Typography
                            variant="body2"
                            style={{ fontSize: '12px', lineHeight: '16px' }}
                          >
                            <FormattedNumber value={promotion.remainNum} />
                          </Typography>
                        </div>
                      </div>
                    </div>

                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Radio
                        checked={promotion.codeDetail === promotionCode}
                        color="primary"
                        value={promotion.codeDetail}
                        icon={<RadioButtonIcon />}
                      />
                    </div>
                  </Row>
                  {drawEllipse(6)}
                  {drawEllipse(22)}
                  {drawEllipse(37)}
                  {drawEllipse(53)}
                  {drawEllipse(68)}
                  {drawEllipse(84)}
                </Paper>
              </ButtonBase>
            ))}
          </InfiniteScroll>
        )}
      </Col>

      {/* <NewTabLink href={ROUTES.reward.myRewards}>
        <Button
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: '12px',
          }}
        >
          <IconGift />
          <Typography variant="body2" style={{ paddingLeft: '3px', color: BLUE }}>
            <FormattedMessage id="pay.seeGiftList" />
          </Typography>
        </Button>
      </NewTabLink> */}

      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          padding: '12px 0',
          marginTop: 24,
          boxShadow: `0px 1px 15px rgba(0, 0, 0, 0.2),
            0px 9px 46px rgba(0, 0, 0, 0.12), 0px 24px 38px rgba(0, 0, 0, 0.14)`,
        }}
      >
        <Button
          variant="outlined"
          color="secondary"
          style={{
            width: '170px',
          }}
          size="large"
          onClick={close}
        >
          <FormattedMessage id="pay.promotion.return" />
        </Button>
        <div style={{ paddingLeft: '32px' }}>
          <Button
            variant="contained"
            color="secondary"
            style={{
              width: '170px',
            }}
            size="large"
            disableElevation
            onClick={() => {
              close();
              selectCode(promotionCode);
            }}
            disabled={!promotionCode}
          >
            <FormattedMessage id="pay.promotion.apply" />
          </Button>
        </div>
      </div>
    </>
  );
};

export default PromotionListBox;
