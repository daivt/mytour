import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import IconClose from '@material-ui/icons/Clear';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useTheme, useMediaQuery } from '@material-ui/core';
import { GREEN, RED } from '../../../../configs/colors';
import { some } from '../../../../constants';
// import { ReactComponent as IconGift } from '../../../../svg/booking/ic_gift.svg';
import { Row } from '../../../common/components/elements';
import FormControlTextField from '../../../common/components/FormControlTextField';
import { Promotions } from '../../utils';
import PromotionListBox from './PromotionListBox';

export interface Props {
  promotion?: some;
  listPromotion?: Promotions;
  promotionCode?: string;
  checkPromotion(code?: string): void;
  fetchRewardsHistory(search: string, page: number): void;
  loading?: boolean;
}

const DiscountCodeBox: React.FC<Props> = props => {
  const {
    checkPromotion,
    promotion,
    promotionCode,
    listPromotion,
    fetchRewardsHistory,
    loading,
  } = props;
  const [open, setOpen] = React.useState(false);
  const intl = useIntl();
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));
  return (
    <>
      <Row style={{ alignItems: 'flex-end' }}>
        <FormControlTextField
          id="flightPay.Flight_Voucher"
          formControlStyle={{
            width: '368px',
            marginBottom: -20,
          }}
          readOnly
          placeholder={intl.formatMessage({ id: 'pay.discountCodeExample' })}
          label={intl.formatMessage({ id: 'pay.enterDiscountCode' })}
          onClick={() => setOpen(true)}
          value={promotionCode || ''}
          optional
        />
        <Row>
          <Button
            id="flightPay.Flight_Voucher"
            variant="contained"
            color="primary"
            disableElevation
            style={{
              width: '175px',
            }}
            onClick={() => setOpen(true)}
          >
            {!promotionCode ? (
              <FormattedMessage id="pay.promotion.selectCode" />
            ) : (
              <FormattedMessage id="pay.promotion.selectAnotherCode" />
            )}
          </Button>
          {promotionCode && (
            <Button
              id="flightPay.Cancel_Flight_Voucher"
              style={{
                marginLeft: 16,
              }}
              variant="outlined"
              onClick={() => checkPromotion()}
            >
              <Typography variant="body1" color="textSecondary">
                <FormattedMessage
                  id={isTablet ? 'pay.promotion.cancelTablet' : 'pay.promotion.cancel'}
                />
              </Typography>
            </Button>
          )}
        </Row>
      </Row>
      <div>
        <Typography
          variant="caption"
          style={{
            color: promotion && promotion.promotionDescription && promotion.price ? GREEN : RED,
            paddingLeft: '16px',
          }}
        >
          {promotion ? promotion.promotionDescription || promotion.message : ''}
        </Typography>
      </div>
      <div>
        {/* <NewTabLink href={ROUTES.reward.myRewards} style={{ marginTop: '4px 0px 12px 8px' }}>
        <Button variant="text" style={{ padding: '4px' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconGift />
            <Typography variant="body2" style={{ paddingLeft: '4px', color: BLUE }}>
              <FormattedMessage id="pay.seeGiftList" />
            </Typography>
          </div>
        </Button>
        </NewTabLink> */}
      </div>
      <Dialog
        open={open}
        fullWidth
        keepMounted={false}
        maxWidth="sm"
        PaperProps={{ style: { overflow: 'hidden' } }}
      >
        <IconButton
          size="small"
          style={{ position: 'absolute', top: 8, right: 8 }}
          onClick={() => setOpen(false)}
        >
          <IconClose />
        </IconButton>
        <PromotionListBox
          close={() => setOpen(false)}
          selectCode={checkPromotion}
          code={promotionCode}
          listPromotion={listPromotion}
          fetchRewardsHistory={fetchRewardsHistory}
          loading={loading}
        />
      </Dialog>
    </>
  );
};

export default DiscountCodeBox;
