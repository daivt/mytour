import {
  FormControl,
  FormControlLabel,
  InputLabel,
  Radio,
  RadioGroup,
  Typography
} from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { redMark } from '../../common/components/Form';
import { Gender } from '../flight/booking/utils';

export const DropDownStyle = {
  padding: '10px 0',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
};

export const DropDownHeader = styled.div`
  cursor: pointer;
`;

export const GenderField = (props: {
  gender: Gender;
  onChange: (gender: Gender) => void;
  formControlStyle?: React.CSSProperties;
  optional?: boolean;
}) => {
  const { formControlStyle, gender, onChange, optional } = props;
  return (
    <FormControl style={formControlStyle}>
      <InputLabel shrink>
        <FormattedMessage id="gender" />
        {!optional && <> &nbsp;{redMark}</>}
      </InputLabel>
      <RadioGroup row style={{ paddingBottom: 16 }}>
        <FormControlLabel
          value="m"
          control={<Radio size="small" />}
          checked={gender === 'm'}
          onChange={(e, checked) => checked && onChange('m')}
          label={
            <Typography variant="body2">
              <FormattedMessage id="male" />
            </Typography>
          }
        />
        <FormControlLabel
          value="f"
          control={<Radio size="small" />}
          checked={gender === 'f'}
          onChange={(e, checked) => checked && onChange('f')}
          label={
            <Typography variant="body2">
              <FormattedMessage id="female" />
            </Typography>
          }
        />
      </RadioGroup>
    </FormControl>
  );
};
