import { TEAL } from '../../configs/colors';

export const HOTEL_SEARCH_HISTORY = 'hotelSearchHistory';
export const FLIGHT_SEARCH_HISTORY = 'flightSearchHistory';

export const CURRENT_LOCATION = 'currentLocation';
export const MAX_SEARCH_HISTORY = 3;
export const MAX_CHILDREN_AGE = 12;
export const TAX = 10;

export const HandleSliderStyle = {
  backgroundColor: TEAL,
  borderRadius: '6px',
  border: `solid 2px ${TEAL}`,
  width: '12px',
  height: '12px',
  boxShadow: 'none',
};
export const TrackSliderStyle = { backgroundColor: TEAL, height: '2px' };
export const RailSliderStyle = { backgroundColor: '#B2DFDB', height: '2px' };
export const TrackSliderWVStyle = { backgroundColor: TEAL, height: '4px', marginTop: '-4px' };
export const RailSliderWVStyle = { backgroundColor: '#B2DFDB', height: '4px' };
export const PAGE_SIZE_20 = 20;

export const PAYMENT_HOLDING_CODE = 'PL';
export const PAYMENT_TRIPI_CREDIT_CODE = 'CD';
export const PAYMENT_ATM_CODE = 'ATM';
export const PAYMENT_VISA_CODE = 'VM';
export const PAYMENT_VNPAY_CODE = 'QR';
export const TRIP_CREDITS_ID = 7;
export const HOLD_PARAM_NAME = 'hold';

export const FLIGHT_BOOKING_PARAM_NAME = 'booking';


