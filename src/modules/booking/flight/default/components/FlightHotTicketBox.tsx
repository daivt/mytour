import { Button, Paper, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import { push } from 'connected-react-router';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconAirplane } from '../../../../../svg/booking/ic_airplane_horizontal.svg';
import { Col, Row } from '../../../../common/components/elements';
import { Airport, FlightSearchParams, stringifyFlightSearchAndFilterParams } from '../../utils';

// import {
//   stringifyFlightSearchStateAndFilterParams,
//   addFlightSearchToHistory,
// } from '../../../search/utils';

// const partners = [
//   IconVietnamAirline,
//   IconVietjetAir,
//   IconJetstar,
//   IconBamboo,
//   IconQatarAirline,
//   IconSingaporeAirline,
// ];

function renderHotTicketSkeleton(isTablet?: boolean) {
  if (isTablet) {
    return (
      <Paper
        variant="outlined"
        style={{
          borderRadius: 0,
          display: 'flex',
          alignItems: 'center',
          padding: '12px 24px',
          marginBottom: 12,
        }}
      >
        <div style={{ width: '100px', display: 'flex', justifyContent: 'center' }}>
          <Skeleton variant="rect" width="60px" height="40px" />
        </div>

        <div style={{ width: '200px', display: 'flex', justifyContent: 'center' }}>
          <Skeleton variant="rect" width="180px" height="50px" />
        </div>

        <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-end' }}>
          <Skeleton variant="text" width="200px" height="30px" />
        </div>
      </Paper>
    );
  }
  return (
    <Paper
      variant="outlined"
      style={{
        borderRadius: 0,
        display: 'flex',
        alignItems: 'center',
        padding: '12px 24px',
        marginBottom: 12,
      }}
    >
      <div style={{ width: '100px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="60px" height="40px" />
      </div>

      <div style={{ width: '200px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="130px" height="50px" />
      </div>

      <div style={{ width: '200px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="130px" height="50px" />
      </div>

      <div style={{ width: '200px', display: 'flex', justifyContent: 'center' }}>
        <Skeleton variant="rect" width="180px" height="50px" />
      </div>

      <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-end' }}>
        <Skeleton variant="text" width="200px" height="30px" />
      </div>
    </Paper>
  );
}

interface Props {
  hotTickets: some[];
  currentLocation: some | null;
  currentDestination: some | null;
  loading: boolean;
  isTablet?: boolean;
}

const FlightHotTicketBox: React.FC<Props> = props => {
  const { hotTickets, currentLocation, currentDestination, isTablet, loading } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const searchFlight = React.useCallback(
    (ticket: some) => {
      const search: FlightSearchParams = {
        origin: {
          code: ticket.fromAirport,
          name: ticket.fromAirportName || '',
          location: currentLocation?.name || ticket.fromAirportName || '',
        } as Airport,
        destination: {
          code: ticket.toAirport,
          name: ticket.toAirportName || '',
          location: currentDestination?.destinationName || ticket.toAirportName || '',
        } as Airport,
        departureDate: moment(ticket.departureTime, DATE_FORMAT).format(DATE_FORMAT_BACK_END),
        travellerCountInfo: {
          adultCount: 1,
          childCount: 0,
          infantCount: 0,
        },
        seatClass: [],
        one_way: true,
      };
      const params = stringifyFlightSearchAndFilterParams(search);
      dispatch(push({ pathname: `${ROUTES.booking.flight.result}`, search: `?${params}` }));
    },
    [currentDestination, currentLocation, dispatch],
  );

  if (isTablet) {
    return (
      <div>
        <div>
          {!loading ? (
            hotTickets.map((ticket: some, index: number) => (
              <Paper
                key={index}
                variant="outlined"
                style={{
                  borderRadius: 0,
                  display: 'flex',
                  alignItems: 'center',
                  padding: '12px 24px',
                  marginBottom: 12,
                }}
              >
                <Col
                  style={{
                    minWidth: '105px',
                  }}
                >
                  <img src={ticket.airlineLogo} alt="" style={{ width: '72px' }} />
                </Col>
                <Col style={{ flex: 1 }}>
                  <Row style={{ alignItems: 'flex-start' }}>
                    <div style={{ maxWidth: 180 }}>
                      <Typography variant="subtitle2">
                        {currentLocation ? currentLocation.name : ''} ({ticket.fromAirport})
                      </Typography>
                      <Typography variant="caption" color="textSecondary">
                        {ticket.fromAirportName}
                      </Typography>
                    </div>
                    <IconAirplane
                      style={{
                        width: 20,
                        marginRight: '20px',
                        marginLeft: '20px',
                      }}
                    />
                    <div>
                      <Typography variant="subtitle2">
                        {currentDestination ? currentDestination.destinationName : ''} (
                        {ticket.toAirport})
                      </Typography>
                      <Typography variant="caption" color="textSecondary">
                        {ticket.toAirportName}
                      </Typography>
                    </div>
                  </Row>
                  <div style={{ marginTop: 8 }}>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.departure" />
                      :&nbsp;
                      {moment(ticket.departureTime, DATE_FORMAT).format('L')}
                    </Typography>
                    <Typography variant="caption" color="textSecondary">
                      <FormattedMessage id="flight.oneWayTicket" />
                    </Typography>
                  </div>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <Typography variant="caption" color="textSecondary">
                    <FormattedMessage id="flight.includeTaxesAndFees" />
                  </Typography>
                  <Typography variant="h5">
                    <FormattedNumber value={ticket.totalPrice} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>

                  <Button
                    style={{
                      marginLeft: 30,
                      width: 120,
                    }}
                    color="secondary"
                    variant="contained"
                    disableElevation
                    onClick={() => searchFlight(ticket)}
                  >
                    <Typography variant="button">
                      <FormattedMessage id="flight.bookingNow" />
                    </Typography>
                  </Button>
                </Col>
              </Paper>
            ))
          ) : (
            <>
              {renderHotTicketSkeleton(isTablet)}
              {renderHotTicketSkeleton(isTablet)}
              {renderHotTicketSkeleton(isTablet)}
            </>
          )}
        </div>
      </div>
    );
  }
  return (
    <div>
      <div>
        {!loading ? (
          hotTickets.map((ticket: some, index: number) => (
            <Paper
              key={index}
              variant="outlined"
              style={{
                borderRadius: 0,
                display: 'flex',
                alignItems: 'center',
                padding: '12px 24px',
                marginBottom: 12,
                flexWrap: 'wrap',
              }}
            >
              <Row
                style={{
                  minWidth: '105px',
                }}
              >
                <img src={ticket.airlineLogo} alt="" style={{ width: '72px' }} />
              </Row>
              <Row style={{ alignItems: 'flex-start' }}>
                <div style={{ minWidth: 130, maxWidth: 180 }}>
                  <Typography variant="subtitle2">
                    {currentLocation ? currentLocation.name : ''} ({ticket.fromAirport})
                  </Typography>
                  <Typography variant="caption" color="textSecondary">
                    {ticket.fromAirportName}
                  </Typography>
                </div>
                <IconAirplane
                  style={{
                    width: 20,
                    marginRight: '20px',
                    marginLeft: '20px',
                  }}
                />
                <div style={{ width: 180 }}>
                  <Typography variant="subtitle2">
                    {currentDestination ? currentDestination.destinationName : ''} (
                    {ticket.toAirport})
                  </Typography>
                  <Typography variant="caption" color="textSecondary">
                    {ticket.toAirportName}
                  </Typography>
                </div>
              </Row>
              <div>
                <Typography variant="body2">
                  <FormattedMessage id="flight.departure" />
                  :&nbsp;
                  {moment(ticket.departureTime, DATE_FORMAT).format('L')}
                </Typography>
                <Typography variant="caption" color="textSecondary">
                  <FormattedMessage id="flight.oneWayTicket" />
                </Typography>
              </div>
              <div
                style={{
                  flex: 1,
                  display: 'flex',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <Col style={{ textAlign: 'end' }}>
                  <Typography variant="h5">
                    <FormattedNumber value={Math.min(ticket.totalPrice, ticket.farePrice)} />
                    &nbsp;
                    <FormattedMessage id="currency" />
                  </Typography>
                </Col>
                <Button
                  id="flight.bookingNow"
                  style={{
                    marginLeft: 30,
                    minWidth: 144,
                  }}
                  color="secondary"
                  variant="contained"
                  disableElevation
                  onClick={() => searchFlight(ticket)}
                >
                  <Typography variant="button">
                    <FormattedMessage id="flight.bookingNow" />
                  </Typography>
                </Button>
              </div>
            </Paper>
          ))
        ) : (
          <>
            {renderHotTicketSkeleton()}
            {renderHotTicketSkeleton()}
            {renderHotTicketSkeleton()}
          </>
        )}
      </div>
    </div>
  );
};

export default FlightHotTicketBox;
