import { Button, Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconArrowRight } from '../../../../../svg/ic_arrow_right.svg';
import { Col, Row } from '../../../../common/components/elements';
import { FlightFromTo } from '../../utils';

interface Props {
  header: string;
  content?: string;
  flightList?: FlightFromTo[];
  style?: React.CSSProperties;
}
const FlightPopularSectorBox: React.FC<Props> = props => {
  const { header, content, flightList, style } = props;
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight.airlines);
  return (
    <Row
      style={{
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        minHeight: 216,
        padding: '24px 0px',
        ...style,
      }}
    >
      <Col style={{ flex: 1, maxWidth: 360 }}>
        <Typography variant="h5" style={{ marginBottom: 8 }}>
          <FormattedMessage id={header} />
        </Typography>
        {!!content && (
          <Typography variant="body2">
            <FormattedMessage id={content} />
          </Typography>
        )}
      </Col>
      <Row style={{ flex: 1, flexWrap: 'wrap' }}>
        {flightList
          ? flightList.map((record: some, index: number) => (
              <Button
                variant="outlined"
                key={index}
                style={{
                  borderRadius: '22px',
                  margin: 4,
                }}
                disableRipple
                onClick={() => {
                  window.open(record.link);
                }}
              >
                <Typography variant="subtitle2" color="textSecondary">
                  {record.origin}
                </Typography>
                <IconArrowRight fontSize="small" color="action" style={{ margin: '0px 4px 2px' }} />
                <Typography variant="subtitle2" color="textSecondary">
                  {record.destination}
                </Typography>
              </Button>
            ))
          : generalFlight
              .slice(0, 20)
              .map((record: some, index: number) => (
                <img key={index} src={record.logo} alt="" style={{ maxHeight: 36, margin: 12 }} />
              ))}
      </Row>
    </Row>
  );
};

export default FlightPopularSectorBox;
