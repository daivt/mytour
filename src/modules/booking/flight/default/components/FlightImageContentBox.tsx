import { Typography } from '@material-ui/core';
import React, { ReactNode } from 'react';
import { FormattedMessage } from 'react-intl';
import { Col } from '../../../../common/components/elements';

interface Props {
  imgUrl: string;
  header: string;
  content?: ReactNode;
  style?: React.CSSProperties;
}
const FlightImageContentBox: React.FC<Props> = props => {
  const { imgUrl, header, content, style } = props;
  return (
    <div
      style={{
        minHeight: 200,
        backgroundSize: 'cover',
        borderRadius: '4px',
        backgroundImage: `linear-gradient(to right,white 0%, #EEEEEE 30%,transparent 60%, transparent 100%),url(${imgUrl})`,
        padding: '24px 16px',
        ...style,
      }}
    >
      <Col style={{ maxWidth: 360 }}>
        <Typography variant="h6" style={{ marginBottom: 12 }}>
          <FormattedMessage id={header} />
        </Typography>
        {content}
      </Col>
    </div>
  );
};

export default FlightImageContentBox;
