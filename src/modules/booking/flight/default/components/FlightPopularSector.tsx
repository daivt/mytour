import React from 'react';
import { Col } from '../../../../common/components/elements';
import { defaultDomesticFlightList, defaultInternationalFlightList } from '../../constants';
import FlightPopularSectorBox from './FlightPopularSectorBox';

interface Props {}
const FlightPopularSector: React.FC<Props> = props => {
  return (
    <Col style={{ marginTop: 24 }}>
      <FlightPopularSectorBox
        header="flight.flightPartner"
        content="flight.popularDomesticFlightSectorContext"
      />
      <FlightPopularSectorBox
        header="flight.popularDomesticFlightSector"
        flightList={defaultDomesticFlightList}
      />
      <FlightPopularSectorBox
        style={{ marginBottom: 16 }}
        header="flight.popularInternationalFlightSector"
        content="flight.popularInternationalFlightSectorContext"
        flightList={defaultInternationalFlightList}
      />
    </Col>
  );
};

export default FlightPopularSector;
