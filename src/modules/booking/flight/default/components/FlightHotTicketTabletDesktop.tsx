import { Container, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import { Moment } from 'moment';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import FlightSearch from '../../common/FlightSearch';
import { FlightSearchParams } from '../../utils';
import FlightHotTicketBox from './FlightHotTicketBox';
import FlightHotTicketBoxHeader from './FlightHotTicketBoxHeader';
import FlightImageContent from './FlightImageContent';
import FlightPopularSector from './FlightPopularSector';
import SelectLocationDialog from './SelectLocationDialog';

interface Props {
  hasLocation?: boolean;
  locations: some[];
  currentLocation: some | null;
  topDestinations: some[];
  currentDestination: some | null;
  hotTickets: some[];
  month: Moment;
  loading: boolean;
  onSelectCurrentLocation(location: some): void;
  onSelectDestination(data: some): void;
  onRefreshData(): void;
  onSelectMonth(date: Moment): void;
  paramsSearch: FlightSearchParams;
  onSearch(params: FlightSearchParams): void;
}

const FlightHotTicketTabletDesktop: React.FC<Props> = props => {
  const {
    hasLocation,
    hotTickets,
    locations,
    currentLocation,
    topDestinations,
    currentDestination,
    onSelectCurrentLocation,
    onSelectDestination,
    onRefreshData,
    month,
    onSelectMonth,
    loading,
    paramsSearch,
    onSearch,
  } = props;
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));
  return (
    <>
      <FlightSearch params={paramsSearch} onSearch={onSearch} isTablet={isTablet} />
      <Container style={{ padding: '16px 24px', alignItems: 'center' }}>
        {hasLocation ? (
          <>
            <Typography className="text=bold" variant="h5" style={{ padding: '10px 0' }}>
              <FormattedMessage id="flight.bestTicketPrice" />
            </Typography>
            <FlightHotTicketBoxHeader
              locations={locations}
              currentLocation={currentLocation}
              topDestinations={topDestinations}
              currentDestination={currentDestination}
              month={month}
              onSelectCurrentLocation={onSelectCurrentLocation}
              onSelectDestination={data => onSelectDestination(data)}
              onRefreshData={onRefreshData}
              onSelectMonth={date => onSelectMonth(date)}
            />

            <FlightHotTicketBox
              isTablet={isTablet}
              hotTickets={hotTickets}
              currentLocation={currentLocation}
              currentDestination={currentDestination}
              loading={loading}
            />
          </>
        ) : (
          <SelectLocationDialog
            show={!hasLocation}
            onClose={onSelectCurrentLocation}
            locations={locations}
          />
        )}
        <Typography variant="h5" style={{ marginBottom: 16 }}>
          <FormattedMessage id="flight.whyBookingOnMyTour" />
        </Typography>
        <FlightImageContent />
        <FlightPopularSector />
      </Container>
    </>
  );
};
export default FlightHotTicketTabletDesktop;
