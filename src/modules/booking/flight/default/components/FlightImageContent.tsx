import { Typography } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Col } from '../../../../common/components/elements';
import { creditCard, phone, planeUrl } from '../utils';
import FlightImageContentBox from './FlightImageContentBox';

interface Props {}
const FlightImageContent: React.FC<Props> = props => {
  return (
    <Col>
      <FlightImageContentBox
        style={{ marginBottom: '2px' }}
        imgUrl={planeUrl}
        header="flight.imageContent.bestSearchHeader"
        content={
          <Typography variant="body2">
            <FormattedMessage id="flight.imageContent.bestSearchContent" />
          </Typography>
        }
      />
      <FlightImageContentBox
        style={{ marginBottom: '2px' }}
        imgUrl={creditCard}
        header="flight.imageContent.easyPaymentHeader"
        content={
          <Col>
            <Typography variant="body2">
              <FormattedMessage id="flight.imageContent.easyPaymentContent" />
            </Typography>
            <Typography variant="body2" component="li">
              <FormattedMessage id="flight.imageContent.easyPaymentContentNote1" />
            </Typography>
            <Typography variant="body2" component="li">
              <FormattedMessage id="flight.imageContent.easyPaymentContentNote2" />
            </Typography>
            <Typography variant="body2" component="li">
              <FormattedMessage id="flight.imageContent.easyPaymentContentNote3" />
            </Typography>
            <Typography variant="body2" component="li">
              <FormattedMessage id="flight.imageContent.easyPaymentContentNote4" />
            </Typography>
          </Col>
        }
      />
      <FlightImageContentBox
        imgUrl={phone}
        header="flight.imageContent.customerSupportHeader"
        content={
          <Col>
            <Typography variant="body2">
              <FormattedMessage id="flight.imageContent.customerSupportContentNote1" />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="flight.imageContent.customerSupportContentNote2" />
            </Typography>
          </Col>
        }
      />
    </Col>
  );
};

export default FlightImageContent;
