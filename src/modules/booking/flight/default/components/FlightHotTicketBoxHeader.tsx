import { IconButton, InputAdornment, Typography } from '@material-ui/core';
import moment, { Moment } from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { some } from '../../../../../constants';
import { ReactComponent as IconPin } from '../../../../../svg/booking/ic_pin.svg';
import { ReactComponent as IconCalender } from '../../../../../svg/ic_calendar.svg';
import { Row } from '../../../../common/components/elements';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';

interface Props {
  locations: some[];
  currentLocation: some | null;
  topDestinations: some[];
  currentDestination: some | null;
  month: Moment;
  onSelectCurrentLocation(location: some): void;
  onSelectDestination(data: some): void;
  onRefreshData(): void;
  onSelectMonth(date: Moment): void;
}
const FlightHotTicketBoxHeader: React.FC<Props> = props => {
  const {
    locations,
    currentLocation,
    topDestinations,
    currentDestination,
    onSelectCurrentLocation,
    onSelectDestination,
    month,
    onSelectMonth,
  } = props;
  const intl = useIntl();

  const listMonth = React.useMemo(() => {
    return [0, 1, 2].map(num => moment().add(num, 'months'));
  }, []);

  return (
    <Row>
      <Typography variant="body2" style={{ marginBottom: 20, marginRight: 12 }}>
        <FormattedMessage id="flight.selectYourLocation" />
      </Typography>
      <FormControlAutoComplete
        placeholder={intl.formatMessage({ id: 'flight.selectYourLocation' })}
        value={currentLocation as any}
        formControlStyle={{ width: 200 }}
        onChange={(e: any, value: some | null) => {
          value && onSelectCurrentLocation(value);
        }}
        getOptionSelected={(option: some, value: some) => {
          return option.id === value.id;
        }}
        inputStyle={{ minHeight: 32 }}
        getOptionLabel={(v: some) => v.name}
        options={locations}
        startAdornment={
          <InputAdornment position="start" style={{ marginLeft: 8 }}>
            <IconButton size="small" edge="start" tabIndex={-1}>
              <IconPin />
            </IconButton>
          </InputAdornment>
        }
        optional
        disableClearable
      />
      <Typography variant="body2" style={{ marginBottom: 20, marginRight: 12 }}>
        <FormattedMessage id="flight.yourDestination" />
      </Typography>

      <FormControlAutoComplete
        id="destinationName"
        placeholder={intl.formatMessage({ id: 'flight.yourDestination' })}
        value={currentDestination as any}
        formControlStyle={{ width: 200 }}
        onChange={(e: any, value: some | null) => {
          value && onSelectDestination(value);
        }}
        inputStyle={{ minHeight: 32 }}
        getOptionLabel={(v: some) => v.destinationName}
        getOptionSelected={(option: some, value: some) => {
          return option.toAirport === value.toAirport;
        }}
        options={topDestinations}
        startAdornment={
          <InputAdornment position="start" style={{ marginLeft: 8 }}>
            <IconButton size="small" edge="start" tabIndex={-1}>
              <IconPin />
            </IconButton>
          </InputAdornment>
        }
        optional
        disableClearable
      />
      <Typography variant="body2" style={{ marginBottom: 20, marginRight: 12 }}>
        <FormattedMessage id="flight.time" />
      </Typography>
      <FormControlAutoComplete<number, undefined, true, undefined>
        id="time"
        placeholder={intl.formatMessage({ id: 'flight.time' })}
        value={month.month()}
        formControlStyle={{ width: 200 }}
        onChange={(e: any, value: number | null) => {
          const tmp = listMonth.find(obj => obj.month() === value);
          tmp && onSelectMonth(tmp);
        }}
        getOptionLabel={(v: number) => intl.formatMessage({ id: 'month' }, { num: v + 1 })}
        inputStyle={{ minHeight: 32 }}
        options={listMonth.map(v => v.month())}
        startAdornment={
          <InputAdornment position="start" style={{ marginLeft: 8 }}>
            <IconButton size="small" edge="start" tabIndex={-1}>
              <IconCalender />
            </IconButton>
          </InputAdornment>
        }
        optional
        disableClearable
      />
    </Row>
  );
};

export default FlightHotTicketBoxHeader;
