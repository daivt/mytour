import { replace } from 'connected-react-router';
import moment, { Moment } from 'moment';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { C_DATE_FORMAT } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { CURRENT_LOCATION } from '../../../constants';
import {
  defaultFlightSearchParams,
  FlightSearchParams,
  parseFlightSearchParams,
  stringifyFlightSearchAndFilterParams,
} from '../../utils';
import FlightHotTicketTabletDesktop from '../components/FlightHotTicketTabletDesktop';

interface Props {}

const Flight: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [locations, setLocations] = React.useState<some[]>([]);
  const [hotTickets, setHotTickets] = React.useState<some[]>([]);
  const [topDestinations, setTopDestinations] = React.useState<some[]>([]);
  const [currentDestination, setCurrentDestination] = React.useState<some | null>(null);
  const [currentLocation, setCurrentLocation] = React.useState<some | null>(null);
  const [month, setMonth] = React.useState(moment());
  const [loading, setLoading] = React.useState(false);
  const [hasLocation, setHasLocation] = React.useState(true);
  const [paramsSearch, setParams] = React.useState<FlightSearchParams>(defaultFlightSearchParams);
  const location = useLocation();

  const searchHotTicket = React.useCallback(
    async (selectedLocation: some, date: Moment | undefined = undefined) => {
      const monthTmp = date || month;
      setLoading(true);
      const params = {
        fromDate: monthTmp.startOf('months').format(C_DATE_FORMAT),
        toDate: monthTmp.endOf('months').format(C_DATE_FORMAT),
        fromAirport: selectedLocation.fromAirport,
        toAirport: selectedLocation.toAirport,
        adults: 1,
        children: 0,
        infants: 0,
        groupByDate: false,
      };

      const json = await dispatch(fetchThunk(API_PATHS.searchHotTickets, 'post', params));

      if (json.code === SUCCESS_CODE || json.code === '200') {
        setHotTickets(json.data);
      }

      setLoading(false);
    },
    [dispatch, month],
  );

  const fetchTopDestination = React.useCallback(
    async (locale: some) => {
      const json = await dispatch(
        fetchThunk(`${API_PATHS.getTopDestinations}?fromAirport=${locale.airportCode}&size=5`),
      );

      if (json?.code === SUCCESS_CODE || json?.code === '200') {
        setTopDestinations(json.data);
        setCurrentDestination(json.data[0]);
        if (!json.data.length) {
          setHotTickets([]);
          return;
        }
        searchHotTicket(json.data[0]);
      }
    },
    [dispatch, searchHotTicket],
  );

  const onSelectLocation = React.useCallback(
    (locale: some) => {
      if (!locale) {
        return;
      }
      localStorage.setItem(CURRENT_LOCATION, JSON.stringify(locale));
      setCurrentLocation(locale);
      setHasLocation(true);
      fetchTopDestination(locale);
    },
    [fetchTopDestination],
  );

  const onSelectDestination = React.useCallback(
    async (data: some) => {
      setCurrentDestination(data);
      searchHotTicket(data);
    },
    [searchHotTicket],
  );
  const onRefreshData = React.useCallback(() => {
    if (currentDestination && !loading) {
      searchHotTicket(currentDestination);
    }
  }, [currentDestination, loading, searchHotTicket]);

  const onSelectMonth = React.useCallback(
    (monthTmp: Moment) => {
      setMonth(monthTmp);
      if (currentDestination) {
        searchHotTicket(currentDestination, monthTmp);
      }
    },
    [currentDestination, searchHotTicket],
  );

  const fetchListLocations = React.useCallback(async () => {
    const currentLocationStr = localStorage.getItem(CURRENT_LOCATION) || false;
    const locationsJson = await dispatch(fetchThunk(API_PATHS.getListLocationsInformation));
    setLocations(locationsJson?.data || []);
    if (!currentLocationStr) {
      setHasLocation(false);
      return;
    }
    const currentLocationTmp = JSON.parse(currentLocationStr);
    setCurrentLocation(currentLocationTmp);
    fetchTopDestination(currentLocationTmp);
  }, [dispatch, fetchTopDestination]);

  React.useEffect(() => {
    fetchListLocations();
  }, [fetchListLocations]);

  React.useEffect(() => {
    try {
      const searchUrl = new URLSearchParams(location.search);
      const params = parseFlightSearchParams(searchUrl);
      
      setParams(params);
    } catch (error) {}
  }, [dispatch, location.search]);

  return (
    <FlightHotTicketTabletDesktop
      hasLocation={hasLocation}
      locations={locations}
      currentLocation={currentLocation}
      topDestinations={topDestinations}
      currentDestination={currentDestination}
      hotTickets={hotTickets}
      month={month}
      loading={loading}
      onSelectCurrentLocation={onSelectLocation}
      onSelectDestination={onSelectDestination}
      onRefreshData={onRefreshData}
      onSelectMonth={onSelectMonth}
      paramsSearch={paramsSearch}
      onSearch={params => {
        const search = stringifyFlightSearchAndFilterParams(params);
        dispatch(replace({ search }));
        dispatch(
          goToAction({
            pathname: ROUTES.booking.flight.result,
            search,
          }),
        );
      }}
    />
    // <FlightSearch params={defaultFlightSearchParams} />
  );
};

export default Flight;
