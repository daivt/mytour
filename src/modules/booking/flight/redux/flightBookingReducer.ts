import { ActionType, createAction, getType } from 'typesafe-actions';
import { FlightInfo } from '../booking/utils';

export interface FlightBookingState {
  info?: FlightInfo;
}
export const defaultFlightBookingState: FlightBookingState = {};

export const setFlightBookingInfo = createAction(
  'common/setFlightBookingInfo',
  (val?: FlightInfo) => ({
    val,
  }),
)();

const actions = {
  setFlightBookingInfo,
};

type ActionT = ActionType<typeof actions>;

function reducer(
  state: FlightBookingState = defaultFlightBookingState,
  action: ActionT,
): FlightBookingState {
  switch (action.type) {
    case getType(setFlightBookingInfo):
      return { ...state, info: action.payload.val };
    default:
      return state;
  }
}

export default reducer;
