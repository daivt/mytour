/* eslint-disable no-nested-ternary */
import moment from 'moment';
import { some } from '../../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../../models/moment';
import { defaultFlightBookingResult, FlightBookingResult } from '../utils';

export type Gender = 'm' | 'f';

export interface PassportInfo {
  passport: string;
  passportExpiredDate: string; // 'DD-MM-YYYY'
  passportCountry: some | null; // Quoc gia cap
  nationalityCountry: some | null; //
}
export const defaultPassportInfo: PassportInfo = {
  passport: '',
  passportExpiredDate: '',
  passportCountry: null,
  nationalityCountry: null,
};

export interface TravellerInfo {
  fullName: string;
  birthday: string;
  gender: Gender;
  passportInfo: PassportInfo;
}

export interface TravellersInfo {
  adults: TravellerInfo[];
  children: TravellerInfo[];
  babies: TravellerInfo[];
}

export interface InvoiceInfo {
  taxInfo?: some;
  taxIdNumber?: string;
  companyName: string;
  companyAddress: string;
  recipientEmail: string;
  recipientAddress: string;
  note: string;
  customerName: string;
  recipientName: string;
  recipientPhone: string;
}

const defaultInvoiceInfo: InvoiceInfo = {
  taxIdNumber: '',
  companyName: '',
  companyAddress: '',
  recipientEmail: '',
  recipientAddress: '',
  note: '',
  customerName: '',
  recipientName: '',
  recipientPhone: '',
};
export interface ContactInfo {
  fullName: string;
  gender: Gender;
  address: string;
  email: string;
  telephone: string;
}
export interface FlightInfo extends FlightBookingResult {
  travellersInfo: TravellersInfo;
  exportInvoice: boolean;
  invoiceInfo: InvoiceInfo;
  buyInsurance: boolean;
  insurancePackage?: some;
  contactInfo: ContactInfo;
  booker?: some;
}

export const defaultFlightInfo: FlightInfo = {
  ...defaultFlightBookingResult,
  exportInvoice: false,
  invoiceInfo: defaultInvoiceInfo,
  buyInsurance: false,
  travellersInfo: { adults: [], babies: [], children: [] },
  contactInfo: {
    address: '',
    email: '',
    fullName: '',
    gender: 'm',
    telephone: '',
  },
};

function convertTravellers(info: TravellersInfo) {
  let value: some[] = [];
  value = value
    .concat(
      info.adults.map(one => ({
        lastName: one.fullName.trim().split(' ')[0],
        firstName: one.fullName
          .trim()
          .split(' ')
          .slice(1)
          .join(' '),
        gender: one.gender.toUpperCase(),
        dob: one.birthday,
      })),
    )
    .concat(
      info.children.map(one => ({
        lastName: one.fullName.trim().split(' ')[0],
        firstName: one.fullName
          .trim()
          .split(' ')
          .slice(1)
          .join(' '),
        gender: one.gender.toUpperCase(),
        dob: one.birthday,
      })),
    )
    .concat(
      info.babies.map(one => ({
        lastName: one.fullName.trim().split(' ')[0],
        firstName: one.fullName
          .trim()
          .split(' ')
          .slice(1)
          .join(' '),
        gender: one.gender.toUpperCase(),
        dob: one.birthday,
      })),
    );
  return value;
}

export function getGuestParams(booking: FlightInfo) {
  const departureOutboundDate = booking.outbound.ticket
    ? moment(booking.outbound.ticket.outbound.departureTime).format(DATE_FORMAT_BACK_END)
    : '';

  let arrivalOutboundDate = booking.outbound.ticket
    ? moment(booking.outbound.ticket.outbound.arrivalTime).format(DATE_FORMAT_BACK_END)
    : '';

  if (booking.inbound.ticket) {
    arrivalOutboundDate = booking.inbound.ticket.outbound.arrivalDayStr;
  }

  const travellerInfo = convertTravellers(booking.travellersInfo);
  const guests = travellerInfo.map(item => ({
    ...item,
    dob: item.dob,
    insuranceInfo: {
      insurancePackageCode: booking.insurancePackage ? booking.insurancePackage.code : '',
      fromDate: departureOutboundDate,
      toDate: arrivalOutboundDate,
    },
    passport: '',
  }));

  return guests;
}
