import { Tab, withStyles } from '@material-ui/core';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { DATE_FORMAT, DATE_TIME_FORMAT } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goBackAction, goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { setFlightBookingInfo } from '../../redux/flightBookingReducer';
import { parseFlightBookingParams } from '../../result/utils';
import FlightBookingInfoForm from '../components/FlightBookingInfoForm';
import { defaultFlightInfo, FlightInfo, TravellerInfo, getGuestParams } from '../utils';

export const CustomTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
  },
  selected: {
    color: theme.palette.primary.main,
  },
}))(Tab);

interface Props {}

const FlightBookingInfo: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const flightInfo = useSelector((state: AppState) => state.booking.flight.info);
  const [bookingInfo, setBookingInfo] = React.useState<FlightInfo>(defaultFlightInfo);
  const [listCounties, setListCounties] = React.useState<some[]>([]);
  const [loading, setLoading] = React.useState(true);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const location = useLocation();

  const fetchListCounties = React.useCallback(async () => {
    const json = await dispatch(fetchThunk(API_PATHS.getAllCountries));
    if (json?.code === SUCCESS_CODE) {
      setListCounties(json.data.countries);
    } else {
      json?.message &&
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar]);

  const fetchInsurancePackage = React.useCallback(
    async (bookingInfoTmp: FlightInfo) => {
      const departureOutboundDate = bookingInfoTmp.outbound.ticket
        ? moment(
            `${bookingInfoTmp.outbound.ticket.outbound.departureDayStr} ${bookingInfoTmp.outbound.ticket.outbound.departureTimeStr}`,
            DATE_FORMAT,
          ).format(DATE_TIME_FORMAT)
        : '';

      let arrivalOutboundDate = bookingInfoTmp.outbound.ticket
        ? moment(
            `${bookingInfoTmp.outbound.ticket.outbound.arrivalDayStr} ${bookingInfoTmp.outbound.ticket.outbound.arrivalTimeStr}`,
            DATE_FORMAT,
          ).format(DATE_TIME_FORMAT)
        : '';

      if (bookingInfoTmp.inbound.ticket) {
        arrivalOutboundDate = moment(
          `${bookingInfoTmp.inbound.ticket.outbound.arrivalDayStr} ${bookingInfoTmp.inbound.ticket.outbound.arrivalTimeStr}`,
          DATE_FORMAT,
        ).format(DATE_TIME_FORMAT);
      }

      const json = await dispatch(
        fetchThunk(`${API_PATHS.getInsurancePackage}`, 'post', {
          fromAirportCode: bookingInfoTmp.outbound.ticket
            ? bookingInfoTmp.outbound.ticket.outbound.departureAirport
            : '',
          toAirportCode: bookingInfoTmp.outbound.ticket
            ? bookingInfoTmp.outbound.ticket.outbound.arrivalAirport
            : '',
          hasTransit: bookingInfoTmp.outbound.ticket
            ? bookingInfoTmp.outbound.ticket.outbound.transitTickets
            : false,
          isOneWay: !bookingInfoTmp.inbound.ticket,
          fromDate: departureOutboundDate,
          toDate: arrivalOutboundDate,
          isMultipleInsurance: true,
        }),
      );

      if (flightInfo && JSON.stringify(flightInfo?.tid) === JSON.stringify(bookingInfoTmp.tid)) {
        setBookingInfo(flightInfo);
      } else if (json?.code === 200 && json?.data.length) {
        setBookingInfo({ ...bookingInfoTmp, insurancePackage: json.data[0] });
      } else {
        setBookingInfo({ ...bookingInfoTmp, insurancePackage: undefined });
      }
      setLoading(false);
    },
    [dispatch, flightInfo],
  );

  const fetchTicketDataAndInsurancePackage = React.useCallback(
    async (bookingInfoTmp: FlightInfo) => {
      if (bookingInfoTmp.tid?.outbound) {
        setLoading(true);
        const [outJson, inJson] = await Promise.all([
          dispatch(
            fetchThunk(API_PATHS.getTicketDetail, 'post', {
              agencyId: `${bookingInfoTmp.tid?.outbound.aid}`,
              requestId: `${bookingInfoTmp.tid?.requestId}`,
              ticketId: `${bookingInfoTmp.tid?.outbound.id}`,
            }),
          ),
          bookingInfoTmp.tid?.inbound
            ? dispatch(
                fetchThunk(API_PATHS.getTicketDetail, 'post', {
                  agencyId: `${bookingInfoTmp.tid?.inbound.aid}`,
                  requestId: `${bookingInfoTmp.tid.requestId}`,
                  ticketId: `${bookingInfoTmp.tid?.inbound.id}`,
                }),
              )
            : Promise.resolve(undefined),
        ]);

        if (outJson?.code === 200) {
          let tmp: FlightInfo = bookingInfoTmp;

          const defaultTravellerInfo: TravellerInfo = {
            fullName: '',
            birthday: '',
            gender: 'm',
            passportInfo: {
              passport: '',
              passportExpiredDate: '',
              passportCountry: null,
              nationalityCountry: null,
            },
          };

          const adultCount = outJson.data.searchRequest.numAdults;
          const childCount = outJson.data.searchRequest.numChildren;
          const infantCount = outJson.data.searchRequest.numInfants;

          const adults = Array(adultCount).fill(defaultTravellerInfo);

          const children = Array(childCount).fill(defaultTravellerInfo);

          const babies = Array(infantCount).fill(defaultTravellerInfo);
          const travellersInfo = {
            adults,
            children,
            babies,
          };
          tmp = {
            ...bookingInfoTmp,
            outbound: {
              ticket: outJson.data.ticket,
              extraBaggages: Array(adultCount + childCount).fill(
                outJson.data.ticket.outbound.baggages[0],
              ),
              searchRequest: outJson.data.searchRequest,
            },
            travellersInfo,
          };

          if (inJson && inJson.code === SUCCESS_CODE) {
            tmp = {
              ...tmp,
              inbound: {
                ticket: inJson.data.ticket,
                extraBaggages: Array(adultCount + childCount).fill(
                  inJson.data.ticket.outbound.baggages[0],
                ),
                searchRequest: outJson.data.searchRequest,
              },
            };
          }
          return tmp;
        }
        if (outJson?.code === 400 || (inJson && inJson.code === 400)) {
          dispatch(goBackAction());
        }
      }
      return undefined;
    },
    [dispatch],
  );

  const updateQueryParamsTab = React.useCallback(async () => {
    if (location.search) {
      const state = parseFlightBookingParams(new URLSearchParams(location.search));
      const tmp = { ...bookingInfo, tid: state };
      await fetchTicketDataAndInsurancePackage(tmp).then((value?: FlightInfo) => {
        value && fetchInsurancePackage(value);
      });
    } else {
      dispatch(goToAction({ pathname: ROUTES.booking.flight.default }));
    }
  }, [
    bookingInfo,
    dispatch,
    fetchInsurancePackage,
    fetchTicketDataAndInsurancePackage,
    location.search,
  ]);
  const validateInsuranceInfo = React.useCallback(async () => {
    if (bookingInfo.insurancePackage && bookingInfo.buyInsurance) {
      const params = {
        fromAirportCode: bookingInfo.outbound.ticket
          ? bookingInfo.outbound.ticket.outbound.departureAirport
          : '',
        toAirportCode: bookingInfo.outbound.ticket
          ? bookingInfo.outbound.ticket.outbound.arrivalAirport
          : '',
        insuranceContact: {
          addr1: bookingInfo.contactInfo.address,
          firstName: bookingInfo.contactInfo.fullName.split(' ')[0],
          lastName: bookingInfo.contactInfo.fullName
            .split(' ')
            .slice(1)
            .join(' '),
          passport: '',
          email: bookingInfo.contactInfo.email,
          phone1: bookingInfo.contactInfo.telephone,
          dob: bookingInfo.travellersInfo.adults[0].birthday,
        },
        guests: getGuestParams(bookingInfo),
        isMultipleInsurance: true,
      };
      const json = await dispatch(fetchThunk(API_PATHS.validateInsuranceInfo, 'post', params));

      if (json.code === SUCCESS_CODE) {
        return true;
      }
      json?.message &&
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      return false;
    }
    return true;
  }, [bookingInfo, closeSnackbar, dispatch, enqueueSnackbar]);

  const onContinue = React.useCallback(
    async (value: FlightInfo) => {
      if (await validateInsuranceInfo()) {
        dispatch(setFlightBookingInfo(value));
        dispatch(goToAction({ pathname: ROUTES.booking.flight.review }));
      }
    },
    [dispatch, validateInsuranceInfo],
  );

  React.useEffect(() => {
    updateQueryParamsTab();
    fetchListCounties();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.search]);

  if (loading) {
    return <LoadingIcon />;
  }
  return (
    <>
      <FlightBookingInfoForm info={bookingInfo} listCounties={listCounties} setInfo={onContinue} />
    </>
  );
};

export default FlightBookingInfo;
