/* eslint-disable react/no-danger */
import {
  Button,
  Checkbox,
  Collapse,
  Container,
  FormControlLabel,
  IconButton,
  InputAdornment,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { FormikErrors, useFormik } from 'formik';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React from 'react';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import voca from 'voca';
import * as yup from 'yup';
import { API_PATHS } from '../../../../../configs/API';
import { some } from '../../../../../constants';
import { HEADER_HEIGHT } from '../../../../../layout/constants';
import { DATE_FORMAT_BACK_END } from '../../../../../models/moment';
import {
  noSpecialText,
  rawAlphabetRegex,
  validPhoneNumberRegex,
} from '../../../../../models/regex';
import { AppState } from '../../../../../redux/reducers';
import BirthDayField from '../../../../common/components/BirthDayField';
import { AntSwitch, Col, Row, snackbarSetting } from '../../../../common/components/elements';
import FormControlAutoComplete from '../../../../common/components/FormControlAutoComplete';
import FormControlTextField from '../../../../common/components/FormControlTextField';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import AsideBound from '../../../common/AsideBound';
import { GenderField } from '../../../common/element';
import { scrollTo } from '../../../utils';
import FlightInfoBox from '../../common/FlightInfoBox';
import FlightTicketDialog from '../../common/FlightTicketDialog';
import { FlightInfo, TravellerInfo } from '../utils';

interface Props {
  info: FlightInfo;
  listCounties: some[];
  setInfo(info: FlightInfo): void;
}

const FlightBookingInfoForm: React.FC<Props> = props => {
  const { info, setInfo, listCounties } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const [seeDetail, setSeeDetail] = React.useState(false);
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const styleCS: React.CSSProperties = { width: isTablet ? 225 : 280 };
  const needPassport = info.outbound.ticket
    ? info.outbound.ticket.outbound?.ticketdetail.needPassport
    : false;

  const departureDate = info.outbound.ticket
    ? info.outbound.ticket?.outbound.arrivalTime
    : undefined;
  const returnDate = info.inbound.ticket
    ? info.inbound.ticket?.outbound.arrivalTime
    : info.outbound.ticket?.outbound.arrivalTime;

  const bookingInfoSchema = React.useMemo(() => {
    return yup.object().shape({
      outbound: yup.mixed().notRequired(),
      travellersInfo: yup.object().shape({
        adults: yup.array().of(
          yup.object().shape({
            fullName: yup
              .string()
              .trim()
              .test({
                name: 'fullName',
                message: intl.formatMessage({ id: 'fullNameValid' }),
                test: value => {
                  return value ? value.split(' ').length > 1 : true;
                },
              })
              .required(intl.formatMessage({ id: 'required' })),
            birthday: yup
              .string()
              .trim()
              .test('birthday', intl.formatMessage({ id: 'required' }), value => {
                return needPassport ? !!value?.trim() : true;
              })
              .test('birthday', intl.formatMessage({ id: 'birthdayValid' }), value => {
                const birthday = moment(value, DATE_FORMAT_BACK_END, true);
                const age = moment(departureDate)
                  .startOf('day')
                  .diff(birthday, 'years');
                return needPassport ? birthday.isValid() === true && age >= 12 : true;
              }),
            gender: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' })),
            passportInfo: yup.object().when('outbound', {
              is: () => needPassport,
              then: yup.object().shape({
                passport: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' })),
                passportExpiredDate: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' }))
                  .test(
                    'passportExpiredDate',
                    intl.formatMessage({ id: 'passportExpiredDateValid' }),
                    value => {
                      const date = moment(value, DATE_FORMAT_BACK_END, true);
                      return (
                        date.isValid() === true &&
                        date.isSameOrAfter(moment().startOf('day')) === true &&
                        departureDate &&
                        date.isSameOrAfter(moment(departureDate)) === true
                      );
                    },
                  ),
                passportCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
                nationalityCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
              }),
              otherwise: yup.object().notRequired(),
            }),
          }),
        ),
        children: yup.array().of(
          yup.object().shape({
            fullName: yup
              .string()
              .trim()
              .test({
                name: 'fullName',
                message: intl.formatMessage({ id: 'fullNameValid' }),
                test: value => {
                  return value ? value.split(' ').length > 1 : true;
                },
              })
              .required(intl.formatMessage({ id: 'required' })),
            birthday: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' }))
              .test('birthday', intl.formatMessage({ id: 'birthdayValid' }), value => {
                const birthday = moment(value, DATE_FORMAT_BACK_END, true);
                const ageStart = moment(departureDate)
                  .startOf('day')
                  .diff(birthday, 'years');
                const ageEnd = moment(returnDate)
                  .startOf('day')
                  .diff(birthday, 'years');
                return !!(birthday.isValid() === true && ageStart >= 2 && ageEnd < 12);
              }),
            gender: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' })),
            passportInfo: yup.object().when('outbound', {
              is: () => needPassport,
              then: yup.object().shape({
                passport: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' })),
                passportExpiredDate: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' }))
                  .test(
                    'passportExpiredDate',
                    intl.formatMessage({ id: 'passportExpiredDateValid' }),
                    value => {
                      const date = moment(value, DATE_FORMAT_BACK_END, true);
                      return (
                        date.isValid() === true &&
                        date.isSameOrAfter(moment().startOf('day')) === true &&
                        departureDate &&
                        date.isSameOrAfter(moment(departureDate)) === true
                      );
                    },
                  ),
                passportCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
                nationalityCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
              }),
              otherwise: yup.object().notRequired(),
            }),
          }),
        ),
        babies: yup.array().of(
          yup.object().shape({
            fullName: yup
              .string()
              .trim()
              .test({
                name: 'fullName',
                message: intl.formatMessage({ id: 'fullNameValid' }),
                test: value => {
                  return value ? value.split(' ').length > 1 : true;
                },
              })
              .required(intl.formatMessage({ id: 'required' })),
            birthday: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' }))
              .test('birthday', intl.formatMessage({ id: 'birthdayValid' }), value => {
                const birthday = moment(value, DATE_FORMAT_BACK_END, true);
                const age = moment(returnDate)
                  .startOf('day')
                  .diff(birthday, 'months');
                return !!(birthday.isValid() === true && age < 24 && age >= 0);
              }),
            gender: yup
              .string()
              .trim()
              .required(intl.formatMessage({ id: 'required' })),
            passportInfo: yup.object().when('outbound', {
              is: () => needPassport,
              then: yup.object().shape({
                passport: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' })),
                passportExpiredDate: yup
                  .string()
                  .trim()
                  .required(intl.formatMessage({ id: 'required' }))
                  .test(
                    'passportExpiredDate',
                    intl.formatMessage({ id: 'passportExpiredDateValid' }),
                    value => {
                      const date = moment(value, DATE_FORMAT_BACK_END, true);
                      return (
                        date.isValid() === true &&
                        date.isSameOrAfter(moment().startOf('day')) === true &&
                        departureDate &&
                        date.isSameOrAfter(moment(departureDate)) === true
                      );
                    },
                  ),
                passportCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
                nationalityCountry: yup
                  .mixed()
                  .nullable()
                  .required(intl.formatMessage({ id: 'required' })),
              }),
              otherwise: yup.object().notRequired(),
            }),
          }),
        ),
      }),
      invoiceInfo: yup.object().when('exportInvoice', {
        is: true,
        then: yup.object().shape({
          taxIdNumber: yup
            .string()
            .trim()
            .required(intl.formatMessage({ id: 'required' })),
          companyName: yup
            .string()
            .trim()
            .required(intl.formatMessage({ id: 'required' })),
          companyAddress: yup
            .string()
            .trim()
            .required(intl.formatMessage({ id: 'required' })),
          recipientEmail: yup
            .string()
            .trim()
            .email(intl.formatMessage({ id: `emailInvalid` }))
            .required(intl.formatMessage({ id: 'required' })),
          customerName: yup
            .string()
            .trim()
            .required(intl.formatMessage({ id: 'required' })),
          recipientAddress: yup
            .string()
            .trim()
            .required(intl.formatMessage({ id: 'required' })),
        }),
        otherwise: yup.object().notRequired(),
      }),
      contactInfo: yup.object().when('buyInsurance', {
        is: true,
        then: yup.object().shape({
          fullName: yup
            .string()
            .trim()
            .test({
              name: 'fullName',
              message: intl.formatMessage({ id: 'fullNameValid' }),
              test: value => {
                return value ? value.split(' ').length > 1 : true;
              },
            })
            .required(intl.formatMessage({ id: 'required' })),
          gender: yup
            .string()
            .trim()
            .required(intl.formatMessage({ id: 'required' })),
          address: yup
            .string()
            .trim()
            .notRequired(),
          email: yup
            .string()
            .email(intl.formatMessage({ id: `emailInvalid` }))
            .required(intl.formatMessage({ id: 'required' })),
          telephone: yup
            .string()
            .trim()
            .min(8, intl.formatMessage({ id: 'phoneNumberValid' }))
            .max(13, intl.formatMessage({ id: 'phoneNumberValid' }))
            .required(intl.formatMessage({ id: 'required' })),
        }),
        otherwise: yup.object().shape({
          fullName: yup
            .string()
            .trim()
            .test({
              name: 'fullName',
              message: intl.formatMessage({ id: 'fullNameValid' }),
              test: value => {
                return value ? value.split(' ').length > 1 : true;
              },
            })
            .required(intl.formatMessage({ id: 'required' })),
          gender: yup
            .string()
            .trim()
            .required(intl.formatMessage({ id: 'required' })),
          address: yup
            .string()
            .trim()
            .notRequired(),
          email: yup
            .string()
            .notRequired()
            .email(intl.formatMessage({ id: `emailInvalid` })),
          telephone: yup
            .string()
            .trim()
            .min(7, intl.formatMessage({ id: 'phoneNumberValid' }))
            .max(12, intl.formatMessage({ id: 'phoneNumberValid' }))
            .required(intl.formatMessage({ id: 'required' })),
        }),
      }),
    });
  }, [intl, needPassport, departureDate, returnDate]);

  const formik = useFormik({
    initialValues: info,
    onSubmit: values => {
      setInfo({
        ...values,
        invoiceInfo: {
          ...values.invoiceInfo,
          recipientName: values.invoiceInfo.customerName,
          recipientPhone: values.contactInfo.telephone,
        },
      });
    },
    validationSchema: bookingInfoSchema,
  });

  const searchByTaxNumber = React.useCallback(async () => {
    if (formik.values.invoiceInfo.taxIdNumber) {
      const json = await dispatch(
        fetchThunk(API_PATHS.getEnterpriseInfo, 'post', {
          taxCode: formik.values.invoiceInfo.taxIdNumber,
        }),
      );
      if (json.code === 200 && json.data) {
        formik.setFieldValue('invoiceInfo', {
          ...formik.values.invoiceInfo,
          taxIdNumber: json.data.taxCode,
          companyName: json.data.companyName,
          companyAddress: json.data.companyAddress,
          recipientAddress:
            !formik.touched.invoiceInfo?.recipientAddress && !info.exportInvoice
              ? json.data.companyAddress
              : formik.values.invoiceInfo.recipientAddress,
        });
      } else {
        json?.message &&
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
      }
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, formik, info.exportInvoice]);

  React.useEffect(() => {
    formik.setValues(info, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [info]);

  React.useEffect(() => {
    if (formik.submitCount > 0 && formik.isSubmitting && !formik.isValid) {
      scrollTo(Object.keys(formik.errors)[0], HEADER_HEIGHT);
    }
  }, [formik.errors, formik.isSubmitting, formik.isValid, formik.submitCount]);

  React.useEffect(() => {
    !formik.touched.invoiceInfo?.customerName &&
      !info.invoiceInfo?.customerName &&
      formik.setFieldValue(
        'invoiceInfo',
        {
          ...formik.values.invoiceInfo,
          customerName: formik.values.contactInfo.fullName,
        },
        !!formik.errors.invoiceInfo?.customerName,
      );
    !formik.touched.invoiceInfo?.customerName &&
      !formik.touched.contactInfo?.fullName &&
      !info.invoiceInfo.customerName &&
      formik.setFieldValue(
        'invoiceInfo',
        {
          ...formik.values.invoiceInfo,
          customerName: formik.values.contactInfo.fullName,
        },
        !!formik.errors.invoiceInfo?.customerName,
      );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formik.values.contactInfo.fullName]);

  return (
    <Container style={{ flex: 1, display: 'flex', marginBottom: 32 }}>
      <form
        autoComplete="off"
        onSubmit={formik.handleSubmit}
        style={{ flex: 1 }}
        onChange={(e: any) => console.log(e.target.value)}
      >
        <Typography id="travellersInfo" variant="h6" style={{ marginBottom: 16 }}>
          <FormattedMessage id="flight.flightBookingInfo" />
          <Typography className="note-text" variant="caption">
            &emsp;
            <FormattedMessage id="flight.inputInfoInstructions" />
          </Typography>
        </Typography>
        <Paper variant="outlined" style={{ overflow: 'hidden' }}>
          <div className="card-background" style={{ padding: '16px 8px' }}>
            <Typography variant="caption">
              &emsp;
              <FormattedMessage id="flight.buyMoreCheckInAd" />
            </Typography>
          </div>
          <div style={{ padding: '12px 16px' }}>
            {formik.values.travellersInfo?.adults.map((obj: TravellerInfo, index: number) => (
              <Col key={index}>
                <Typography variant="subtitle1" style={{ marginBottom: 16 }}>
                  {index + 1}.<FormattedMessage id="flight.adult" values={{ num: index + 1 }} />
                </Typography>
                <Row style={{ flexWrap: 'wrap' }}>
                  <FormControlTextField
                    label={intl.formatMessage({ id: 'flight.fullName' })}
                    placeholder={intl.formatMessage({ id: 'flight.fullNameEx' })}
                    formControlStyle={styleCS}
                    value={obj.fullName}
                    onChange={e => {
                      rawAlphabetRegex.test(voca.latinise(e.target.value)) &&
                        formik.setFieldValue(
                          'travellersInfo',
                          {
                            ...formik.values.travellersInfo,
                            adults: formik.values.travellersInfo?.adults.map((v, i) => {
                              if (i === index) {
                                return {
                                  ...v,
                                  fullName: voca.latinise(e.target.value).toUpperCase(),
                                };
                              }
                              return v;
                            }),
                          },
                          !!(formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.fullName,
                        );
                      index === 0 &&
                        !formik.touched.contactInfo &&
                        !info.contactInfo.fullName &&
                        formik.setFieldValue(
                          'contactInfo',
                          {
                            ...formik.values.contactInfo,
                            fullName: voca.latinise(e.target.value).toUpperCase(),
                          },
                          false,
                        );
                    }}
                    errorMessage={
                      (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)
                        ?.fullName && formik.submitCount
                        ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.fullName
                        : undefined
                    }
                  />
                  {needPassport && (
                    <BirthDayField
                      id="birthday"
                      disableFuture
                      date={obj.birthday ? moment(obj.birthday, DATE_FORMAT_BACK_END) : undefined}
                      update={value => {
                        formik.setFieldValue(
                          'travellersInfo',
                          {
                            ...formik.values.travellersInfo,
                            adults: formik.values.travellersInfo?.adults.map((v, i) => {
                              if (i === index) {
                                return { ...v, birthday: value?.format(DATE_FORMAT_BACK_END) };
                              }
                              return v;
                            }),
                          },
                          !!(formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.birthday,
                        );
                      }}
                      label={
                        <Typography variant="subtitle2" component="span">
                          <FormattedMessage id="birthday" />
                          &nbsp;
                          <Typography variant="caption" component="span">
                            <FormattedMessage id="flight.adultBirthday" />
                          </Typography>
                        </Typography>
                      }
                      inputStyle={styleCS}
                      errorMessage={
                        (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.birthday && formik.submitCount
                          ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.birthday
                          : undefined
                      }
                    />
                  )}
                  <GenderField
                    gender={obj.gender}
                    onChange={e => {
                      formik.setFieldValue(
                        'travellersInfo',
                        {
                          ...formik.values.travellersInfo,
                          adults: formik.values.travellersInfo?.adults.map((v, i) => {
                            if (i === index) {
                              return { ...v, gender: e };
                            }
                            return v;
                          }),
                        },
                        false,
                      );
                      index === 0 &&
                      !formik.touched.contactInfo &&
                      !info.contactInfo.fullName && // gender luc nao cung co giá trị nên lấy tạm của fullname
                        formik.setFieldValue(
                          'contactInfo',
                          {
                            ...formik.values.contactInfo,
                            gender: e,
                          },
                          false,
                        );
                    }}
                  />
                </Row>
                {needPassport && (
                  <Row style={{ flexWrap: 'wrap' }}>
                    <FormControlTextField
                      label={intl.formatMessage({ id: 'flight.passport' })}
                      placeholder={intl.formatMessage({ id: 'flight.passportEx' })}
                      formControlStyle={styleCS}
                      value={obj.passportInfo.passport}
                      onChange={e =>
                        formik.setFieldValue('travellersInfo', {
                          ...formik.values.travellersInfo,
                          adults: formik.values.travellersInfo?.adults.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: { ...v.passportInfo, passport: e.target.value },
                              };
                            }
                            return v;
                          }, (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)?.passportInfo?.passport),
                        })
                      }
                      errorMessage={
                        (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passport && formik.submitCount
                          ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passport
                          : undefined
                      }
                    />
                    <BirthDayField
                      id="passportExpiredDate"
                      disablePast
                      date={
                        obj.passportInfo.passportExpiredDate
                          ? moment(obj.passportInfo.passportExpiredDate, DATE_FORMAT_BACK_END)
                          : undefined
                      }
                      update={value => {
                        formik.setFieldValue('travellersInfo', {
                          ...formik.values.travellersInfo,
                          adults: formik.values.travellersInfo?.adults.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: {
                                  ...v.passportInfo,
                                  passportExpiredDate: value?.format(DATE_FORMAT_BACK_END),
                                },
                              };
                            }
                            return v;
                          }, (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<TravellerInfo>)?.passportInfo?.passportExpiredDate),
                        });
                      }}
                      label={intl.formatMessage({ id: 'flight.passportExpired' })}
                      inputStyle={styleCS}
                      errorMessage={
                        (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passportExpiredDate && formik.submitCount
                          ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportExpiredDate
                          : undefined
                      }
                    />
                    <FormControlAutoComplete
                      id="passportCountry"
                      label={intl.formatMessage({ id: 'flight.passportCountry' })}
                      placeholder={intl.formatMessage({ id: 'flight.selectPassportCountry' })}
                      value={obj.passportInfo.passportCountry}
                      formControlStyle={styleCS}
                      onChange={(e: any, value: some | null) => {
                        formik.setFieldValue(
                          'travellersInfo',
                          {
                            ...formik.values.travellersInfo,
                            adults: formik.values.travellersInfo?.adults.map((v, i) => {
                              if (i === index) {
                                return {
                                  ...v,
                                  passportInfo: { ...v.passportInfo, passportCountry: value },
                                };
                              }
                              return v;
                            }),
                          },
                          !!(formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.passportCountry,
                        );
                      }}
                      getOptionSelected={(option: some, value: some) => {
                        return option.id === value.id;
                      }}
                      getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                      options={listCounties}
                      errorMessage={
                        (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passportCountry && formik.submitCount
                          ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportCountry
                          : undefined
                      }
                    />

                    <FormControlAutoComplete
                      id="nationalityCountry"
                      label={intl.formatMessage({ id: 'flight.nationalityCountry' })}
                      placeholder={intl.formatMessage({ id: 'flight.selectNationalityCountry' })}
                      value={obj.passportInfo.nationalityCountry}
                      formControlStyle={styleCS}
                      onChange={(e: any, value: some | null) => {
                        formik.setFieldValue(
                          'travellersInfo',
                          {
                            ...formik.values.travellersInfo,
                            adults: formik.values.travellersInfo?.adults.map((v, i) => {
                              if (i === index) {
                                return {
                                  ...v,
                                  passportInfo: { ...v.passportInfo, nationalityCountry: value },
                                };
                              }
                              return v;
                            }),
                          },
                          !!(formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.passportInfo?.nationalityCountry,
                        );
                      }}
                      getOptionSelected={(option: some, value: some) => {
                        return option.id === value.id;
                      }}
                      getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                      options={listCounties}
                      errorMessage={
                        (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.nationalityCountry && formik.submitCount
                          ? (formik.errors.travellersInfo?.adults?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.nationalityCountry
                          : undefined
                      }
                    />
                  </Row>
                )}
                <Row style={{ flexWrap: 'wrap' }}>
                  {formik.values.outbound.ticket?.outbound.baggages &&
                  formik.values.outbound.ticket?.outbound.baggages[0] &&
                  formik.values.outbound.extraBaggages ? (
                    <FormControlAutoComplete
                      id="flightBookingInfo.More_packages"
                      label={intl.formatMessage({ id: 'flight.outBaggages' })}
                      value={formik.values.outbound?.extraBaggages?.[index] || null}
                      options={formik.values.outbound.ticket?.outbound.baggages}
                      getOptionSelected={(option: some, value: some) => {
                        return option.id === value.id;
                      }}
                      getOptionLabel={(v: some) =>
                        `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                          id: 'currency',
                        })}`
                      }
                      onChange={(e: any, value?: any) => {
                        formik.setFieldValue('outbound', {
                          ...formik.values.outbound,
                          extraBaggages: formik.values.outbound?.extraBaggages?.map((v, i) => {
                            if (i === index) {
                              return value;
                            }
                            return v;
                          }),
                        });
                      }}
                      formControlStyle={styleCS}
                      optional
                      disableClearable
                      readOnly
                    />
                  ) : (
                    <Col style={{ ...styleCS, marginRight: 16 }}>
                      <Typography variant="body2">
                        <FormattedMessage id="flight.outBaggages" />
                      </Typography>
                      <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                        <Typography variant="body2" color="textSecondary">
                          <FormattedMessage id="flight.noSupportBuyBaggage" />
                        </Typography>
                      </div>
                    </Col>
                  )}
                  {formik.values.inbound.ticket && (
                    <>
                      {formik.values.inbound.ticket?.outbound.baggages &&
                      formik.values.inbound.ticket?.outbound.baggages[0] &&
                      formik.values.inbound.extraBaggages ? (
                        <FormControlAutoComplete
                          label={intl.formatMessage({ id: 'flight.inBaggages' })}
                          value={formik.values.inbound?.extraBaggages?.[index]}
                          options={formik.values.inbound.ticket?.outbound.baggages}
                          getOptionSelected={(option: some, value: some) => {
                            return option.id === value.id;
                          }}
                          getOptionLabel={(v: some) =>
                            `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                              id: 'currency',
                            })}`
                          }
                          onChange={(e: any, value?: any) => {
                            formik.setFieldValue('inbound', {
                              ...formik.values.inbound,
                              extraBaggages: formik.values.inbound?.extraBaggages?.map((v, i) => {
                                if (i === index) {
                                  return value;
                                }
                                return v;
                              }),
                            });
                          }}
                          formControlStyle={styleCS}
                          optional
                          disableClearable
                          readOnly
                        />
                      ) : (
                        <Col style={{ ...styleCS, marginRight: 16 }}>
                          <Typography variant="body2">
                            <FormattedMessage id="flight.inBaggages" />
                          </Typography>
                          <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                            <Typography variant="body2" color="textSecondary">
                              <FormattedMessage id="flight.noSupportBuyBaggage" />
                            </Typography>
                          </div>
                        </Col>
                      )}
                    </>
                  )}
                </Row>
              </Col>
            ))}
            {formik.values.travellersInfo?.children.map((obj: TravellerInfo, index: number) => {
              const indexTmp = index + (formik.values.travellersInfo?.adults.length || 0);
              return (
                <Col key={index} style={{ marginBottom: 12, marginTop: 16 }}>
                  <Typography variant="subtitle1" style={{ marginBottom: 16 }}>
                    {index + 1 + (formik.values?.travellersInfo?.adults?.length || 0)}.
                    <FormattedMessage id="flight.children" values={{ num: index + 1 }} />
                  </Typography>
                  <Row style={{ flexWrap: 'wrap' }}>
                    <FormControlTextField
                      label={intl.formatMessage({ id: 'flight.fullName' })}
                      placeholder={intl.formatMessage({ id: 'flight.fullNameEx' })}
                      formControlStyle={styleCS}
                      value={obj.fullName}
                      onChange={e =>
                        rawAlphabetRegex.test(voca.latinise(e.target.value)) &&
                        formik.setFieldValue(
                          'travellersInfo',
                          {
                            ...formik.values.travellersInfo,
                            children: formik.values.travellersInfo?.children.map((v, i) => {
                              if (i === index) {
                                return {
                                  ...v,
                                  fullName: voca.latinise(e.target.value).toUpperCase(),
                                };
                              }
                              return v;
                            }),
                          },
                          !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.fullName,
                        )
                      }
                      errorMessage={
                        (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.fullName && formik.submitCount
                          ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.fullName
                          : undefined
                      }
                    />
                    <BirthDayField
                      id="birthday"
                      disableFuture
                      date={obj.birthday ? moment(obj.birthday, DATE_FORMAT_BACK_END) : undefined}
                      update={value => {
                        formik.setFieldValue(
                          'travellersInfo',
                          {
                            ...formik.values.travellersInfo,
                            children: formik.values.travellersInfo?.children.map((v, i) => {
                              if (i === index) {
                                return { ...v, birthday: value?.format(DATE_FORMAT_BACK_END) };
                              }
                              return v;
                            }),
                          },
                          !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.birthday,
                        );
                      }}
                      label={
                        <Typography variant="subtitle2" component="span">
                          <FormattedMessage id="birthday" />
                          &nbsp;
                          <Typography variant="caption" component="span">
                            <FormattedMessage id="flight.childBirthday" />
                          </Typography>
                        </Typography>
                      }
                      inputStyle={styleCS}
                      errorMessage={
                        (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.birthday && formik.submitCount
                          ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.birthday
                          : undefined
                      }
                    />
                    <GenderField
                      gender={obj.gender}
                      onChange={e =>
                        formik.setFieldValue(
                          'travellersInfo',
                          {
                            ...formik.values.travellersInfo,
                            children: formik.values.travellersInfo?.children.map((v, i) => {
                              if (i === index) {
                                return { ...v, gender: e };
                              }
                              return v;
                            }),
                          },
                          false,
                        )
                      }
                    />

                    {needPassport && (
                      <Row style={{ flexWrap: 'wrap' }}>
                        <FormControlTextField
                          label={intl.formatMessage({ id: 'flight.passport' })}
                          placeholder={intl.formatMessage({ id: 'flight.passportEx' })}
                          formControlStyle={styleCS}
                          value={obj.passportInfo.passport}
                          onChange={e =>
                            formik.setFieldValue(
                              'travellersInfo',
                              {
                                ...formik.values.travellersInfo,
                                children: formik.values.travellersInfo?.children.map((v, i) => {
                                  if (i === index) {
                                    return {
                                      ...v,
                                      passportInfo: { ...v.passportInfo, passport: e.target.value },
                                    };
                                  }
                                  return v;
                                }),
                              },
                              !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.passport,
                            )
                          }
                          errorMessage={
                            (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passport && formik.submitCount
                              ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                  TravellerInfo
                                >)?.passportInfo?.passport
                              : undefined
                          }
                        />
                        <BirthDayField
                          id="passportExpiredDate"
                          disablePast
                          date={
                            obj.passportInfo.passportExpiredDate
                              ? moment(obj.passportInfo.passportExpiredDate, DATE_FORMAT_BACK_END)
                              : undefined
                          }
                          update={value => {
                            formik.setFieldValue(
                              'travellersInfo',
                              {
                                ...formik.values.travellersInfo,
                                children: formik.values.travellersInfo?.children.map((v, i) => {
                                  if (i === index) {
                                    return {
                                      ...v,
                                      passportInfo: {
                                        ...v.passportInfo,
                                        passportExpiredDate: value?.format(DATE_FORMAT_BACK_END),
                                      },
                                    };
                                  }
                                  return v;
                                }),
                              },
                              !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.passportExpiredDate,
                            );
                          }}
                          label={intl.formatMessage({ id: 'flight.passportExpired' })}
                          inputStyle={styleCS}
                          errorMessage={
                            (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportExpiredDate && formik.submitCount
                              ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                  TravellerInfo
                                >)?.passportInfo?.passportExpiredDate
                              : undefined
                          }
                        />
                        <FormControlAutoComplete
                          id="passportCountry"
                          label={intl.formatMessage({ id: 'flight.passportCountry' })}
                          placeholder={intl.formatMessage({ id: 'flight.selectPassportCountry' })}
                          value={obj.passportInfo.passportCountry}
                          formControlStyle={styleCS}
                          onChange={(e: any, value: some | null) => {
                            formik.setFieldValue(
                              'travellersInfo',
                              {
                                ...formik.values.travellersInfo,
                                children: formik.values.travellersInfo?.children.map((v, i) => {
                                  if (i === index) {
                                    return {
                                      ...v,
                                      passportInfo: { ...v.passportInfo, passportCountry: value },
                                    };
                                  }
                                  return v;
                                }),
                              },
                              !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.passportCountry,
                            );
                          }}
                          getOptionSelected={(option: some, value: some) => {
                            return option.id === value.id;
                          }}
                          getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                          options={listCounties}
                          errorMessage={
                            (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportCountry && formik.submitCount
                              ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                  TravellerInfo
                                >)?.passportInfo?.passportCountry
                              : undefined
                          }
                        />

                        <FormControlAutoComplete
                          id="nationalityCountry"
                          label={intl.formatMessage({ id: 'flight.nationalityCountry' })}
                          placeholder={intl.formatMessage({
                            id: 'flight.selectNationalityCountry',
                          })}
                          value={obj.passportInfo.nationalityCountry}
                          formControlStyle={styleCS}
                          onChange={(e: any, value: some | null) => {
                            formik.setFieldValue(
                              'travellersInfo',
                              {
                                ...formik.values.travellersInfo,
                                children: formik.values.travellersInfo?.children.map((v, i) => {
                                  if (i === index) {
                                    return {
                                      ...v,
                                      passportInfo: {
                                        ...v.passportInfo,
                                        nationalityCountry: value,
                                      },
                                    };
                                  }
                                  return v;
                                }),
                              },
                              !!(formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                TravellerInfo
                              >)?.passportInfo?.nationalityCountry,
                            );
                          }}
                          getOptionSelected={(option: some, value: some) => {
                            return option.id === value.id;
                          }}
                          getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                          options={listCounties}
                          errorMessage={
                            (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.nationalityCountry && formik.submitCount
                              ? (formik.errors.travellersInfo?.children?.[index] as FormikErrors<
                                  TravellerInfo
                                >)?.passportInfo?.nationalityCountry
                              : undefined
                          }
                        />
                      </Row>
                    )}
                  </Row>
                  <Row>
                    {formik.values.outbound.ticket?.outbound.baggages &&
                    formik.values.outbound.ticket?.outbound.baggages[0] &&
                    formik.values.outbound.extraBaggages ? (
                      <FormControlAutoComplete
                        label={intl.formatMessage({ id: 'flight.outBaggages' })}
                        value={formik.values.outbound?.extraBaggages?.[indexTmp] || null}
                        options={formik.values.outbound.ticket?.outbound.baggages}
                        getOptionSelected={(option: some, value: some) => {
                          return option.id === value.id;
                        }}
                        getOptionLabel={(v: some) =>
                          `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                            id: 'currency',
                          })}`
                        }
                        onChange={(e: any, value?: any) => {
                          formik.setFieldValue('outbound', {
                            ...formik.values.outbound,
                            extraBaggages: formik.values.outbound?.extraBaggages?.map((v, i) => {
                              if (i === indexTmp) {
                                return value;
                              }
                              return v;
                            }),
                          });
                        }}
                        formControlStyle={styleCS}
                        optional
                        disableClearable
                        readOnly
                      />
                    ) : (
                      <Col style={{ ...styleCS, marginRight: 16 }}>
                        <Typography variant="body2">
                          <FormattedMessage id="flight.outBaggages" />
                        </Typography>
                        <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                          <Typography variant="body2" color="textSecondary">
                            <FormattedMessage id="flight.noSupportBuyBaggage" />
                          </Typography>
                        </div>
                      </Col>
                    )}
                    {formik.values.inbound.ticket && (
                      <>
                        {formik.values.inbound.ticket?.outbound.baggages &&
                        formik.values.inbound.ticket?.outbound.baggages[0] &&
                        formik.values.inbound.extraBaggages ? (
                          <FormControlAutoComplete
                            label={intl.formatMessage({ id: 'flight.inBaggages' })}
                            value={formik.values.inbound?.extraBaggages?.[indexTmp]}
                            options={formik.values.inbound.ticket?.outbound.baggages}
                            getOptionSelected={(option: some, value: some) => {
                              return option.id === value.id;
                            }}
                            getOptionLabel={(v: some) =>
                              `${v.name} - ${intl.formatNumber(v.price)} ${intl.formatMessage({
                                id: 'currency',
                              })}`
                            }
                            onChange={(e: any, value?: any) => {
                              formik.setFieldValue('inbound', {
                                ...formik.values.inbound,
                                extraBaggages: formik.values.inbound?.extraBaggages?.map((v, i) => {
                                  if (i === indexTmp) {
                                    return value;
                                  }
                                  return v;
                                }),
                              });
                            }}
                            formControlStyle={styleCS}
                            optional
                            disableClearable
                            readOnly
                          />
                        ) : (
                          <Col style={{ ...styleCS, marginRight: 16 }}>
                            <Typography variant="body2">
                              <FormattedMessage id="flight.inBaggages" />
                            </Typography>
                            <div style={{ minHeight: 64, paddingTop: 14, boxSizing: 'border-box' }}>
                              <Typography variant="body2" color="textSecondary">
                                <FormattedMessage id="flight.noSupportBuyBaggage" />
                              </Typography>
                            </div>
                          </Col>
                        )}
                      </>
                    )}
                  </Row>
                </Col>
              );
            })}
            {formik.values.travellersInfo?.babies.map((obj: TravellerInfo, index: number) => (
              <Col key={index} style={{ marginBottom: 12, marginTop: 16 }}>
                <Typography variant="subtitle1" style={{ marginBottom: 16 }}>
                  {index +
                    1 +
                    (formik.values?.travellersInfo?.adults?.length || 0) +
                    (formik.values?.travellersInfo?.children?.length || 0)}
                  .
                  <FormattedMessage id="flight.infant" values={{ num: index + 1 }} />
                </Typography>
                <Row style={{ flexWrap: 'wrap' }}>
                  <FormControlTextField
                    label={intl.formatMessage({ id: 'flight.fullName' })}
                    placeholder={intl.formatMessage({ id: 'flight.fullNameEx' })}
                    formControlStyle={styleCS}
                    value={obj.fullName}
                    onChange={e =>
                      rawAlphabetRegex.test(voca.latinise(e.target.value)) &&
                      formik.setFieldValue('travellersInfo', {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return { ...v, fullName: voca.latinise(e.target.value).toUpperCase() };
                          }
                          return v;
                        }),
                      })
                    }
                    errorMessage={
                      (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                        ?.fullName && formik.submitCount
                        ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.fullName
                        : undefined
                    }
                  />

                  <BirthDayField
                    id="birthday"
                    disableFuture
                    date={obj.birthday ? moment(obj.birthday, DATE_FORMAT_BACK_END) : undefined}
                    update={value => {
                      formik.setFieldValue('travellersInfo', {
                        ...formik.values.travellersInfo,
                        babies: formik.values.travellersInfo?.babies.map((v, i) => {
                          if (i === index) {
                            return { ...v, birthday: value?.format(DATE_FORMAT_BACK_END) };
                          }
                          return v;
                        }),
                      });
                    }}
                    label={
                      <Typography variant="subtitle2" component="span">
                        <FormattedMessage id="birthday" />
                        &nbsp;
                        <Typography variant="caption" component="span">
                          <FormattedMessage id="flight.infantBirthday" />
                        </Typography>
                      </Typography>
                    }
                    inputStyle={styleCS}
                    errorMessage={
                      (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<TravellerInfo>)
                        ?.birthday && formik.submitCount
                        ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                            TravellerInfo
                          >)?.birthday
                        : undefined
                    }
                  />
                  <GenderField
                    gender={obj.gender}
                    onChange={e =>
                      formik.setFieldValue(
                        'travellersInfo',
                        {
                          ...formik.values.travellersInfo,
                          babies: formik.values.travellersInfo?.babies.map((v, i) => {
                            if (i === index) {
                              return { ...v, gender: e };
                            }
                            return v;
                          }),
                        },
                        false,
                      )
                    }
                  />
                </Row>
                {needPassport && (
                  <Row style={{ flexWrap: 'wrap' }}>
                    <FormControlTextField
                      label={intl.formatMessage({ id: 'flight.passport' })}
                      placeholder={intl.formatMessage({ id: 'flight.passportEx' })}
                      formControlStyle={styleCS}
                      value={obj.passportInfo.passport}
                      onChange={e =>
                        formik.setFieldValue('travellersInfo', {
                          ...formik.values.travellersInfo,
                          babies: formik.values.travellersInfo?.babies.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: { ...v.passportInfo, passport: e.target.value },
                              };
                            }
                            return v;
                          }),
                        })
                      }
                      errorMessage={
                        (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passport && formik.submitCount
                          ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passport
                          : undefined
                      }
                    />
                    <BirthDayField
                      id="passportExpiredDate"
                      disablePast
                      date={
                        obj.passportInfo.passportExpiredDate
                          ? moment(obj.passportInfo.passportExpiredDate, DATE_FORMAT_BACK_END)
                          : undefined
                      }
                      update={value => {
                        formik.setFieldValue('travellersInfo', {
                          ...formik.values.travellersInfo,
                          babies: formik.values.travellersInfo?.babies.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: {
                                  ...v.passportInfo,
                                  passportExpiredDate: value?.format(DATE_FORMAT_BACK_END),
                                },
                              };
                            }
                            return v;
                          }),
                        });
                      }}
                      label={intl.formatMessage({ id: 'flight.passportExpired' })}
                      inputStyle={styleCS}
                      errorMessage={
                        (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passportExpiredDate && formik.submitCount
                          ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportExpiredDate
                          : undefined
                      }
                    />
                    <FormControlAutoComplete
                      id="passportCountry"
                      label={intl.formatMessage({ id: 'flight.passportCountry' })}
                      placeholder={intl.formatMessage({ id: 'flight.selectPassportCountry' })}
                      value={obj.passportInfo.passportCountry}
                      formControlStyle={styleCS}
                      onChange={(e: any, value: some | null) => {
                        formik.setFieldValue('travellersInfo', {
                          ...formik.values.travellersInfo,
                          babies: formik.values.travellersInfo?.babies.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: { ...v.passportInfo, passportCountry: value },
                              };
                            }
                            return v;
                          }),
                        });
                      }}
                      getOptionSelected={(option: some, value: some) => {
                        return option.id === value.id;
                      }}
                      getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                      options={listCounties}
                      errorMessage={
                        (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.passportCountry && formik.submitCount
                          ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.passportCountry
                          : undefined
                      }
                    />

                    <FormControlAutoComplete
                      id="nationalityCountry"
                      label={intl.formatMessage({ id: 'flight.nationalityCountry' })}
                      placeholder={intl.formatMessage({
                        id: 'flight.selectNationalityCountry',
                      })}
                      value={obj.passportInfo.nationalityCountry}
                      formControlStyle={styleCS}
                      onChange={(e: any, value: some | null) => {
                        formik.setFieldValue('travellersInfo', {
                          ...formik.values.travellersInfo,
                          babies: formik.values.travellersInfo?.babies.map((v, i) => {
                            if (i === index) {
                              return {
                                ...v,
                                passportInfo: { ...v.passportInfo, nationalityCountry: value },
                              };
                            }
                            return v;
                          }),
                        });
                      }}
                      getOptionSelected={(option: some, value: some) => {
                        return option.id === value.id;
                      }}
                      getOptionLabel={(v: some) => `${v.name} (${v.code})` || ''}
                      options={listCounties}
                      errorMessage={
                        (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                          TravellerInfo
                        >)?.passportInfo?.nationalityCountry && formik.submitCount
                          ? (formik.errors.travellersInfo?.babies?.[index] as FormikErrors<
                              TravellerInfo
                            >)?.passportInfo?.nationalityCountry
                          : undefined
                      }
                    />
                  </Row>
                )}
              </Col>
            ))}
          </div>
        </Paper>
        <Typography id="contactInfo" variant="h6" style={{ margin: '24px 0px 16px' }}>
          <FormattedMessage id="flight.contactInfo" />
          <Typography className="note-text" variant="caption">
            &emsp;
            <FormattedMessage id="flight.contactNote" />
          </Typography>
        </Typography>
        <Paper variant="outlined" style={{ padding: '12px 16px', marginTop: 16 }}>
          <Row style={{ flexWrap: 'wrap' }}>
            <FormControlTextField
              label={intl.formatMessage({ id: 'flight.fullName' })}
              placeholder={intl.formatMessage({ id: 'flight.fullNameEx' })}
              formControlStyle={styleCS}
              value={formik.values.contactInfo.fullName}
              onChange={e => {
                formik.setFieldValue(
                  'contactInfo',
                  {
                    ...formik.values.contactInfo,
                    fullName: e.target.value.toUpperCase(),
                  },
                  !!formik.errors.contactInfo?.fullName,
                );
                !formik.touched.contactInfo?.fullName &&
                  formik.setTouched({
                    ...formik.touched,
                    contactInfo: {
                      ...formik.touched.contactInfo,
                      fullName: true,
                    },
                  });
              }}
              errorMessage={
                formik.errors.contactInfo?.fullName && formik.submitCount
                  ? formik.errors.contactInfo?.fullName
                  : undefined
              }
            />
            <FormControlTextField
              label={intl.formatMessage({ id: 'phoneNumber' })}
              placeholder={intl.formatMessage({ id: 'phoneNumberEx' })}
              formControlStyle={styleCS}
              value={formik.values.contactInfo.telephone}
              onChange={e => {
                validPhoneNumberRegex.test(e.target.value) &&
                  formik.setFieldValue(
                    'contactInfo',
                    {
                      ...formik.values.contactInfo,
                      telephone: e.target.value,
                    },
                    !!formik.errors.contactInfo?.telephone,
                  );
                !formik.touched.contactInfo?.telephone &&
                  formik.setTouched({
                    ...formik.touched,
                    contactInfo: {
                      ...formik.touched.contactInfo,
                      telephone: true,
                    },
                  });
              }}
              errorMessage={
                formik.errors.contactInfo?.telephone && formik.submitCount
                  ? formik.errors.contactInfo?.telephone
                  : undefined
              }
            />
            <GenderField
              gender={formik.values.contactInfo.gender}
              onChange={e => {
                formik.setFieldValue(
                  'contactInfo',
                  {
                    ...formik.values.contactInfo,
                    gender: e,
                  },
                  false,
                );
                !formik.touched.contactInfo?.gender &&
                  formik.setTouched({
                    ...formik.touched,
                    contactInfo: {
                      ...formik.touched.contactInfo,
                      gender: true,
                    },
                  });
              }}
            />
            <FormControlTextField
              placeholder={intl.formatMessage({ id: 'emailEx' })}
              formControlStyle={styleCS}
              value={formik.values.contactInfo.email}
              onChange={e => {
                formik.setFieldValue(
                  'contactInfo',
                  {
                    ...formik.values.contactInfo,
                    email: e.target.value,
                  },
                  !!formik.errors.contactInfo?.email,
                );
                !formik.touched.invoiceInfo &&
                  formik.setFieldValue(
                    'invoiceInfo',
                    {
                      ...formik.values.invoiceInfo,
                      recipientEmail: e.target.value,
                    },
                    false,
                  );

                !formik.touched.contactInfo?.email &&
                  formik.setTouched({
                    ...formik.touched,
                    contactInfo: {
                      ...formik.touched.contactInfo,
                      email: true,
                    },
                  });
              }}
              errorMessage={
                formik.errors.contactInfo?.email && formik.submitCount
                  ? formik.errors.contactInfo?.email
                  : undefined
              }
              label={
                formik.values.buyInsurance ? (
                  <Typography variant="subtitle2" component="span">
                    <FormattedMessage id="email" />
                  </Typography>
                ) : (
                  <Typography variant="subtitle2">
                    <FormattedMessage id="email" />
                    &nbsp;
                    <Typography variant="caption" component="span">
                      (<FormattedMessage id="unRequired" />)
                    </Typography>
                  </Typography>
                )
              }
              optional={!formik.values.buyInsurance}
            />
            {/* <FormControlTextField
              label={
                <Typography variant="subtitle2">
                  <FormattedMessage id="address" />
                  &nbsp;
                  <Typography variant="caption" component="span">
                    (<FormattedMessage id="unRequired" />)
                  </Typography>
                </Typography>
              }
              placeholder={intl.formatMessage({ id: 'enterAddress' })}
              value={formik.values.contactInfo.address}
              formControlStyle={{ flex: 1 }}
              onChange={e => {
                formik.setFieldValue(
                  'contactInfo',
                  {
                    ...formik.values.contactInfo,
                    address: e.target.value,
                  },
                  false,
                );
                !formik.touched.invoiceInfo &&
                  formik.setFieldValue(
                    'invoiceInfo',
                    {
                      ...formik.values.invoiceInfo,
                      recipientAddress: e.target.value,
                    },
                    false,
                  );
                !formik.touched.contactInfo?.address &&
                  formik.setTouched({
                    ...formik.touched,
                    contactInfo: {
                      ...formik.touched.contactInfo,
                      address: true,
                    },
                  });
              }}
              errorMessage={
                formik.errors.contactInfo?.address && formik.submitCount
                  ? formik.errors.contactInfo?.address
                  : undefined
              }
              optional
            /> */}
          </Row>
        </Paper>
        <Typography id="invoiceInfo" variant="h6" style={{ margin: '24px 0px 16px' }}>
          <FormattedMessage id="flight.invoiceInfo" />
        </Typography>
        <Paper variant="outlined" style={{ padding: '12px 16px', marginBottom: 24 }}>
          <Row style={{ padding: '8px 0px' }}>
            <AntSwitch
              checked={formik.values.exportInvoice}
              onChange={(e, value) => {
                e.stopPropagation();
                formik.setFieldValue('exportInvoice', !formik.values.exportInvoice, false);
              }}
            />
            <Typography variant="subtitle2" style={{ marginLeft: 8 }}>
              <FormattedMessage id="flight.invoiceNote" />
            </Typography>
          </Row>
          <Collapse in={formik.values.exportInvoice}>
            <Row style={{ flex: 'wrap', alignItems: 'flex-start', paddingTop: 8 }}>
              <Col style={{ width: 320, marginRight: 42 }}>
                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.taxNumber' })}
                  placeholder={intl.formatMessage({ id: 'flight.taxNumberEx' })}
                  fullWidth
                  value={formik.values.invoiceInfo.taxIdNumber}
                  onChange={e => {
                    noSpecialText.test(e.target.value) &&
                      formik.setFieldValue(
                        'invoiceInfo',
                        {
                          ...formik.values.invoiceInfo,
                          taxIdNumber: voca.latinise(e.target.value),
                        },
                        !!formik.errors.invoiceInfo?.taxIdNumber,
                      );
                    !formik.touched.invoiceInfo?.taxIdNumber &&
                      formik.setTouched({
                        ...formik.touched,
                        invoiceInfo: {
                          ...formik.touched.invoiceInfo,
                          taxIdNumber: true,
                        },
                      });
                  }}
                  errorMessage={
                    formik.errors.invoiceInfo?.taxIdNumber && formik.submitCount
                      ? formik.errors.invoiceInfo?.taxIdNumber
                      : undefined
                  }
                  endAdornment={
                    <InputAdornment position="end" style={{ marginRight: 8 }}>
                      <IconButton
                        size="small"
                        edge="start"
                        tabIndex={-1}
                        onClick={() => {
                          searchByTaxNumber();
                        }}
                      >
                        <SearchIcon />
                      </IconButton>
                    </InputAdornment>
                  }
                />
                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.companyName' })}
                  placeholder={intl.formatMessage({ id: 'flight.companyNameEx' })}
                  fullWidth
                  value={formik.values.invoiceInfo.companyName}
                  onChange={e => {
                    formik.setFieldValue(
                      'invoiceInfo',
                      {
                        ...formik.values.invoiceInfo,
                        companyName: e.target.value,
                      },
                      !!formik.errors.invoiceInfo?.companyName,
                    );
                    !formik.touched.invoiceInfo?.companyName &&
                      formik.setTouched({
                        ...formik.touched,
                        invoiceInfo: {
                          ...formik.touched.invoiceInfo,
                          companyName: true,
                        },
                      });
                  }}
                  errorMessage={
                    formik.errors.invoiceInfo?.companyName && formik.submitCount
                      ? formik.errors.invoiceInfo?.companyName
                      : undefined
                  }
                />
                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.companyAddress' })}
                  placeholder={intl.formatMessage({ id: 'flight.companyAddressEx' })}
                  fullWidth
                  value={formik.values.invoiceInfo.companyAddress}
                  multiline
                  rows={2}
                  onChange={e => {
                    formik.setFieldValue(
                      'invoiceInfo',
                      {
                        ...formik.values.invoiceInfo,
                        companyAddress: e.target.value,
                        recipientAddress:
                          !formik.touched.invoiceInfo?.recipientAddress && !info.exportInvoice
                            ? e.target.value
                            : formik.values.invoiceInfo.recipientAddress,
                      },
                      !!formik.errors.invoiceInfo?.companyAddress,
                    );
                    !formik.touched.invoiceInfo?.companyAddress &&
                      formik.setTouched({
                        ...formik.touched,
                        invoiceInfo: {
                          ...formik.touched.invoiceInfo,
                          companyAddress: true,
                        },
                      });
                  }}
                  errorMessage={
                    formik.errors.invoiceInfo?.companyAddress && formik.submitCount
                      ? formik.errors.invoiceInfo?.companyAddress
                      : undefined
                  }
                />

                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.recipientAddress' })}
                  placeholder={intl.formatMessage({ id: 'flight.recipientAddressEx' })}
                  fullWidth
                  multiline
                  rows={2}
                  value={formik.values.invoiceInfo.recipientAddress}
                  onChange={e => {
                    formik.setFieldValue(
                      'invoiceInfo',
                      {
                        ...formik.values.invoiceInfo,
                        recipientAddress: e.target.value,
                      },
                      !!formik.errors.invoiceInfo?.recipientAddress,
                    );
                    !formik.touched.invoiceInfo?.recipientAddress &&
                      formik.setTouched({
                        ...formik.touched,
                        invoiceInfo: {
                          ...formik.touched.invoiceInfo,
                          recipientAddress: true,
                        },
                      });
                  }}
                  errorMessage={
                    formik.errors.invoiceInfo?.recipientAddress && formik.submitCount
                      ? formik.errors.invoiceInfo?.recipientAddress
                      : undefined
                  }
                />
                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.customerName' })}
                  placeholder={intl.formatMessage({ id: 'flight.customerNameEx' })}
                  fullWidth
                  value={formik.values.invoiceInfo.customerName}
                  onChange={e => {
                    formik.setFieldValue(
                      'invoiceInfo',
                      {
                        ...formik.values.invoiceInfo,
                        customerName: e.target.value,
                      },
                      !!formik.errors.invoiceInfo?.customerName,
                    );
                    !formik.touched.invoiceInfo?.customerName &&
                      formik.setTouched({
                        ...formik.touched,
                        invoiceInfo: {
                          ...formik.touched.invoiceInfo,
                          customerName: true,
                        },
                      });
                  }}
                  errorMessage={
                    formik.errors.invoiceInfo?.customerName && formik.submitCount
                      ? formik.errors.invoiceInfo?.customerName
                      : undefined
                  }
                />
                <FormControlTextField
                  label={intl.formatMessage({ id: 'email' })}
                  placeholder={intl.formatMessage({ id: 'emailEx' })}
                  fullWidth
                  value={formik.values.invoiceInfo.recipientEmail}
                  onChange={e => {
                    formik.setFieldValue(
                      'invoiceInfo',
                      {
                        ...formik.values.invoiceInfo,
                        recipientEmail: e.target.value,
                      },
                      !!formik.errors.invoiceInfo?.recipientEmail,
                    );
                    !formik.touched.invoiceInfo?.recipientEmail &&
                      formik.setTouched({
                        ...formik.touched,
                        invoiceInfo: {
                          ...formik.touched.invoiceInfo,
                          recipientEmail: true,
                        },
                      });
                  }}
                  errorMessage={
                    formik.errors.invoiceInfo?.recipientEmail && formik.submitCount
                      ? formik.errors.invoiceInfo?.recipientEmail
                      : undefined
                  }
                />
                <FormControlTextField
                  label={intl.formatMessage({ id: 'flight.note' })}
                  placeholder={intl.formatMessage({ id: 'flight.note' })}
                  fullWidth
                  value={formik.values.invoiceInfo.note}
                  multiline
                  rows={2}
                  onChange={e => {
                    formik.setFieldValue('invoiceInfo', {
                      ...formik.values.invoiceInfo,
                      note: e.target.value,
                    });
                  }}
                  errorMessage={
                    formik.errors.invoiceInfo?.note && formik.submitCount
                      ? formik.errors.invoiceInfo?.note
                      : undefined
                  }
                  optional
                />
              </Col>
              <Col style={{ flex: 1 }}>
                <ul style={{ margin: 0, padding: '0px 50px' }}>
                  <Typography variant="subtitle2" style={{ marginBottom: 16 }}>
                    <FormattedMessage id="flight.conditionInvoice" />
                  </Typography>
                  <li>
                    <Typography variant="body2" style={{ margin: '24px 0px 16px' }}>
                      <FormattedMessage id="flight.condition1" />
                    </Typography>
                  </li>
                  <li>
                    <Typography variant="body2" style={{ margin: '24px 0px 16px' }}>
                      <FormattedMessage id="flight.condition2" />
                    </Typography>
                  </li>
                  <li>
                    <Typography variant="body2" style={{ margin: '24px 0px 16px' }}>
                      <FormattedMessage id="flight.condition3" />
                    </Typography>
                  </li>
                  <li>
                    <Typography variant="body2" style={{ margin: '24px 0px 16px' }}>
                      <FormattedMessage id="flight.condition4" />
                    </Typography>
                  </li>
                  <li>
                    <Typography variant="body2" style={{ margin: '24px 0px 16px' }}>
                      <FormattedMessage id="flight.condition5" />
                    </Typography>
                  </li>
                </ul>
              </Col>
            </Row>
          </Collapse>
        </Paper>
        <Typography variant="h6" style={{ margin: '24px 0px 16px' }}>
          <FormattedMessage id="flight.travelInsurance" />
        </Typography>
        <Paper variant="outlined" style={{ padding: '12px 16px', marginTop: 16 }}>
          <Row style={{ alignItems: 'flex-start' }}>
            <FormControlLabel
              label={
                <Typography variant="subtitle2">
                  <FormattedMessage id="flight.buyInsurance" />
                </Typography>
              }
              disabled={!formik.values.insurancePackage}
              style={{ paddingBottom: '8px' }}
              labelPlacement="end"
              value={formik.values.buyInsurance}
              onChange={(e, value) =>
                formik.setFieldValue('buyInsurance', !formik.values.buyInsurance)
              }
              control={
                <Checkbox
                  id="flightBookingInfo.buyInsurance"
                  color="secondary"
                  checked={formik.values.buyInsurance}
                />
              }
            />
            <Col style={{ textAlign: 'end', alignItems: 'flex-end', flex: 1 }}>
              {formik.values.insurancePackage && (
                <>
                  <img
                    alt=""
                    src={formik.values.insurancePackage.image}
                    style={{ height: 36, objectFit: 'cover', minWidth: 100 }}
                  />

                  <Typography variant="caption">
                    <FormattedMessage id="flight.providedByInsurance" />
                  </Typography>
                </>
              )}
            </Col>
          </Row>
          <Row>
            {formik.values.insurancePackage && (
              <Typography
                variant="body1"
                className="final-price"
                style={{ flex: 1, textAlign: 'end' }}
              >
                <FormattedMessage
                  id="flight.chubbInsurance.price"
                  values={{
                    price: <FormattedNumber value={formik.values.insurancePackage.price} />,
                  }}
                />
              </Typography>
            )}
          </Row>
          {formik.values.insurancePackage ? (
            <Typography variant="caption">
              <span
                dangerouslySetInnerHTML={{ __html: formik.values.insurancePackage.introduction }}
              />
            </Typography>
          ) : (
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="flight.noSupportBuyInsurance" />
            </Typography>
          )}
        </Paper>
        <Row style={{ justifyContent: 'center', marginTop: 24 }}>
          <Button
            id="flightBookingInfo.back"
            variant="outlined"
            style={{ marginRight: 24, minWidth: 160 }}
            size="large"
            disableElevation
            onClick={() => dispatch(goBackAction())}
          >
            <FormattedMessage id="back" />
          </Button>
          <Button
            id="flightBookingInfo.continue"
            variant="contained"
            color="secondary"
            style={{ minWidth: 160 }}
            size="large"
            type="submit"
            disableElevation
          >
            <FormattedMessage id="continue" />
          </Button>
        </Row>
      </form>
      <AsideBound
        isTablet={isTablet}
        style={{ width: 360, marginLeft: isTablet ? 0 : 24 }}
        direction="right"
      >
        <FlightInfoBox
          hideTravellersBox
          seeDetail={() => setSeeDetail(true)}
          booking={formik.values}
        />
      </AsideBound>
      <FlightTicketDialog
        close={() => setSeeDetail(false)}
        open={seeDetail}
        booking={formik.values}
      />
    </Container>
  );
};

export default FlightBookingInfoForm;
