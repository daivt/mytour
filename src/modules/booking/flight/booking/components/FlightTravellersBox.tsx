import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { TravellersInfo } from '../utils';

interface Props {
  travellers: TravellersInfo;
}
const Line = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const FlightTravellersBox: React.FunctionComponent<Props> = props => {
  const { travellers } = props;
  const { adults, children, babies } = travellers;
  return (
    <div style={{ flex: 1, marginTop: '10px' }}>
      <Typography variant="subtitle1">
        <FormattedMessage id="flight.travellersList" />
      </Typography>

      <div className="card" style={{ flex: 1, marginTop: '10px', padding: "8px 16px" }}>
        {adults.map((v, i) => (
          <Line key={i}>
            <Typography variant="body2">{v.fullName}</Typography>
            <Typography color="textSecondary" variant="body2">
              <FormattedMessage id="flight.adult" values={{ num: '' }} />
            </Typography>
          </Line>
        ))}
        {children.map((v, i) => (
          <Line key={i}>
            <Typography variant="body2">{v.fullName}</Typography>
            <Typography color="textSecondary" variant="body2">
              <FormattedMessage id="flight.children" values={{ num: '' }} />
            </Typography>
          </Line>
        ))}
        {babies.map((v, i) => (
          <Line key={i}>
            <Typography variant="body2">{v.fullName}</Typography>
            <Typography color="textSecondary" variant="body2">
              <FormattedMessage id="flight.infant" values={{ num: '' }} />
            </Typography>
          </Line>
        ))}
      </div>
    </div>
  );
};

export default FlightTravellersBox;
