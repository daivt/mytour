import { Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { GREY } from '../../../../../configs/colors';
import { ReactComponent as IconCoin } from '../../../../../svg/booking/ic_coin.svg';
import { PAYMENT_HOLDING_CODE, PAYMENT_TRIPI_CREDIT_CODE } from '../../../constants';
import { PointPayment } from '../../../utils';
import { FlightInfo } from '../../booking/utils';
import { FlightPayParams } from '../../payment/utils';
import { computePoints } from '../../result/utils';
import { computeFlightPayableNumbers } from '../../utils';

const Line = styled.div`
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const CountSpan = styled.span`
  color: ${GREY};
`;
interface Props {
  booking: FlightInfo;
  params?: FlightPayParams;
  pointPaymentData?: PointPayment;
}

const FlightPriceBox: React.FunctionComponent<Props> = props => {
  const { booking, params, pointPaymentData } = props;
  const payableNumbers = computeFlightPayableNumbers(booking, params, pointPaymentData);

  const {
    adult,
    children,
    infant,
    vatPrice,
    extraBaggagesCosts,
    finalPrice,
    paymentFee,
    discountAmount,
    pointToAmount,
  } = payableNumbers;

  const totalPayable = finalPrice - pointToAmount + paymentFee;

  const getArrayDetail = React.useMemo(() => {
    const array = [];

    if (booking.insurancePackage && booking.buyInsurance) {
      array.push(
        <Line key={1}>
          <FormattedMessage id="flight.travelInsurance" />
          <div className="price">
            <CountSpan>
              {adult.number + children.number + infant.number}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={booking.insurancePackage.price} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>,
      );
    }
    if (extraBaggagesCosts > 0) {
      array.push(
        <Line key={2}>
          <FormattedMessage id="flight.buyMoreCheckIn" />
          <div className="price">
            <FormattedNumber value={extraBaggagesCosts} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>,
      );
    }
    if (
      params?.usingPoint &&
      params?.selectedPaymentMethod &&
      params?.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
      !!params?.point
    ) {
      array.push(
        <Line key={3}>
          <FormattedMessage id="pointPayment" />
          <div className="price">
            <FormattedNumber value={0 - payableNumbers.pointToAmount} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>,
      );
    }
    if (params?.promotion && !!discountAmount) {
      array.push(
        <Line key={4}>
          {params?.selectedPaymentMethod &&
          params?.selectedPaymentMethod?.code === PAYMENT_HOLDING_CODE ? (
            <FormattedMessage id="pay.discountCodeNotAcceptPaymentMethodHolding" />
          ) : (
            <>
              {params?.promotion.promotionDescription}
              <div className="price">
                <FormattedNumber value={0 - discountAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </>
          )}
        </Line>,
      );
    }
    if (paymentFee) {
      array.push(
        <Line key={5}>
          {paymentFee > 0 ? (
            <FormattedMessage id="flight.paymentFixedFeeSide" />
          ) : (
            <FormattedMessage
              id="flight.discountPaymentMethod"
              values={{
                methodName: params?.selectedPaymentMethod?.name || '',
              }}
            />
          )}
          <div className={paymentFee > 0 ? 'price' : 'fee'}>
            <FormattedNumber value={paymentFee} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>,
      );
    }
    return array;
  }, [
    adult.number,
    infant.number,
    booking.buyInsurance,
    booking.insurancePackage,
    children.number,
    discountAmount,
    extraBaggagesCosts,
    params,
    payableNumbers.pointToAmount,
    paymentFee,
  ]);
  return (
    <div style={{ flex: 1, padding: '12px 16px 16px' }}>
      <Typography variant="subtitle1">
        <FormattedMessage id="flight.priceDetails" />
      </Typography>
      <div className="card" style={{ flex: 1, marginTop: '8px' }}>
        <div style={{ padding: '8px 16px' }}>
          {adult.number > 0 && (
            <Line>
              <FormattedMessage id="flight.adultPrice" />
              <div className="price">
                <CountSpan>
                  {adult.number}
                  &nbsp;x&nbsp;
                </CountSpan>
                <FormattedNumber value={adult.unitPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </Line>
          )}
          {children.number > 0 && (
            <Line>
              <FormattedMessage id="flight.childPrice" />
              <div className="price">
                <CountSpan>
                  {children.number}
                  &nbsp;x&nbsp;
                </CountSpan>
                <FormattedNumber value={children.unitPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </Line>
          )}
          {infant.number > 0 && (
            <Line>
              <FormattedMessage id="flight.infantPrice" />
              <div className="price">
                <CountSpan>
                  {infant.number}
                  &nbsp;x&nbsp;
                </CountSpan>
                <FormattedNumber value={infant.unitPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </Line>
          )}
          {vatPrice > 0 && (
            <Line>
              <FormattedMessage id="flight.vatFee" />
              <div className="price">
                <FormattedNumber value={vatPrice} />
                &nbsp;
                <FormattedMessage id="currency" />
              </div>
            </Line>
          )}
        </div>

        {getArrayDetail.length > 0 && (
          <>
            <div className="card-divider" />
            <div style={{ padding: '8px 16px' }}>{getArrayDetail}</div>
          </>
        )}
        <div className="card-background" style={{ padding: 16 }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Typography variant="subtitle1">
              <FormattedMessage id="flight.totalPayable" />
            </Typography>
            <Typography variant="subtitle1" className="final-price">
              <FormattedNumber value={totalPayable > 0 ? totalPayable : 0} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </div>

          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              color: `${GREY}`,
            }}
          >
            <Line>
              <Typography variant="caption">
                <FormattedMessage id="flight.includeTaxesAndFees" />
              </Typography>
            </Line>
          </div>
          {computePoints(booking) > 0 && (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
              <IconCoin style={{ marginRight: '10px' }} />
              <Typography variant="body2" className="point">
                <FormattedNumber value={computePoints(booking)} />
                &nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default FlightPriceBox;
