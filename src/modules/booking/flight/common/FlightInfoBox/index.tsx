import { Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../../configs/colors';
import { Row } from '../../../../common/components/elements';
import { PointPayment } from '../../../utils';
import FlightTravellersBox from '../../booking/components/FlightTravellersBox';
import { FlightInfo } from '../../booking/utils';
import { FlightPayParams } from '../../payment/utils';
import FlightItineraryBox from './FlightItineraryBox';
import FlightPriceBox from './FlightPriceBox';

interface Props {
  seeDetail(): void;
  styles?: React.CSSProperties;
  booking: FlightInfo;
  params?: FlightPayParams;
  pointPaymentData?: PointPayment;
  hideTravellersBox?: boolean;
}

const FlightInfoBox: React.FunctionComponent<Props> = props => {
  const { booking, params, pointPaymentData, styles, seeDetail, hideTravellersBox } = props;
  return (
    <>
      <Row style={{ marginBottom: 16 }}>
        <Typography variant="h6" style={{ flex: 1 }}>
          <FormattedMessage id="flight.result.flightInfo" />
        </Typography>
        <Typography
          variant="caption"
          style={{ color: BLUE, cursor: 'pointer' }}
          onClick={seeDetail}
        >
          <FormattedMessage id="flight.seeDetails" />
        </Typography>
      </Row>
      <Paper
        variant="outlined"
        style={{
          ...styles,
          marginBottom: '20px',
          alignSelf: 'flex-start',
        }}
      >
        <div style={{ padding: '16px 16px 0px' }}>
          <FlightItineraryBox booking={booking} />
          {!hideTravellersBox && booking.travellersInfo && (
            <FlightTravellersBox travellers={booking.travellersInfo} />
          )}
        </div>
        <FlightPriceBox booking={booking} params={params} pointPaymentData={pointPaymentData} />
      </Paper>
    </>
  );
};

export default FlightInfoBox;
