import { Button, Container, IconButton, InputAdornment, Typography } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import * as yup from 'yup';
import { API_PATHS } from '../../../../configs/API';
import { DATE_FORMAT_BACK_END } from '../../../../models/moment';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconAirport } from '../../../../svg/booking/ic_airport.svg';
import { ReactComponent as IconFlightDown } from '../../../../svg/booking/ic_flight_down.svg';
import { ReactComponent as IconFlightUp } from '../../../../svg/booking/ic_flight_up.svg';
import DateRangeFormControl from '../../../common/components/DateRangeFormControl';
import { AntSwitch, Row } from '../../../common/components/elements';
import FormControlAutoComplete from '../../../common/components/FormControlAutoComplete';
import { fetchThunk } from '../../../common/redux/thunk';
import { defaultAirports } from '../constants';
import { Airport, FlightSearchParams, SeatClass } from '../utils';
import FlightFindRecentBox from './FlightFindRecentBox';
import TravellerCountInfoBox from './TravellerCountInfoBox';
import { addFlightSearchToHistory } from './utils';

export function renderOption(option: Airport) {
  return (
    <div style={{ padding: '5px', display: 'flex', alignItems: 'center' }}>
      <IconAirport style={{ margin: '0 15px 0 5px' }} />
      <div>
        <Typography variant="body2">
          {option.location}
          &nbsp;&#40;
          {option.code}
          &#41;
        </Typography>
        <Typography variant="caption" color="textSecondary">
          {option.name}
        </Typography>
      </div>
    </div>
  );
}

export function getLabel(option: Airport) {
  return `${option.location} (${option.code})`;
}

interface Props {
  params: FlightSearchParams;
  onSearch(params: FlightSearchParams): void;
  hiddenRecent?: boolean;
  isTablet?: boolean;
}

const FlightSearch: React.FC<Props> = props => {
  const { params, onSearch, hiddenRecent, isTablet } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight, shallowEqual);
  const intl = useIntl();
  const origin = React.createRef<HTMLDivElement>();
  const dest = React.createRef<HTMLDivElement>();
  const date = React.createRef<HTMLDivElement>();
  const guest = React.createRef<HTMLDivElement>();

  const flightSearchSchema = yup.object().shape({
    one_way: yup
      .boolean()
      .nullable()
      .required(),
    origin: yup
      .object()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
    destination: yup
      .object()
      .nullable()
      .when('origin', (ori: any, schema: any) => {
        return schema.test({
          name: 'sameLocation',
          test: (des: any) => (ori && des ? des?.code !== ori?.code : true),
          message: intl.formatMessage({ id: 'flight.sameLocation' }),
        });
      })
      .required(intl.formatMessage({ id: 'required' })),
    departureDate: yup
      .string()
      .trim()
      .required(intl.formatMessage({ id: 'required' })),
    returnDate: yup.string().when('one_way', {
      is: true,
      then: yup
        .string()
        .trim()
        .notRequired(),
      otherwise: yup
        .string()
        .trim()
        .required(intl.formatMessage({ id: 'required' })),
    }),
    seatClass: yup
      .mixed()
      .nullable()
      .required(intl.formatMessage({ id: 'required' })),
  });

  const formik = useFormik({
    initialValues: params,
    onSubmit: values => {
      addFlightSearchToHistory(values);
      onSearch(values);
    },
    validationSchema: flightSearchSchema,
  });

  React.useEffect(() => {
    formik.setValues(params, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  React.useEffect(() => {
    if (formik.submitCount > 0 && formik.isSubmitting && !formik.isValid) {
      if (formik.errors.origin) {
        origin.current?.focus();
        return;
      }
      if (formik.errors.destination) {
        dest.current?.focus();
        return;
      }
      if (formik.errors.departureDate || formik.errors.returnDate) {
        date.current?.focus();
      }
    }
  }, [date, dest, formik, origin]);

  return (
    <div className="flight-search" style={{}}>
      <Container
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          padding: isTablet ? '16px 8px' : '32px 24px',
          color: 'white',
        }}
      >
        <form autoComplete="off" onSubmit={formik.handleSubmit}>
          <Row>
            <FormControlAutoComplete<Airport, undefined, undefined, undefined>
              iRef={origin}
              label={
                <Typography variant="body2">
                  <FormattedMessage id="flight.chooseOrigin" />
                </Typography>
              }
              labelStyle={{ color: 'inherit' }}
              optional
              placeholder={intl.formatMessage({ id: 'flight.chooseOrigin' })}
              value={formik.values.origin as any}
              formControlStyle={{ width: 236, marginRight: 0 }}
              inputStyle={{ minHeight: 48 }}
              onChange={(e: any, value: Airport | null) => {
                formik.setFieldValue('origin', value || null, true);
                if (dest.current && !formik.touched.returnDate && value) {
                  dest.current?.focus();
                }
              }}
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }}
              filterOptions={(options, state) => options}
              getOptionSelected={(option: Airport, value: Airport) => {
                return option.code === value.code;
              }}
              getOptionLabel={(v: Airport) => `${v.location} (${v.code})` || ''}
              options={defaultAirports}
              renderOption={renderOption}
              startAdornment={
                <InputAdornment position="start" style={{ marginLeft: 8 }}>
                  <IconButton size="small" edge="start" tabIndex={-1}>
                    <IconFlightUp />
                  </IconButton>
                </InputAdornment>
              }
              errorMessage={
                formik.errors.origin && formik.submitCount > 0 ? formik.errors.origin : undefined
              }
            />
            <div
              style={{
                padding: '8px 0',
                width: isTablet ? '16px' : '32px',
                overflow: 'visible',
                position: 'relative',
                zIndex: 1000,
                display: 'flex',
                justifyContent: 'center',
                height: '48px',
                boxSizing: 'content-box',
                alignItems: 'center',
              }}
            >
              <Button
                tabIndex={-1}
                variant="contained"
                size="small"
                style={{
                  height: '40px',
                  width: '40px',
                  borderRadius: '50%',
                }}
                color="secondary"
                onClick={async () => {
                  formik.setValues(
                    {
                      ...formik.values,
                      origin: formik.values.destination,
                      destination: formik.values.origin,
                    },
                    true,
                  );
                }}
              >
                <SwapHorizIcon style={{ color: 'white' }} />
              </Button>
            </div>
            <FormControlAutoComplete<Airport, undefined, undefined, undefined>
              iRef={dest}
              openOnFocus
              label={
                <Typography variant="body2">
                  <FormattedMessage id="flight.chooseDestination" />
                </Typography>
              }
              placeholder={intl.formatMessage({ id: 'flight.chooseDestination' })}
              labelStyle={{ color: 'inherit' }}
              inputStyle={{ minHeight: 48 }}
              formControlStyle={{ width: 236 }}
              optional
              value={formik.values.destination}
              onChange={(e: any, value: Airport | null) => {
                formik.setFieldValue('destination', value || null, true);
                if (date.current && !formik.touched.departureDate && value) {
                  date.current?.focus();
                }
              }}
              loadOptions={async (str: string) => {
                const json = await dispatch(fetchThunk(API_PATHS.searchAirports(str)));
                return json.data;
              }}
              filterOptions={(options, state) => options}
              getOptionSelected={(option: Airport, value: Airport) => {
                return option?.code === value?.code;
              }}
              getOptionLabel={(v: Airport) => `${v.location} (${v.code})` || ''}
              options={defaultAirports}
              renderOption={renderOption}
              startAdornment={
                <InputAdornment position="start" style={{ marginLeft: 8 }}>
                  <IconButton size="small" edge="start" tabIndex={-1}>
                    <IconFlightDown />
                  </IconButton>
                </InputAdornment>
              }
              errorMessage={
                formik.errors.destination && formik.submitCount > 0
                  ? formik.errors.destination
                  : undefined
              }
            />
            <DateRangeFormControl
              iRef={date}
              idFocus={['antSwitch']}
              label={
                <Row>
                  <Typography variant="body2" style={{ flex: 1 }}>
                    <FormattedMessage id="flight.time" />
                  </Typography>
                  <Row>
                    <Typography variant="caption" style={{ marginRight: 8 }}>
                      <FormattedMessage id="flight.twoWay" />
                    </Typography>
                    <AntSwitch
                      id="antSwitch"
                      checked={!formik.values.one_way}
                      onChange={e => {
                        e.stopPropagation();
                        formik.setValues(
                          {
                            ...formik.values,
                            one_way: !e.target.checked,
                            returnDate: e.target.checked
                              ? moment(formik.values.departureDate, DATE_FORMAT_BACK_END)
                                  .clone()
                                  .add(3, 'days')
                                  .format(DATE_FORMAT_BACK_END)
                              : undefined,
                          },
                          true,
                        );
                      }}
                    />
                  </Row>
                </Row>
              }
              placeholder={intl.formatMessage({ id: 'flight.chooseTime' })}
              style={{ width: 250, marginRight: 16 }}
              inputStyle={{ minHeight: 48 }}
              labelStyle={{ color: 'inherit' }}
              optional
              startDate={
                formik.values.departureDate
                  ? moment(formik.values.departureDate, DATE_FORMAT_BACK_END)
                  : undefined
              }
              endDate={
                formik.values.returnDate
                  ? moment(formik.values.returnDate, DATE_FORMAT_BACK_END)
                  : undefined
              }
              onChange={(start?: Moment, end?: Moment) => {
                formik.setFieldValue(
                  'departureDate',
                  start ? start.format(DATE_FORMAT_BACK_END) : undefined,
                );
                !formik.values.one_way &&
                  formik.setFieldValue(
                    'returnDate',
                    end ? end.format(DATE_FORMAT_BACK_END) : undefined,
                  );
              }}
              onClickBtn={() => {
                // if (
                //   guest.current &&
                //   (!formik.touched.travellerCountInfo || !formik.touched.seatClass)
                // ) {
                //   guest.current?.focus();
                // }
              }}
              numberOfMonths={isTablet ? 1 : 2}
              singleValue={formik.values.one_way}
              errorMessage={
                formik.submitCount > 0
                  ? formik.errors.departureDate || formik.errors.returnDate
                  : undefined
              }
              startAdornment
              isOutsideRange={(e: any) =>
                moment()
                  .startOf('day')
                  .isAfter(e) ||
                moment()
                  .add(1, 'years')
                  .endOf('day')
                  .isBefore(e)
              }
            />

            <TravellerCountInfoBox
              iRef={guest}
              labelStyle={{ color: 'inherit' }}
              label={
                <Typography variant="body2">
                  <FormattedMessage id="flight.travellerInfo" />
                </Typography>
              }
              onChange={travellerInfo => formik.setFieldValue('travellerCountInfo', travellerInfo)}
              onSeatChange={seatClass => formik.setFieldValue('seatClass', seatClass)}
              travellerInfo={formik.values.travellerCountInfo}
              seatClasses={formik.values.seatClass}
              style={{ width: 240 }}
              inputStyle={{ minHeight: 48 }}
              ticketClass={generalFlight.ticketclass as SeatClass[]}
              direction={isTablet ? 'left' : 'right'}
            />
            <Button
              // loading={loading}
              id="Search_flight.search"
              variant="contained"
              color="secondary"
              size="large"
              style={{ marginTop: 4, minWidth: 48, flex: 1 }}
              type="submit"
              disableElevation
            >
              <SearchIcon />
              {!isTablet && <FormattedMessage id="search" />}
            </Button>
          </Row>
        </form>
        {!hiddenRecent && <FlightFindRecentBox key={JSON.stringify(params)} />}
      </Container>
    </div>
  );
};

export default FlightSearch;
