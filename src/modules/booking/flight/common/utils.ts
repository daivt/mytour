import { remove, uniq } from 'lodash';
import {
  FlightSearchParams,
  stringifyFlightSearchAndFilterParams,
  parseFlightSearchParams,
} from '../utils';
import { FLIGHT_SEARCH_HISTORY, MAX_SEARCH_HISTORY } from '../../constants';

function removeInValidFlightSearch(flightSearch: string[]): string[] {
  remove(flightSearch, search => {
    try {
      parseFlightSearchParams(new URLSearchParams(search));
      return false;
    } catch (_) {
      return true;
    }
  });

  return flightSearch;
}

export function addFlightSearchToHistory(search: FlightSearchParams) {
  const flightSearchStr = localStorage.getItem(FLIGHT_SEARCH_HISTORY) || false;

  const searchString = stringifyFlightSearchAndFilterParams(search);

  if (!flightSearchStr) {
    localStorage.setItem(FLIGHT_SEARCH_HISTORY, JSON.stringify([searchString]));
    return;
  }

  let flightSearch: string[] = removeInValidFlightSearch(JSON.parse(flightSearchStr));
  flightSearch.unshift(searchString);

  flightSearch = [...uniq(flightSearch)];

  if (flightSearch.length > MAX_SEARCH_HISTORY) {
    flightSearch.pop();
  }

  localStorage.setItem(FLIGHT_SEARCH_HISTORY, JSON.stringify(flightSearch));
}

export function getFlightSearchHistory(): string[] {
  const flightSearchStr = localStorage.getItem(FLIGHT_SEARCH_HISTORY) || false;

  if (!flightSearchStr) {
    return [];
  }

  const flightSearch: string[] = removeInValidFlightSearch(JSON.parse(flightSearchStr));

  return flightSearch;
}
export function clearFlightSearchHistory() {
  localStorage.removeItem(FLIGHT_SEARCH_HISTORY);
}

