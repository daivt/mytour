import {
  Dialog,
  DialogActions,
  DialogContent,
  Divider,
  IconButton,
  Tab,
  Tabs,
  Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import SwipeableViews from 'react-swipeable-views';
import styled from 'styled-components';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { PointPayment } from '../../../utils';
import { FlightInfo } from '../../booking/utils';
import { FlightPayParams } from '../../payment/utils';
import FlightLiteraryDetail from './FlightLiteraryDetail';
import FlightPriceDetail from './FlightPriceDetail';

const ID = 'swipeableView';

const Content = styled.div`
  transition: opacity 300ms;
  overflow-x: visible;
`;

interface Props {
  close(): void;
  open: boolean;
  button?: React.ReactNode;
  booking: FlightInfo;
  params?: FlightPayParams;
  pointPaymentData?: PointPayment;
}

const FlightTicketDialog: React.FC<Props> = props => {
  const { booking, close, open, button, params, pointPaymentData } = props;
  const [tabValue, setTabValue] = React.useState(1);
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight);

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );

  const scrollTop = React.useCallback(() => {
    // scrollTo(ID, 0);
  }, []);

  return open ? ( // do this because of bug in Material UI dialog rendering dialog even when open is false
    <Dialog open fullWidth maxWidth="md">
      <div style={{ position: 'relative' }}>
        <div
          style={{
            right: '3px',
            top: '3px',
            position: 'absolute',
            zIndex: 1000,
          }}
        >
          <IconButton size="small" onClick={close}>
            <CloseIcon style={{ fontSize: '24px' }} />
          </IconButton>
        </div>
      </div>
      <DialogContent
        style={{
          padding: '0 34px',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <Tabs
          variant="fullWidth"
          value={tabValue}
          centered
          onChange={(e, val) => {
            scrollTop();
            setTabValue(val);
          }}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab
            fullWidth
            label={
              <Typography variant="h6">
                <FormattedMessage id="flight.flightDetails" />
              </Typography>
            }
          />
          <Tab
            fullWidth
            label={
              <Typography variant="h6">
                <FormattedMessage id="flight.priceDetails" />
              </Typography>
            }
          />
        </Tabs>
        <Divider />
        <div
          style={{
            maxHeight: 'calc(100vh - 250px)',
            display: 'flex',
            justifyContent: 'center',
            flex: 1,
          }}
        >
          <SwipeableViews
            id={ID}
            index={tabValue}
            onSwitching={() => {
              scrollTop();
            }}
          >
            <Content>
              <FlightLiteraryDetail booking={booking} getFlightGeneralInfo={getFlightGeneralInfo} />
            </Content>
            <Content>
              <FlightPriceDetail
                booking={booking}
                pointPaymentData={pointPaymentData}
                params={params}
              />
            </Content>
          </SwipeableViews>
        </div>
      </DialogContent>
      {button ? (
        <DialogActions
          style={{
            margin: '0 34px 20px 34px',
            padding: '15px 0 0 0',
            justifyContent: 'center',
          }}
        >
          {button}
        </DialogActions>
      ) : (
        <br />
      )}
    </Dialog>
  ) : (
    <></>
  );
};

export default FlightTicketDialog;
