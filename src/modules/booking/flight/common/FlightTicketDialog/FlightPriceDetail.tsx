import { Divider, Typography } from '@material-ui/core';
import { sum } from 'lodash';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import styled from 'styled-components';
import { GREY, GREY_300 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { ReactComponent as IconCoin } from '../../../../../svg/booking/ic_coin.svg';
import { Col } from '../../../../common/components/elements';
import { PointPayment } from '../../../utils';
import { FlightInfo } from '../../booking/utils';
import { FlightPayParams } from '../../payment/utils';
import { computePoints } from '../../result/utils';
import { computeFlightPayableNumbers } from '../../utils';

const Line = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const CountSpan = styled.span`
  color: ${GREY};
`;

const Item = (props: {
  data: some;
  titleId: string;
  searchRequest: some;
  extraBaggages: some[];
  style?: React.CSSProperties;
}) => {
  const { data, searchRequest, extraBaggages, titleId, style } = props;
  const { numAdults, numChildren, numInfants } = searchRequest;
  const totalPriceExtraBaggages =
    data.baggages && data.baggages.length ? sum(extraBaggages.map(val => val.price)) : 0;
  const { farePrice, grandTotal } = data.ticketdetail;
  const adultPrice = farePrice;
  const childPrice = farePrice;
  const infantPrice = 0;
  const vatPrice =
    grandTotal - numAdults * adultPrice - numChildren * childPrice - numInfants * infantPrice;
  const finalPrice = grandTotal + totalPriceExtraBaggages;
  return (
    <div style={{ flex: 1, marginTop: '10px', ...style }}>
      <div style={{ height: '40px', display: 'flex', alignItems: 'center' }}>
        <Typography variant="subtitle1">
          <FormattedMessage id={titleId} />
        </Typography>
      </div>
      {numAdults > 0 && (
        <Line>
          <FormattedMessage id="flight.adultPrice" />
          <div className="price">
            <CountSpan>
              {numAdults}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={adultPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>
      )}
      {numChildren > 0 && (
        <Line>
          <FormattedMessage id="flight.childPrice" />
          <div className="price">
            <CountSpan>
              {numChildren}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={childPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>
      )}
      {numInfants > 0 && (
        <Line>
          <FormattedMessage id="flight.infantPrice" />
          <div className="price">
            <CountSpan>
              {numInfants}
              &nbsp;x&nbsp;
            </CountSpan>
            <FormattedNumber value={infantPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>
      )}
      {vatPrice > 0 && (
        <Line>
          <FormattedMessage id="flight.vatFee" />
          <div className="price">
            <FormattedNumber value={vatPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>
      )}
      {totalPriceExtraBaggages > 0 && (
        <Line>
          <FormattedMessage id="flight.buyMoreCheckIn" />
          <div>
            <FormattedNumber value={totalPriceExtraBaggages} />
            &nbsp;
            <FormattedMessage id="currency" />
          </div>
        </Line>
      )}
      <Line>
        <Typography variant="body2">
          <FormattedMessage
            id="flight.total"
            values={{
              text: (
                <span style={{ textTransform: 'lowercase' }}>
                  <FormattedMessage id={titleId} />
                </span>
              ),
            }}
          />
        </Typography>
        <Typography className="price" variant="subtitle2">
          <FormattedNumber value={finalPrice} />
          &nbsp;
          <FormattedMessage id="currency" />
        </Typography>
      </Line>
    </div>
  );
};

interface IFlightPriceDetailProps {
  booking: FlightInfo;
  params?: FlightPayParams;
  pointPaymentData?: PointPayment;
}

const FlightPriceDetail = (props: IFlightPriceDetailProps) => {
  const { booking, params, pointPaymentData } = props;
  const payableNumbers = computeFlightPayableNumbers(booking, params, pointPaymentData);
  const { adult, infant, children } = payableNumbers;
  const totalPayable = payableNumbers.originPrice;

  return (
    <div style={{ margin: '10px 58px' }}>
      <div style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'space-between' }}>
        {booking.outbound.ticket && (
          <Item
            data={booking.outbound.ticket.outbound}
            searchRequest={booking.outbound.searchRequest}
            extraBaggages={booking.outbound.extraBaggages}
            titleId="flight.outboundPrice"
          />
        )}
        {booking.inbound.ticket && (
          <Item
            style={{ marginLeft: 24, paddingLeft: 24, borderLeft: `1px solid ${GREY_300}` }}
            data={booking.inbound.ticket.outbound}
            searchRequest={booking.inbound.searchRequest}
            extraBaggages={booking.inbound.extraBaggages}
            titleId="flight.inboundPrice"
          />
        )}
      </div>
      <Divider />
      <Col style={{ marginTop: '12px' }}>
        {booking.insurancePackage && booking.buyInsurance && (
          <Line>
            <FormattedMessage id="flight.travelInsurance" />
            <div>
              <CountSpan>
                {adult.number + children.number + infant.number}
                &nbsp;x&nbsp;
              </CountSpan>
              <FormattedNumber value={booking.insurancePackage.price} />
              &nbsp;
              <FormattedMessage id="currency" />
            </div>
          </Line>
        )}

        <Line style={{ justifyContent: 'space-between' }}>
          <Typography variant="subtitle1">
            <FormattedMessage id="flight.totalPayable" />
          </Typography>
          <Typography variant="subtitle1" className="final-price">
            <FormattedNumber value={totalPayable > 0 ? totalPayable : 0} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Line>

        <Line style={{ justifyContent: 'flex-end' }}>
          <Typography variant="caption">
            <FormattedMessage id="flight.includeTaxesAndFees" />
          </Typography>
          {computePoints(booking) > 0 && (
            <div
              style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', flex: 1 }}
            >
              <IconCoin style={{ marginRight: '10px' }} />
              <Typography variant="body2" className="point">
                <FormattedNumber value={computePoints(booking)} />
                &nbsp;
                <FormattedMessage id="point" />
              </Typography>
            </div>
          )}
        </Line>
      </Col>
    </div>
  );
};

export default FlightPriceDetail;
