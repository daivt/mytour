import { Divider } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../../constants';
import { FlightInfo } from '../../booking/utils';
import { FlightDirectionDetail } from '../../result/components/FlightTicketDetail';

export interface Props {
  booking: FlightInfo;
  getFlightGeneralInfo(id: number): some;
}

const FlightLiteraryDetail = (props: Props) => {
  const { booking, getFlightGeneralInfo } = props;
  return (
    <div style={{ margin: '10px 0' }}>
      {booking.outbound.ticket && (
        <FlightDirectionDetail
          ticket={booking.outbound.ticket}
          getFlightGeneralInfo={getFlightGeneralInfo}
        />
      )}
      {booking.inbound.ticket && (
        <>
          <Divider style={{ margin: '10px 0' }} />
          <FlightDirectionDetail
            ticket={booking.inbound.ticket}
            getFlightGeneralInfo={getFlightGeneralInfo}
          />
        </>
      )}
    </div>
  );
};

export default FlightLiteraryDetail;
