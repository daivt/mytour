import { ButtonBase, Tooltip } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { WHITE } from '../../../../configs/colors';
import { ROUTES } from '../../../../configs/routes';
import { DATE_FORMAT_BACK_END } from '../../../../models/moment';
import { Row } from '../../../common/components/elements';
import Link from '../../../common/components/Link';
import { parseFlightSearchParams } from '../utils';
import { getInputStr } from './TravellerCountInfoBox';
import { getFlightSearchHistory } from './utils';

interface PropCustom {
  search: string;
  index: number;
}

const CustomSlide: React.FC<PropCustom> = props => {
  const { index, search } = props;
  const params = parseFlightSearchParams(new URLSearchParams(search));
  const intl = useIntl();
  return (
    <Link to={{ pathname: ROUTES.booking.flight.result, search }}>
      <ButtonBase
        key={index}
        style={{
          padding: '4px 8px',
          border: `1px solid ${WHITE}`,
          display: 'flex',
          flexDirection: 'column',
          minWidth: 156,
          marginRight: 8,
          alignItems: 'flex-start',
          borderRadius: 4,
        }}
      >
        <Row>
          <Typography variant="subtitle2" color="inherit">
            {params.origin?.location}
          </Typography>
          {params.one_way ? (
            <KeyboardBackspaceIcon
              fontSize="small"
              color="inherit"
              style={{ transform: 'rotate(180deg)', margin: '0px 8px 4px' }}
            />
          ) : (
            <SyncAltIcon fontSize="small" color="inherit" style={{ margin: '0px 8px 4px' }} />
          )}
          <Typography variant="subtitle2">{params.destination?.location}</Typography>
        </Row>
        <Row>
          <Typography noWrap variant="caption" color="inherit" style={{ flexShrink: 0 }}>
            {moment(params.departureDate, DATE_FORMAT_BACK_END).format('L')}
            {params.returnDate && (
              <>&nbsp;- {moment(params.returnDate, DATE_FORMAT_BACK_END).format('L')}</>
            )}
          </Typography>
          <div
            style={{
              width: '4px',
              height: '4px',
              borderRadius: '4px',
              marginLeft: '12px',
              marginRight: '6px',
              background: WHITE,
            }}
          />
          <Tooltip title={getInputStr(params.seatClass, params.travellerCountInfo, intl)}>
            <Typography noWrap variant="caption" color="inherit" style={{ maxWidth: 120 }}>
              {getInputStr(params.seatClass, params.travellerCountInfo, intl)}
            </Typography>
          </Tooltip>
        </Row>
      </ButtonBase>
    </Link>
  );
};

const FlightFindRecentBox: React.FC = () => {
  const searchHistory = React.useMemo(() => getFlightSearchHistory(), []);
  if (!searchHistory.length) {
    return <div />;
  }
  return (
    <Row>
      <Typography variant="body2" color="inherit" style={{ marginRight: 12 }}>
        <FormattedMessage id="flight.searchRecent" />:
      </Typography>
      {searchHistory.map((item: string, index: number) => (
        <CustomSlide key={index} index={index} search={item} />
      ))}
    </Row>
  );
};

export default FlightFindRecentBox;
