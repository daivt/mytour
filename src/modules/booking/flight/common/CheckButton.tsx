/* eslint-disable no-nested-ternary */
import { Button, ButtonProps } from '@material-ui/core';
import React from 'react';
import { GREY_700, PRIMARY, WHITE } from '../../../../configs/colors';

const CheckButton = (
  props: ButtonProps & {
    active: boolean;
  },
) => {
  const { active, style, children, ...rest } = props;

  return (
    <Button
      variant="outlined"
      color={active ? 'primary' : 'default'}
      size="small"
      disableElevation
      style={{
        borderRadius: 32,
        textTransform: 'none',
        color: active ? WHITE : GREY_700,
        backgroundColor: active ? PRIMARY : undefined,
        borderColor: active ? PRIMARY : undefined,
        minHeight: 32,
        padding: '0px 16px',
        ...style,
      }}
      {...rest}
    >
      {children}
    </Button>
  );
};
export default CheckButton;
