import {
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  RootRef,
  Typography,
} from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline';
import { map, remove } from 'lodash';
import React from 'react';
import { FormattedMessage, IntlShape, useIntl } from 'react-intl';
import { useSelector } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';
import { GREY } from '../../../../configs/colors';
import { EN_US } from '../../../../models/intl';
import { AppState } from '../../../../redux/reducers';
import { ReactComponent as IconSeat } from '../../../../svg/booking/ic_seat.svg';
import { Wrapper } from '../../../common/components/elements';
import FormControlTextField from '../../../common/components/FormControlTextField';
import styles from '../../../common/components/styles.module.scss';
import { MIN_WIDTH_FORM } from '../../../common/components/utils';
import { SeatClass, TravellerCountInfo } from '../../flight/utils';
import CheckButton from './CheckButton';

export const CountBox = styled.div`
  height: 52px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const TypeBox = styled.div`
  height: 40px;
`;

export const TypeLine = styled.div`
  height: 20px;
  display: flex;
  align-items: center;
`;

export const ValueBox = styled.div`
  width: 44px;
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ValueControl = styled.div`
  display: flex;
  align-items: center;
`;

export function validTravellerCount(adultCount: number, childCount: number, infantCount: number) {
  return (
    adultCount >= 1 &&
    childCount >= 0 &&
    infantCount >= 0 &&
    adultCount + childCount <= 9 &&
    infantCount <= adultCount
  );
}

export function getInputStr(
  seatClass: SeatClass[],
  travellerInfo: TravellerCountInfo,
  intl: IntlShape,
) {
  return `${intl.formatMessage(
    {
      id: 'flight.travellerInfo.adultCount',
    },
    { adultCount: travellerInfo.adultCount },
  )}${
    travellerInfo.childCount
      ? `, ${intl.formatMessage(
          { id: 'flight.travellerInfo.childCount' },
          { childCount: travellerInfo.childCount },
        )}`
      : ''
  }${
    travellerInfo.infantCount
      ? `, ${intl.formatMessage(
          { id: 'flight.travellerInfo.infantCount' },
          { infantCount: travellerInfo.infantCount },
        )}`
      : ''
  }${
    seatClass.length
      ? `, ${map(seatClass, intl.locale.startsWith(EN_US) ? 'v_name' : 'i_name').join(', ')}`
      : ''
  }`;
}

interface Props {
  minimizedWidth?: string | number;
  travellerInfo: TravellerCountInfo;
  seatClasses: SeatClass[];
  onChange(newValue: TravellerCountInfo): void;
  onSeatChange?: (newValue: SeatClass[]) => void;
  ticketClass: SeatClass[];
  formControlStyle?: React.CSSProperties;
  style?: React.CSSProperties;
  labelStyle?: React.CSSProperties;
  inputStyle?: React.CSSProperties;
  label?: React.ReactNode;
  iRef?: React.RefObject<HTMLDivElement>;
  disableAdornment?: boolean;
  direction?: 'left' | 'center' | 'right';
}

interface State {
  isFocused: boolean;
}

const TravellerCountInfoBox: React.FC<Props> = props => {
  const {
    minimizedWidth,
    travellerInfo,
    seatClasses,
    onChange,
    ticketClass,
    onSeatChange,
    style,
    labelStyle,
    label,
    inputStyle,
    formControlStyle,
    disableAdornment,
    direction,
    iRef,
  } = props;
  const intl = useIntl();
  const locale = useSelector((state: AppState) => state.intl.locale);
  const [isFocused, setFocus] = React.useState(false);
  const [height, setHeight] = React.useState(0);

  const parent = iRef || React.createRef<HTMLDivElement>();
  const innerRef = React.useRef<HTMLInputElement>();
  const onBlur = React.useCallback(
    (e: React.FocusEvent<HTMLDivElement>) => {
      if (e.relatedTarget instanceof Element) {
        if (e.currentTarget.contains(e.relatedTarget as Element)) {
          if (parent.current) {
            parent.current.focus();
            return;
          }
        }
      }
      setFocus(false);
    },
    [parent],
  );

  const changeSeat = React.useCallback(
    (seat: SeatClass) => {
      const classesSeat = [...seatClasses];
      const classesCodes = map(classesSeat, 'code');

      if (classesCodes.indexOf(seat.code) === -1) {
        classesSeat.push(seat);
      } else {
        remove(classesSeat as SeatClass[], (obj: SeatClass) => obj.code === seat.code);
      }

      if (onSeatChange) {
        onSeatChange(classesSeat);
      }
    },
    [onSeatChange, seatClasses],
  );

  React.useEffect(() => {
    setHeight(innerRef.current?.offsetHeight as number);
  }, []);

  return (
    <FormControl
      style={{
        position: 'relative',
        outline: 'none',
        minHeight: height,
        color: isFocused ? 'black' : undefined,
        minWidth: MIN_WIDTH_FORM,
        ...style,
      }}
      // className={classes.margin}
      tabIndex={-1}
      ref={parent}
      onBlur={onBlur}
      onFocus={e => {
        setFocus(true);
      }}
    >
      <Wrapper
        style={{
          boxShadow: isFocused ? '0px 4px 8px rgba(0, 0, 0, 0.25)' : undefined,
          zIndex: isFocused ? 100 : 0,
          backgroundColor: isFocused ? 'rgba(255,255,255,1)' : 'rgba(255,255,255,0)',
          margin: isFocused ? -12 : undefined,
          transition: 'none',
          right: direction === 'left' ? 0 : undefined,
          left: direction === 'left' ? 'unset' : direction === 'center' ? '50%' : undefined,
        }}
      >
        <div style={{ padding: isFocused ? '12px 12px 0px 12px' : undefined }}>
          <RootRef rootRef={innerRef}>
            <FormControlTextField
              id="dateField"
              label={label}
              formControlStyle={{
                margin: 0,
                width: '100%',
                minWidth: minimizedWidth,
                ...formControlStyle,
              }}
              style={{
                background: 'white',
                ...inputStyle,
              }}
              fullWidth
              labelStyle={labelStyle}
              value={getInputStr(seatClasses, travellerInfo, intl)}
              optional
              readOnly
              inputProps={{ style: { width: '100%' } }}
              startAdornment={
                !disableAdornment && (
                  <InputAdornment position="start" style={{ marginLeft: 8 }}>
                    <IconButton size="small" edge="start" tabIndex={-1}>
                      <IconSeat />
                    </IconButton>
                  </InputAdornment>
                )
              }
            />
          </RootRef>
        </div>
        <CSSTransition
          classNames={{
            enter: styles.enter,
            enterActive: styles.enterActive,
            exit: styles.leave,
            exitActive: styles.leaveActive,
          }}
          timeout={200}
          in={isFocused}
          unmountOnExit
        >
          <div
            key={1}
            style={{
              transition: 'all 300ms ease',
              textAlign: 'start',
              transformOrigin: '0 0',
              backgroundColor: 'white',
              padding: '0 12px 8px 12px',
              minWidth: '320px',
            }}
          >
            <div>
              <div style={{ display: 'flex', alignItems: 'center', height: '40px' }}>
                <Typography variant="subtitle2">
                  <FormattedMessage id="flight.travellerInfo.seatClass" />
                </Typography>
              </div>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '300px',
                  whiteSpace: 'nowrap',
                  flexWrap: 'wrap',
                  margin: 'auto',
                }}
              >
                {ticketClass &&
                  ticketClass.map(item => {
                    const seatClassCodeArr = map(seatClasses, 'code');
                    const selected = seatClassCodeArr.indexOf(item.code) !== -1;
                    return (
                      <CheckButton
                        key={item.code}
                        active={selected}
                        onClick={() => changeSeat(item)}
                        style={{ marginTop: '4px' }}
                      >
                        <Typography variant="body2">
                          {locale.startsWith(EN_US) ? item.v_name : item.i_name}
                        </Typography>
                      </CheckButton>
                    );
                  })}
              </div>
            </div>
            <div>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  height: '40px',
                  paddingTop: '12px',
                }}
              >
                <Typography variant="subtitle2">
                  <FormattedMessage id="flight.travellerInfo.travellers" />
                </Typography>
              </div>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.travellerInfo.adult" />
                    </Typography>
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.travellerInfo.adultDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={
                      !validTravellerCount(
                        travellerInfo.adultCount - 1,
                        travellerInfo.childCount,
                        travellerInfo.infantCount,
                      )
                    }
                    onClick={() =>
                      onChange({ ...travellerInfo, adultCount: travellerInfo.adultCount - 1 })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validTravellerCount(
                          travellerInfo.adultCount - 1,
                          travellerInfo.childCount,
                          travellerInfo.infantCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{travellerInfo.adultCount || 0}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={
                      !validTravellerCount(
                        travellerInfo.adultCount + 1,
                        travellerInfo.childCount,
                        travellerInfo.infantCount,
                      )
                    }
                    onClick={() =>
                      onChange({ ...travellerInfo, adultCount: travellerInfo.adultCount + 1 })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validTravellerCount(
                          travellerInfo.adultCount + 1,
                          travellerInfo.childCount,
                          travellerInfo.infantCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.travellerInfo.children" />
                    </Typography>
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.travellerInfo.childrenDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={
                      !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount - 1,
                        travellerInfo.infantCount,
                      )
                    }
                    onClick={() =>
                      onChange({ ...travellerInfo, childCount: travellerInfo.childCount - 1 })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount - 1,
                          travellerInfo.infantCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{travellerInfo.childCount || 0}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={
                      !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount + 1,
                        travellerInfo.infantCount,
                      )
                    }
                    onClick={() =>
                      onChange({ ...travellerInfo, childCount: travellerInfo.childCount + 1 })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount + 1,
                          travellerInfo.infantCount,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <CountBox>
                <TypeBox>
                  <TypeLine>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.travellerInfo.infant" />
                    </Typography>
                  </TypeLine>

                  <TypeLine style={{ color: GREY }}>
                    <Typography variant="body2">
                      <FormattedMessage id="flight.travellerInfo.infantDef" />
                    </Typography>
                  </TypeLine>
                </TypeBox>
                <ValueControl>
                  <IconButton
                    size="small"
                    disabled={
                      !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount,
                        travellerInfo.infantCount - 1,
                      )
                    }
                    onClick={() =>
                      onChange({ ...travellerInfo, infantCount: travellerInfo.infantCount - 1 })
                    }
                  >
                    <RemoveCircleOutline
                      style={{
                        fontSize: '16px',
                        color: !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount,
                          travellerInfo.infantCount - 1,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                  <ValueBox>{travellerInfo.infantCount || 0}</ValueBox>
                  <IconButton
                    size="small"
                    disabled={
                      !validTravellerCount(
                        travellerInfo.adultCount,
                        travellerInfo.childCount,
                        travellerInfo.infantCount + 1,
                      )
                    }
                    onClick={() =>
                      onChange({ ...travellerInfo, infantCount: travellerInfo.infantCount + 1 })
                    }
                  >
                    <AddCircle
                      style={{
                        fontSize: '16px',
                        color: !validTravellerCount(
                          travellerInfo.adultCount,
                          travellerInfo.childCount,
                          travellerInfo.infantCount + 1,
                        )
                          ? GREY
                          : undefined,
                      }}
                      color="primary"
                    />
                  </IconButton>
                </ValueControl>
              </CountBox>
              <div style={{ marginBottom: '10px', textAlign: 'end', position: 'relative' }}>
                <Button
                  variant="contained"
                  color="secondary"
                  disableElevation
                  onClick={() => {
                    setFocus(false);
                    parent.current?.blur();
                  }}
                  style={{ minWidth: 120 }}
                >
                  <FormattedMessage id="apply" />
                </Button>
              </div>
            </div>
          </div>
        </CSSTransition>
      </Wrapper>
    </FormControl>
  );
};

export default TravellerCountInfoBox;
