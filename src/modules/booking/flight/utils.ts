/* eslint-disable no-nested-ternary */
import moment from 'moment';
import { some } from '../../../constants';
import { DATE_FORMAT_BACK_END } from '../../../models/moment';
import { PAYMENT_HOLDING_CODE, PAYMENT_TRIPI_CREDIT_CODE } from '../constants';
import { computePaymentFees, PointPayment } from '../utils';
import { FlightInfo } from './booking/utils';
import { FlightPayParams } from './payment/utils';

/* eslint-disable camelcase */
export interface Airport {
  code: string;
  name: string;
  location: string;
}
export interface TravellerCountInfo {
  adultCount: number;
  childCount: number;
  infantCount: number;
}

export const defaultTravellerInfo: TravellerCountInfo = {
  adultCount: 1,
  childCount: 0,
  infantCount: 0,
};

export interface SeatClass {
  id: number;
  code: string;
  i_name: string;
  v_name: string;
}
export interface FlightSearchParams {
  origin: Airport | null;
  destination: Airport | null;
  departureDate: string;
  returnDate?: string;
  travellerCountInfo: TravellerCountInfo;
  seatClass: SeatClass[];
  one_way: boolean;
}
export const defaultFlightSearchParams: FlightSearchParams = {
  origin: null,
  destination: null,
  departureDate: moment()
    .startOf('day')
    .format(DATE_FORMAT_BACK_END),
  returnDate: '',
  travellerCountInfo: defaultTravellerInfo,
  seatClass: [],
  one_way: true,
};

export const FLIGHT_SORT_BY = {
  fastest: 'fastest',
  cheapest: 'cheap',
  best: 'best',
  takeOff: 'takeOff',
};

interface PairNumber {
  inbound: number;
  outbound: number;
}
export interface FlightFilterParams {
  price: number[];
  ticketClass: some[];
  numStops: number[];
  timeTakeOff: some[];
  timeLand: some[];
  flightDuration: number[];
  transitDuration: number[];
  airline: number[];
  sortBy: string;
  isFarePrice: boolean;
  maxTransitDuration: PairNumber;
  maxFlightDuration: PairNumber;
  filters?: some;
}
export const defaultFlightFilterParams: FlightFilterParams = {
  price: [0, 1],
  numStops: [],
  timeLand: [],
  flightDuration: [],
  transitDuration: [],
  timeTakeOff: [],
  airline: [],
  ticketClass: [],
  maxTransitDuration: { inbound: 0, outbound: 0 },
  maxFlightDuration: { inbound: 0, outbound: 0 },
  sortBy: FLIGHT_SORT_BY.best,
  isFarePrice: true,
};

export type OneDirection =
  | {
      ticket: some;
      extraBaggages: some[];
      searchRequest: some;
    }
  | { ticket: null; extraBaggages: null };

export interface TicketId {
  id: string;
  aid: number;
}
export interface FlightBookingParams {
  inbound?: TicketId;
  outbound: TicketId;
  requestId: number;
}

export interface FlightBookingResult {
  inbound: OneDirection;
  outbound: OneDirection;
  tid?: FlightBookingParams;
}

export interface FlightFromTo {
  origin: string;
  destination: string;
  link: string;
}

export const defaultFlightBookingResult: FlightBookingResult = {
  inbound: { ticket: null, extraBaggages: null },
  outbound: { ticket: null, extraBaggages: null },
};

export const FLIGHT_SEARCH_PARAM_NAMES = {
  destination: 'destination',
  origin: 'origin',
  departureDate: 'departDate',
  returnDate: 'returnDate',
  travellerInfo: 'travellerInfo',
  seatClass: 'class',
  oneWay: 'oneWay',
};
export const FLIGHT_SEARCH_FILTER_PARAM_NAMES = {
  airline: 'airlineIds',
};

export function stringifyFlightSearchAndFilterParams(
  params: FlightSearchParams,
  filterParams?: FlightFilterParams,
) {
  const arr = [];
  arr.push(
    `${FLIGHT_SEARCH_PARAM_NAMES.destination}=${encodeURIComponent(
      JSON.stringify(params.destination),
    )}`,
  );
  arr.push(
    `${FLIGHT_SEARCH_PARAM_NAMES.origin}=${encodeURIComponent(JSON.stringify(params.origin))}`,
  );
  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.departureDate}=${params.departureDate}`);
  if (params.returnDate) {
    arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.returnDate}=${params.returnDate}`);
  }
  arr.push(
    `${FLIGHT_SEARCH_PARAM_NAMES.travellerInfo}=${encodeURIComponent(
      JSON.stringify(params.travellerCountInfo),
    )}`,
  );
  arr.push(
    `${FLIGHT_SEARCH_PARAM_NAMES.seatClass}=${encodeURIComponent(
      JSON.stringify(params.seatClass),
    )}`,
  );
  arr.push(`${FLIGHT_SEARCH_PARAM_NAMES.oneWay}=${encodeURIComponent(params.one_way)}`);
  if (filterParams) {
    arr.push(
      `${FLIGHT_SEARCH_FILTER_PARAM_NAMES.airline}=${encodeURIComponent(
        JSON.stringify(filterParams.airline),
      )}`,
    );
  }
  return arr.join('&');
}

export function parseFlightSearchParams(params: URLSearchParams): FlightSearchParams {
  const destination = params.get(FLIGHT_SEARCH_PARAM_NAMES.destination);
  if (!destination) {
    throw new Error('No destination');
  }
  const destinationJson = JSON.parse(destination);

  const origin = params.get(FLIGHT_SEARCH_PARAM_NAMES.origin);
  if (!origin) {
    throw new Error('No origin');
  }
  const originJson = JSON.parse(origin);

  const oneWay = params.get(FLIGHT_SEARCH_PARAM_NAMES.oneWay) === 'true';

  const departureDate = params.get(FLIGHT_SEARCH_PARAM_NAMES.departureDate);
  if (!departureDate) {
    throw new Error('No departure date');
  }

  const departureDateMoment = moment(departureDate, DATE_FORMAT_BACK_END);
  if (!departureDateMoment.isValid() || departureDateMoment.isBefore(moment().startOf('day'))) {
    throw new Error('Invalid departure date');
  }

  const returnDate = params.get(FLIGHT_SEARCH_PARAM_NAMES.returnDate) || '';
  if (returnDate) {
    const returnDateMoment = moment(returnDate, DATE_FORMAT_BACK_END, true);
    if (!returnDateMoment.isValid() || returnDateMoment.isBefore(departureDateMoment)) {
      throw new Error('Invalid return date');
    }
  } else if (!oneWay) {
    throw new Error('Invalid return date');
  }
  const travellerInfoStr = params.get(FLIGHT_SEARCH_PARAM_NAMES.travellerInfo);
  if (!travellerInfoStr) {
    throw new Error('No traveller info');
  }

  const travellerInfo = JSON.parse(travellerInfoStr);

  const classStr = params.get(FLIGHT_SEARCH_PARAM_NAMES.seatClass);
  if (!classStr) {
    throw new Error('No seat class');
  }
  const seatClass: SeatClass[] = JSON.parse(classStr);

  if (!seatClass) {
    throw new Error('Invalid seat class');
  }

  if (!classStr) {
    throw new Error('No seat class');
  }
  return {
    returnDate,
    seatClass,
    departureDate,
    travellerCountInfo: travellerInfo,
    destination: destinationJson,
    origin: originJson,
    one_way: oneWay,
  };
}

export function parseFlightSearchFilterParams(params: URLSearchParams): { airlineIds: number[] } {
  const airlineIdsStr = params.get(FLIGHT_SEARCH_FILTER_PARAM_NAMES.airline);
  const airlineIds = airlineIdsStr ? JSON.parse(airlineIdsStr) : [];

  return { airlineIds };
}

export function computeExtraBaggagesCost(booking: FlightInfo) {
  const value = { inbound: 0, outbound: 0 };

  if (
    booking.outbound.extraBaggages &&
    booking.outbound.ticket.outbound.baggages &&
    booking.outbound.ticket.outbound.baggages.length
  ) {
    const eb = booking.outbound.extraBaggages;
    for (let i = 0; i < eb.length; i += 1) {
      value.outbound += eb[i].price;
    }
  }

  if (
    booking.inbound.extraBaggages &&
    booking.inbound.ticket.outbound.baggages &&
    booking.inbound.ticket.outbound.baggages.length
  ) {
    const eb = booking.inbound.extraBaggages;
    for (let i = 0; i < eb.length; i += 1) {
      value.inbound += eb[i].price;
    }
  }
  return value;
}

export function computeFlightPayableNumbers(
  booking: FlightInfo,
  params?: FlightPayParams,
  pointPaymentData?: PointPayment,
) {
  const adult = { number: 0, price: 0, unitPrice: 0 };
  const children = { number: 0, price: 0, unitPrice: 0 };
  const infant = { number: 0, price: 0, unitPrice: 0 };
  if (booking.outbound.ticket) {
    adult.number = booking.outbound.searchRequest.numAdults;
    children.number = booking.outbound.searchRequest.numChildren;
    infant.number = booking.outbound.searchRequest.numInfants;

    const detail = booking.outbound.ticket.outbound.ticketdetail;
    const adultPrice = detail.farePrice;
    adult.unitPrice += adultPrice;
    adult.price += adult.number * adultPrice;

    const childPrice = detail.farePrice;
    children.unitPrice += childPrice;
    children.price += children.number * childPrice;

    const infantPrice = 0;
    infant.unitPrice += infantPrice;
    infant.price += infant.number * infantPrice;
  }
  if (booking.inbound.ticket) {
    const detail = booking.inbound.ticket.outbound.ticketdetail;
    const adultPrice = detail.farePrice;
    adult.unitPrice += adultPrice;
    adult.price += adult.number * adultPrice;

    const childPrice = detail.farePrice;
    children.unitPrice += childPrice;
    children.price += children.number * childPrice;

    const infantPrice = 0;
    infant.unitPrice += infantPrice;
    infant.price += infant.number * infantPrice;
  }

  let ticketTotal = 0;
  if (booking.inbound.ticket) {
    ticketTotal += booking.inbound.ticket.outbound.ticketdetail.grandTotal;
  }
  if (booking.outbound.ticket) {
    ticketTotal += booking.outbound.ticket.outbound.ticketdetail.grandTotal;
  }

  const vatPrice = ticketTotal - adult.price - children.price - infant.price;

  const pointToAmount = params?.usingPoint
    ? pointPaymentData &&
      params?.selectedPaymentMethod &&
      params?.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? params?.point * pointPaymentData.pointFactor
      : 0
    : 0;

  const extraBaggagesCostsObj = computeExtraBaggagesCost(booking);
  const extraBaggagesCosts = extraBaggagesCostsObj.inbound + extraBaggagesCostsObj.outbound;

  let insuranceCost = 0;
  if (booking.insurancePackage && booking.buyInsurance) {
    insuranceCost =
      (adult.number + children.number + infant.number) * booking.insurancePackage.price;
  }

  const preDiscountTotal = ticketTotal + extraBaggagesCosts + insuranceCost;

  const discountAmount =
    params?.promotion && params?.promotion.price ? preDiscountTotal - params?.promotion?.price : 0;

  const originPrice = ticketTotal + insuranceCost + extraBaggagesCosts;

  const finalPrice =
    params?.selectedPaymentMethod && params?.selectedPaymentMethod.code === PAYMENT_HOLDING_CODE
      ? originPrice
      : originPrice - discountAmount;

  const paymentFee = params?.selectedPaymentMethod
    ? computePaymentFees(params?.selectedPaymentMethod, finalPrice)
    : 0;

  return {
    ticketTotal,
    insuranceCost,
    extraBaggagesCostsObj,
    extraBaggagesCosts,
    pointToAmount,
    discountAmount,
    adult,
    children,
    infant,
    vatPrice,
    paymentFee,
    originPrice,
    finalPrice,
  };
}
