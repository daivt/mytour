import { Airport, FlightFromTo } from './utils';

export const defaultAirports: Airport[] = [
  { code: 'HAN', location: 'Hà Nội', name: 'Nội Bài' },
  { code: 'SGN', location: 'Hồ Chí Minh', name: 'Tân Sơn Nhất' },
  { code: 'DAD', location: 'Đà Nẵng', name: 'Quốc tế Đà Nẵng' },
  { code: 'CXR', location: 'Nha Trang', name: 'Cam Ranh' },
  { code: 'PQC', location: 'Phú Quốc', name: 'Quốc tế Phú Quốc' },
  { code: 'HUI', location: 'Huế', name: 'Quốc tế Phú Bài' },
  { code: 'HPH', location: 'Hải Phòng', name: 'Quốc tế Cát bi' },
];

export const defaultDomesticFlightList: FlightFromTo[] = [
  {
    origin: 'Hồ Chí Minh',
    destination: 'Vinh',
    link:
      'https://mytour.vn/location/14070-bi-quyet-san-ve-may-bay-ho-chi-minh-vinh-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Thanh Hóa',
    link:
      'https://mytour.vn/location/14071-bi-quyet-san-ve-may-bay-ho-chi-minh-thanh-hoa-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Hà Nội',
    link:
      'https://mytour.vn/location/14078-bi-quyet-san-ve-may-bay-ho-chi-minh-ha-noi-gia-re-moi-nhat.html',
  },
  {
    origin: 'Đà Nẵng',
    destination: 'Hà Nội',
    link:
      'https://mytour.vn/location/14072-bi-quyet-san-ve-may-bay-da-nang-ha-noi-gia-re-moi-nhat.html',
  },
  {
    origin: 'Đà Nẵng',
    destination: 'Hồ Chí Minh',
    link:
      'https://mytour.vn/location/14073-bi-quyet-san-ve-may-bay-da-nang-ho-chi-minh-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hà Nội',
    destination: 'Đà Nẵng',
    link:
      'https://mytour.vn/location/14074-bi-quyet-san-ve-may-bay-ha-noi-da-nang-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hà Nội',
    destination: 'Hồ Chí Minh',
    link:
      'https://mytour.vn/location/14075-bi-quyet-san-ve-may-bay-ha-noi-ho-chi-minh-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hà Nội',
    destination: 'Nha Trang',
    link:
      'https://mytour.vn/location/14076-bi-quyet-san-ve-may-bay-ha-noi-nha-trang-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hải Phòng',
    destination: 'Hồ Chí Minh',
    link:
      'https://mytour.vn/location/14077-bi-quyet-san-ve-may-bay-hai-phong-ho-chi-minh-gia-re-moi-nhat.html',
  },
];
export const defaultInternationalFlightList: FlightFromTo[] = [
  {
    origin: 'Hà Nội',
    destination: 'Singapore',
    link:
      'https://mytour.vn/location/14079-bi-quyet-san-ve-may-bay-ha-noi-singapore-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hà Nội',
    destination: 'Singapore',
    link:
      'https://mytour.vn/location/14079-bi-quyet-san-ve-may-bay-ha-noi-singapore-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Seoul',
    link:
      'https://mytour.vn/location/14081-bi-quyet-san-ve-may-bay-ho-chi-minh-seoul-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Taipei',
    link:
      'https://mytour.vn/location/14082-bi-quyet-san-ve-may-bay-ho-chi-minh-taipei-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Singapore',
    link:
      'https://mytour.vn/location/14083-bi-quyet-san-ve-may-bay-ho-chi-minh-singapore-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Kuala Lumpur',
    link:
      'https://mytour.vn/location/14085-bi-quyet-san-ve-may-bay-ho-chi-minh-kuala-lumpur-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Guangzhou',
    link:
      'https://mytour.vn/location/14086-bi-quyet-san-ngay-ve-may-bay-ho-chi-minh-guangzhou-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Bangkok',
    link:
      'https://mytour.vn/location/14087-bi-quyet-san-ve-may-bay-ho-chi-minh-bangkok-gia-re-moi-nhat.html',
  },
  {
    origin: 'Hồ Chí Minh',
    destination: 'Phnom Penh',
    link:
      'https://mytour.vn/location/14080-bi-quyet-san-ve-may-bay-ho-chi-minh-phnom-penh-gia-re-moi-nhat.html',
  },
];
export const TopDestinations = [
  {
    airportCode: 'HAN',
    id: 11,
    latitude: 21.028333,
    longitude: 105.853333,
    name: 'Hà Nội',
    nameNoAccent: 'ha noi',
  },
  {
    airportCode: 'SGN',
    id: 33,
    latitude: 10.769444,
    longitude: 106.681944,
    name: 'Hồ Chí Minh',
    nameNoAccent: 'ho chi minh',
  },
  {
    airportCode: 'DAD',
    id: 50,
    latitude: 16.031944,
    longitude: 108.220556,
    name: 'Đà Nẵng',
    nameNoAccent: 'da nang',
  },
  {
    airportCode: 'HPH',
    id: 3,
    latitude: 20.866389,
    longitude: 106.6825,
    name: 'Hải Phòng',
    nameNoAccent: 'hai phong',
  },
  {
    airportCode: 'VCA',
    id: 38,
    latitude: 10.032415,
    longitude: 105.784092,
    name: 'Cần Thơ',
    nameNoAccent: 'can tho',
  },
];
