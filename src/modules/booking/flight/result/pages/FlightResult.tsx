import { get, set } from 'js-cookie';
import { debounce, map } from 'lodash';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React from 'react';
import Helmet from 'react-helmet';
import { useIntl } from 'react-intl';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import {
  defaultFlightBookingResult,
  defaultFlightFilterParams,
  FlightBookingResult,
  FlightFilterParams,
  FlightSearchParams,
  parseFlightSearchFilterParams,
  parseFlightSearchParams,
  stringifyFlightSearchAndFilterParams,
} from '../../utils';
import FlightResultDesktop from '../components/FlightResultDesktop';
import { FARE_PRICE } from '../constants';
import { getMaxFlightDuration, getMaxTransitDuration } from '../utils';

interface Props {}

const FlightResult: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();
  const locale = useSelector((state: AppState) => state.intl.locale, shallowEqual);
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [paramsSearch, setParams] = React.useState<FlightSearchParams | undefined>(undefined);
  const [filter, setFilter] = React.useState<FlightFilterParams>(defaultFlightFilterParams);
  const [booking, setBooking] = React.useState<FlightBookingResult>(defaultFlightBookingResult);
  const [time, setTime] = React.useState(moment());
  const [searchCompleted, setSearchCompleted] = React.useState(0);
  const location = useLocation();
  const history = useHistory();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const resetFilter = React.useCallback(() => {
    const tmp: FlightFilterParams = {
      ...defaultFlightFilterParams,
      transitDuration: [0, filter.maxTransitDuration.outbound],
      flightDuration: [0, filter.maxFlightDuration.outbound],
      maxTransitDuration: filter.maxTransitDuration,
      maxFlightDuration: filter.maxFlightDuration,
      price: [0, filter.isFarePrice ? filter.filters?.maxFarePrice : filter.filters?.maxPrice],
      isFarePrice: filter.isFarePrice,
      sortBy: filter.sortBy,
      filters: filter.filters,
    };
    setFilter(tmp);
  }, [
    filter.filters,
    filter.isFarePrice,
    filter.maxFlightDuration,
    filter.maxTransitDuration,
    filter.sortBy,
  ]);

  const searchData = React.useCallback(
    debounce(
      async (searchState?: FlightSearchParams, airlineId?: number) => {
        if (!searchState) {
          return;
        }
        setSearchCompleted(0);
        let waitFor = null;
        let polling = true;
        setData(undefined);
        const filterParamsAirLines = parseFlightSearchFilterParams(
          new URLSearchParams(location.search),
        );
        const { airlineIds } = filterParamsAirLines;

        let i = 1;
        let n = 1;
        while (polling) {
          const json: some = await dispatch(
            fetchThunk(
              searchState.returnDate
                ? API_PATHS.searchTwoWayTickets
                : API_PATHS.searchOneWayTickets,
              'post',
              {
                waitFor,
                from_airport: searchState.origin && searchState.origin.code,
                to_airport: searchState.destination && searchState.destination.code,
                depart: `${searchState.departureDate}`,
                return: searchState.returnDate ? `${searchState.returnDate}` : undefined,
                num_adults: `${searchState.travellerCountInfo.adultCount}`,
                num_childs: `${searchState.travellerCountInfo.childCount}`,
                num_infants: `${searchState.travellerCountInfo.infantCount}`,
                one_way: !searchState.returnDate ? '1' : '0',
                currency: 'VND',
                lang: locale.substring(0, 2),
                filters: {
                  airlines: airlineId ? [airlineId] : airlineIds,
                  paging: { itemsPerPage: 2000, page: 1 },
                  sort: { priceUp: true },
                  ticketClassCodes: map(searchState.seatClass, 'code'),
                },
                sort: 'tripi_recommended',
              },
            ),
          );

          if (json?.code !== SUCCESS_CODE) {
            json?.message &&
              enqueueSnackbar(
                json.message,
                snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
              );
            // dispatch(goBackAction());
            break;
          }
          setData(json.data);
          const maxTransitDuration = {
            outbound: json.data.tickets
              ? getMaxTransitDuration(json.data.tickets)
              : getMaxTransitDuration(json.data.outbound.tickets),
            inbound: json.data.inbound ? getMaxTransitDuration(json.data.inbound.tickets) : 0,
          };
          const maxFlightDuration = {
            outbound: json.data.tickets
              ? json.data.filters.maxDuration
              : getMaxFlightDuration(json.data.outbound.tickets),
            inbound: json.data.inbound ? getMaxFlightDuration(json.data.inbound.tickets) : 0,
          };
          const filterParams: FlightFilterParams = {
            ...defaultFlightFilterParams,
            filters: json.data.filters,
            isFarePrice: get(FARE_PRICE) !== 'false',
            price: [
              0,
              // searchState.returnDate ? json.data.filters.maxGroupPrice : json.data.filters.maxPrice,
              get(FARE_PRICE) !== 'false'
                ? json.data.filters.maxFarePrice || 0
                : json.data.filters.maxPrice,
            ],
            transitDuration: [0, maxTransitDuration.outbound],
            flightDuration: [0, maxFlightDuration.outbound],
            maxTransitDuration,
            maxFlightDuration,
          };
          setFilter(filterParams);
          waitFor = json.data.waitFor as string;
          polling = json.data.polling as boolean;
          if (i === 1) {
            n = 1 + waitFor.split(',').length;
          }
          const searchPercent = (100 * i) / n;

          setSearchCompleted(searchPercent < 100 ? searchPercent : 99);
          i += 1;
        }
        setTime(moment());
        setSearchCompleted(100);
      },
      500,
      {
        trailing: true,
        leading: false,
      },
    ),
    [],
  );

  React.useEffect(() => {
    try {
      const searchUrl = new URLSearchParams(location.search);
      const params = parseFlightSearchParams(searchUrl);
      setBooking(defaultFlightBookingResult);
      setParams(params);
    } catch (error) {
      // enqueueSnackbar(
      //   intl.formatMessage({ id: 'linkValid' }),
      //   snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      // );
      dispatch(goToAction({ pathname: ROUTES.booking.flight.default }));
    }
  }, [closeSnackbar, dispatch, enqueueSnackbar, location.search]);

  React.useEffect(() => {
    searchData(paramsSearch);
  }, [paramsSearch, searchData]);

  if (!paramsSearch) {
    return <LoadingIcon />;
  }
  return (
    <>
      <Helmet>
        <title>
          {paramsSearch
            ? `${intl.formatMessage({ id: 'flight.outbound' })}: ${
                paramsSearch.origin?.location
              } (${paramsSearch.origin?.code}) > ${paramsSearch.destination?.location} (${
                paramsSearch.destination?.code
              }) - ${paramsSearch.departureDate} ${
                paramsSearch.returnDate ? `-${paramsSearch.returnDate}` : ''
              }`
            : 'Flight search result'}
        </title>
      </Helmet>
      <FlightResultDesktop
        data={data}
        paramsSearch={paramsSearch}
        filterParams={filter}
        booking={booking}
        setParamsSearch={value => {
          if (value !== paramsSearch) {
            const params = stringifyFlightSearchAndFilterParams(value);
            history.push({ search: params });
          } else if (moment.duration(moment().diff(time)).asMinutes() > 5) {
            searchData(paramsSearch);
          }
        }}
        setFilterParams={value => {
          window.scrollTo({ top: 0, behavior: 'smooth' });
          setFilter(value);
          set(FARE_PRICE, value.isFarePrice.toString());
        }}
        resetFilter={resetFilter}
        setInBound={(val: some, requestId: number, req: some) => {
          const searchRequest = req;
          const adultCount = searchRequest.numAdults || 0;
          const childCount = searchRequest.numChildren || 0;
          const bookingTmp = {
            ...booking,
            inbound: {
              ...booking.inbound,
              ticket: val,
              extraBaggages: Array(adultCount + childCount).fill(0),
              searchRequest: req,
            },
            tid: booking.tid?.outbound
              ? {
                  ...booking.tid,
                  inbound: { id: val.tid, aid: val.outbound?.aid },
                  requestId,
                }
              : undefined,
          };
          setBooking(bookingTmp);
        }}
        clearInBound={() => {
          setBooking({ ...booking, inbound: defaultFlightBookingResult.inbound });
        }}
        setOutBound={(val: some, requestId: number, req: some) => {
          const searchRequest = req;
          const adultCount = searchRequest.numAdults || 0;
          const childCount = searchRequest.numChildren || 0;
          const bookingTmp = {
            ...booking,
            outbound: {
              ...booking.outbound,
              ticket: val,
              extraBaggages: Array(adultCount + childCount).fill(0),
              searchRequest: req,
            },
            tid: { ...booking.tid, outbound: { id: val.tid, aid: val.outbound.aid }, requestId },
          };
          setBooking(bookingTmp);
          if (data?.inbound) {
            resetFilter();
          }
        }}
        clearOutBound={() => {
          if (!paramsSearch.one_way && moment.duration(moment().diff(time)).asMinutes() > 5) {
            searchData(paramsSearch);
          }
          if (data?.inbound) {
            resetFilter();
          }
          setBooking({ ...booking, outbound: defaultFlightBookingResult.outbound });
        }}
        searchCompleted={searchCompleted}
      />
    </>
  );
};

export default FlightResult;
