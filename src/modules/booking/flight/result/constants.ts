import { FLIGHT_SORT_BY } from '../utils';

export const TIME_TAKE_OFF_AND_LAND = [
  {
    id: 0,
    text: '00:00 - 06:00',
    value: ['00:00', '05:59'],
    isSelect: false,
  },
  {
    id: 1,
    text: '06:00 - 12:00',
    value: ['06:00', '11:59'],
    isSelect: false,
  },
  {
    id: 2,
    text: '12:00 - 18:00',
    value: ['12:00', '17:59'],
    isSelect: false,
  },
  {
    id: 3,
    text: '18:00 - 24:00',
    value: ['18:00', '23:59'],
    isSelect: false,
  },
];

export const PRICE_STEP = 10000;

export const TECHNICAL_STOP = 'TECHNICAL_STOP';

export const LONG_TRANSIT_DURATION = 5 * 3600 * 1000;

export const FLIGHT_SORT_BY_DATA = [
  {
    name: 'flight.filter.sortBy.default',
    id: FLIGHT_SORT_BY.best,
  },
  {
    name: 'flight.filter.sortBy.cheap',
    id: FLIGHT_SORT_BY.cheapest,
  },
  {
    name: 'flight.filter.sortBy.takeOff',
    id: FLIGHT_SORT_BY.takeOff,
  },
  {
    name: 'flight.filter.sortBy.fastest',
    id: FLIGHT_SORT_BY.fastest,
  },
];

export const FARE_PRICE = 'fare_price';
