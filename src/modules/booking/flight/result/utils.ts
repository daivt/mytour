import moment from 'moment';
import { some } from '../../../../constants';
import { FLIGHT_BOOKING_PARAM_NAME } from '../../constants';
import { FlightBookingParams, FlightBookingResult, FlightFilterParams } from '../utils';
import { TECHNICAL_STOP } from './constants';

function getTransitDuration(ticket: some) {
  let transitDuration = 0;
  if (ticket.outbound.transitTickets) {
    for (let i = 0; i < ticket.outbound.transitTickets.length; i += 1) {
      transitDuration += ticket.outbound.transitTickets[i].transitDuration;
    }
  }
  return transitDuration;
}

export const flightCheapestRoundtripCmp = (a: some, b: some) =>
  a.outbound.ticketdetail.bestGroupPriceAdultUnit - b.outbound.ticketdetail.bestGroupPriceAdultUnit;
export const flightCheapestCmp = (a: some, b: some) =>
  a.outbound.ticketdetail.priceAdultUnit - b.outbound.ticketdetail.priceAdultUnit;
export const flightFastestCmp = (a: some, b: some) => a.outbound.duration - b.outbound.duration;
export const flightTimeTakeOffCmp = (a: some, b: some) =>
  a.outbound.departureTime - b.outbound.departureTime;

export function getMaxTransitDuration(tickets: some[]) {
  const value = tickets.reduce((prev, ticket) => Math.max(prev, getTransitDuration(ticket)), 0);
  return value;
}
export function getMaxFlightDuration(tickets: some[]) {
  const value = tickets.reduce((prev, ticket) => Math.max(prev, ticket.outbound.duration), 0);
  return value;
}

function getTechnicalStop(policies: some[]) {
  const stop = policies.find(policy => {
    if (policy.code === TECHNICAL_STOP) {
      return true;
    }
    return false;
  });
  if (stop) {
    const { moreInfo } = stop;
    if (moreInfo) {
      return moreInfo.airportCode;
    }
  }
  return null;
}

export function getStops(ticket: some) {
  const value = [];
  if (ticket.outbound.transitTickets) {
    for (let i = 0; i < ticket.outbound.transitTickets.length; i += 1) {
      const transitTicket = ticket.outbound.transitTickets[i];
      const tsTop = getTechnicalStop(transitTicket.polices);
      if (tsTop) {
        value.push({ code: tsTop, city: '', technical: true });
      }

      if (i !== ticket.outbound.transitTickets.length - 1) {
        value.push({ code: transitTicket.arrivalAirport, city: transitTicket.arrivalCity });
      }
    }
  } else {
    const tsTop = getTechnicalStop(ticket.outbound.ticketdetail.polices);
    if (tsTop) {
      value.push({ code: tsTop, city: '' });
    }
  }
  return value;
}

export function filterAndSort(
  tickets: some[],
  filter: FlightFilterParams,
  cmp?: (a: some, b: some) => number,
) {
  const filteredTickets = tickets?.filter((ticket: some) => {
    if (filter.airline.length > 0) {
      if (!filter.airline.find(one => one === ticket.outbound.aid)) {
        return false;
      }
    }

    if (
      ticket.outbound.duration < filter.flightDuration[0] ||
      ticket.outbound.duration > filter.flightDuration[1]
    ) {
      return false;
    }

    if (filter.numStops.length > 0) {
      const stopCount = ticket.outbound.ticketdetail.numStops;
      if (filter.numStops.indexOf(stopCount) === -1) {
        return false;
      }
    }

    // if (
    //   ticket.outbound.ticketdetail.bestGroupPriceAdultUnit < filter.price[0] ||
    //   ticket.outbound.ticketdetail.bestGroupPriceAdultUnit > filter.price[1]
    // ) {
    //   return false;
    // }
    if (filter.isFarePrice) {
      if (
        ticket.outbound.ticketdetail.farePrice < filter.price[0] ||
        ticket.outbound.ticketdetail.farePrice > filter.price[1]
      ) {
        return false;
      }
    } else if (
      ticket.outbound.ticketdetail.priceAdultUnit < filter.price[0] ||
      ticket.outbound.ticketdetail.priceAdultUnit > filter.price[1]
    ) {
      return false;
    }

    if (filter.ticketClass.length > 0) {
      const find = filter.ticketClass.find(
        one => ticket.outbound.ticketdetail.ticketClassCode === one.code,
      );
      if (!find) {
        return false;
      }
    }

    if (filter.timeLand.length > 0) {
      const find = filter.timeLand.find((one: some) => {
        const ticketTime = moment(ticket.outbound.arrivalTimeStr, 'HH:mm');
        const lowerTime = moment(one.value[0], 'HH:mm');
        const upperTime = moment(one.value[1], 'HH:mm');

        return !ticketTime.isBefore(lowerTime) && !ticketTime.isAfter(upperTime);
      });
      if (!find) {
        return false;
      }
    }

    if (filter.timeTakeOff.length > 0) {
      const find = filter.timeTakeOff.find((one: some) => {
        const ticketTime = moment(ticket.outbound.departureTimeStr, 'HH:mm');
        const lowerTime = moment(one.value[0], 'HH:mm');
        const upperTime = moment(one.value[1], 'HH:mm');

        return !ticketTime.isBefore(lowerTime) && !ticketTime.isAfter(upperTime);
      });
      if (!find) {
        return false;
      }
    }

    const transitDuration = getTransitDuration(ticket);
    if (
      transitDuration < filter.transitDuration[0] ||
      transitDuration > filter.transitDuration[1]
    ) {
      return false;
    }

    return true;
  });
  if (cmp) {
    return filteredTickets?.sort(cmp);
  }

  return filteredTickets;
}

export function computePoints(booking: FlightBookingResult) {
  let points = 0;
  if (booking.inbound.ticket) {
    points += booking.inbound.ticket.outbound.ticketdetail.bonusPoint || 0;
  }
  if (booking.outbound.ticket) {
    points += booking.outbound.ticket.outbound.ticketdetail.bonusPoint || 0;
  }
  return points;
}

export function stringifyParams(state?: FlightBookingParams) {
  return `${FLIGHT_BOOKING_PARAM_NAME}=${encodeURIComponent(JSON.stringify(state))}`;
}

export function parseFlightBookingParams(params: URLSearchParams): FlightBookingParams {
  const str = params.get(FLIGHT_BOOKING_PARAM_NAME);
  if (!str) {
    throw new Error('No booking param');
  }
  const state: FlightBookingParams = JSON.parse(str);

  if (!state.outbound) {
    throw new Error('tid not specified');
  }

  if (!state.requestId) {
    throw new Error('No request id');
  }

  return state;
}
