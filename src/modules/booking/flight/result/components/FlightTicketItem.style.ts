import styled from 'styled-components';
import { GREY_700 } from '../../../../../configs/colors';

export const FlightTransit = styled.span`
  ::after {
    content: '';
    display: block;
    width: 100%;
    height: 2px;
    top: 50%;
    background: ${props => props.theme.primary};
    transition: all ease-in-out 300ms;
  }
`;

export const LabelTicketDetail = styled.span`
  color: ${GREY_700};
`;

export const BoxLogoAirline = styled.div`
  display: flex;
  width: 148px;
  flex-flow: column;
  text-align: center;
  align-items: center;
`;

export const BoxInfoFlight = styled.div`
  max-height: 160px;
  display: flex;
  align-items: start;
`;
