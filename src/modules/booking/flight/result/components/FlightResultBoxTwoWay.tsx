/* eslint-disable no-nested-ternary */
import { Button, Divider, Theme, Typography, withStyles } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import { useSelector } from 'react-redux';
import styled, { keyframes } from 'styled-components';
import { BLUE, GREY_500, WHITE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { EN_US } from '../../../../../models/intl';
import { DATE_FORMAT } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { Col } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { durationMillisecondToHour } from '../../../utils';
import {
  FlightBookingResult,
  FlightFilterParams,
  FlightSearchParams,
  FLIGHT_SORT_BY,
} from '../../utils';
import { filterAndSort, flightCheapestCmp, flightFastestCmp, flightTimeTakeOffCmp } from '../utils';
import FlightResultHeader from './FlightResultHeader';
import { FlightSortBox } from './FlightSortBox';
import FlightTicketItem from './FlightTicketItem';
import { BoxLogoAirline } from './FlightTicketItem.style';

const STEP = 10;
const WIDTH = 190;
const MARGIN = 10;

const slideLeft = keyframes`
  from {
    transform: translateX(0);
  }

  to {
    transform: translateX(-${WIDTH + MARGIN}px);
  }
`;

const SlideLeft = styled.div`
  animation-name: ${slideLeft};
  animation-duration: 0.4s;
`;

const WhiteOutlineButton = withStyles((theme: Theme) => ({
  root: {
    color: 'white',
    borderColor: 'white',
    fontWeight: 400,
    fontSize: theme.typography.body2.fontSize,
  },
}))(Button);

const PairDiv = (props: {
  firstDiv: React.ReactNode;
  secondDiv: React.ReactNode;
  fadeColor?: boolean;
}) => {
  const { fadeColor, firstDiv, secondDiv } = props;
  return (
    <div
      className="flight-ticket"
      style={{
        flex: 1,
        color: 'white',
        display: 'flex',
        borderRadius: '4px',
        opacity: fadeColor ? 0.5 : 1,
      }}
    >
      <div style={{ flex: 1 }}>{firstDiv}</div>
      <div
        style={{
          width: 0,
          borderLeft: '1px dashed white',
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        <div
          style={{
            width: '24px',
            height: '24px',
            borderRadius: '50%',
            background: '#fafafc',
            transform: 'translateY(-50%)',
          }}
        />
        <div
          style={{
            width: '24px',
            height: '24px',
            borderRadius: '50%',
            background: '#fafafc',
            transform: 'translateY(50%)',
          }}
        />
      </div>
      <div
        style={{
          width: '230px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        {secondDiv}
      </div>
    </div>
  );
};

const transition: React.CSSProperties = { transition: 'all 0.4s' };

interface Props {
  paramsSearch: FlightSearchParams;
  filterParams: FlightFilterParams;
  setFilterParams(filterParams: FlightFilterParams): void;
  booking: FlightBookingResult;
  setOutBound(val: some, requestId: number, req: some): void;
  setInBound(val: some, requestId: number, req: some): void;
  data?: some;
  searchCompleted: number;
  clearOutBound(): void;
}

const FlightResultBoxTwoWay: React.FC<Props> = props => {
  const {
    data,
    paramsSearch,
    filterParams,
    setFilterParams,
    booking,
    setOutBound,
    setInBound,
    searchCompleted,
    clearOutBound,
  } = props;
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight);
  const intl = useIntl();
  const [maxDisplayInbound, setMaxDisplayInbound] = React.useState(10);
  const [maxDisplayOutbound, setMaxDisplayOutbound] = React.useState(10);

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );
  const getFlightGeneralClasses = React.useCallback(
    (code: string) => {
      return generalFlight?.ticketclass?.find((v: some) => v.code === code) || {};
    },
    [generalFlight],
  );

  const matchingGroupId = React.useMemo(() => {
    return booking?.outbound?.ticket?.outbound?.matchingGroupId;
  }, [booking]);

  const outbound = React.useMemo(() => {
    return booking?.outbound?.ticket?.outbound as some;
  }, [booking]);

  const seatClass = React.useMemo(() => {
    return getFlightGeneralClasses(outbound?.ticketdetail.ticketClassCode);
  }, [getFlightGeneralClasses, outbound]);

  const matchedInboundTickets = React.useMemo(() => {
    return data?.inbound.tickets.filter(
      (ticket: some) => ticket.outbound.matchingGroupId === matchingGroupId,
    );
  }, [data, matchingGroupId]);

  const filteredInboundTickets = React.useMemo(() => {
    const { sortBy } = filterParams;
    return filterAndSort(
      matchedInboundTickets,
      filterParams,
      sortBy === FLIGHT_SORT_BY.cheapest
        ? flightCheapestCmp
        : sortBy === FLIGHT_SORT_BY.fastest
        ? flightFastestCmp
        : sortBy === FLIGHT_SORT_BY.takeOff
        ? flightTimeTakeOffCmp
        : undefined,
    );
  }, [filterParams, matchedInboundTickets]);

  const inboundTickets = React.useMemo(() => {
    return filteredInboundTickets.filter((_: some, i: number) => i < maxDisplayInbound);
  }, [filteredInboundTickets, maxDisplayInbound]);

  const filteredOutboundTickets = React.useMemo(() => {
    const { sortBy } = filterParams;
    return filterAndSort(
      data?.outbound.tickets as some[],
      filterParams,
      sortBy === FLIGHT_SORT_BY.cheapest
        ? flightCheapestCmp
        : sortBy === FLIGHT_SORT_BY.fastest
        ? flightFastestCmp
        : sortBy === FLIGHT_SORT_BY.takeOff
        ? flightTimeTakeOffCmp
        : undefined,
    );
  }, [data, filterParams]);

  const outboundTickets = filteredOutboundTickets.filter(
    (_: some, i: number) => i < maxDisplayOutbound,
  );

  return (
    <div style={{ width: '100%', position: 'relative' }}>
      <FlightResultHeader
        count={
          booking.outbound.ticket ? filteredInboundTickets.length : filteredOutboundTickets.length
        }
        booking={booking}
        paramsSearch={paramsSearch}
      />

      <FlightSortBox filterParams={filterParams} setFilterParams={setFilterParams} />
      {data && (
        <>
          <div
            style={{
              marginTop: '20px',
              overflow: 'hidden',
              height: '110px',
              display: 'flex',
              position: 'relative',
            }}
          >
            <SlideLeft
              style={{
                flex: 1,
                display: 'flex',
                transform: booking.outbound.ticket
                  ? `translateX(-${MARGIN + WIDTH}px)`
                  : 'translateX(0)',
                height: '100%',
                ...transition,
                animation: !booking.outbound.ticket ? 'none' : undefined,
              }}
            >
              <Col
                className="flight-ticket"
                style={{
                  marginRight: MARGIN,
                  width: WIDTH,
                  borderRadius: '4px',
                  color: 'white',
                  padding: '8px 12px',
                }}
              >
                <Typography className="text=bold" style={{ marginBottom: '8px' }}>
                  <FormattedMessage id="flight.outbound" />
                </Typography>
                <Divider style={{ background: 'white' }} />
                <Typography variant="body2" style={{ marginTop: '8px' }}>
                  <FormattedMessage id="flight.result.pleaseChooseOutboundTicket" />
                </Typography>
              </Col>
              {!booking.outbound.ticket ? (
                <PairDiv
                  fadeColor
                  firstDiv={
                    <div
                      style={{
                        marginRight: '10px',
                        width: '100%',
                        borderRadius: '4px',

                        color: 'white',
                        padding: '8px 12px',
                      }}
                    >
                      <Typography className="text=bold" style={{ marginBottom: '8px' }}>
                        <FormattedMessage id="flight.inbound" />
                      </Typography>
                      <Divider style={{ background: 'white' }} />
                    </div>
                  }
                  secondDiv={<div />}
                />
              ) : (
                <PairDiv
                  firstDiv={
                    <div
                      style={{
                        marginRight: '10px',
                        width: '100%',
                        borderRadius: '4px',
                        color: 'white',
                        padding: '8px 12px',
                      }}
                    >
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'space-between',
                          marginBottom: '8px',
                        }}
                      >
                        <Typography className="text=bold" style={{ display: 'flex' }}>
                          <FormattedMessage id="flight.outbound" />
                          &nbsp;-&nbsp;
                          {outbound
                            ? moment(outbound.departureDayStr, DATE_FORMAT).format('L')
                            : ''}
                        </Typography>
                        <div style={{ display: 'flex' }}>
                          <BoxLogoAirline
                            style={{ width: 'auto', paddingTop: '4px', paddingRight: '4px' }}
                          >
                            <img
                              alt=""
                              style={{ height: '14px' }}
                              src={outbound ? getFlightGeneralInfo(outbound.aid).logo : ''}
                            />
                          </BoxLogoAirline>
                          {outbound ? getFlightGeneralInfo(outbound.aid).name : ''}
                        </div>
                      </div>
                      <Divider style={{ background: 'white' }} />
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'column',
                          marginTop: '8px',
                          justifyContent: 'space-between',
                          height: '50px',
                        }}
                      >
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                          <div style={{ display: 'flex' }}>
                            <Typography variant="body2" style={{ width: '52px' }}>
                              {outbound?.departureTimeStr}
                            </Typography>
                            <Typography variant="body2">
                              {outbound?.departureCity}
                              &nbsp;&#40;
                              {outbound?.departureAirport}
                              &#41;
                            </Typography>
                          </div>
                          <div style={{ display: 'flex' }}>
                            <Typography variant="body2">
                              <FormattedMessage id="flight.travellerInfo.seatClass" />
                              :&nbsp;
                              {seatClass &&
                                (intl.locale.startsWith(EN_US)
                                  ? seatClass.v_name
                                  : seatClass.e_name)}
                            </Typography>
                          </div>
                        </div>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                          <div style={{ display: 'flex' }}>
                            <Typography variant="body2" style={{ width: '52px' }}>
                              {outbound?.arrivalTimeStr}
                            </Typography>
                            <Typography variant="body2">
                              {outbound?.arrivalCity}
                              &nbsp;&#40;
                              {outbound?.arrivalAirport}
                              &#41;
                            </Typography>
                          </div>
                          <div style={{ display: 'flex' }}>
                            <FormattedMessage id="flight.flyDuration" />
                            :&nbsp;
                            {outbound ? durationMillisecondToHour(outbound.duration) : ''}
                          </div>
                        </div>
                      </div>
                    </div>
                  }
                  secondDiv={
                    <Col style={{ alignItems: 'center' }}>
                      <Typography
                        variant="body2"
                        style={{
                          padding: '12px 0px',
                        }}
                      >
                        <FormattedMessage id="flight.oneWayPrice" />
                        &nbsp;
                        <Typography variant="subtitle2" component="span">
                          {booking.outbound.ticket?.outbound?.ticketdetail.priceAdultUnit ? (
                            <FormattedNumber
                              value={booking.outbound.ticket.outbound?.ticketdetail.priceAdultUnit}
                            />
                          ) : (
                            '---'
                          )}
                          &nbsp;
                          <FormattedMessage id="currency" />
                        </Typography>
                      </Typography>
                      <WhiteOutlineButton
                        className="flight-ticket"
                        variant="outlined"
                        style={{ background: 'unset' }}
                        onClick={() => {
                          clearOutBound();
                        }}
                      >
                        <FormattedMessage id="flight.changeFlight" />
                      </WhiteOutlineButton>
                    </Col>
                  }
                />
              )}
            </SlideLeft>
            <SlideLeft
              style={{
                width: WIDTH,
                height: '108px',
                border: `1px solid ${GREY_500}`,
                borderRadius: '4px',
                marginLeft: MARGIN,
                position: 'absolute',
                right: `-${WIDTH + MARGIN}px`,
                transform: booking.outbound.ticket
                  ? `translateX(-${MARGIN + WIDTH}px)`
                  : 'translateX(0)',
                animation: !booking.outbound.ticket ? 'none' : undefined,
                ...transition,
                background: WHITE,
                padding: '8px 12px',
              }}
            >
              <Typography className="text=bold" style={{ marginBottom: '8px' }}>
                <FormattedMessage id="flight.inbound" />
              </Typography>
              <Divider />
              <Typography variant="body2" style={{ marginTop: '8px' }}>
                <FormattedMessage id="flight.pleaseChooseInboundTicket" />
              </Typography>
            </SlideLeft>
          </div>
          {booking.outbound.ticket ? (
            <>
              {inboundTickets.map((ticket: some) => (
                <FlightTicketItem
                  showFarePrice={filterParams.isFarePrice}
                  getFlightGeneralInfo={getFlightGeneralInfo}
                  getTicketClass={getFlightGeneralClasses}
                  key={ticket.tid}
                  ticket={ticket}
                  onClick={() => {
                    setInBound(
                      ticket,
                      data.requestId,
                      paramsSearch
                        ? {
                            numAdults: paramsSearch.travellerCountInfo.adultCount,
                            numChildren: paramsSearch.travellerCountInfo.childCount,
                            numInfants: paramsSearch.travellerCountInfo.infantCount,
                          }
                        : {},
                    );
                  }}
                  priceRemark={
                    <Typography variant="caption">
                      <FormattedMessage id="flight.inboundPrice" />
                    </Typography>
                  }
                />
              ))}
              {searchCompleted < 99.9 && (
                <div
                  style={{
                    height: '200px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <LoadingIcon />
                </div>
              )}
              {maxDisplayInbound < filteredInboundTickets.length && (
                <div
                  style={{ textAlign: 'center', color: BLUE, lineHeight: '24px', margin: '20px 0' }}
                >
                  <Button
                    onClick={() => {
                      if (maxDisplayInbound < filteredInboundTickets.length) {
                        setMaxDisplayInbound(maxDisplayInbound + STEP);
                      }
                    }}
                  >
                    <Typography variant="body2" style={{ color: BLUE }}>
                      <FormattedMessage
                        id="flight.result.displayMore"
                        values={{
                          num: filteredInboundTickets.length - maxDisplayInbound,
                        }}
                      />
                    </Typography>
                  </Button>
                </div>
              )}
            </>
          ) : (
            <>
              {outboundTickets.map((ticket: some) => (
                <FlightTicketItem
                  showFarePrice={filterParams.isFarePrice}
                  getFlightGeneralInfo={getFlightGeneralInfo}
                  getTicketClass={getFlightGeneralClasses}
                  key={ticket.tid}
                  ticket={ticket}
                  buttonMsgId="flight.result.selectTicket"
                  onClick={() => {
                    setOutBound(
                      ticket,
                      data.requestId,
                      paramsSearch
                        ? {
                            numAdults: paramsSearch.travellerCountInfo.adultCount,
                            numChildren: paramsSearch.travellerCountInfo.childCount,
                            numInfants: paramsSearch.travellerCountInfo.infantCount,
                          }
                        : {},
                    );
                  }}
                  priceRemark={
                    <Typography variant="caption">
                      <FormattedMessage id="flight.outboundPrice" />
                    </Typography>
                  }
                />
              ))}
              {searchCompleted < 99.9 && (
                <div
                  style={{
                    height: '200px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <LoadingIcon />
                </div>
              )}
              {maxDisplayOutbound < filteredOutboundTickets.length && (
                <div style={{ textAlign: 'center', lineHeight: '24px', margin: '20px 0' }}>
                  <Button
                    onClick={() => {
                      if (maxDisplayOutbound < filteredOutboundTickets.length) {
                        setMaxDisplayOutbound(maxDisplayOutbound + STEP);
                      }
                    }}
                  >
                    <Typography variant="body2" style={{ color: BLUE }}>
                      <FormattedMessage
                        id="flight.result.displayMore"
                        values={{
                          num: filteredOutboundTickets.length - maxDisplayOutbound,
                        }}
                      />
                    </Typography>
                  </Button>
                </div>
              )}
            </>
          )}
        </>
      )}
    </div>
  );
};

export default FlightResultBoxTwoWay;
