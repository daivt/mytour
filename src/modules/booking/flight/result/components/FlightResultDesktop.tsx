import { Button, Container, LinearProgress, useMediaQuery, useTheme } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { Col } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import AsideBound from '../../../common/AsideBound';
import { FlightInfo } from '../../booking/utils';
import FlightSearch from '../../common/FlightSearch';
import FlightTicketDialog from '../../common/FlightTicketDialog';
import { FlightBookingResult, FlightFilterParams, FlightSearchParams } from '../../utils';
import { stringifyParams } from '../utils';
import FlightFilter from './FlightFilter';
import FlightResultBoxOneWay from './FlightResultBoxOneWay';
import FlightResultBoxTwoWay from './FlightResultBoxTwoWay';

interface Props {
  paramsSearch: FlightSearchParams;
  setParamsSearch(paramsSearch: FlightSearchParams): void;
  filterParams: FlightFilterParams;
  setFilterParams(filterParams: FlightFilterParams): void;
  resetFilter(): void;
  booking: FlightBookingResult;
  setOutBound(val: some, requestId: number, req: some): void;
  setInBound(val: some, requestId: number, req: some): void;
  clearOutBound(): void;
  clearInBound(): void;
  data?: some;
  searchCompleted: number;
}

const FlightResultDesktop: React.FC<Props> = props => {
  const {
    paramsSearch,
    setParamsSearch,
    filterParams,
    setFilterParams,
    resetFilter,
    data,
    searchCompleted,
    booking,
    setOutBound,
    setInBound,
    clearOutBound,
    clearInBound,
  } = props;

  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));

  const closeDialog = React.useCallback(() => {
    if (booking.inbound.ticket) {
      clearInBound();
    } else {
      clearOutBound();
    }
  }, [booking.inbound.ticket, clearInBound, clearOutBound]);

  return (
    <>
      <FlightSearch
        params={paramsSearch}
        onSearch={setParamsSearch}
        hiddenRecent
        isTablet={isTablet}
      />
      <LinearProgress variant="determinate" value={searchCompleted} color="secondary" />
      <Col style={{ alignItems: 'center' }}>
        <Container style={{ flex: 1, display: 'flex', padding: 0 }}>
          <AsideBound
            isTablet={isTablet}
            style={{ width: isTablet ? 320 : 270, padding: isTablet ? 0 : '24px 0px' }}
          >
            <FlightFilter
              resetFilter={resetFilter}
              filterParams={filterParams}
              setFilterParams={setFilterParams}
              data={data}
              booking={booking}
            />
          </AsideBound>

          <div
            style={{
              flex: 1,
              minHeight: '400px',
              padding: 24,
            }}
          >
            {data?.outbound ? (
              <FlightResultBoxTwoWay
                data={data}
                paramsSearch={paramsSearch}
                filterParams={filterParams}
                booking={booking}
                setFilterParams={setFilterParams}
                setOutBound={setOutBound}
                setInBound={setInBound}
                clearOutBound={clearOutBound}
                searchCompleted={searchCompleted}
              />
            ) : (
              <FlightResultBoxOneWay
                data={data}
                paramsSearch={paramsSearch}
                filterParams={filterParams}
                setFilterParams={setFilterParams}
                setOutBound={setOutBound}
                searchCompleted={searchCompleted}
              />
            )}
          </div>
        </Container>
        <FlightTicketDialog
          close={closeDialog}
          open={
            (paramsSearch.one_way && !!booking.outbound.ticket) ||
            (!paramsSearch.one_way && !!booking.inbound.ticket)
          }
          button={
            <Link
              to={{
                pathname: ROUTES.booking.flight.bookingInfo,
                search: stringifyParams(booking.tid),
              }}
            >
              <Button
                id="flightResult.continue"
                color="secondary"
                variant="contained"
                size="large"
                style={{ minWidth: 160 }}
                disableElevation
              >
                <FormattedMessage id="continue" />
              </Button>
            </Link>
          }
          booking={booking as FlightInfo}
        />
      </Col>
    </>
  );
};

export default FlightResultDesktop;
