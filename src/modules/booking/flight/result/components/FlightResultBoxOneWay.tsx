/* eslint-disable no-nested-ternary */
import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallowEqual, useSelector } from 'react-redux';
import { BLUE } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { FlightFilterParams, FlightSearchParams, FLIGHT_SORT_BY } from '../../utils';
import { filterAndSort, flightCheapestCmp, flightFastestCmp, flightTimeTakeOffCmp } from '../utils';
import FlightResultHeader from './FlightResultHeader';
import { FlightSortBox } from './FlightSortBox';
import FlightTicketItem from './FlightTicketItem';

const STEP = 10;

interface Props {
  paramsSearch: FlightSearchParams;
  filterParams: FlightFilterParams;
  setFilterParams(filterParams: FlightFilterParams): void;
  setOutBound(val: some, requestId: number, req: some): void;
  data?: some;
  searchCompleted: number;
}

const FlightResultBoxOneWay: React.FC<Props> = props => {
  const [maxDisplay, setDisplay] = React.useState(STEP);
  const { data, paramsSearch, filterParams, setFilterParams, setOutBound, searchCompleted } = props;
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight, shallowEqual);

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );
  const getFlightGeneralClasses = React.useCallback(
    (code: string) => {
      return generalFlight?.ticketclass?.find((v: some) => v.code === code) || {};
    },
    [generalFlight],
  );

  const filteredTickets = React.useMemo(() => {
    const { sortBy } = filterParams;
    return filterAndSort(
      data?.tickets as some[],
      filterParams,
      sortBy === FLIGHT_SORT_BY.cheapest
        ? flightCheapestCmp
        : sortBy === FLIGHT_SORT_BY.fastest
        ? flightFastestCmp
        : sortBy === FLIGHT_SORT_BY.takeOff
        ? flightTimeTakeOffCmp
        : undefined,
    );
  }, [data, filterParams]);

  const tickets = filteredTickets?.filter((ticket: some, i: number) => i < maxDisplay);

  return (
    <>
      <FlightResultHeader count={filteredTickets?.length || 0} paramsSearch={paramsSearch} />
      <FlightSortBox filterParams={filterParams} setFilterParams={setFilterParams} />
      {data &&
        tickets.map(ticket => (
          <FlightTicketItem
            showFarePrice={filterParams.isFarePrice}
            getFlightGeneralInfo={getFlightGeneralInfo}
            getTicketClass={getFlightGeneralClasses}
            key={ticket.tid}
            ticket={ticket}
            onClick={() =>
              setOutBound(
                ticket,
                data.requestId,
                paramsSearch
                  ? {
                      numAdults: paramsSearch.travellerCountInfo.adultCount,
                      numChildren: paramsSearch.travellerCountInfo.childCount,
                      numInfants: paramsSearch.travellerCountInfo.infantCount,
                    }
                  : {},
              )
            }
            priceRemark={
              <Typography variant="caption">
                <FormattedMessage id="flight.oneWayPrice" />
              </Typography>
            }
          />
        ))}
      {searchCompleted < 99.9 && (
        <div
          style={{
            height: '200px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      )}
      {maxDisplay < filteredTickets?.length && (
        <div style={{ textAlign: 'center', color: BLUE, lineHeight: '24px', margin: '20px 0' }}>
          <Button
            onClick={() => {
              if (maxDisplay < filteredTickets.length) {
                setDisplay(maxDisplay + STEP);
              }
            }}
          >
            <Typography variant="body2" style={{ color: BLUE }}>
              <FormattedMessage
                id="flight.result.displayMore"
                values={{ num: filteredTickets.length - maxDisplay }}
              />
            </Typography>
          </Button>
        </div>
      )}
    </>
  );
};

export default FlightResultBoxOneWay;
