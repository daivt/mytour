import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { some } from '../../../../../constants';
import { AntSwitchLarge, Row } from '../../../../common/components/elements';
import SingleSelect from '../../../../common/components/SingleSelect';
import { FlightFilterParams } from '../../utils';
import { FLIGHT_SORT_BY_DATA } from '../constants';

export interface Props {
  filterParams: FlightFilterParams;
  setFilterParams(filterParams: FlightFilterParams): void;
}

export const FlightSortBox: React.FC<Props> = props => {
  const { filterParams, setFilterParams } = props;
  const intl = useIntl();

  return (
    <Row style={{ marginTop: 24, marginBottom: -20 }}>
      <Row style={{ flex: 1 }}>
        <Typography
          variant="body2"
          color="textSecondary"
          style={{ marginBottom: 20, marginRight: 16 }}
        >
          <FormattedMessage id="flight.result.sortBy" />:
        </Typography>
        <SingleSelect
          id="Flight_listing.Flight_Filter"
          value={filterParams.sortBy}
          formControlStyle={{ width: 180, minWidth: 180 }}
          onSelectOption={(value: any | null) => {
            setFilterParams({ ...filterParams, sortBy: value as string });
          }}
          getOptionLabel={(v: some) => intl.formatMessage({ id: v.name })}
          options={FLIGHT_SORT_BY_DATA}
          optional
        />
      </Row>
      <Row style={{ marginBottom: 20 }}>
        <Typography variant="body2" color="textSecondary" style={{ marginRight: 16 }}>
          <FormattedMessage id="flight.filter.sortBy" />
        </Typography>
        <AntSwitchLarge
          id="Flight_listing.Flight_total_price"
          checked={!filterParams.isFarePrice}
          onClick={(e: any) => {
            const checked = !filterParams.isFarePrice;
            setFilterParams({
              ...filterParams,
              isFarePrice: checked,
              price: [
                0,
                (checked ? filterParams.filters?.maxFarePrice : filterParams.filters?.maxPrice) ||
                  1,
              ],
            });
          }}
        />
      </Row>
    </Row>
  );
};
