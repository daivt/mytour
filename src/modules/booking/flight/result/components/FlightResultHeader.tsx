import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREY } from '../../../../../configs/colors';
import { DATE_FORMAT } from '../../../../../models/moment';
import { FlightBookingResult, FlightSearchParams } from '../../utils';

export interface Props {
  count: number;
  booking?: FlightBookingResult;
  paramsSearch: FlightSearchParams;
}

const FlightResultHeader: React.FC<Props> = props => {
  const { paramsSearch, booking, count } = props;

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        flex: 1,
        paddingRight: '8px',
      }}
    >
      {!booking?.outbound.ticket ? (
        <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
          <Typography className="text=bold" variant="h5">
            {paramsSearch.origin
              ? `${paramsSearch.origin.name} (${paramsSearch.origin.code}) - `
              : ''}
            {paramsSearch.destination
              ? `${paramsSearch.destination.name} (${paramsSearch.destination.code}):`
              : ''}
            &nbsp;
          </Typography>
          <Typography className="text=bold" variant="h5">
            <FormattedMessage id="flight.result.totalResult" values={{ num: count }} />
          </Typography>
        </div>
      ) : (
        <div style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
          <Typography className="text=bold" variant="h5">
            {paramsSearch.destination
              ? `${paramsSearch.destination.name} (${paramsSearch.destination.code}) - `
              : ''}
            {paramsSearch.origin
              ? `${paramsSearch.origin.name} (${paramsSearch.origin.code}): `
              : ''}
            &nbsp;
          </Typography>
          <Typography className="text=bold" variant="h5">
            <FormattedMessage id="flight.result.totalResult" values={{ num: count }} />
          </Typography>
        </div>
      )}
      <div style={{ display: 'flex', alignItems: 'center', marginTop: 8 }}>
        <Typography variant="body2">
          {moment(paramsSearch.departureDate, DATE_FORMAT).format('L')}
          {paramsSearch.returnDate
            ? ` - ${moment(paramsSearch.returnDate, DATE_FORMAT).format('L')}`
            : ''}
        </Typography>
        <Typography
          style={{
            width: '4px',
            height: '4px',
            borderRadius: '50%',
            background: GREY,
            margin: '0 6px',
          }}
        />
        <Typography variant="body2">
          <FormattedMessage
            id="flight.result.totalPassengers"
            values={{
              num:
                paramsSearch.travellerCountInfo.adultCount +
                paramsSearch.travellerCountInfo.childCount +
                paramsSearch.travellerCountInfo.infantCount,
            }}
          />
        </Typography>
      </div>
    </div>
  );
};
export default FlightResultHeader;
