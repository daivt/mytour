import { Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { GREY_500, PRIMARY, ORANGE_500 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { DATE_FORMAT } from '../../../../../models/moment';
import { ReactComponent as IconBaggage } from '../../../../../svg/booking/ic_baggage.svg';
import { ReactComponent as IconRefresh } from '../../../../../svg/booking/ic_flight_change_time.svg';
import { ReactComponent as IconAirPlaneDown } from '../../../../../svg/booking/ic_flight_down.svg';
import { ReactComponent as IconTicketBack } from '../../../../../svg/booking/ic_flight_ticket_back.svg';
import { ReactComponent as IconAirPlaneUp } from '../../../../../svg/booking/ic_flight_up.svg';
import { ReactComponent as IconClock } from '../../../../../svg/booking/ic_time.svg';
import { ReactComponent as IconVoid } from '../../../../../svg/booking/ic_void.svg';
import { Col, Row } from '../../../../common/components/elements';
import { durationMillisecondToHour } from '../../../utils';
import { LONG_TRANSIT_DURATION, TECHNICAL_STOP } from '../constants';
import { BoxInfoFlight, BoxLogoAirline } from './FlightTicketItem.style';

const LongTransitMessage = (props: { from: string; duration: string }) => {
  const { from, duration } = props;
  return (
    <div style={{ paddingLeft: '128px' }}>
      <div
        style={{
          margin: 'auto',
          width: '710px',
          display: 'flex',
          alignItems: 'center',
          background: '#F5F5F5',
          height: '47px',
        }}
      >
        <div style={{ width: '116px', textAlign: 'center' }}>
          <Typography variant="body1" color="textSecondary">
            {duration}
          </Typography>
        </div>
        <Typography variant="body2">
          <FormattedMessage id="flight.result.flightFrom" values={{ from }} />
        </Typography>
        &nbsp;-&nbsp;
        <Typography variant="caption" color="textSecondary">
          <FormattedMessage id="flight.result.longTransitRecheckin" />
        </Typography>
      </div>
    </div>
  );
};

function getPolicyIcon(code: string) {
  if (code === 'LUGGAGE_INFO') {
    return <IconBaggage style={{ height: '24px', width: 'auto', marginRight: '4px' }} />;
  }

  if (code === 'CHANGE_ROUTE') {
    return <IconRefresh style={{ height: '24px', width: 'auto', marginRight: '4px' }} />;
  }
  if (code === 'VOID_TICKET') {
    return <IconVoid style={{ height: '24px', width: 'auto', marginRight: '4px' }} />;
  }

  return <IconTicketBack style={{ height: '24px', width: 'auto', marginRight: '4px' }} />;
}

const Policy: React.FC<{ item: some }> = props => {
  const { item } = props;
  return (
    <div style={{ display: 'flex', minWidth: '125px', alignItems: 'flex-start', marginBottom: 8 }}>
      <div style={{ flexShrink: 0 }}>{getPolicyIcon(item.code)}</div>
      <Typography>
        <Typography
          variant="body2"
          component="span"
          style={{ color: item.code === 'VOID_TICKET' ? ORANGE_500 : 'textPrimary' }}
        >
          {item.description}
        </Typography>
        {item.code === TECHNICAL_STOP && (
          <Typography color="error" variant="subtitle2" component="span">
            &nbsp;
            <FormattedMessage id="result.needVisa" />
          </Typography>
        )}
      </Typography>
    </div>
  );
};

interface Props {
  ticket: some;
  airlineInfo: some;
  isTransit: boolean;
  onTicket?: boolean;
}

export const FlightTicketDetail: React.FC<Props> = props => {
  const { ticket, airlineInfo, isTransit, onTicket } = props;
  if (!ticket) {
    return <div />;
  }

  const flightTime = isTransit ? ticket.flightDuration : ticket.duration;

  const aircraft = ((ticket.ticketdetail || {}).facilities || []).find(
    (f: some) => f.code === 'AIRCRAFT_INFO',
  );

  return (
    <div
      style={{
        padding: 16,
        display: 'flex',
      }}
    >
      <BoxLogoAirline
        style={{ flex: 1, alignItems: onTicket ? 'flex-start' : undefined, marginRight: -20 }}
      >
        {onTicket ? (
          <>
            <Typography variant="subtitle2">
              {airlineInfo.name}&nbsp;-&nbsp;
              <Typography variant="body2" component="span">
                {ticket.flightNumber}
              </Typography>
            </Typography>
          </>
        ) : (
          <>
            <img alt="" style={{ height: '32px' }} src={airlineInfo.logo} />
            <Typography variant="body2">{ticket.flightNumber}</Typography>
          </>
        )}
        <Typography variant="body2">{aircraft && aircraft.description}</Typography>
      </BoxLogoAirline>
      <BoxInfoFlight style={{ flex: 1.5 }}>
        <Col
          style={{
            alignItems: 'center',
            height: '85%',
            justifyContent: 'space-between',
            marginRight: 8,
          }}
        >
          <IconAirPlaneUp style={{ height: 'auto', width: '42px' }} />
          <div style={{ borderLeft: `1px solid ${GREY_500}`, height: '100%' }} />
          <IconAirPlaneDown style={{ height: 'auto', width: '42px' }} />
        </Col>
        <Col
          style={{
            height: '100%',
            justifyContent: 'space-between',
          }}
        >
          <div style={{ display: 'flex' }}>
            <Col style={{ minWidth: 80, marginRight: 8 }}>
              <Typography variant="subtitle1">
                <span>{ticket.departureTimeStr}</span>
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {moment(ticket.departureDayStr, DATE_FORMAT).format('L')}
              </Typography>
            </Col>
            <Col>
              <Typography variant="subtitle1">
                {ticket.departureCity}
                &nbsp;&#40;
                {ticket.departureAirport}
                &#41;
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {ticket.departureAirportName}
              </Typography>
            </Col>
          </div>
          <Row style={{ padding: '8px 0px' }}>
            <IconClock
              className="svgFillAll"
              style={{ width: 20, height: 'auto', marginRight: 8, stroke: PRIMARY }}
            />
            <Typography variant="subtitle2" color="primary">
              {durationMillisecondToHour(flightTime)}
            </Typography>
          </Row>
          <div style={{ display: 'flex' }}>
            <Col style={{ minWidth: 80, marginRight: 8 }}>
              <Typography variant="subtitle1">
                <span>{ticket.arrivalTimeStr}</span>
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {moment(ticket.arrivalDayStr, DATE_FORMAT).format('L')}
              </Typography>
            </Col>
            <Col>
              <Typography variant="subtitle1">
                {ticket.arrivalCity}
                &nbsp;&#40;
                {ticket.arrivalAirport}
                &#41;
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {ticket.arrivalAirportName}
              </Typography>
            </Col>
          </div>
        </Col>
      </BoxInfoFlight>
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          padding: '10px 5px',
          flex: 1.5,
          flexFlow: 'column',
        }}
      >
        {isTransit &&
          ticket.polices &&
          ticket.polices.map((item: some, index: number) => <Policy item={item} key={index} />)}

        {!isTransit &&
          ticket.ticketdetail.polices &&
          ticket.ticketdetail.polices.map((item: some, index: number) => (
            <Policy item={item} key={index} />
          ))}
      </div>
    </div>
  );
};

export const FlightDirectionDetail = (props: {
  ticket: some;
  getFlightGeneralInfo(id: number): some;
  onTicket?: boolean;
}) => {
  const { ticket, getFlightGeneralInfo, onTicket } = props;
  return ticket.outbound.transitTickets ? (
    ticket.outbound.transitTickets.map((transitTicket: some, index: number) => (
      <React.Fragment key={index}>
        {transitTicket.transitDuration > LONG_TRANSIT_DURATION && (
          <LongTransitMessage
            from={`${transitTicket.departureCity} (${transitTicket.departureAirport})`}
            duration={durationMillisecondToHour(transitTicket.transitDuration)}
          />
        )}
        <FlightTicketDetail
          ticket={transitTicket}
          isTransit
          airlineInfo={getFlightGeneralInfo(transitTicket.aid)}
          onTicket={onTicket}
        />
      </React.Fragment>
    ))
  ) : (
    <FlightTicketDetail
      ticket={ticket.outbound}
      isTransit={false}
      airlineInfo={getFlightGeneralInfo(ticket.outbound.aid)}
      onTicket={onTicket}
    />
  );
};
