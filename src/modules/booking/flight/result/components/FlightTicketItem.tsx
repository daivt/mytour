import { Button, Collapse, Paper, Tooltip, Typography, withStyles } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, FormattedNumber, injectIntl, WrappedComponentProps } from 'react-intl';
import styled from 'styled-components';
import { BLUE_300, GREEN, GREY_100 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import { DATE_FORMAT } from '../../../../../models/moment';
import { ReactComponent as IconCoin } from '../../../../../svg/booking/ic_coin.svg';
import { ReactComponent as IconSeat } from '../../../../../svg/booking/ic_seat.svg';
import { ReactComponent as IconClock } from '../../../../../svg/booking/ic_time.svg';
import { ReactComponent as IconArrowRight } from '../../../../../svg/ic_arrow_right.svg';
import { Col } from '../../../../common/components/elements';
import { durationMillisecondToHour } from '../../../utils';
import { getStops } from '../utils';
import { FlightDirectionDetail } from './FlightTicketDetail';
import { BoxInfoFlight, BoxLogoAirline } from './FlightTicketItem.style';
import { EN_US } from '../../../../../models/intl';

const PartnershipBadge = styled.div`
  display: flex;
  align-items: center;
  max-width: fit-content;
  background: ${BLUE_300};
  overflow: hidden;
  padding: 0 8px;
  border-radius: 4px 0px 4px 0px;
`;
const Row = styled.div`
  display: flex;
`;

const HtmlTooltip = withStyles(theme => ({
  tooltip: {
    fontSize: '14px',
    padding: '8px',
  },
}))(Tooltip);

interface Props extends WrappedComponentProps {
  showFarePrice: boolean;
  ticket: some;
  airlines?: some[];
  onClick(): void;
  buttonMsgId?: string;
  getFlightGeneralInfo(id: number): some;
  getTicketClass: (classCode: string) => some | undefined;
  priceRemark?: JSX.Element;
}

const FlightTicketItem: React.FC<Props> = props => {
  const {
    ticket,
    getFlightGeneralInfo,
    getTicketClass,
    intl,
    buttonMsgId,
    onClick,
    showFarePrice,
  } = props;
  const [showInfo, setShowInfo] = React.useState(false);

  const renderPartnershipPolicies = React.useCallback((data?: some) => {
    if (!data) {
      return '';
    }
    if (data?.transitTickets) {
      return (
        <Col>
          {/* {data?.transitTickets[0].polices[0]?.description} */}
          {data?.transitTickets[0].polices.map((val: some, index: number) => {
            // if (index === 0) return null;
            if (val?.code === 'CHANGE_ROUTE') return null;
            if (val?.code === 'VOID_TICKET') return null;
            return (
              <Row key={index} style={{ padding: '4px 0px' }}>
                <Typography variant="caption">-&nbsp;{val.description}</Typography>
              </Row>
            );
          })}
        </Col>
      );
    }
    return (
      <Col>
        <Row style={{ padding: '4px 0px' }}>
          <Typography variant="body2">{data?.ticketdetail.polices?.[0]?.description}</Typography>
        </Row>
        {data?.ticketdetail.polices?.map((val: some, index: number) => {
          if (index === 0) return null;
          if (val?.code === 'CHANGE_ROUTE') return null;
          if (val?.code === 'VOID_TICKET') return null;
          return (
            <Row key={index} style={{ padding: '4px 0px' }}>
              <Typography variant="caption">-&nbsp;{val.description}</Typography>
            </Row>
          );
        })}
      </Col>
    );
  }, []);

  const flightTime = ticket.outbound.duration;
  const { bonusPoint } = ticket.outbound.ticketdetail;

  const arrivalDay = moment(ticket.outbound.arrivalDayStr, DATE_FORMAT);
  const departureDay = moment(ticket.outbound.departureDayStr, DATE_FORMAT);
  const diffDay = arrivalDay.diff(departureDay, 'days');

  const ticketClass = getTicketClass(ticket.outbound.ticketdetail.ticketClassCode);

  const discountPrice = ticket.outbound.ticketdetail.discountAdultUnit;
  const { farePrice, priceAdultUnit } = ticket.outbound.ticketdetail;
  const totalPriceAfterDiscount = ticket.outbound.ticketdetail.priceAdultUnit + discountPrice;

  if (!ticket || !ticket.outbound) {
    return <div />;
  }

  return (
    <Paper
      variant="outlined"
      elevation={2}
      style={{ marginTop: 16, borderColor: showInfo ? BLUE_300 : undefined }}
    >
      {ticket.outbound.operatingAirlineId &&
        ticket.outbound.operatingAirlineId !== ticket.outbound.aid && (
          <div style={{ height: 12 }}>
            <HtmlTooltip title={renderPartnershipPolicies(ticket.outbound)} arrow>
              <PartnershipBadge>
                <Typography variant="caption">
                  <FormattedMessage id="flight.result.partnershipAction" />
                  &nbsp;
                  {getFlightGeneralInfo(ticket.outbound.operatingAirlineId).name}&nbsp;
                </Typography>
                <img
                  alt=""
                  style={{ width: '22px' }}
                  src={getFlightGeneralInfo(ticket.outbound.operatingAirlineId).logo}
                />
              </PartnershipBadge>
            </HtmlTooltip>
          </div>
        )}
      <Row style={{ padding: 16, paddingBottom: 0 }}>
        <Col style={{ flex: 1 }}>
          <Row style={{ flex: 1 }}>
            <BoxLogoAirline style={{ alignItems: 'start', flex: 1 }}>
              <img
                alt=""
                style={{ height: '32px' }}
                src={getFlightGeneralInfo(ticket.outbound.aid).logo}
              />
              <Typography variant="body2" style={{ marginTop: 8 }}>
                {getFlightGeneralInfo(ticket.outbound.aid).name}&nbsp;
                {ticket.outbound.flightNumber && <>-&nbsp;{ticket.outbound.flightNumber}</>}
              </Typography>
            </BoxLogoAirline>
            <BoxInfoFlight style={{ flex: 1 }}>
              <Col
                style={{
                  textAlign: 'right',
                }}
              >
                <Typography variant="body1">{ticket.outbound.departureTimeStr}</Typography>
                <Typography variant="subtitle2">{ticket.outbound.departureAirport}</Typography>
              </Col>

              <Col
                style={{
                  flex: 1,
                  alignItems: 'center',
                }}
              >
                <IconArrowRight style={{ height: 24 }} />
                <Typography variant="body2">
                  {getStops(ticket)
                    .map((item, i) => item.code)
                    .join(',')}
                </Typography>
              </Col>
              <Col
                style={{
                  flex: 1,
                }}
              >
                <Typography variant="body1">
                  {ticket.outbound.arrivalTimeStr}
                  <Typography
                    variant="caption"
                    style={{
                      width: '32px',
                      paddingLeft: '9px',
                    }}
                    component="span"
                  >
                    {diffDay ? `(+${diffDay}d)` : ''}
                  </Typography>
                </Typography>
                <Typography variant="subtitle2">{ticket.outbound.arrivalAirport}</Typography>
              </Col>
            </BoxInfoFlight>
          </Row>
          <div>
            <Button
              variant="text"
              style={{ padding: '8px 0px' }}
              onClick={() => setShowInfo(!showInfo)}
            >
              <Typography variant="body2" color="textSecondary">
                <FormattedMessage id="flight.result.flightInfo" />
              </Typography>
              <ExpandMoreIcon
                color="disabled"
                style={{
                  transition: 'all 300ms',
                  transform: showInfo ? 'rotate(-180deg)' : 'rotate(0deg)',
                  marginRight: 8,
                  marginLeft: 8,
                }}
              />
            </Button>
          </div>
        </Col>
          <Row
            style={{
              flex: 1,
            }}
          >
            <Col
              style={{
                flex: 1,
                paddingLeft: 32,
              }}
            >
              <Row style={{ marginBottom: 8 }}>
                <IconClock style={{ width: '24px', height: 'auto', marginRight: '8px' }} />
                <Typography variant="body2" color="textSecondary">
                  {durationMillisecondToHour(flightTime)}
                </Typography>
              </Row>
              <Row
                style={{
                  marginBottom: 8,
                }}
              >
                <IconSeat style={{ width: '24px', height: 'auto', marginRight: '8px' }} />
                <Typography variant="body2" color="textSecondary">
                  {ticketClass
                    ? intl.locale.startsWith(EN_US)
                      ? ticketClass.v_name
                      : ticketClass.i_name
                    : ''}
                </Typography>
              </Row>
              {bonusPoint > 0 && (
                <Row>
                  <IconCoin style={{ width: '24px', height: 'auto', marginRight: '8px' }} />
                  <Typography variant="body2" style={{ color: GREEN }}>
                    <FormattedNumber value={bonusPoint} />
                    &nbsp;
                    <FormattedMessage id="point" />
                  </Typography>
                </Row>
              )}
            </Col>
            <Col
              style={{
                alignItems: 'flex-end',
                flex: 1,
              }}
            >
              {!!discountPrice && (
                <Typography
                  className="price"
                  variant="h6"
                  color="textSecondary"
                  style={{ textDecoration: 'line-through' }}
                >
                  <FormattedNumber value={totalPriceAfterDiscount} />
                  &nbsp; <FormattedMessage id="currency" />
                </Typography>
              )}

              <Typography className="price" variant="h5">
                {<FormattedNumber value={showFarePrice ? farePrice : priceAdultUnit} /> || '---'}
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
              {/* <Typography
                  variant="body2"
                  color="textSecondary"
                  style={{ marginTop: '-12px', marginBottom: '5px' }}
                >
                  {priceRemar16
                </Typography> */}
              <Button
                id="flight.bookTicket"
                style={{ width: '128px', marginTop: 16, marginBottom: 16 }}
                color="secondary"
                variant="contained"
                disableElevation
                onClick={onClick}
                disabled={!priceAdultUnit}
              >
                <FormattedMessage id={buttonMsgId || 'flight.result.bookTicket'} />
              </Button>
            </Col>
          </Row>
        </Row>
      <Collapse collapsedHeight="0px" in={showInfo}>
        <div style={{ margin: '0px 16px 16px', background: GREY_100 }}>
          <FlightDirectionDetail
            ticket={ticket}
            getFlightGeneralInfo={getFlightGeneralInfo}
            onTicket
          />
        </div>
      </Collapse>
    </Paper>
  );
};

export default injectIntl(FlightTicketItem);
