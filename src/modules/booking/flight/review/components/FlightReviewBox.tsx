import { Button, Grid, Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BLUE } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Col, Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import { goBackAction } from '../../../../common/redux/reducer';
import { ContactInfo, FlightInfo, TravellerInfo, InvoiceInfo } from '../../booking/utils';

// const Typography = (props: some) => (
//   <Typography variant="body2" variant="body2" style={{ ...props.style, flexShrink: 0 }}>
//     {props.children}
//   </Typography>
// );

interface IFlightReviewBoxProps {
  bookingInfo: FlightInfo;
}

const FlightReviewBox: React.FunctionComponent<IFlightReviewBoxProps> = props => {
  const { bookingInfo } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const intl = useIntl();

  const renderInfo = React.useCallback(
    (name: string, infos: TravellerInfo[], offset: number, data: FlightInfo) => {
      if (!data.outbound.ticket) {
        return false;
      }

      const outboundBag = data.outbound.extraBaggages;
      const inboundBag = data.inbound.extraBaggages;
      const ticketOutboundBag = data.outbound.ticket.outbound.baggages;
      let ticketInboundBag: some;
      if (data.inbound.ticket) {
        ticketInboundBag = data.inbound.ticket.outbound.baggages;
      }

      const extraBaggagesMsg = outboundBag.map((v, index) => ({
        out:
          ticketOutboundBag && outboundBag[offset + index] ? (
            <>
              {`${outboundBag[offset + index].name} - ${intl.formatNumber(
                outboundBag[offset + index]?.price,
              )}`}
              &nbsp;
              <FormattedMessage id="currency" />
            </>
          ) : (
            <FormattedMessage id="none" />
          ),
        in: ticketInboundBag ? (
          inboundBag && inboundBag[offset + index] ? (
            <>
              {`${inboundBag[offset + index]?.name} - ${intl.formatNumber(
                inboundBag[offset + index]?.price,
              )}`}
              &nbsp;
              <FormattedMessage id="currency" />
            </>
          ) : (
            <FormattedMessage id="none" />
          )
        ) : null,
      }));

      return infos.map((info, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <div key={index} style={{ marginBottom: 16 }}>
            <Typography
              className="text=bold"
              variant="h6"
              style={{
                height: '40px',
                flexBasis: '160px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              {index + 1 + offset}
              .&nbsp;
              <FormattedMessage id={name} values={{ num: infos.length !== 1 ? index + 1 : '' }} />
            </Typography>
            <Col className="card" style={{ width: '100%' }}>
              <Grid container spacing={2} style={{ padding: 16 }}>
                <Grid item xs={4}>
                  <Typography variant="subtitle2">
                    <FormattedMessage id="flight.fullName" />
                  </Typography>
                  <Typography variant="body2" style={{ marginTop: 8 }}>
                    {info.fullName}
                  </Typography>
                </Grid>
                <Grid item xs={4}>
                  <Typography variant="subtitle2">
                    <FormattedMessage id="gender" />:
                  </Typography>
                  <Typography variant="body2" style={{ marginTop: 8 }}>
                    <FormattedMessage id={info.gender === 'm' ? 'male' : 'female'} />
                  </Typography>
                </Grid>
                {((name === 'flight.adult' && !!info.passportInfo.passport) ||
                  name !== 'flight.adult') && (
                  <Grid item xs={4}>
                    <Typography variant="subtitle2">
                      <FormattedMessage id="birthday" />
                    </Typography>
                    <Typography variant="body2" style={{ marginTop: 8 }}>
                      {info.birthday}
                    </Typography>
                  </Grid>
                )}
                {!!info.passportInfo.passport && (
                  <>
                    <Grid item xs={4}>
                      <Typography variant="subtitle2">
                        <FormattedMessage id="flight.passport" />
                        :&nbsp;
                      </Typography>
                      <Typography variant="body2" style={{ marginTop: 8 }}>
                        {info.passportInfo.passport}
                      </Typography>
                    </Grid>
                    <Grid item xs={4}>
                      <Typography variant="subtitle2">
                        <FormattedMessage id="flight.passportExpired" />:
                      </Typography>
                      <Typography variant="body2" style={{ marginTop: 8 }}>
                        {info.passportInfo.passportExpiredDate}
                      </Typography>
                    </Grid>
                    <Grid item xs={4}>
                      <Typography variant="subtitle2">
                        <FormattedMessage id="flight.passportCountry" />
                        :&nbsp;
                      </Typography>
                      <Typography variant="body2" style={{ marginTop: 8 }}>
                        {info.passportInfo.passportCountry?.name}
                      </Typography>
                    </Grid>
                    <Grid item xs={4}>
                      <Typography variant="subtitle2">
                        <FormattedMessage id="flight.nationalityCountry" />
                        :&nbsp;
                      </Typography>
                      <Typography variant="body2" style={{ marginTop: 8 }}>
                        {info.passportInfo.nationalityCountry?.name}
                      </Typography>
                    </Grid>
                  </>
                )}
              </Grid>
              {name !== 'flight.infant' && (
                <Grid className="card-background" container style={{ padding: '16px' }}>
                  <Grid item xs={4}>
                    <Typography variant="subtitle2">
                      <FormattedMessage id="flight.outBaggages" />
                    </Typography>
                    <Typography variant="body2" style={{ marginTop: 8 }}>
                      {extraBaggagesMsg[index].out}
                    </Typography>
                  </Grid>
                  {extraBaggagesMsg[index].in && (
                    <Grid item xs={4}>
                      <Typography variant="subtitle2">
                        <FormattedMessage id="flight.inBaggages" />
                      </Typography>
                      <Typography variant="body2" style={{ marginTop: 8 }}>
                        {extraBaggagesMsg[index].in}
                      </Typography>
                    </Grid>
                  )}
                </Grid>
              )}
            </Col>
          </div>
        );
      });
    },
    [intl],
  );

  const renderContactInfo = React.useCallback((infos: ContactInfo) => {
    return (
      <div style={{ display: 'flex' }}>
        <Grid container spacing={2} style={{ flex: 2 }}>
          <Grid item xs={6}>
            <Typography variant="subtitle2">
              <FormattedMessage id="flight.fullName" />:
            </Typography>
            <Typography variant="body2" style={{ marginTop: 8 }}>
              {infos.fullName}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="subtitle2">
              <FormattedMessage id="gender" />:
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id={infos.gender === 'm' ? 'male' : 'female'} />
            </Typography>
          </Grid>

          <Grid item xs={6}>
            <Typography variant="subtitle2">
              <FormattedMessage id="phoneNumber" />
            </Typography>
            <Typography variant="body2" style={{ marginTop: 8 }}>
              {infos.telephone}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="body2">
              <FormattedMessage id="email" />:
            </Typography>
            <Typography variant="body2" style={{ color: `${BLUE}`, marginTop: 8 }}>
              {infos.email}
            </Typography>
          </Grid>
        </Grid>
        <div style={{ flex: 1 }}>
          <Typography variant="subtitle2">
            <FormattedMessage id="address" />
          </Typography>
          <Typography variant="body2">{infos.address}</Typography>
        </div>
      </div>
    );
  }, []);

  const renderInvoiceInfo = React.useCallback((infos: InvoiceInfo) => {
    return (
      <Grid container spacing={2} style={{ flex: 2 }}>
        <Grid item xs={4}>
          <Typography variant="subtitle2">
            <FormattedMessage id="flight.taxNumber" />:
          </Typography>
          <Typography variant="body2" style={{ marginTop: 8 }}>
            {infos.taxIdNumber}
          </Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="subtitle2">
            <FormattedMessage id="flight.companyName" />:
          </Typography>
          <Typography variant="body2" style={{ marginTop: 8 }}>
            {infos.companyName}
          </Typography>
        </Grid>

        <Grid item xs={4}>
          <Typography variant="subtitle2">
            <FormattedMessage id="flight.companyAddress" />:
          </Typography>
          <Typography variant="body2" style={{ marginTop: 8 }}>
            {infos.companyAddress}
          </Typography>
        </Grid>

        <Grid item xs={4}>
          <Typography variant="subtitle2">
            <FormattedMessage id="email" />:
          </Typography>
          <Typography variant="body2" style={{ marginTop: 8 }}>
            {infos.recipientEmail}
          </Typography>
        </Grid>

        <Grid item xs={4}>
          <Typography variant="subtitle2">
            <FormattedMessage id="flight.recipientAddress" />:
          </Typography>
          <Typography variant="body2" style={{ marginTop: 8 }}>
            {infos.recipientAddress}
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <Typography variant="subtitle2">
            <FormattedMessage id="flight.note" />:
          </Typography>
          <Typography variant="body2" style={{ marginTop: 8 }}>
            {infos.note}
          </Typography>
        </Grid>
      </Grid>
    );
  }, []);
  return (
    <Col style={{ flex: 1 }}>
      <Typography className="text=bold" variant="h6" style={{ marginBottom: 16 }}>
        <FormattedMessage id="flight.travellersInfo" />
      </Typography>
      <Paper variant="outlined" style={{ padding: 16 }}>
        {renderInfo('flight.adult', bookingInfo.travellersInfo.adults, 0, bookingInfo)}
        {renderInfo(
          'flight.children',
          bookingInfo.travellersInfo.children,
          bookingInfo.travellersInfo.adults.length,
          bookingInfo,
        )}
        {renderInfo(
          'flight.infant',
          bookingInfo.travellersInfo.babies,
          bookingInfo.travellersInfo.adults.length + bookingInfo.travellersInfo.children.length,
          bookingInfo,
        )}
      </Paper>

      <Typography className="text=bold" variant="h6" style={{ margin: '24px 0px 16px' }}>
        <FormattedMessage id="flight.contactInfo" />
      </Typography>
      <Paper variant="outlined" style={{ padding: 16 }}>
        {renderContactInfo(bookingInfo.contactInfo)}
      </Paper>
      {bookingInfo.exportInvoice && (
        <>
          <Typography className="text=bold" variant="h6" style={{ margin: '24px 0px 16px' }}>
            <FormattedMessage id="flight.invoiceInfo" />
          </Typography>
          <Paper variant="outlined" style={{ padding: 16 }}>
            {renderInvoiceInfo(bookingInfo.invoiceInfo)}
          </Paper>
        </>
      )}
      <Row style={{ justifyContent: 'center', marginTop: 24 }}>
        <Button
          id="flightReview.back"
          variant="outlined"
          size="large"
          style={{ marginRight: 16, minWidth: 160 }}
          onClick={() => dispatch(goBackAction())}
        >
          <FormattedMessage id="back" />
        </Button>
        <Link to={ROUTES.booking.flight.pay}>
          <Button
            id="flightReview.continue"
            variant="contained"
            color="secondary"
            disableElevation
            size="large"
            style={{ minWidth: 160 }}
          >
            <FormattedMessage id="continue" />
          </Button>
        </Link>
      </Row>
    </Col>
  );
};

export default FlightReviewBox;
