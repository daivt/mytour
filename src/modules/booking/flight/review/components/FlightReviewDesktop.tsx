import { Container, useMediaQuery, useTheme } from '@material-ui/core';
import React from 'react';
import AsideBound from '../../../common/AsideBound';
import { FlightInfo } from '../../booking/utils';
import FlightInfoBox from '../../common/FlightInfoBox';
import FlightTicketDialog from '../../common/FlightTicketDialog';
import FlightReviewBox from './FlightReviewBox';

interface Props {
  bookingInfo: FlightInfo;
}

const FlightReviewDesktop: React.FC<Props> = props => {
  const { bookingInfo } = props;
  const [seeDetail, setSeeDetail] = React.useState(false);
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <div>
      <Container style={{ flex: 1, display: 'flex', marginBottom: 32 }}>
        <FlightReviewBox bookingInfo={bookingInfo} />
        <AsideBound
          isTablet={isTablet}
          style={{ width: 360, marginLeft: isTablet ? 0 : 24 }}
          direction="right"
        >
          <FlightInfoBox seeDetail={() => setSeeDetail(true)} booking={bookingInfo} />
        </AsideBound>
      </Container>
      <FlightTicketDialog
        close={() => setSeeDetail(false)}
        open={seeDetail}
        booking={bookingInfo}
      />
    </div>
  );
};

export default FlightReviewDesktop;
