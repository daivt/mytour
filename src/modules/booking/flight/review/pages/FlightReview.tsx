import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { AppState } from '../../../../../redux/reducers';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import FlightReviewDesktop from '../components/FlightReviewDesktop';

interface Props {}

const FlightReview: React.FC<Props> = props => {
  const booingInfo = useSelector((state: AppState) => state.booking.flight.info, shallowEqual);
  if (!booingInfo) {
    return <RedirectDiv />;
  }
  return <FlightReviewDesktop bookingInfo={booingInfo} />;
};

export default FlightReview;
