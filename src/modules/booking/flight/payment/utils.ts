import moment from 'moment';
import { some } from '../../../../constants';
import { DATE_TIME_FORMAT } from '../../../../models/moment';
import { FlightInfo, TravellersInfo } from '../booking/utils';
import { OneDirection } from '../utils';

export interface FlightPayParams {
  point: number;
  usingPoint: boolean;
  fromMobile: boolean;
  promotionCode?: string;
  promotion?: some;
  selectedPaymentMethod?: some;
}

export const defaultFlightPayParams: FlightPayParams = {
  point: 0,
  usingPoint: false,
  fromMobile: false,
};

function genInsuranceInfo(outbound: OneDirection, inbound: OneDirection, insurancePackage?: some) {
  const departureOutboundDate = outbound.ticket
    ? moment(outbound.ticket.outbound.departureTime).format(DATE_TIME_FORMAT)
    : '';

  let arrivalOutboundDate = outbound.ticket
    ? moment(outbound.ticket.outbound.arrivalTime).format(DATE_TIME_FORMAT)
    : '';

  if (inbound.ticket) {
    arrivalOutboundDate = moment(inbound.ticket.outbound.arrivalTime).format(DATE_TIME_FORMAT);
  }
  return {
    insurancePackageCode: insurancePackage ? insurancePackage.code : '',
    fromDate: departureOutboundDate,
    toDate: arrivalOutboundDate,
  };
}

export function convertTravellersFull(
  info: TravellersInfo,
  outbound: OneDirection,
  inbound: OneDirection,
  insurancePackage?: some,
) {
  let value: some[] = [];
  // const needPassport = outbound.ticket ? outbound.ticket.outbound.ticketdetail.needPassport : false;
  value = value
    .concat(
      info?.adults?.map((one, i) => ({
        lastName: one.fullName.trim().split(' ')[0],
        firstName: one.fullName
          .trim()
          .split(' ')
          .slice(1)
          .join(' '),
        gender: one.gender.toUpperCase(),
        dob: one.birthday,
        outboundBaggageId: outbound.ticket ? outbound.extraBaggages[i]?.id : null,
        inboundBaggageId: inbound.ticket ? inbound.extraBaggages[i]?.id : null,
        insuranceInfo: insurancePackage
          ? genInsuranceInfo(outbound, inbound, insurancePackage)
          : null,
        passport: one.passportInfo.passport,
        passportExpiredDate: one.passportInfo.passportExpiredDate,
        passportCountryId: one.passportInfo.passportCountry?.id || null,
        nationalityCountryId: one.passportInfo.nationalityCountry?.id || null,
      })),
    )
    .concat(
      info?.children?.map((one, i) => ({
        lastName: one.fullName.trim().split(' ')[0],
        firstName: one.fullName
          .trim()
          .split(' ')
          .slice(1)
          .join(' '),
        gender: one.gender.toUpperCase(),
        dob: one.birthday,
        outboundBaggageId: outbound.ticket
          ? outbound.extraBaggages[info.adults.length + i]?.id
          : null,
        inboundBaggageId: inbound.ticket ? inbound.extraBaggages[info.adults.length + i]?.id : null,
        insuranceInfo: insurancePackage
          ? genInsuranceInfo(outbound, inbound, insurancePackage)
          : undefined,
        passport: one.passportInfo.passport,
        passportExpiredDate: one.passportInfo.passportExpiredDate,
        passportCountryId: one.passportInfo.passportCountry?.id || null,
        nationalityCountryId: one.passportInfo.nationalityCountry?.id || null,
      })),
    )
    .concat(
      info?.babies?.map(one => ({
        lastName: one.fullName.trim().split(' ')[0],
        firstName: one.fullName
          .trim()
          .split(' ')
          .slice(1)
          .join(' '),
        gender: one.gender.toUpperCase(),
        dob: one.birthday,
        insuranceInfo: insurancePackage
          ? genInsuranceInfo(outbound, inbound, insurancePackage)
          : undefined,
        passport: one.passportInfo.passport,
        passportExpiredDate: one.passportInfo.passportExpiredDate,
        passportCountryId: one.passportInfo.passportCountry?.id || null,
        nationalityCountryId: one.passportInfo.nationalityCountry?.id || null,
      })),
    );
  return value;
}

export function genFlightPayParams(booking: FlightInfo, params: FlightPayParams) {
  const {
    booker,
    tid,
    contactInfo,
    travellersInfo,
    inbound,
    outbound,
    buyInsurance,
    insurancePackage,
  } = booking;
  const { selectedPaymentMethod, usingPoint, point, promotionCode } = params;
  if (!tid || !contactInfo || !travellersInfo || !selectedPaymentMethod) {
    return {};
  }

  return {
    bookerContactId: booker?.id,
    contact: {
      addr1: contactInfo.address,
      email: contactInfo.email || 'hotro@mytour.vn',
      lastName: contactInfo.fullName.trim().split(' ')[0],
      firstName: contactInfo.fullName
        .trim()
        .split(' ')
        .slice(1)
        .join(' '),
      phone1: contactInfo.telephone,
      gender: contactInfo.gender.toUpperCase(),
      title: contactInfo.gender === 'f' ? 'Mrs' : 'Mr',
    },
    guests: convertTravellersFull(
      travellersInfo,
      outbound,
      inbound,
      buyInsurance ? insurancePackage : undefined,
    ),
    insuranceContact: {
      addr1: contactInfo.address,
      email: contactInfo.email,
      lastName: contactInfo.fullName.trim().split(' ')[0],
      firstName: contactInfo.fullName
        .trim()
        .split(' ')
        .slice(1)
        .join(' '),
      phone1: contactInfo.telephone,
      gender: contactInfo.gender.toUpperCase(),
      title: contactInfo.gender === 'f' ? 'Mrs' : 'Mr',
    },
    paymentMethodId: selectedPaymentMethod.id,
    paymentMethodBankId: selectedPaymentMethod.bankId,
    point: usingPoint ? point : 0,
    promoCode: promotionCode,
    tickets: {
      inbound: tid.inbound
        ? { agencyId: tid.inbound.aid, requestId: tid.requestId, ticketId: tid.inbound.id }
        : null,
      outbound: {
        agencyId: tid.outbound.aid,
        requestId: tid.requestId,
        ticketId: tid.outbound.id,
      },
    },
  };
}
