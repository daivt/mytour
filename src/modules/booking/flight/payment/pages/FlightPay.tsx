import { useSnackbar } from 'notistack';
import React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { goBackAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import { HOLD_PARAM_NAME, PAGE_SIZE_20, PAYMENT_HOLDING_CODE } from '../../../constants';
import {
  computePaymentFees,
  defaultPointPayment,
  getPointPaymentData,
  PointPayment,
  Promotions,
} from '../../../utils';
import { computePoints } from '../../result/utils';
import { computeFlightPayableNumbers } from '../../utils';
import FlightPayDesktop from '../components/FlightPayDesktop';
import { defaultFlightPayParams, FlightPayParams, genFlightPayParams } from '../utils';

interface Props {}

const FlightPay: React.FC<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [params, setParams] = React.useState<FlightPayParams>(defaultFlightPayParams);
  const [paymentMethods, setPayment] = React.useState<some[]>([]);
  const [pointPaymentData, setPointPaymentData] = React.useState<PointPayment>(defaultPointPayment);
  const [listPromotion, setListPromotion] = React.useState<Promotions | undefined>(undefined);
  const [loadingPromotion, setLoadingPromotion] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [paying, setPaying] = React.useState(false);
  const paramsBooking = useSelector((state: AppState) => state.booking.flight.info, shallowEqual);

  const fetchPaymentMethods = React.useCallback(async () => {
    const tid = paramsBooking?.tid;
    if (!tid) {
      return;
    }
    setLoading(true);
    const json = await dispatch(
      fetchThunk(API_PATHS.getFlightPaymentMethods, 'post', {
        supportCOD: true,
        tickets: {
          inbound: tid.inbound
            ? {
                agencyId: `${tid.inbound.aid}`,
                requestId: `${tid.requestId}`,
                ticketId: `${tid.inbound.id}`,
              }
            : undefined,
          outbound: {
            agencyId: `${tid.outbound.aid}`,
            requestId: `${tid.requestId}`,
            ticketId: `${tid.outbound.id}`,
          },
        },
      }),
    );
    if (json.code === SUCCESS_CODE || json.code === '200') {
      setPayment(json.data);
      json.data[0] && setParams(one => ({ ...one, selectedPaymentMethod: json.data[0] }));
    } else if (json.code === 400 || json.code === '400') {
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
      dispatch(goBackAction());
    }
    setLoading(false);
  }, [closeSnackbar, dispatch, enqueueSnackbar, paramsBooking]);

  const fetchPointPayment = React.useCallback(async () => {
    if (!paymentMethods || !paramsBooking) {
      return;
    }
    const payableNumbers = computeFlightPayableNumbers(paramsBooking, params, pointPaymentData);

    const originAmount = payableNumbers.originPrice;
    const amount =
      params.promotion && params.promotion.price !== undefined
        ? params.promotion.price
        : originAmount;
    const json = await dispatch(
      fetchThunk(`${API_PATHS.getPointPayment}`, 'post', {
        amounts: paymentMethods.map(obj => ({
          originAmount: originAmount + computePaymentFees(obj, originAmount),
          amount: amount + computePaymentFees(obj, originAmount),
          code: obj.code,
        })),
        module: 'flight',
        usePromoCode: originAmount !== amount,
      }),
    );

    if (json.code === SUCCESS_CODE) {
      const tmp = getPointPaymentData(json.data);
      setParams(one => ({ ...one, point: tmp.min, usingPoint: false }));
      setPointPaymentData(tmp);
    }
  }, [dispatch, params, paramsBooking, paymentMethods, pointPaymentData]);

  const fetchRewardsHistory = React.useCallback(
    async (search: string = '', page: number = 1) => {
      if (!paymentMethods) {
        return;
      }
      setLoadingPromotion(true);
      const json = await dispatch(
        fetchThunk(`${API_PATHS.getRewardsHistory}`, 'post', {
          page,
          isUsed: false,
          term: search || '',
          info: {
            productType: 'flight',
          },
          module: 'flight',
          size: PAGE_SIZE_20,
        }),
      );
      if (json.code === SUCCESS_CODE) {
        setListPromotion(one =>
          one && page > 1
            ? {
                ...one,
                list: one.list.concat(json.data.list),
              }
            : json.data,
        );
      } else {
        enqueueSnackbar(
          json?.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoadingPromotion(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, paymentMethods],
  );

  const checkPromotion = React.useCallback(
    async (code: string) => {
      if (!paramsBooking) {
        return;
      }
      const json = await dispatch(
        fetchThunk(`${API_PATHS.checkPromotion}`, 'post', {
          code,
          module: 'flight',
          originPoint: computePoints(paramsBooking),
          data: genFlightPayParams(paramsBooking, params),
        }),
      );

      if (json.code === SUCCESS_CODE) {
        setParams(one => ({ ...one, promotion: json.data, promotionCode: code }));
      } else {
        setParams(one => ({
          ...one,
          promotion: { message: json.message },
          promotionCode: undefined,
        }));
      }
    },
    [dispatch, params, paramsBooking],
  );

  const flightPay = React.useCallback(
    async (smartOTPPass?: string) => {
      const { selectedPaymentMethod } = params;

      if (!paramsBooking || !selectedPaymentMethod) {
        return;
      }
      setPaying(true);
      const json = await dispatch(
        fetchThunk(API_PATHS.bookTicket, 'post', {
          ...genFlightPayParams(paramsBooking, params),
          creditPassword: smartOTPPass,
          callbackDomain: `${window.location.origin}`,
          isMultipleInsurance: true,
          invoiceInfo: paramsBooking.exportInvoice ? paramsBooking.invoiceInfo : undefined,
        }),
      );
      setPaying(false);

      if (json.code === SUCCESS_CODE || json.code === '200') {
        const { paymentLink } = json.data;
        if (selectedPaymentMethod.code === PAYMENT_HOLDING_CODE) {
          window.location.replace(`${paymentLink}?${HOLD_PARAM_NAME}=true`);
        } else {
          window.location.replace(paymentLink);
        }
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
    },
    [closeSnackbar, dispatch, enqueueSnackbar, params, paramsBooking],
  );

  React.useEffect(() => {
    fetchPaymentMethods();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    fetchPointPayment();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [paymentMethods, params.promotion]);

  if (!paramsBooking) {
    return <RedirectDiv />;
  }
  if (loading) {
    return <LoadingIcon />;
  }
  return (
    <>
      <FlightPayDesktop
        paymentMethods={paymentMethods}
        params={params}
        setParams={setParams}
        pointPaymentData={pointPaymentData}
        listPromotion={listPromotion}
        fetchRewardsHistory={fetchRewardsHistory}
        checkPromotion={(code?: string) => {
          if (code) {
            checkPromotion(code);
          } else {
            setParams(one => ({ ...one, promotionCode: undefined, promotion: undefined }));
            setListPromotion(undefined);
          }
        }}
        flightPay={flightPay}
        loadingPromotion={loadingPromotion}
        paying={paying}
        paramsBooking={paramsBooking}
      />
    </>
  );
};

export default FlightPay;
