import { Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../../../constants';
import DiscountCodeBox from '../../../common/payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../common/payment/PaymentMethodsBox';
import { PointPayment, Promotions } from '../../../utils';
import { FlightInfo } from '../../booking/utils';
import { computeFlightPayableNumbers } from '../../utils';
import { FlightPayParams } from '../utils';

interface Props {
  paymentMethods: some[];
  params: FlightPayParams;
  setParams(params: FlightPayParams): void;
  pointPaymentData: PointPayment;
  listPromotion?: Promotions;
  checkPromotion(code?: string): void;
  fetchRewardsHistory(search: string, page: number): void;
  flightPay(smartOTPPass?: string): void;
  loadingPromotion: boolean;
  paying: boolean;
  paramsBooking: FlightInfo;
}

const FlightPayBox: React.FunctionComponent<Props> = props => {
  const {
    paymentMethods,
    params,
    setParams,
    pointPaymentData,
    listPromotion,
    fetchRewardsHistory,
    checkPromotion,
    flightPay,
    loadingPromotion,
    paying,
    paramsBooking,
  } = props;

  const payableNumbers = React.useMemo(() => {
    return computeFlightPayableNumbers(paramsBooking, params, pointPaymentData);
  }, [params, paramsBooking, pointPaymentData]);

  const priceAfter = payableNumbers.finalPrice - payableNumbers.pointToAmount;
  return (
    <>
      <Typography variant="h6" style={{ marginBottom: 16 }}>
        <FormattedMessage id="pay.discountCode" />
      </Typography>
      <Paper variant="outlined" style={{ padding: 16 }}>
        <DiscountCodeBox
          fetchRewardsHistory={fetchRewardsHistory}
          checkPromotion={checkPromotion}
          promotion={params.promotion}
          listPromotion={listPromotion}
          promotionCode={params.promotionCode}
          loading={loadingPromotion}
        />
      </Paper>
      <Typography variant="h6" style={{ margin: '24px 0px 16px' }}>
        <FormattedMessage id="pay.paymentMethod" />
      </Typography>
      <Paper variant="outlined" style={{ padding: 16, marginBottom: 32 }}>
        <PaymentMethodsBox
          paymentMethods={paymentMethods}
          total={payableNumbers.finalPrice}
          priceAfter={priceAfter}
          setSelectedMethod={method =>
            setParams({
              ...params,
              selectedPaymentMethod: method,
              usingPoint: false,
              point: pointPaymentData.min,
            })
          }
          selectedMethod={params.selectedPaymentMethod}
          promotionCode={params.promotionCode}
          pointPayment={pointPaymentData}
          point={params.point}
          usingPoint={params.usingPoint}
          onChangePoint={(point, usingPoint) => setParams({ ...params, point, usingPoint })}
          pay={flightPay}
          paying={paying}
        />
      </Paper>
      {/* <div
        style={{
          padding: '16px 16px 32px 16px',
          color: BLUE,
        }}
      >
        <Typography variant="body2">
          <FormattedMessage id="pay.seePaymentDetail" />
        </Typography>
      </div> */}

      {/* <TermsAndConditions /> */}
    </>
  );
};

export default FlightPayBox;
