import { Container, useMediaQuery, useTheme } from '@material-ui/core';
import * as React from 'react';
import { some } from '../../../../../constants';
import AsideBound from '../../../common/AsideBound';
import { PointPayment, Promotions } from '../../../utils';
import { FlightInfo } from '../../booking/utils';
import FlightInfoBox from '../../common/FlightInfoBox';
import FlightTicketDialog from '../../common/FlightTicketDialog';
import { FlightPayParams } from '../utils';
import FlightPayBox from './FlightPayBox';

export interface Props {
  paymentMethods: some[];
  params: FlightPayParams;
  setParams(params: FlightPayParams): void;
  pointPaymentData: PointPayment;
  listPromotion?: Promotions;
  checkPromotion(code?: string): void;
  fetchRewardsHistory(search: string, page: number): void;
  flightPay(smartOTPPass?: string): void;
  loadingPromotion: boolean;
  paying: boolean;
  paramsBooking: FlightInfo;
}

const FlightPayDesktop: React.FC<Props> = props => {
  const {
    paymentMethods,
    params,
    setParams,
    pointPaymentData,
    listPromotion,
    fetchRewardsHistory,
    checkPromotion,
    flightPay,
    paying,
    loadingPromotion,
    paramsBooking,
  } = props;
  const [seeDetail, setSeeDetail] = React.useState(false);
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));
  return (
    <Container style={{ flex: 1 }}>
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <FlightPayBox
            paymentMethods={paymentMethods}
            params={params}
            setParams={setParams}
            pointPaymentData={pointPaymentData}
            listPromotion={listPromotion}
            checkPromotion={checkPromotion}
            fetchRewardsHistory={fetchRewardsHistory}
            flightPay={flightPay}
            loadingPromotion={loadingPromotion}
            paying={paying}
            paramsBooking={paramsBooking}
          />
        </div>
        {paramsBooking && (
          <AsideBound
            isTablet={isTablet}
            style={{ width: 360, marginLeft: isTablet ? 0 : 24 }}
            direction="right"
          >
            <FlightInfoBox
              hideTravellersBox
              seeDetail={() => setSeeDetail(true)}
              booking={paramsBooking}
              params={params}
              pointPaymentData={pointPaymentData}
            />
          </AsideBound>
        )}
        <FlightTicketDialog
          close={() => setSeeDetail(false)}
          open={seeDetail}
          booking={paramsBooking}
          params={params}
          pointPaymentData={pointPaymentData}
        />
      </div>
    </Container>
  );
};

export default FlightPayDesktop;
