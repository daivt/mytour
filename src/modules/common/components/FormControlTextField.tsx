import { FormControl, FormHelperText, InputBaseProps, InputLabel } from '@material-ui/core';
import React from 'react';
import { BootstrapInput, redMark } from './Form';
import { MIN_WIDTH_FORM } from './utils';

export interface FormControlTextFieldProps extends InputBaseProps {
  id?: string;
  label?: React.ReactNode;
  formControlStyle?: React.CSSProperties;
  labelStyle?: React.CSSProperties;
  errorMessage?: string;
  optional?: boolean;
  focused?: boolean;
  disabledHelper?: boolean;
}

const FormControlTextField = (props: FormControlTextFieldProps) => {
  const {
    id,
    label,
    formControlStyle,
    labelStyle,
    errorMessage,
    optional,
    focused,
    value,
    fullWidth,
    disabledHelper,
    ...rest
  } = props;

  return (
    <FormControl
      focused={focused}
      style={{ minWidth: MIN_WIDTH_FORM, ...formControlStyle }}
      error={focused ? false : !!errorMessage}
      fullWidth
    >
      {label && (
        <InputLabel shrink htmlFor={id} style={labelStyle}>
          {label}
          {!optional && <span> &nbsp;{redMark}</span>}
        </InputLabel>
      )}
      <BootstrapInput
        id={id}
        value={value || ''}
        {...rest}
        error={focused ? false : !!errorMessage}
      />
      {!disabledHelper && (
        <FormHelperText id={id} style={{ minHeight: 20 }}>
          {errorMessage}
        </FormHelperText>
      )}
    </FormControl>
  );
};

export default FormControlTextField;
