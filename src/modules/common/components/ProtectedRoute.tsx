/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Route, RouteProps } from 'react-router';

interface Props extends RouteProps {
  auth: boolean;
}

const ProtectedRoute: React.SFC<Props> = props => {
  const { auth, ...restProps } = props;
  // const from = (props.location && `${props.location.pathname}${props.location.search}`) || '/';
  // const state = props.location && props.location.state;

  if (auth) {
    return <Route {...restProps} />;
  }

  return (
    // <Redirect
    //   to={{
    //     state,
    //     pathname: `${ROUTES.login}`,
    //     search: `?from=${encodeURIComponent(from)}`,
    //   }}
    // />
    null
  );
};

export default ProtectedRoute;
