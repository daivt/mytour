import { Button, Dialog, DialogActions, DialogProps, Divider, IconButton } from '@material-ui/core';
import IconClose from '@material-ui/icons/CloseOutlined';
import React from 'react';
import { FormattedMessage } from 'react-intl';

interface Props extends DialogProps {
  acceptId?: string;
  rejectId?: string;
  closeId?: string;
  open: boolean;
  acceptLabel?: string;
  rejectLabel?: string;
  styleCloseBtn?: React.CSSProperties;
  onClose(): void;
  onAccept(): void;
  titleLabel?: React.ReactNode;
  footerLabel?: React.ReactNode;
  onReject?: () => void;
  onExited?: () => void;
}

const ConfirmDialog: React.FC<Props> = props => {
  const {
    open,
    styleCloseBtn,
    onClose,
    onExited,
    onAccept,
    onReject,
    titleLabel,
    footerLabel,
    acceptLabel,
    rejectLabel,
    children,
    acceptId,
    rejectId,
    closeId,
    ...rest
  } = props;
  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{
        style: {
          minWidth: 420,
        },
      }}
      maxWidth="md"
      onExited={onExited}
      {...rest}
    >
      {titleLabel && (
        <>
          {titleLabel}
          <Divider />
        </>
      )}
      <IconButton
        id={closeId}
        style={{
          position: 'absolute',
          top: 8,
          right: 8,
          padding: '8px',
          ...styleCloseBtn,
        }}
        onClick={onClose}
      >
        <IconClose />
      </IconButton>
      {children}
      <Divider />
      <DialogActions style={{ padding: 16, justifyContent: 'center' }}>
        {onReject && (
          <Button
            id={rejectId} 
            variant="outlined"
            size="large"
            style={{ minWidth: 144, marginRight: 12 }}
            onClick={onReject}
            disableElevation
          >
            <FormattedMessage id={rejectLabel || 'reject'} />
          </Button>
        )}
        <Button
          id={acceptId}
          variant="contained"
          color="secondary"
          size="large"
          style={{ minWidth: 144 }}
          onClick={onAccept}
          disableElevation
        >
          <FormattedMessage id={acceptLabel || 'accept'} />
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
