import React from 'react';
import ReactGA from 'react-ga';
import { useSelector, shallowEqual } from 'react-redux';
import { ROUTES } from '../../../configs/routes';
import { parseFlightSearchParams } from '../../booking/flight/utils';
import { AppState } from '../../../redux/reducers';

interface Props {
  children: JSX.Element;
}

const GAListener: React.FC<Props> = (props: Props) => {
  const { children } = props;
  const { location } = useSelector((state: AppState) => state.router, shallowEqual);
  const trackingGA = React.useCallback(
    (e: any) => {
      const id = e.path.filter((v: any) => v.nodeName === 'BUTTON' || v.nodeName === 'INPUT')[0]
        ?.id;
      if (id) {
        const content = id.split('.');
        if (content && content.length > 1) {
          if (location.pathname === ROUTES.booking.flight.result) {
            try {
              const searchUrl = new URLSearchParams(location.search);
              const params = parseFlightSearchParams(searchUrl);
              const category = content?.[0];
              const action = JSON.stringify({
                action: content?.[1],
                Flight_From: params.origin?.location,
                Flight_To: params.destination?.location,
                Flight_Passenger:
                  params.travellerCountInfo.adultCount +
                  params.travellerCountInfo.childCount +
                  params.travellerCountInfo.infantCount,
                Round_trip: params.one_way,
                Flight_Departure: params.departureDate,
                Flight_Leadtime: Math.floor(
                  (Date.parse(
                    params.departureDate
                      .split('-')
                      .reverse()
                      .join('-'),
                  ) -
                    Date.now()) /
                    (1000 * 60 * 60 * 24),
                ),
                Flight_Return: params.returnDate,
                Trip_Duration: Math.floor(
                  (Date.parse(
                    params.returnDate
                      ? params.returnDate
                      : ''
                          .split('-')
                          .reverse()
                          .join('-'),
                  ) -
                    Date.parse(
                      params.departureDate
                        .split('-')
                        .reverse()
                        .join('-'),
                    )) /
                    (1000 * 60 * 60 * 24),
                ),
                Flight_SeatClass: params.seatClass,
              });
              ReactGA.event({
                category,
                action,
              });
              return;
            } catch (error) {}
          }
          ReactGA.event({
            category: content?.[0],
            action: content?.[1],
          });
        }
      }
    },
    [location.pathname, location.search],
  );

  React.useEffect(() => {
    document.addEventListener('click', trackingGA);
    return () => document.removeEventListener('click', trackingGA);
  }, [trackingGA]);
  return children;
};

export default GAListener;
