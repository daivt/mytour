import { Grid } from '@material-ui/core';
import { useFormik } from 'formik';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from '../../../models/moment';
import DateRangeFormControl from '../../common/components/DateRangeFormControl';
import FormControlTextField from '../../common/components/FormControlTextField';
import LoadingButton from '../../common/components/LoadingButton';
import { FilterParams } from './utils';
import { validNumberRegex } from '../../../models/regex';

interface Props {
  filters: FilterParams;
  onChange: (params: FilterParams) => void;
  loading: boolean;
}

const ExtendedFilter: React.FunctionComponent<Props> = props => {
  const { filters, onChange, loading } = props;
  const intl = useIntl();
  const formik = useFormik({
    initialValues: filters,
    onSubmit: values => {
      onChange(values);
    },
  });

  React.useEffect(() => {
    formik.setValues(filters);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters]);

  return (
    <form
      onSubmit={formik.handleSubmit}
      autoComplete="off"
      style={{ marginBottom: '20px', display: 'flex' }}
    >
      <Grid container spacing={2}>
        <Grid item style={{ width: '19%' }}>
          <FormControlTextField
            label={intl.formatMessage({ id: 'order.filter.bookingCode' })}
            placeholder={intl.formatMessage({ id: 'order.filter.bookingCodeEx' })}
            fullWidth
            value={formik.values.bookingCode}
            onChange={e => {
              formik.setFieldValue('bookingCode', e.target.value.trim());
            }}
            optional
            disabledHelper
          />
        </Grid>
        <Grid item style={{ width: '19%' }}>
          <FormControlTextField
            label={intl.formatMessage({ id: 'order.filter.bookingId' })}
            placeholder={intl.formatMessage({ id: 'order.filter.bookingIdEx' })}
            fullWidth
            value={formik.values.bookingId}
            onChange={e => {
              validNumberRegex.test(e.target.value) &&
                formik.setFieldValue('bookingId', e.target.value.trim());
            }}
            optional
            disabledHelper
          />
        </Grid>
        <Grid item style={{ width: '25%' }}>
          <DateRangeFormControl
            label={intl.formatMessage({ id: 'order.filter.departureDate' })}
            placeholder={intl.formatMessage({ id: 'order.filter.departureDateEx' })}
            style={{ width: '100%', margin: 0 }}
            optional
            startDate={
              formik.values.departureFromDate
                ? moment(formik.values.departureFromDate, DATE_FORMAT_BACK_END)
                : undefined
            }
            endDate={
              formik.values.departureToDate
                ? moment(formik.values.departureToDate, DATE_FORMAT_BACK_END)
                : undefined
            }
            onChange={(start?: Moment, end?: Moment) => {
              formik.setFieldValue(
                'departureFromDate',
                start ? start.format(DATE_FORMAT_BACK_END) : undefined,
              );
              formik.setFieldValue(
                'departureToDate',
                end ? end.format(DATE_FORMAT_BACK_END) : undefined,
              );
            }}
            numberOfMonths={1}
            startAdornment
            isOutsideRange={(e: any) => false}
            disabledHelper
            renderString={(start, end, single) =>
              single
                ? `${start ? start.format(DATE_FORMAT) : intl.formatMessage({ id: 'notChosen' })}`
                : `${
                    start ? start.format(DATE_FORMAT) : intl.formatMessage({ id: 'notChosen' })
                  }${end ? ` - ${end.format(DATE_FORMAT)}` : ''}`
            }
            direction="center"
          />
        </Grid>
        <Grid item style={{ width: '25%' }}>
          <DateRangeFormControl
            label={intl.formatMessage({ id: 'order.filter.createDate' })}
            placeholder={intl.formatMessage({ id: 'order.filter.createDateEx' })}
            style={{ width: '100%', margin: 0 }}
            optional
            startDate={
              formik.values.createdFromDate
                ? moment(formik.values.createdFromDate, DATE_FORMAT_BACK_END)
                : undefined
            }
            endDate={
              formik.values.createdToDate
                ? moment(formik.values.createdToDate, DATE_FORMAT_BACK_END)
                : undefined
            }
            onChange={(start?: Moment, end?: Moment) => {
              formik.setFieldValue(
                'createdFromDate',
                start ? start.format(DATE_FORMAT_BACK_END) : undefined,
              );
              formik.setFieldValue(
                'createdToDate',
                end ? end.format(DATE_FORMAT_BACK_END) : undefined,
              );
            }}
            numberOfMonths={1}
            startAdornment
            isOutsideRange={(e: any) => false}
            disabledHelper
            renderString={(start, end, single) =>
              single
                ? `${start ? start.format(DATE_FORMAT) : intl.formatMessage({ id: 'notChosen' })}`
                : `${
                    start ? start.format(DATE_FORMAT) : intl.formatMessage({ id: 'notChosen' })
                  }${end ? ` - ${end.format(DATE_FORMAT)}` : ''}`
            }
            direction="center"
          />
        </Grid>
        <Grid item>
          <LoadingButton
            type="submit"
            loading={loading}
            variant="contained"
            color="primary"
            disableElevation
            style={{ minWidth: 100, marginTop: 24 }}
          >
            <FormattedMessage id="apply" />
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default ExtendedFilter;
