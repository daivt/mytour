import * as React from 'react';
import { Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import { ReactComponent as IcNoResult } from '../../../svg/booking/ic_contact_no_result.svg';
import { BLUE } from '../../../configs/colors';

interface Props {
  id?: string;
  style?: React.CSSProperties;
  content?: React.ReactNode;
  linkText?: string;
  action?: () => void;
}

const NoDataResult: React.FunctionComponent<Props> = props => {
  const { id, style, content, linkText, action } = props;
  return (
    <div style={style}>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'column',
          textAlign: 'center',
        }}
      >
        <IcNoResult />
        {!!id && (
          <div style={{ display: 'flex', marginTop: '16px' }}>
            <Typography variant="body1">
              <FormattedMessage id={id} />
            </Typography>
            &nbsp;
            {linkText && action && (
              <Typography
                variant="body1"
                style={{ color: BLUE, cursor: 'pointer' }}
                onClick={action}
              >
                <FormattedMessage id={linkText} />
              </Typography>
            )}
          </div>
        )}
        {content}
      </div>
    </div>
  );
};

export default NoDataResult;
