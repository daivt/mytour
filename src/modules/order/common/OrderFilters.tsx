import { Button, Collapse, Typography } from '@material-ui/core';
import ArrowDropUpRoundedIcon from '@material-ui/icons/ArrowDropUpRounded';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { some } from '../../../constants';
import { CheckLoadingButton } from './element';
import { OrderFiltersParams, defaultOrderFiltersParams } from './utils';
import ExtendedFilter from './ExtendedFilter';

const FILTER = [
  { id: 'order.filter.all', value: undefined },
  { id: 'order.filter.holding', value: ['holding'] },
  { id: 'order.filter.success', value: ['completed', 'success'] },
];

interface Props {
  filterParams: OrderFiltersParams;
  onChange: (filterParams: OrderFiltersParams) => void;
  loading: boolean;
}

const OrderFilters: React.FunctionComponent<Props> = props => {
  const { filterParams, onChange, loading } = props;
  const [open, setOpen] = React.useState<boolean>(true);
  const [params, setParams] = React.useState<OrderFiltersParams>(filterParams);

  const setPaymentStatuses = React.useCallback(
    (obj: some) => {
      const newParams: OrderFiltersParams = {
        filters: {
          ...filterParams.filters,
          paymentStatuses: obj.value,
        },
        paging: { ...filterParams.paging, page: 1 },
      };
      onChange(newParams);
      setParams(newParams);
    },
    [filterParams.filters, filterParams.paging, onChange],
  );

  // const setPaymentStatuses = React.useCallback(
  //   (obj: some) => {
  //     let paymentStatuses: string[] | undefined;
  //     if (obj.id === 'order.filter.all') {
  //       paymentStatuses = obj.value;
  //     } else if (params.filters.paymentStatuses) {
  //       paymentStatuses = params.filters.paymentStatuses.filter(
  //         x => !obj.value?.includes(x) && x !== undefined,
  //       );
  //       paymentStatuses =
  //         paymentStatuses?.join(',') === params.filters.paymentStatuses?.join(',')
  //           ? paymentStatuses?.concat(obj.value)
  //           : paymentStatuses?.length > 0
  //           ? paymentStatuses
  //           : undefined;
  //       let count = 0;
  //       FILTER.forEach(v => {
  //         if (v.value) {
  //           count += v?.value?.length;
  //         }
  //       });
  //       if (count === paymentStatuses?.length) {
  //         paymentStatuses = undefined;
  //       }
  //     } else {
  //       paymentStatuses = obj.value;
  //     }
  //     const newParams: OrderFiltersParams = {
  //       filters: {
  //         ...filterParams.filters,
  //         paymentStatuses,
  //       },
  //       paging: { ...filterParams.paging, page: 0 },
  //     };
  //     onChange(newParams);
  //     setParams(newParams);
  //   },
  //   [params.filters.paymentStatuses, filterParams.filters, filterParams.paging, onChange],
  // );

  React.useEffect(() => {
    setParams(filterParams);
  }, [filterParams]);
  return (
    <div>
      <div style={{ display: 'flex', alignItems: 'center', padding: '16px 0' }}>
        <Typography variant="body2" style={{ marginRight: '9px' }}>
          <FormattedMessage id="order.filter.status" />
        </Typography>
        {FILTER.map(obj => (
          <CheckLoadingButton
            loadingColor="primary"
            loading={loading}
            key={obj.id}
            active={
              params?.filters.paymentStatuses && obj.value
                ? params?.filters.paymentStatuses.join(',').includes(obj.value.join(','))
                : !!(!params?.filters.paymentStatuses && !obj.value)
            }
            style={{ marginRight: '12px' }}
            onClick={() => setPaymentStatuses(obj)}
          >
            <Typography variant="body2">
              <FormattedMessage id={obj.id} />
            </Typography>
          </CheckLoadingButton>
        ))}
        <div style={{ display: 'flex', justifyContent: 'flex-end', flex: 1 }}>
          <Button
            size="small"
            variant="text"
            onClick={() => {
              onChange(defaultOrderFiltersParams);
              setParams(defaultOrderFiltersParams);
            }}
          >
            <Typography variant="body2" color="textSecondary">
              <FormattedMessage id="reset" />
            </Typography>
          </Button>
          <Button
            size="small"
            onClick={() => {
              setOpen(!open);
            }}
          >
            <Typography variant="body2" color="primary">
              <FormattedMessage id="order.filter" />
            </Typography>
            <ArrowDropUpRoundedIcon
              fontSize="large"
              color="primary"
              style={{
                transition: 'all 300ms',
                transform: open ? 'rotate(0deg)' : 'rotate(180deg)',
                cursor: 'pointer',
              }}
            />
          </Button>
        </div>
      </div>
      <Collapse in={open}>
        <ExtendedFilter
          loading={loading}
          filters={filterParams.filters}
          onChange={filters => {
            onChange({
              filters: { ...filters, paymentStatuses: filterParams.filters.paymentStatuses },
              paging: { ...filterParams.paging, page: 1 },
            });
          }}
        />
      </Collapse>
    </div>
  );
};
export default OrderFilters;
