import * as React from 'react';
import {
  ButtonProps,
  fade,
  PropTypes,
  withStyles,
  Typography as TypographyRaw,
} from '@material-ui/core';
import styled from 'styled-components';
import LoadingButton from '../../common/components/LoadingButton';
import { SECONDARY, PRIMARY } from '../../../configs/colors';

export const CheckLoadingButton = (
  props: ButtonProps & {
    active: boolean;
    loading?: boolean;
    loadingColor?: PropTypes.Color;
    backgroundColor?: PropTypes.Color;
  },
) => {
  const { active, style, ref, loading, loadingColor, backgroundColor, children, ...rest } = props;
  return (
    <LoadingButton
      {...rest}
      loadingColor={loadingColor}
      loading={loading}
      variant="outlined"
      color={active ? 'secondary' : 'default'}
      size="small"
      style={{
        backgroundColor: active
          ? loading
            ? fade(backgroundColor === 'secondary' ? SECONDARY : PRIMARY, 0.4)
            : backgroundColor === 'secondary'
            ? SECONDARY
            : PRIMARY
          : undefined,
        borderRadius: '15px',
        boxShadow: 'none',
        textTransform: 'none',
        color: active ? 'white' : '#757575',
        borderColor: active ? (backgroundColor === 'secondary' ? SECONDARY : PRIMARY) : undefined,
        minHeight: '30px',
        display: 'flex',
        alignItems: 'center',
        padding: '0px 16px',
        ...style,
      }}
    >
      {children}
    </LoadingButton>
  );
};

export const TypographyRow = withStyles(theme => ({
  root: {
    padding: '8px 0px',
  },
}))(TypographyRaw);

export const Line = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 8px 0px;
`;
