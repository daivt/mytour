import { defaultPaginationFilter2, PaginationFilter2 } from '../../../models/pagination';

export interface FilterParams {
  paymentStatuses?: string[];
  createdFromDate?: string;
  createdToDate?: string;
  departureFromDate?: string;
  departureToDate?: string;
  bookingId?: string;
  bookingCode?: string;
  bookerContactId?: number;
}
export interface OrderFiltersParams {
  filters: FilterParams;
  paging: PaginationFilter2;
}

export const defaultOrderFiltersParams: OrderFiltersParams = {
  filters: {},
  paging: defaultPaginationFilter2,
};
