import { Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedDate, useIntl } from 'react-intl';
import { ReactComponent as IconFlight } from '../../../../svg/booking/ic_airplane_horizontal.svg';
import { durationMillisecondToHour } from '../result/utils';
import { GREY_300 } from '../../../../configs/colors';
import { DATE_FORMAT } from '../../../../models/moment';
import { some } from '../../../../constants';
import { EN_US } from '../../../../models/intl';
import { Row, Col } from '../../../common/components/elements';

interface Props {
  ticket: some;
  airlineInfo: some;
  ticketClass: some | undefined;
  isInbound?: boolean;
}

export const FlightAirlineInfoItem: React.FC<Props> = props => {
  const { ticket, airlineInfo, ticketClass } = props;
  const lang = useIntl().locale;
  const departureDate = moment(ticket.departureDate, DATE_FORMAT);
  const dateDiff = moment(ticket.arrivalDate, DATE_FORMAT).diff(departureDate, 'days');

  return (
    <Row>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          marginRight: 32,
          minWidth: 120,
        }}
      >
        <img src={airlineInfo.logo} alt="" style={{ width: 62 }} />
        <Typography variant="caption">{airlineInfo.name}</Typography>
        <Typography variant="caption">{ticket.flightCode}</Typography>
        <Typography variant="caption">
          {ticketClass ? (lang.startsWith(EN_US) ? ticketClass.v_name : ticketClass.i_name) : ''}
        </Typography>
      </div>
      <div style={{ minWidth: '250px', paddingLeft: '20px' }}>
        <Typography variant="body2">
          <FormattedDate value={departureDate.toDate()} weekday="short" />, &nbsp;
          {departureDate.format('L')}
        </Typography>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Typography variant="subtitle1">{ticket.fromAirport}</Typography>
            <Typography variant="body2">{ticket.departureTime}</Typography>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              flex: 1,
              alignItems: 'center',
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                height: '35px',
                flex: 1,
              }}
            >
              <div style={{ flex: 1 }}>
                <div style={{ borderBottom: `2px solid ${GREY_300}`, margin: '0 8px' }} />
              </div>
              <IconFlight />
              <div style={{ flex: 1 }}>
                <div style={{ borderBottom: `2px solid ${GREY_300}`, margin: '0 8px' }} />
              </div>
            </div>
            <Typography variant="body2" color="textSecondary">
              {durationMillisecondToHour(ticket.duration)}
            </Typography>
          </div>
          <Col>
            <Typography variant="subtitle1">{ticket.toAirport}</Typography>
            <Typography variant="body2">
              {ticket.arrivalTime}
              <span style={{ display: dateDiff ? undefined : 'none' }}>
                <span>
                  &nbsp;(+
                  {dateDiff}
                  d)
                </span>
              </span>
            </Typography>
          </Col>
        </div>
      </div>
    </Row>
  );
};
