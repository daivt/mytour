import { Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { some } from '../../../../../constants';
import PaymentMethodsBox from '../../../../booking/common/payment/PaymentMethodsBox';
import { computePaymentFees } from '../../../../booking/utils';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import { FlightBaggagePayParams } from '../utils';
import { Line } from '../../../common/element';
import { GREY_300 } from '../../../../../configs/colors';

interface Props {
  data?: some;
  loading?: boolean;
  paying?: boolean;
  paymentMethods: some[];
  params: FlightBaggagePayParams;
  setParams(params: FlightBaggagePayParams): void;
  pay(creditPassword?: string): void;
}

const FlightOrderBaggagePayDesktop: React.FunctionComponent<Props> = props => {
  const { data, loading, params, setParams, paymentMethods, pay, paying } = props;

  if (loading || !data) {
    return <LoadingIcon />;
  }
  const { payableNumber } = data;
  const { totalPrice } = payableNumber;
  const paymentFee = params.selectedPaymentMethod
    ? computePaymentFees(params.selectedPaymentMethod, totalPrice)
    : 0;
  const totalPayable = totalPrice + paymentFee;

  return (
    <>
      <>
        <Typography variant="h6" style={{ marginBottom: 8 }}>
          <FormattedMessage id="order.totalPay" />
        </Typography>
        <Paper variant="outlined" style={{ padding: '8px 16px' }}>
          <Line
            style={{
              justifyContent: 'space-between',
            }}
          >
            <Typography variant="body2">
              <FormattedMessage id="order.totalCost" />
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={totalPrice} /> <FormattedMessage id="currency" />
            </Typography>
          </Line>
          {!!paymentFee && (
            <Line
              style={{
                justifyContent: 'space-between',
              }}
            >
              <Typography variant="body2">
                <FormattedMessage id="order.paymentFixedFeeSide" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={paymentFee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line style={{ borderTop: `1px dashed ${GREY_300}`, paddingTop: 16, paddingBottom: 12 }}>
            <Typography variant="subtitle1">
              <FormattedMessage id="order.totalPayable" />
            </Typography>
            <Typography className="final-price" variant="subtitle1">
              <FormattedNumber
                value={totalPayable < 0 ? 0 : totalPayable}
                maximumFractionDigits={0}
              />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        </Paper>
      </>
      <>
        <Typography variant="h6" style={{ margin: '16px 0px 8px' }}>
          <FormattedMessage id="pay.paymentMethod" />
        </Typography>
        <Paper variant="outlined" style={{ padding: 16, marginBottom: 32 }}>
          <PaymentMethodsBox
            paymentMethods={paymentMethods}
            total={totalPrice}
            priceAfter={totalPrice}
            setSelectedMethod={method =>
              setParams({
                ...params,
                selectedPaymentMethod: method,
              })
            }
            selectedMethod={params.selectedPaymentMethod}
            pay={pay}
            paying={paying}
          />
        </Paper>
      </>
    </>
  );
};

export default FlightOrderBaggagePayDesktop;
