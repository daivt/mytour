import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import FlightOrderBaggagePayDesktop from '../components/FlightOrderBaggagePayDesktop';
import { FlightBaggagePayParams } from '../utils';

interface Props {}

const FlightOrderBaggagePay: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();
  const [loading, setLoading] = React.useState(true);
  const [paying, setPaying] = React.useState(false);
  const [paymentMethods, setPayment] = React.useState<some[]>([]);
  const [params, setParams] = React.useState<FlightBaggagePayParams>({});

  const getData = React.useMemo(() => {
    return (location.state as some)?.baggagePaymentData;
  }, [location.state]);

  const fetchPaymentMethods = React.useCallback(async () => {
    if (!getData) {
      return;
    }
    const json = await dispatch(
      fetchThunk(
        `${API_PATHS.getPaymentMethodsForAddingBaggages}?bookingId=${getData.bookingId}`,
        'get',
      ),
    );

    if (json.code === SUCCESS_CODE || json.code === '200') {
      setPayment(json.data);
      json.data[0] && setParams(one => ({ ...one, selectedPaymentMethod: json.data[0] }));
    } else if (json.code === 400 || json.code === '400') {
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
      dispatch(goToAction({ pathname: ROUTES.order.flight.result }));
    }
    setLoading(false);
  }, [closeSnackbar, dispatch, enqueueSnackbar, getData]);

  const pay = React.useCallback(
    async (creditPassword?: string) => {
      if (!getData || !params.selectedPaymentMethod) {
        return;
      }
      setPaying(true);
      const guests = getData.guests.map((guest: some) => ({
        guestId: guest.id,
        outboundBaggageId: guest.outboundBaggage ? guest.outboundBaggage.id : null,
        inboundBaggageId: guest.inboundBaggage ? guest.inboundBaggage.id : null,
      }));
      const json = await dispatch(
        fetchThunk(API_PATHS.addBaggagesForFlightBooking, 'post', {
          guests,
          creditPassword,
          paymentMethodId: params.selectedPaymentMethod.id,
          paymentMethodBankId: params.selectedPaymentMethod.bankId,
          bookingId: getData.bookingId,
          callbackDomain: `${window.location.origin}`,
        }),
      );
      if (json.code === SUCCESS_CODE) {
        if (json.data.paymentLink) {
          const { paymentLink } = json.data;
          window.location.replace(paymentLink);
        }
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setPaying(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, getData, params.selectedPaymentMethod],
  );

  React.useEffect(() => {
    fetchPaymentMethods();
  }, [fetchPaymentMethods]);

  if (!getData) {
    return <RedirectDiv />;
  }

  return (
    <>
      <FlightOrderBaggagePayDesktop
        data={getData}
        paymentMethods={paymentMethods}
        params={params}
        setParams={setParams}
        loading={loading}
        pay={pay}
        paying={paying}
      />
    </>
  );
};

export default FlightOrderBaggagePay;
