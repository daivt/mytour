import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps, useLocation } from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { fetchThunk } from '../../../../common/redux/thunk';
import NoDataResult from '../../../common/NoDataResult';
import FlightOrderAddBaggageDesktop from '../components/FlightOrderAddBaggageDesktop';

interface Props extends RouteComponentProps<{ bookingId: string }> {}

const FlightOrderAddBaggage: React.FunctionComponent<Props> = props => {
  const { match } = props;

  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { bookingId } = match.params;
  const location = useLocation();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [guests, setGuest] = React.useState<some[]>([]);
  const [baggageData, setBaggageData] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(true);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const dataTmp = (location.state as some)?.baggageData;

  const fetchData = React.useCallback(async () => {
    if (Number.isInteger(Number(bookingId))) {
      const json = await dispatch(
        fetchThunk(`${API_PATHS.getBaggageForFlightBooking}?bookingId=${bookingId}`),
      );

      if (json.code === SUCCESS_CODE) {
        setBaggageData(json.data);
      } else {
        json.message &&
          enqueueSnackbar(
            json.message,
            snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
          );
      }

      if (dataTmp?.id !== Number(bookingId)) {
        const json2 = await dispatch(
          fetchThunk(`${API_PATHS.getFlightBookingDetail}?id=${bookingId}`, 'get'),
        );
        if (json2.code === 200) {
          setData(json2.data);
          setGuest(json2.data.guests || []);
        } else {
          json2.message &&
            enqueueSnackbar(
              json2.message,
              snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
            );
        }
      } else {
        setData(dataTmp);
        setGuest(dataTmp.guests || []);
      }
    }
    setLoading(false);
  }, [bookingId, closeSnackbar, dataTmp, dispatch, enqueueSnackbar]);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  if (!Number.isInteger(Number(bookingId)) || !dataTmp) {
    return <RedirectDiv />;
  }

  if (loading) {
    return <LoadingIcon />;
  }
  if (!data || !baggageData) {
    return <NoDataResult id="order.noData" style={{ marginTop: 48 }} />;
  }
  return (
    <>
      <FlightOrderAddBaggageDesktop
        data={data}
        baggageData={baggageData}
        guests={guests}
        setGuest={setGuest}
      />
    </>
  );
};

export default FlightOrderAddBaggage;
