import { Button, Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { useDispatch } from 'react-redux';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { Row } from '../../../../common/components/elements';
import { goToAction } from '../../../../common/redux/reducer';
import { computeBaggagePayable } from '../utils';
import { FlightBaggageItem } from './FlightBaggageItem';

interface Props {
  data: some;
  baggageData: some;
  guests: some[];
  setGuest(guests: some[]): void;
}

const FlightOrderAddBaggageDesktop: React.FunctionComponent<Props> = props => {
  const { data, guests, setGuest, baggageData } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();

  const payableNumber = computeBaggagePayable(data.guests, guests);
  return (
    <>
      <Paper variant="outlined" style={{ overflow: 'hidden' }}>
        <div className="card-background" style={{ padding: '12px 16px' }}>
          <Typography variant="subtitle1">
            <FormattedMessage id="order.outbound" />
          </Typography>
        </div>
        <div style={{ padding: 16 }}>
          <FlightBaggageItem
            bookingDetail={data}
            baggages={baggageData.outbound}
            guests={guests}
            isOutbound
            onSelect={(outboundBaggage: some, index: number) =>
              setGuest([
                ...guests.slice(0, index),
                { ...guests[index], outboundBaggage },
                ...guests.slice(index + 1),
              ])
            }
          />
        </div>
        {data.inbound && (
          <>
            <div className="card-background" style={{ padding: '12px 16px' }}>
              <Typography variant="subtitle1">
                <FormattedMessage id="order.inbound" />
              </Typography>
            </div>
            <div style={{ padding: 16 }}>
              <FlightBaggageItem
                bookingDetail={data}
                baggages={baggageData.inbound}
                guests={guests}
                isOutbound={false}
                onSelect={(inboundBaggage: some, index: number) =>
                  setGuest([
                    ...guests.slice(0, index),
                    { ...guests[index], inboundBaggage },
                    ...guests.slice(index + 1),
                  ])
                }
              />
            </div>
          </>
        )}
      </Paper>

      <Paper variant="outlined" className="card-background" style={{ padding: 16, marginTop: 24 }}>
        <Typography variant="subtitle1" style={{ marginBottom: 12 }}>
          <FormattedMessage id="order.baggagePay" />
        </Typography>

        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: 1 }}>
            <FormattedMessage id="order.totalCost" />
          </Typography>

          <Typography variant="body2">
            <FormattedNumber value={payableNumber.totalPriceNoTax} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>
        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: 1 }}>
            <FormattedMessage id="order.vat" />
          </Typography>

          <Typography variant="body2">
            <FormattedNumber value={payableNumber.totalTax} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>
        <Row style={{ marginBottom: 8 }}>
          <Typography variant="body2" style={{ flex: 1 }}>
            <FormattedMessage id="order.totalPayable" />
          </Typography>

          <Typography className="final-price" variant="h6">
            <FormattedNumber value={payableNumber.totalPrice} />
            &nbsp;
            <FormattedMessage id="currency" />
          </Typography>
        </Row>
      </Paper>
      <Row style={{ justifyContent: 'center', marginTop: 24 }}>
        <Button
          variant="contained"
          size="large"
          color="secondary"
          disableElevation
          disabled={!payableNumber.totalPrice}
          style={{ width: 160 }}
          onClick={() => {
            dispatch(
              goToAction({
                pathname: ROUTES.order.flight.baggagePay,
                state: {
                  baggagePaymentData: {
                    bookingId: data.id,
                    payableNumber,
                    guests,
                  },
                },
              }),
            );
          }}
        >
          <Typography variant="button">
            <FormattedMessage id="pay" />
          </Typography>
        </Button>
      </Row>
    </>
  );
};

export default FlightOrderAddBaggageDesktop;
