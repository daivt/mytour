import { Button, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { some } from '../../../../../constants';
import { DATE_TIME_FORMAT } from '../../../../../models/moment';
import { Col, Row } from '../../../../common/components/elements';

interface Props {
  bookingDetail?: some;
  baggages: some[];
  isOutbound?: boolean;
  guests: some[];
  onSelect(data: some, index: number): void;
}

export const FlightBaggageItem: React.FC<Props> = props => {
  const { bookingDetail, baggages, guests, isOutbound, onSelect } = props;

  if (!bookingDetail) {
    return <div style={{ margin: '42px' }}>{/* <LoadingIcon /> */}</div>;
  }

  const isExpired = moment(
    isOutbound
      ? `${bookingDetail.outbound.departureDate} ${bookingDetail.outbound.departureTime}`
      : `${bookingDetail.inbound.departureDate} ${bookingDetail.inbound.departureTime}`,
    DATE_TIME_FORMAT,
  ).isSameOrBefore(moment());

  return (
    <Col>
      {bookingDetail && baggages.length && !isExpired ? (
        bookingDetail.guests.map(
          (guest: some, index: number) =>
            guest.ageCategory !== 'infant' && (
              <Col key={guest.id}>
                <Row>
                  <Row style={{ flex: 1, marginBottom: 12, marginTop: index ? 24 : 0 }}>
                    <Typography variant="subtitle2">{guest.fullName}</Typography>&nbsp;
                    {isOutbound
                      ? guest.outboundBaggage && (
                          <>
                            - &nbsp;
                            <Typography color="textSecondary" variant="body1">
                              <FormattedMessage
                                id="order.bought"
                                values={{ text: guest.outboundBaggage.name }}
                              />
                            </Typography>
                          </>
                        )
                      : guest.inboundBaggage && (
                          <>
                            - &nbsp;
                            <Typography color="textSecondary" variant="body1">
                              <FormattedMessage
                                id="order.bought"
                                values={{ text: guest.inboundBaggage.name }}
                              />
                            </Typography>
                          </>
                        )}
                  </Row>
                  <Row>
                    <Typography variant="subtitle2">
                      <FormattedMessage id="order.buyMore" />
                      :&nbsp;
                    </Typography>
                    {isOutbound
                      ? guests[index].outboundBaggage && (
                          <Typography className="final-price" variant="subtitle2">
                            <FormattedNumber
                              value={
                                guests[index].outboundBaggage.price -
                                (guest.outboundBaggage ? guest.outboundBaggage.price : 0)
                              }
                            />
                            &nbsp;
                            <FormattedMessage id="currency" />
                          </Typography>
                        )
                      : guests[index].inboundBaggage && (
                          <Typography className="final-price" variant="subtitle2">
                            <FormattedNumber
                              value={
                                guests[index].inboundBaggage.price -
                                (guest.inboundBaggage ? guest.inboundBaggage.price : 0)
                              }
                            />
                            &nbsp;
                            <FormattedMessage id="currency" />
                          </Typography>
                        )}
                  </Row>
                </Row>
                <div style={{ display: 'flex', flexWrap: 'wrap', margin: -8 }}>
                  {isOutbound ? (
                    <>
                      {baggages.map((baggage: some) => {
                        const isCurrent =
                          guests[index].outboundBaggage &&
                          baggage.weight === guests[index].outboundBaggage.weight;
                        return (
                          (!guest.outboundBaggage ||
                            baggage.weight >= guest.outboundBaggage.weight) && (
                            <Button
                              key={baggage.id}
                              variant="outlined"
                              color={isCurrent ? 'primary' : undefined}
                              style={{
                                width: 112,
                                height: 64,
                                margin: 8,
                              }}
                              onClick={() => onSelect(baggage, index)}
                            >
                              <div style={{ display: 'flex', flexDirection: 'column' }}>
                                <Typography variant="body1">{baggage.weight}kg</Typography>
                                <Typography variant="body1">
                                  <FormattedNumber value={baggage.price} />
                                  &nbsp;
                                  <FormattedMessage id="currency" />
                                </Typography>
                              </div>
                            </Button>
                          )
                        );
                      })}
                    </>
                  ) : (
                    <>
                      {baggages.map((baggage: some) => {
                        const isCurrent =
                          guests[index].inboundBaggage &&
                          baggage.weight === guests[index].inboundBaggage.weight;
                        return (
                          (!guest.inboundBaggage ||
                            baggage.weight >= guest.inboundBaggage.weight) && (
                            <Button
                              key={baggage.id}
                              variant="outlined"
                              color={isCurrent ? 'primary' : undefined}
                              style={{
                                width: 112,
                                height: 64,
                                margin: 8,
                              }}
                              onClick={() => onSelect(baggage, index)}
                            >
                              <div style={{ display: 'flex', flexDirection: 'column' }}>
                                <Typography variant="body1">{baggage.weight}kg</Typography>
                                <Typography variant="body1">
                                  <FormattedNumber value={baggage.price} />
                                  &nbsp;
                                  <FormattedMessage id="currency" />
                                </Typography>
                              </div>
                            </Button>
                          )
                        );
                      })}
                    </>
                  )}
                </div>
              </Col>
            ),
        )
      ) : (
        <Typography variant="body1">
          <FormattedMessage
            id="order.notSupportBuyBaggage"
            values={{
              text: (
                <span style={{ textTransform: 'lowercase' }}>
                  <FormattedMessage id={isOutbound ? 'order.outbound' : 'order.inbound'} />
                </span>
              ),
            }}
          />
        </Typography>
      )}
    </Col>
  );
};
