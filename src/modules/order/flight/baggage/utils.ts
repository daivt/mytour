import { sumBy } from 'lodash';
import { some } from '../../../../constants';

export const TAX = 10;

export function calculateTax(num: number) {
  return Math.round((num * TAX) / (100 + TAX));
}

export function computeBaggagePayable(oldGuestInfo: some[], newGuestInfo: some[]) {
  const oldPrice =
    sumBy(oldGuestInfo, one => (one.outboundBaggage ? one.outboundBaggage.price : 0)) +
    sumBy(oldGuestInfo, one => (one.inboundBaggage ? one.inboundBaggage.price : 0));
  const newPrice =
    sumBy(newGuestInfo, one => (one.outboundBaggage ? one.outboundBaggage.price : 0)) +
    sumBy(newGuestInfo, one => (one.inboundBaggage ? one.inboundBaggage.price : 0));
  const totalPrice = newPrice - oldPrice;
  const totalTax = calculateTax(totalPrice);

  return {
    totalPrice,
    totalTax,
    totalPriceNoTax: totalPrice - totalTax,
  };
}
