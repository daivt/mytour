import { Button, ButtonBase, Paper, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { BLUE, ORANGE } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { DATE_FORMAT, DATE_FORMAT_BACK_END, DATE_TIME_FORMAT } from '../../../../../models/moment';
import { Row } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import { NewTabLink } from '../../../../common/components/NewTabLink';
import { Line, TypographyRow } from '../../../common/element';
import { getStatusColor, getTicketStatus } from '../../result/utils';

interface Props {
  data: some;
}

const FlightOrderDetailInfoBox: React.FC<Props> = props => {
  const { data } = props;
  const outboundTicketStatus = getTicketStatus(data, data.outbound);
  const inboundTicketStatus = data.isTwoWay ? getTicketStatus(data, data.inbound) : '';
  const invoiceInfo = data.vatInvoiceInfo;
  return (
    <Paper
      variant="outlined"
      style={{
        padding: 16,
        display: 'flex',
      }}
    >
      <div style={{ flex: 1 }}>
        <>
          <TypographyRow variant="subtitle1" style={{ paddingBottom: 8 }}>
            <FormattedMessage id="order.orderInfo" />
          </TypographyRow>
          <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
            <FormattedMessage id="order.status" />
            :&nbsp;
            <Typography
              variant="subtitle2"
              style={{
                color: `${getStatusColor(data.status)}`,
              }}
              component="span"
            >
              {!!data.status && <FormattedMessage id={`payment.${data.status}`} />}
            </Typography>
          </TypographyRow>
          <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
            <FormattedMessage id="order.bookingId" />
            :&nbsp;
            <Typography variant="subtitle2" component="span">
              {data.orderCode}
            </Typography>
          </TypographyRow>

          {data.parentBookingCode && (
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="order.separateFromOrder" />: {data.parentBookingCode}
            </TypographyRow>
          )}
          {data.isTwoWay ? (
            <>
              <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="order.outboundCode" />
                :&nbsp;
                <Typography
                  variant="subtitle2"
                  component="span"
                  style={{ color: outboundTicketStatus ? ORANGE : BLUE }}
                >
                  {outboundTicketStatus ? (
                    <FormattedMessage id={outboundTicketStatus} />
                  ) : (
                    data.outboundPnrCode
                  )}
                </Typography>
              </TypographyRow>
              <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="order.inboundCode" />
                :&nbsp;
                <Typography
                  variant="subtitle2"
                  component="span"
                  style={{ color: inboundTicketStatus ? ORANGE : BLUE }}
                >
                  {inboundTicketStatus ? (
                    <FormattedMessage id={inboundTicketStatus} />
                  ) : (
                    data.inboundPnrCode
                  )}
                </Typography>
              </TypographyRow>
            </>
          ) : (
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="order.bookingCode" />
              :&nbsp;
              <Typography
                variant="body2"
                component="span"
                style={{ color: outboundTicketStatus ? ORANGE : BLUE }}
              >
                {outboundTicketStatus ? (
                  <FormattedMessage id={outboundTicketStatus} />
                ) : (
                  data.outboundPnrCode
                )}
              </Typography>
            </TypographyRow>
          )}

          <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
            <FormattedMessage id="order.bookingDate" />
            :&nbsp;
            {moment(data.bookedDate, DATE_TIME_FORMAT).format('L HH:mm:ss')}
          </TypographyRow>
        </>
        <>
          {data.bookerContact && (
            <>
              <TypographyRow variant="subtitle1" style={{ paddingTop: 20, paddingBottom: 8 }}>
                <FormattedMessage id="order.customerInfo" />
              </TypographyRow>

              <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="fullName" />
                :&nbsp;
                <span style={{ textTransform: 'capitalize' }}>
                  {data.bookerContact.lastName} {data.bookerContact.firstName}
                </span>
              </TypographyRow>
              <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="telephone" />
                :&nbsp;
                <span style={{ textTransform: 'capitalize' }}>{data.bookerContact.phone}</span>
              </TypographyRow>
              <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="email" />
                :&nbsp;
                <a
                  href={`mailto:${data.bookerContact.email}`}
                  style={{ textDecoration: 'none', color: BLUE }}
                >
                  {data.bookerContact.email}
                </a>
              </TypographyRow>
            </>
          )}
        </>
        <>
          <TypographyRow variant="subtitle1" style={{ paddingTop: 20, paddingBottom: 8 }}>
            <FormattedMessage id="order.contactInfo" />
          </TypographyRow>
          <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
            <FormattedMessage id="fullName" />
            :&nbsp;
            <span style={{ textTransform: 'capitalize' }}>{data.mainContact.fullName}</span>
          </TypographyRow>
          <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
            <FormattedMessage id="phoneNumber" />
            :&nbsp;
            <span style={{ textTransform: 'capitalize' }}>{data.mainContact.phone1}</span>
          </TypographyRow>
          {data.mainContact.addr1 && (
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="address" />
              :&nbsp;
              {data.mainContact.addr1}
            </TypographyRow>
          )}
        </>
        <>
          <TypographyRow variant="subtitle1" style={{ marginBottom: 8, paddingTop: 20 }}>
            <FormattedMessage id="order.guestList" />
          </TypographyRow>
          {data.guests.map((value: some, index: number) => (
            <div key={index}>
              <TypographyRow variant="subtitle2">
                {value.fullName}
                &nbsp;-&nbsp;
                <Typography
                  variant="body1"
                  color="textSecondary"
                  component="span"
                  style={{ padding: '8px 0' }}
                >
                  <FormattedMessage
                    id={value.ageCategory === 'infant' ? 'baby' : value.ageCategory}
                  />
                </Typography>
              </TypographyRow>
              {!!value.dob && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="birthday" />
                  :&nbsp;
                  {moment(value.dob, DATE_FORMAT_BACK_END).format(DATE_FORMAT)}
                </TypographyRow>
              )}
              {!!value.outboundEticketNo && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.outboundTicketNumber" />: {value.outboundEticketNo}
                </TypographyRow>
              )}
              {value.outboundBaggage && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.outBaggages" />: {value.outboundBaggage.name}
                </TypographyRow>
              )}
              {!!value.inboundEticketNo && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.inboundTicketNumber" />: {value.inboundEticketNo}
                </TypographyRow>
              )}
              {!!value.inboundBaggage && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.inBaggages" />: {value.inboundBaggage.name}
                </TypographyRow>
              )}
              {!!value.nationality && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.nationalityCountry" />: {value.nationality}
                </TypographyRow>
              )}
              {!!value.passport && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.passport" />: {value.passport}
                </TypographyRow>
              )}
              {!!value.passportCountry && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.passportCountry" />: {value.passportCountry}
                </TypographyRow>
              )}
              {!!value.passportExpiry && (
                <TypographyRow variant="body2" color="textSecondary" style={{ padding: '8px 0' }}>
                  <FormattedMessage id="order.passportExpired" />:{' '}
                  {moment(value.passportExpiry, DATE_FORMAT_BACK_END).format('L')}
                </TypographyRow>
              )}
            </div>
          ))}
        </>
      </div>
      <div style={{ flex: 1 }}>
        {data.insuranceContact && (
          <div style={{ marginBottom: '16px' }}>
            <Typography variant="subtitle1" style={{ padding: '8px 0px' }}>
              <FormattedMessage id="order.travelInsurance" />
            </Typography>

            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.insurancePackage" />
              </Typography>
              <Typography variant="body2">
                {data.guests[0]?.insuranceInfo?.insurancePackageName}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.start" />
              </Typography>
              <Typography variant="body2">
                {data.guests[0].insuranceInfo &&
                  moment(data.guests[0].insuranceInfo.fromDate, DATE_FORMAT_BACK_END).format('L')}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.end" />
              </Typography>
              <Typography variant="body2">
                {data.guests[0].insuranceInfo &&
                  moment(data.guests[0].insuranceInfo.toDate, DATE_FORMAT_BACK_END).format('L')}
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.unitPrice" />
              </Typography>
              <Typography variant="body2">
                <FormattedMessage
                  id="order.unitPricePerPerson"
                  values={{
                    num: <FormattedNumber value={data.insuranceAmount / data.guests.length} />,
                  }}
                />
              </Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.numPassenger" />
              </Typography>
              <Typography variant="body2">{data.guests.length}</Typography>
            </Line>
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.finalPrice" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data.insuranceAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          </div>
        )}
        <>
          <Typography variant="subtitle1" style={{ paddingBottom: 8 }}>
            <FormattedMessage id="order.paymentDetail" />
          </Typography>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="order.totalPayable" />
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={data.amount} /> <FormattedMessage id="currency" />
            </Typography>
          </Line>
          {data.refundAmount > 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="payment.refundedPosted" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data.refundAmount} /> <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          {data.discount > 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.discountCode" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={0 - data.discount} /> <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="order.paymentMethod" />
            </Typography>
            <Typography variant="body2">{data.paymentMethod}</Typography>
          </Line>
          {!!data.paymentMethodFee && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage
                  id={
                    data.paymentMethodFee < 0
                      ? 'order.paymentFixedDiscountSide'
                      : 'order.paymentFixedFeeSide'
                  }
                />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data.paymentMethodFee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          {data.transferMoneyMethod && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.transferMoneyMethod" />
              </Typography>
              <Typography variant="body2">{data.transferMoneyMethod}</Typography>
            </Line>
          )}
          {data.pointAmount > 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.pointPayment" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data.paidPoint} />
                &nbsp;
                <FormattedMessage id="point" />
                &nbsp;/&nbsp;
                <FormattedNumber value={-data.pointAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          {!!data.refundAmount && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="refunded" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data.refundAmount} /> <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="order.paymentPrice" />
            </Typography>
            <Typography variant="subtitle2">
              <FormattedNumber value={data.finalPrice} /> <FormattedMessage id="currency" />
            </Typography>
          </Line>
          {data.bonusPoint > 0 && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.pointBonus" />
              </Typography>
              <Typography variant="body2">
                <FormattedNumber value={data.bonusPoint} /> <FormattedMessage id="point" />
              </Typography>
            </Line>
          )}
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="order.paymentStatus" />
            </Typography>
            <Typography
              variant="subtitle2"
              style={{
                color: `${getStatusColor(data.paymentStatus)}`,
              }}
            >
              {!!data.paymentStatus && <FormattedMessage id={`payment.${data.paymentStatus}`} />}
            </Typography>
          </Line>
        </>
        {invoiceInfo && (
          <>
            <TypographyRow variant="subtitle1" style={{ marginBottom: 8, paddingTop: 20 }}>
              <FormattedMessage id="order.invoiceInfo" />
            </TypographyRow>
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="order.taxNumber" />: {invoiceInfo.taxIdNumber}
            </TypographyRow>
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="order.companyName" />: {invoiceInfo.companyName}
            </TypographyRow>
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="order.companyAddress" />: {invoiceInfo.companyAddress}
            </TypographyRow>
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="email" />: {invoiceInfo.recipientEmail}
            </TypographyRow>
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="order.recipientAddress" />: {invoiceInfo.recipientAddress}
            </TypographyRow>
            {invoiceInfo.note && (
              <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
                <FormattedMessage id="order.note" />: {invoiceInfo.note}
              </TypographyRow>
            )}
            <TypographyRow variant="body2" style={{ padding: '8px 0' }}>
              <FormattedMessage id="order.status" />
              :&nbsp;
              <Typography
                variant="body2"
                component="span"
                style={{ color: getStatusColor(invoiceInfo.status || 'open') }}
              >
                <FormattedMessage
                  id={
                    invoiceInfo.status ? `payment.invoice.${invoiceInfo.status}` : 'payment.pending'
                  }
                />
              </Typography>
            </TypographyRow>

            {invoiceInfo.outboundAttachLinks && invoiceInfo.outboundAttachLinks.length > 0 && (
              <>
                <Row>
                  <TypographyRow variant="subtitle2" style={{ padding: '8px 0' }}>
                    <FormattedMessage id="order.invoice.outbound" />
                    :&nbsp;
                  </TypographyRow>
                  {invoiceInfo.outboundAttachLinks.map((v: string, index: number) => (
                    <NewTabLink href={v} key={index}>
                      <ButtonBase style={{ padding: 0, justifyContent: 'space-between' }}>
                        <TypographyRow variant="body2" style={{ color: BLUE }}>
                          <FormattedMessage
                            id="order.invoice.outbound.title"
                            values={{
                              num: invoiceInfo.outboundAttachLinks.length > 1 ? index + 1 : '',
                            }}
                          />{' '}
                        </TypographyRow>
                      </ButtonBase>
                    </NewTabLink>
                  ))}
                </Row>
              </>
            )}
            {invoiceInfo.inboundAttachLinks && invoiceInfo.inboundAttachLinks.length > 0 && (
              <>
                <Row>
                  <TypographyRow variant="subtitle2" style={{ padding: '8px 0' }}>
                    <FormattedMessage id="order.invoice.inbound" />
                    :&nbsp;
                  </TypographyRow>
                  {invoiceInfo.inboundAttachLinks.map((v: string, index: number) => (
                    <NewTabLink href={v} key={index}>
                      <ButtonBase style={{ padding: 0, justifyContent: 'space-between' }}>
                        <TypographyRow variant="body2" style={{ color: BLUE }}>
                          <FormattedMessage
                            id="order.invoice.inbound.title"
                            values={{
                              num: invoiceInfo.inboundAttachLinks.length > 1 ? index + 1 : '',
                            }}
                          />{' '}
                        </TypographyRow>
                      </ButtonBase>
                    </NewTabLink>
                  ))}
                </Row>
              </>
            )}
          </>
        )}
        {data.paymentStatus === 'holding' && moment(data.expiredTime).isAfter(moment()) && (
          <Link
            to={{
              pathname: ROUTES.order.flight.holdingPay,
              state: { holdingPaymentData: data },
            }}
          >
            <Button
              variant="contained"
              color="secondary"
              disableElevation
              style={{ marginTop: '16px', width: 158 }}
            >
              <Typography variant="button">
                <FormattedMessage id="pay" />
              </Typography>
            </Button>
          </Link>
        )}
      </div>
      {/* <OrderRequestList bookingId={data.id} bookModule="flight" /> */}
    </Paper>
  );
};

export default FlightOrderDetailInfoBox;
