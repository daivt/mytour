import { Button, Paper, Typography } from '@material-ui/core';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { GREY_300, PRIMARY } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { DATE_TIME_FORMAT } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import { ReactComponent as IconLuggage } from '../../../../../svg/booking/luggage.svg';
import Link from '../../../../common/components/Link';
import { FlightAirlineInfoItem } from '../../common/FlightAirlineInfoItem';
import FlightOrderDetailInfoBox from './FlightOrderDetailInfoBox';

interface Props {
  data: some;
}

const FlightOrderDetailDesktop: React.FunctionComponent<Props> = props => {
  const { data } = props;
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight);

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );
  const getFlightGeneralClasses = React.useCallback(
    (code: string) => {
      return generalFlight?.ticketclass?.find((v: some) => v.code === code) || {};
    },
    [generalFlight],
  );

  const ticketExpired = data?.isTwoWay
    ? moment(
        `${data?.inbound.departureDate} ${data?.inbound.departureTime}`,
        DATE_TIME_FORMAT,
      ).isSameOrAfter(moment())
    : moment(
        `${data?.outbound.departureDate} ${data?.outbound.departureTime}`,
        DATE_TIME_FORMAT,
      ).isSameOrAfter(moment());
  return (
    <>
      {data.paymentStatus === 'success' && (
        <>
          {ticketExpired && (
            <>
              <div
                style={{
                  minWidth: 160,
                  marginTop: 24,
                }}
              >
                <Link
                  to={{
                    pathname: ROUTES.order.flight.baggage.gen(data.id),
                    state: {
                      baggageData: data,
                    },
                  }}
                >
                  <Button variant="outlined" color="primary">
                    <Typography variant="body2">
                      <FormattedMessage id="order.baggage" />
                    </Typography>
                    <IconLuggage className="svgFillAll" style={{ marginLeft: 8, fill: PRIMARY }} />
                  </Button>
                </Link>
              </div>
            </>
          )}
        </>
      )}
      <Paper
        variant="outlined"
        style={{ display: 'flex', padding: '12px 24px', marginBottom: 24, marginTop: 24 }}
      >
        <div>
          <Typography variant="subtitle1" style={{ marginBottom: 8 }}>
            <FormattedMessage id="order.outbound" />
          </Typography>
          <FlightAirlineInfoItem
            airlineInfo={getFlightGeneralInfo(data.outbound.airlineId)}
            ticketClass={getFlightGeneralClasses(data.outbound.ticketClassCode)}
            ticket={data.outbound}
          />
        </div>
        {data.isTwoWay && (
          <>
            <div style={{ flex: 1, display: 'flex', justifyContent: 'center' }}>
              <div style={{ border: `0.5px solid ${GREY_300}`, width: 1 }} />
            </div>

            <div>
              <Typography variant="subtitle1" style={{ marginBottom: 8 }}>
                <FormattedMessage id="order.inbound" />
              </Typography>
              <FlightAirlineInfoItem
                isInbound
                airlineInfo={getFlightGeneralInfo(data.inbound.airlineId)}
                ticketClass={getFlightGeneralClasses(data.inbound.ticketClassCode)}
                ticket={data.inbound}
              />
            </div>
          </>
        )}
        {/* <Typography variant="body2" style={{ color: BLUE }}>
          <FormattedMessage id="m.order.flightDetail" />
        </Typography> */}
      </Paper>
      <FlightOrderDetailInfoBox data={data} />
    </>
  );
};

export default FlightOrderDetailDesktop;
