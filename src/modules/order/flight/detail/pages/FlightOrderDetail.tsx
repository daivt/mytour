import * as React from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import LoadingIcon from '../../../../common/components/LoadingIcon';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { fetchThunk } from '../../../../common/redux/thunk';
import NoDataResult from '../../../common/NoDataResult';
import FlightOrderDetailDesktop from '../components/FlightOrderDetailDesktop';

interface Props extends RouteComponentProps<{ bookingId: string }> {}

const FlightOrderDetail: React.FunctionComponent<Props> = props => {
  const { match } = props;
  const { bookingId } = match.params;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(false);

  const fetchData = React.useCallback(async () => {
    if (Number.isInteger(Number(bookingId))) {
      setLoading(true);
      const json = await dispatch(
        fetchThunk(`${API_PATHS.getFlightBookingDetail}?id=${bookingId}`, 'get'),
      );
      if (json.code === SUCCESS_CODE) {
        setData(json.data);
      }
    }
    setLoading(false);
  }, [bookingId, dispatch]);

  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  if (!Number.isInteger(Number(bookingId))) {
    return <RedirectDiv />;
  }

  if (loading) {
    return <LoadingIcon />;
  }
  if (!data) {
    return <NoDataResult id="order.noData" style={{ marginTop: 48 }} />;
  }

  return (
    <>
      <FlightOrderDetailDesktop data={data} />
    </>
  );
};

export default FlightOrderDetail;
