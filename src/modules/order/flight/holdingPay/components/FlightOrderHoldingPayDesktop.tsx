import { Paper, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { GREY_300 } from '../../../../../configs/colors';
import { some } from '../../../../../constants';
import DiscountCodeBox from '../../../../booking/common/payment/DiscountCodeBox';
import PaymentMethodsBox from '../../../../booking/common/payment/PaymentMethodsBox';
import { PAYMENT_TRIPI_CREDIT_CODE } from '../../../../booking/constants';
import { PointPayment, Promotions } from '../../../../booking/utils';
import { Line } from '../../../common/element';
import { computeHoldingBookingPayable, FlightHoldingPayParams } from '../utils';

interface Props {
  dataDetail: some;
  paying?: boolean;
  loadingPromotion: boolean;
  paymentMethods: some[];
  params: FlightHoldingPayParams;
  setParams(params: FlightHoldingPayParams): void;
  pointPaymentData: PointPayment;
  listPromotion?: Promotions;
  checkPromotion(code?: string): void;
  fetchRewardsHistory(search: string, page: number): void;
  pay(creditPassword?: string): void;
}

const dataDetailHoldingPayDesktop: React.FunctionComponent<Props> = props => {
  const {
    dataDetail,
    paymentMethods,
    params,
    setParams,
    pointPaymentData,
    listPromotion,
    fetchRewardsHistory,
    checkPromotion,
    paying,
    pay,
    loadingPromotion,
  } = props;

  const payableNumber = computeHoldingBookingPayable(params, dataDetail, pointPaymentData);
  const priceAfter = payableNumber.total - payableNumber.pointToAmount;
  const totalPayable = priceAfter + payableNumber.paymentFee;

  return (
    <>
      <>
        <Typography variant="h6" style={{ marginBottom: 8 }}>
          <FormattedMessage id="order.totalPay" />
        </Typography>
        <Paper variant="outlined" style={{ padding: '8px 16px', marginBottom: 16 }}>
          <Line>
            <Typography variant="body2">
              <FormattedMessage id="order.totalCost" />
            </Typography>
            <Typography variant="body2">
              <FormattedNumber value={payableNumber.originAmount} />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
          {!!payableNumber.discountAmount && (
            <Line>
              <Typography variant="body2">
                <FormattedMessage id="order.addPromotionCode" />
                &nbsp;
                {dataDetail.promotion && dataDetail.promotionCode}
                {dataDetail.dataDetailDetail && dataDetail.dataDetailDetail.promotionCode}
              </Typography>

              <Typography variant="body2">
                <FormattedNumber value={0 - payableNumber.discountAmount} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          {dataDetail.usePointPayment &&
            dataDetail.selectedPaymentMethod &&
            dataDetail.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE &&
            !!dataDetail.pointUsing && (
              <Line>
                <Typography variant="body2">
                  <FormattedMessage id="order.pointPayment" />
                </Typography>

                <Typography variant="body2">
                  <FormattedNumber value={-payableNumber.pointToAmount} />
                  &nbsp;
                  <FormattedMessage id="currency" />
                </Typography>
              </Line>
            )}
          {payableNumber.paymentFee > 0 && (
            <Line>
              {payableNumber.paymentFee > 0 ? (
                <Typography variant="body2">
                  <FormattedMessage id="order.paymentFixedFeeSide" />
                </Typography>
              ) : (
                <Typography variant="body2">
                  <FormattedMessage
                    id="order.discountPaymentMethod"
                    values={{
                      methodName: dataDetail.selectedPaymentMethod
                        ? dataDetail.selectedPaymentMethod.name
                        : '',
                    }}
                  />
                </Typography>
              )}
              <Typography
                variant="body2"
                // color={payableNumber.paymentFee > 0 ? 'secondary' : 'primary'}
              >
                <FormattedNumber value={payableNumber.paymentFee} />
                &nbsp;
                <FormattedMessage id="currency" />
              </Typography>
            </Line>
          )}
          <Line style={{ borderTop: `1px dashed ${GREY_300}`, paddingTop: 16, paddingBottom: 12 }}>
            <Typography variant="subtitle1">
              <FormattedMessage id="order.totalPayable" />
            </Typography>
            <Typography className="final-price" variant="subtitle1">
              <FormattedNumber
                value={totalPayable < 0 ? 0 : totalPayable}
                maximumFractionDigits={0}
              />
              &nbsp;
              <FormattedMessage id="currency" />
            </Typography>
          </Line>
        </Paper>
      </>
      {dataDetail && !dataDetail?.promotionCode && (
        <>
          <Paper variant="outlined" style={{ padding: 16 }}>
            <DiscountCodeBox
              fetchRewardsHistory={fetchRewardsHistory}
              checkPromotion={checkPromotion}
              promotion={params.promotion}
              listPromotion={listPromotion}
              promotionCode={params.promotionCode}
              loading={loadingPromotion}
            />
          </Paper>
        </>
      )}
      <>
        <Typography variant="h6" style={{ margin: '16px 0px 8px' }}>
          <FormattedMessage id="pay.paymentMethod" />
        </Typography>
        <Paper variant="outlined" style={{ padding: 16, marginBottom: 32 }}>
          <PaymentMethodsBox
            paymentMethods={paymentMethods}
            total={payableNumber.total}
            priceAfter={priceAfter}
            setSelectedMethod={method =>
              setParams({
                ...params,
                selectedPaymentMethod: method,
                usingPoint: false,
                point: pointPaymentData.min,
              })
            }
            selectedMethod={params.selectedPaymentMethod}
            promotionCode={params.promotionCode}
            pointPayment={pointPaymentData}
            point={params.point}
            usingPoint={params.usingPoint}
            onChangePoint={(point, usingPoint) => setParams({ ...params, point, usingPoint })}
            pay={pay}
            paying={paying}
          />
        </Paper>
      </>
    </>
  );
};

export default dataDetailHoldingPayDesktop;
