import { some } from '../../../../constants';
import { PointPayment, computePaymentFees } from '../../../booking/utils';
import { PAYMENT_TRIPI_CREDIT_CODE } from '../../../booking/constants';

export interface FlightHoldingPayParams {
  selectedPaymentMethod?: some;
  point: number;
  usingPoint: boolean;
  promotionCode?: string;
  promotion?: some;
}
export const defaultFlightHoldingPayParams: FlightHoldingPayParams = {
  point: 0,
  usingPoint: false,
};

export function computeHoldingBookingPayable(
  params: FlightHoldingPayParams,
  flightOrderDetail?: some,
  pointPaymentData?: PointPayment,
) {
  if (!flightOrderDetail) {
    return {
      originAmount: 0,
      discountAmount: 0,
      pointToAmount: 0,
      paymentFee: 0,
      total: 0,
    };
  }

  const originAmount = flightOrderDetail.grandTotal;

  const discountAmount =
    params.promotion && params.promotion.price
      ? originAmount - params.promotion.price
      : flightOrderDetail.discount;

  const pointToAmount = params.usingPoint
    ? pointPaymentData &&
      params.selectedPaymentMethod &&
      params.selectedPaymentMethod.code === PAYMENT_TRIPI_CREDIT_CODE
      ? params.point * pointPaymentData.pointFactor
      : 0
    : 0;

  const total = originAmount - discountAmount;

  const paymentFee = params.selectedPaymentMethod
    ? computePaymentFees(params.selectedPaymentMethod, total)
    : 0;

  return {
    originAmount,
    discountAmount,
    pointToAmount,
    paymentFee,
    total,
  };
}
