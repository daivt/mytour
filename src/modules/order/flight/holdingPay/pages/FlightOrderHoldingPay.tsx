import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../../configs/API';
import { ROUTES } from '../../../../../configs/routes';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { snackbarSetting } from '../../../../common/components/elements';
import RedirectDiv from '../../../../common/components/RedirectDiv';
import { goToAction } from '../../../../common/redux/reducer';
import { fetchThunk } from '../../../../common/redux/thunk';
import FlightOrderBaggagePayDesktop from '../components/FlightOrderHoldingPayDesktop';
import {
  FlightHoldingPayParams,
  defaultFlightHoldingPayParams,
  computeHoldingBookingPayable,
} from '../utils';
import { PAGE_SIZE_20 } from '../../../../booking/constants';
import {
  Promotions,
  computePaymentFees,
  defaultPointPayment,
  PointPayment,
  getPointPaymentData,
} from '../../../../booking/utils';
import LoadingIcon from '../../../../common/components/LoadingIcon';

interface Props {}

const FlightOrderHoldingPay: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();
  const [loading, setLoading] = React.useState(true);
  const [paying, setPaying] = React.useState(false);
  const [loadingPromotion, setLoadingPromotion] = React.useState(false);
  const [paymentMethods, setPayment] = React.useState<some[]>([]);
  const [params, setParams] = React.useState<FlightHoldingPayParams>(defaultFlightHoldingPayParams);
  const [pointPaymentData, setPointPaymentData] = React.useState<PointPayment>(defaultPointPayment);
  const [listPromotion, setListPromotion] = React.useState<Promotions | undefined>(undefined);

  const getData = React.useMemo(() => {
    return (location.state as some)?.holdingPaymentData;
  }, [location.state]);

  const fetchPaymentMethods = React.useCallback(async () => {
    if (!getData) {
      return;
    }
    const json = await dispatch(
      fetchThunk(API_PATHS.getPaymentMethodOfHoldingBooking, 'post', {
        module: 'flight',
        bookingId: getData.id,
      }),
    );

    if (json.code === SUCCESS_CODE || json.code === '200') {
      setPayment(json.data);
      json.data[0] && setParams(one => ({ ...one, selectedPaymentMethod: json.data[0] }));
    } else if (json.code === 400 || json.code === '400') {
      enqueueSnackbar(
        json.message,
        snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
      );
      dispatch(goToAction({ pathname: ROUTES.order.flight.result }));
    }
    setLoading(false);
  }, [closeSnackbar, dispatch, enqueueSnackbar, getData]);

  const fetchPointPayment = React.useCallback(async () => {
    if (!paymentMethods || !getData) {
      return;
    }
    const payable = computeHoldingBookingPayable(params, getData, pointPaymentData);
    const originAmount = getData.grandTotal;
    const amount = payable.total;

    const json = await dispatch(
      fetchThunk(`${API_PATHS.getPointPayment}`, 'post', {
        amounts: paymentMethods
          ? paymentMethods.map(obj => ({
              originAmount: originAmount + computePaymentFees(obj, originAmount),
              amount: amount + computePaymentFees(obj, originAmount),
              code: obj.code,
            }))
          : [],
        module: 'flight',
        usePromoCode: originAmount !== amount,
      }),
    );

    if (json.code === SUCCESS_CODE) {
      const tmp = getPointPaymentData(json.data);
      setParams(one => ({ ...one, point: tmp.min, usingPoint: false }));
      setPointPaymentData(tmp);
    }
  }, [dispatch, getData, params, paymentMethods, pointPaymentData]);

  const fetchRewardsHistory = React.useCallback(
    async (search: string = '', page: number = 1) => {
      if (!paymentMethods) {
        return;
      }
      setLoadingPromotion(true);
      const json = await dispatch(
        fetchThunk(`${API_PATHS.getRewardsHistory}`, 'post', {
          page,
          isUsed: false,
          term: search || '',
          info: {
            productType: 'flight',
          },
          module: 'flight',
          size: PAGE_SIZE_20,
        }),
      );
      if (json.code === SUCCESS_CODE) {
        setListPromotion(one =>
          one && page > 1
            ? {
                ...one,
                list: one.list.concat(json.data.list),
              }
            : json.data,
        );
      } else {
        enqueueSnackbar(
          json?.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setLoadingPromotion(false);
    },
    [closeSnackbar, dispatch, enqueueSnackbar, paymentMethods],
  );

  const checkPromotion = React.useCallback(
    async (code: string) => {
      if (!getData) {
        return;
      }
      const json = await dispatch(
        fetchThunk(`${API_PATHS.checkPromotion}`, 'post', {
          code,
          module: 'flight',
          originPoint: getData.bonusPoint,
          bookingId: getData.id,
        }),
      );

      if (json.code === SUCCESS_CODE) {
        setParams(one => ({ ...one, promotion: json.data, promotionCode: code }));
      } else {
        setParams(one => ({
          ...one,
          promotion: { message: json.message },
          promotionCode: undefined,
        }));
      }
    },
    [dispatch, getData],
  );

  const pay = React.useCallback(
    async (creditPassword?: string) => {
      if (!getData || !params.selectedPaymentMethod) {
        return;
      }
      setPaying(true);
      const json = await dispatch(
        fetchThunk(API_PATHS.paymentForHoldingBooking, 'post', {
          creditPassword,
          module: 'flight',
          promotionCode: params.promotionCode,
          point: params.usingPoint ? params.point : 0,
          paymentMethodId: params.selectedPaymentMethod.id,
          paymentMethodBankId: params.selectedPaymentMethod.bankId,
          bookingId: getData.id,
          callbackDomain: `${window.location.origin}`,
        }),
      );
      if (json.code === SUCCESS_CODE || json.code === SUCCESS_CODE.toString()) {
        if (json.data) {
          const paymentLink = json.data;
          window.location.replace(paymentLink);
        }
      } else {
        enqueueSnackbar(
          json.message,
          snackbarSetting(key => closeSnackbar(key), { color: 'error' }),
        );
      }
      setPaying(false);
    },
    [
      closeSnackbar,
      dispatch,
      enqueueSnackbar,
      getData,
      params.point,
      params.promotionCode,
      params.selectedPaymentMethod,
      params.usingPoint,
    ],
  );

  React.useEffect(() => {
    fetchPaymentMethods();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    fetchPointPayment();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [paymentMethods, params.promotion]);

  if (!getData) {
    return <RedirectDiv />;
  }
  if (loading) {
    return <LoadingIcon />;
  }
  return (
    <>
      <FlightOrderBaggagePayDesktop
        dataDetail={getData}
        paymentMethods={paymentMethods}
        params={params}
        setParams={setParams}
        pointPaymentData={pointPaymentData}
        listPromotion={listPromotion}
        fetchRewardsHistory={fetchRewardsHistory}
        checkPromotion={(code?: string) => {
          if (code) {
            checkPromotion(code);
          } else {
            setParams(one => ({ ...one, promotionCode: undefined, promotion: undefined }));
            setListPromotion(undefined);
          }
        }}
        pay={pay}
        loadingPromotion={loadingPromotion}
        paying={paying}
      />
    </>
  );
};

export default FlightOrderHoldingPay;
