import { Button, Divider, Paper, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { GREEN_300, RED_100, RED_300 } from '../../../../../configs/colors';
import { ROUTES } from '../../../../../configs/routes';
import { some } from '../../../../../constants';
import { DATE_TIME_FORMAT } from '../../../../../models/moment';
import { AppState } from '../../../../../redux/reducers';
import PassengerCount from '../../../../booking/common/PassengerCount';
import { Col } from '../../../../common/components/elements';
import Link from '../../../../common/components/Link';
import { FlightAirlineInfoItem } from '../../common/FlightAirlineInfoItem';
import { getStatusColor } from '../utils';

interface IFlightOrderItemProps {
  data: some;
}

const FlightBookingItem: React.FunctionComponent<IFlightOrderItemProps> = props => {
  const { data } = props;
  const generalFlight = useSelector((state: AppState) => state.common.generalFlight);

  const getFlightGeneralInfo = React.useCallback(
    (id: number) => {
      return generalFlight?.airlines?.find((v: some) => v.aid === id) || {};
    },
    [generalFlight],
  );
  const getFlightGeneralClasses = React.useCallback(
    (code: string) => {
      return generalFlight?.ticketclass?.find((v: some) => v.code === code) || {};
    },
    [generalFlight],
  );

  return (
    <Paper variant="outlined" style={{ marginBottom: 12, overflow: 'hidden' }}>
      {data.paymentStatus === 'holding' && (
        <>
          {moment(data.expiredTime).isAfter(moment()) ? (
            <div style={{ display: 'flex', padding: '8px 24px', backgroundColor: GREEN_300 }}>
              <Typography variant="subtitle1" style={{ color: 'white' }}>
                <FormattedMessage id="order.paymentExpiredTime" />
              </Typography>
              &nbsp;
              <Typography variant="subtitle1" style={{ color: 'white' }}>
                {moment(data.expiredTime).format('LT L')}
              </Typography>
            </div>
          ) : (
            <div style={{ display: 'flex', padding: '8px 24px', backgroundColor: RED_100 }}>
              <Typography variant="subtitle1" style={{ color: RED_300 }}>
                <FormattedMessage id="order.paymentExpired" />
              </Typography>
            </div>
          )}
        </>
      )}
      <div style={{ display: 'flex', padding: 16 }}>
        <div style={{ display: 'flex', flexDirection: 'column', minWidth: 400 }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <FlightAirlineInfoItem
              airlineInfo={getFlightGeneralInfo(data.outbound.airlineId)}
              ticketClass={getFlightGeneralClasses(data.outbound.ticketClassCode)}
              ticket={data.outbound}
            />
          </div>

          {data.isTwoWay && (
            <>
              <Divider style={{ margin: 12 }} />
              <FlightAirlineInfoItem
                airlineInfo={getFlightGeneralInfo(data.inbound.airlineId)}
                ticketClass={getFlightGeneralClasses(data.inbound.ticketClassCode)}
                ticket={data.inbound}
                isInbound
              />
            </>
          )}
        </div>
        <div style={{ paddingLeft: '30px', flex: 1 }}>
          <Typography variant="body2">
            <FormattedMessage id="order.status" />
            :&nbsp;
            <span
              style={{
                color: `${getStatusColor(data.paymentStatus)}`,
              }}
            >
              {!!data.paymentStatus && <FormattedMessage id={`payment.${data.paymentStatus}`} />}
            </span>
          </Typography>
          <Typography variant="body2">
            <FormattedMessage id="order.bookingId" />
            :&nbsp;
            <Typography variant="subtitle2" component="span">
              {data.id}
            </Typography>
          </Typography>
          {data.parentBookingCode && (
            <Typography variant="body2">
              <FormattedMessage id="order.separateFromOrder" />: {data.parentBookingCode}
            </Typography>
          )}
          <Typography variant="body2">
            <FormattedMessage id="order.customer" />
            :&nbsp;
            {data.mainContact ? data.mainContact.fullName : ''}
          </Typography>
          {data.bookedDate && (
            <Typography variant="body2">
              <FormattedMessage id="order.bookingDate" />
              :&nbsp;
              {moment(data.bookedDate, DATE_TIME_FORMAT).format('L HH:mm:ss')}
            </Typography>
          )}
          <Typography variant="body2">
            <FormattedMessage id="order.passengerCount" />
            :&nbsp;
            <PassengerCount
              adults={data.numAdults}
              child={data.numChildren}
              babies={data.numInfants}
            />
          </Typography>
        </div>

        <Col style={{ flex: 1, marginLeft: 32 }}>
          <Col
            className="card-background"
            style={{ alignItems: 'flex-end', padding: '8px 8px 8px 24px' }}
          >
            <Typography variant="subtitle2">
              <FormattedMessage id="order.totalPayable" />
            </Typography>
            <Typography className="final-price" variant="h6">
              {data.finalPriceFormatted}
            </Typography>
            <Typography variant="caption" color="textSecondary">
              <FormattedMessage id="flight.includeTaxesAndFees" />
            </Typography>
            {data.paymentStatus === 'holding' && moment(data.expiredTime).isAfter(moment()) && (
              <Link
                to={{
                  pathname: ROUTES.order.flight.holdingPayNow,
                  state: {
                    holdingPaymentData: data,
                  },
                }}
              >
                <Button
                  variant="contained"
                  color="secondary"
                  disableElevation
                  style={{ marginTop: '16px', width: 158 }}
                >
                  <Typography variant="button">
                    <FormattedMessage id="pay" />
                  </Typography>
                </Button>
              </Link>
            )}
            <Link to={ROUTES.order.flight.detail.gen(data.id)}>
              <Button variant="outlined" color="primary" style={{ width: 160, marginTop: 12 }}>
                <FormattedMessage id="order.viewDetail" />
              </Button>
            </Link>
          </Col>
        </Col>
      </div>
    </Paper>
  );
};

export const FlightBookingItemSkeleton: React.FunctionComponent = props => {
  return (
    <Paper
      variant="outlined"
      style={{ display: 'flex', marginBottom: 12, height: '175px', padding: 16 }}
    >
      <Col style={{ width: '120px', alignItems: 'center' }}>
        <Skeleton width="40px" height="40px" variant="circle" style={{ margin: '3px 0' }} />
        <Skeleton width="95px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="50px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="115px" height="12px" variant="text" style={{ margin: '3px 0' }} />
      </Col>
      <Col style={{ paddingLeft: '20px' }}>
        <Skeleton width="120px" variant="text" />
        <Skeleton width="230px" height="60px" variant="rect" style={{ marginTop: '6px' }} />
      </Col>

      <Col style={{ flex: 1, paddingLeft: '30px' }}>
        <Skeleton width="160px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="170px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="190px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="190px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="180px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="200px" height="12px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="170px" height="12px" variant="text" style={{ margin: '3px 0' }} />
      </Col>

      <Col
        className="card-background"
        style={{ alignItems: 'flex-end', padding: '8px 8px 8px 24px' }}
      >
        <Skeleton width="130px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="90px" variant="text" style={{ margin: '3px 0' }} />
        <Skeleton width="90px" variant="text" style={{ margin: '3px 0' }} />
        <div style={{ paddingTop: '12px' }}>
          <Skeleton width="160px" height="30px" variant="rect" style={{ margin: '3px 0' }} />
        </div>
      </Col>
    </Paper>
  );
};

export default FlightBookingItem;
