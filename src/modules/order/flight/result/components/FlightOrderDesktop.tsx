import { Button, Typography } from '@material-ui/core';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { BLUE } from '../../../../../configs/colors';
import { PAGE_SIZE, some } from '../../../../../constants';
import NoDataResult from '../../../common/NoDataResult';
import OrderFilters from '../../../common/OrderFilters';
import { OrderFiltersParams } from '../../../common/utils';
import FlightBookingItem, { FlightBookingItemSkeleton } from './FlightBookingItem';

interface Props {
  filterParams: OrderFiltersParams;
  setFilter(filterParams: OrderFiltersParams): void;
  setPagination(page: number): void;
  loading: boolean;
  data?: some;
}

const FlightOrderDesktop: React.FunctionComponent<Props> = props => {
  const { filterParams, setFilter, setPagination, loading, data } = props;

  return (
    <>
      <OrderFilters filterParams={filterParams} onChange={setFilter} loading={loading} />
      <div style={{ marginBottom: 32 }}>
        {data ? (
          <>
            {data.data?.map((one: some) => (
              <FlightBookingItem key={one.id} data={one} />
            ))}
            {!loading && data?.total - PAGE_SIZE * filterParams.paging.page > 0 && (
              <div style={{ textAlign: 'center', margin: '24px 0px' }}>
                <Button onClick={() => setPagination(filterParams.paging.page + 1)}>
                  <Typography style={{ color: BLUE }}>
                    <FormattedMessage
                      id="order.displayMore"
                      values={{ num: data.total - PAGE_SIZE * filterParams.paging.page }}
                    />
                  </Typography>
                </Button>
              </div>
            )}
            {loading && (
              <>
                <FlightBookingItemSkeleton />
                <FlightBookingItemSkeleton />
                <FlightBookingItemSkeleton />
              </>
            )}
            {!data.data.length && !loading && (
              <NoDataResult id="order.noData" style={{ marginTop: 48 }} />
            )}
          </>
        ) : (
          <>
            <FlightBookingItemSkeleton />
            <FlightBookingItemSkeleton />
            <FlightBookingItemSkeleton />
            <FlightBookingItemSkeleton />
            <FlightBookingItemSkeleton />
          </>
        )}
      </div>

    </>
  );
};

export default FlightOrderDesktop;
