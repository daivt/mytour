import { BLACK, BLUE, BLUE_300, GREEN, ORANGE, RED_300, YELLOW, RED_200 } from '../../../../configs/colors';
import { some } from '../../../../constants';

export function getTicketStatus(data: some, ticket: some) {
  if (ticket.ticketStatus === 'voided') {
    return 'payment.cancelByProvider';
  }

  if (data.paymentStatus === 'fail') {
    return 'order.noBookingCode';
  }

  return '';
}

export function durationMillisecondToHour(millisecond: number) {
  const second = millisecond / 1000;
  const hours = Math.floor(second / 3600);
  const minutes = Math.floor((second - hours * 3600) / 60);

  if (minutes) {
    return `${hours}h ${minutes}m`;
  }

  return `${hours}h`;
}
export function getStatusColor(status: string) {
  if (status === 'success' || status === 'completed') {
    return GREEN;
  }
  if (status === 'open') {
    return BLUE;
  }
  if (status === 'fail') {
    return RED_300;
  }
  if (status === 'handling') {
    return RED_200;
  }
  if (status === 'waiting') {
    return BLUE_300;
  }
  if (status === 'holding') {
    return ORANGE;
  }
  if (status === 'pending') {
    return ORANGE;
  }
  if (status === 'refunded') {
    return YELLOW;
  }

  return BLACK;
}
