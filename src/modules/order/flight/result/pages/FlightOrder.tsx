import queryString from 'query-string';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { API_PATHS } from '../../../../../configs/API';
import { some, SUCCESS_CODE } from '../../../../../constants';
import { AppState } from '../../../../../redux/reducers';
import { fetchThunk } from '../../../../common/redux/thunk';
import { defaultOrderFiltersParams, FilterParams, OrderFiltersParams } from '../../../common/utils';
import FlightOrderDesktop from '../components/FlightOrderDesktop';

interface Props {}

const FlightOrder: React.FunctionComponent<Props> = props => {
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();
  const location = useLocation();
  const history = useHistory();
  const [data, setData] = React.useState<some | undefined>(undefined);
  const [filters, setFilter] = React.useState<OrderFiltersParams>(defaultOrderFiltersParams);
  const prev = React.useRef(filters);
  const [loading, setLoading] = React.useState(false);

  const updateQueryParams = React.useCallback(() => {
    const filterParams = (queryString.parse(location.search) as unknown) as any;
    const filterTmp = {
      ...filterParams,
      paymentStatuses: filterParams.paymentStatuses && JSON.parse(filterParams.paymentStatuses),
    } as FilterParams;
    setFilter({ paging: defaultOrderFiltersParams.paging, filters: filterTmp });
  }, [location.search]);

  const fetchData = React.useCallback(
    async (filterParams: OrderFiltersParams) => {
      setLoading(true);
      if (filterParams.paging.page === 1) {
        setData(undefined);
      }
      const json = await dispatch(
        fetchThunk(`${API_PATHS.getFlightBookings}`, 'post', filterParams),
      );
      if (json.code === SUCCESS_CODE) {
        setData(one => {
          return {
            ...json,
            data:
              one?.data && filterParams.paging.page !== 1 ? [...one.data, ...json.data] : json.data,
          };
        });
      }
      setLoading(false);
    },
    [dispatch],
  );

  React.useEffect(() => {
    if (prev.current !== filters) {
      fetchData(filters);
    }
  }, [fetchData, filters]);

  React.useEffect(() => {
    updateQueryParams();
  }, [updateQueryParams]);

  return (
    <>
      <FlightOrderDesktop
        data={data}
        filterParams={filters}
        setFilter={value => {
          history.replace({
            search: queryString.stringify({
              ...value.filters,
              paymentStatuses: JSON.stringify(value.filters.paymentStatuses),
            }),
          });
        }}
        setPagination={value =>
          setFilter({
            ...filters,
            paging: { ...filters.paging, page: value },
          })
        }
        loading={loading}
      />
    </>
  );
};

export default FlightOrder;
