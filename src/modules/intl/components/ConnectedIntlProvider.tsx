import React from 'react';
import { IntlProvider } from 'react-intl';
import { connect } from 'react-redux';
import { some, MT_ID, CA_ID } from '../../../constants';
import { AppState } from '../../../redux/reducers.js';
import enMessages from '../en.json';
import viMessages from '../vi.json';
import viMTMessages from '../vi_my_tour.json';
import { EN_US } from '../../../models/intl';

function getMessages(locale: string): some {
  if (locale.startsWith(EN_US)) {
    if (CA_ID === MT_ID) {
      return { ...viMessages, ...viMTMessages };
    }
    return viMessages;
  }
  return enMessages;
}

function mapStateToProps(state: AppState) {
  return {
    locale: state.intl.locale,
    messages: getMessages(state.intl.locale),
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {}

const ConnectedIntlProvider: React.FunctionComponent<Props> = props => {
  return <IntlProvider {...props} />;
};

export default connect(mapStateToProps)(ConnectedIntlProvider);
