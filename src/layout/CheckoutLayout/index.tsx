import { Container } from '@material-ui/core';
import * as React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, useLocation } from 'react-router';
import { ROUTES_CHECKOUT } from '../../configs/routes';
import { PageWrapper } from '../../modules/common/components/elements';
import LoadingIcon from '../../modules/common/components/LoadingIcon';
import { AppState } from '../../redux/reducers';
import BookingBreadcrumbs from '../BookingLayout/BookingBreadcrumbs';
import { flatRoutes, getListRoutesActivate } from '../utils';

interface Props {}

const CheckoutLayout: React.FunctionComponent<Props> = props => {
  const userData = useSelector((state: AppState) => state.account.userData);
  const location = useLocation();

  const listRoutes = React.useMemo(() => {
    return getListRoutesActivate(userData?.roleGroup?.role, flatRoutes(ROUTES_CHECKOUT));
  }, [userData]);

  return (
    <>
      <PageWrapper>
        <div
          style={{
            flex: 1,
          }}
        >
          <Container>
            <BookingBreadcrumbs />
          </Container>
          <React.Suspense fallback={<LoadingIcon />}>
            <Switch location={location}>
              {listRoutes.map(
                (route, index) =>
                  route.component && (
                    <Route
                      key={index}
                      exact={route.exact}
                      path={route.path}
                      component={route.component}
                    />
                  ),
              )}
              {/* <Redirect to={ROUTES.booking.flight.default} /> */}
            </Switch>
          </React.Suspense>
        </div>
      </PageWrapper>
    </>
  );
};

export default CheckoutLayout;
