import React from 'react';
import { CA_ID, MT_ID } from '../../constants';
import MyTourFooter from '../myTour/MyTourFooter';

interface Props {}

const DefaultFooter: React.FC<Props> = () => {
  if (CA_ID === MT_ID) {
    return <MyTourFooter />;
  }
  return null;
};

export default DefaultFooter;
