import { AppBar, Container, fade, Typography, useTheme, useMediaQuery } from '@material-ui/core';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import { GREY_100, GREY_300, PRIMARY, RED } from '../../configs/colors';
import { ROUTES } from '../../configs/routes';
import { Col, Row } from '../../modules/common/components/elements';
import Link from '../../modules/common/components/Link';
import { NewTabLink } from '../../modules/common/components/NewTabLink';
import iconPhone from '../../svg/myTour/ic_phone.svg';
import { ReactComponent as Logo } from '../../svg/myTour/logo.svg';
import { HEADER_HEIGHT } from '../constants';
import { HeaderProps } from '../utils';
import LanguageSelect from '../../modules/intl/components/LanguageSelect';
import { DEV } from '../../constants';
import UserInfoDropdown from './UserInfoDropdown';

export const MenuLabelMT = styled.li`
  margin-right: 8px;
  border-radius: 26px;
  height: 26px;
  display: flex;
  align-items: center;
  font-size: 14px;
  position: relative;
  padding: 3px 6px;
  :hover {
    background: ${GREY_100};
  }
`;

const menus = [
  {
    name: 'header.hotel',
    url: 'https://mytour.vn/',
  },
  {
    name: 'header.flight',
    url: ROUTES.booking.flight.default,
    current: true,
    new: true,
  },
  {
    name: 'header.promotion',
    url: 'https://mytour.vn/promo',
  },
  {
    name: 'header.tour',
    url: 'https://mytour.vn/tour',
  },
  {
    name: 'header.handBook',
    url: 'https://mytour.vn/location',
  },
];

interface Props extends HeaderProps {}
const MyTourHeader: React.FC<Props> = props => {
  const { noSticky } = props;
  const history = useHistory();
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('md'));
  return (
    <AppBar
      position={noSticky ? 'relative' : 'sticky'}
      color="inherit"
      style={{
        minHeight: HEADER_HEIGHT,
        height: HEADER_HEIGHT,
        backgroundColor: 'white',
        boxShadow: 'none',
        borderRadius: 0,
        borderBottom: `1px solid ${GREY_300}`,
      }}
    >
      <Container
        style={{
          display: 'flex',
          alignItems: 'center',
          height: '100%',
          position: 'relative',
        }}
      >
        <a href="https://mytour.vn/" style={{ outline: 'none' }}>
          <Logo
            style={{
              marginRight: 32,
            }}
          />
        </a>
        <ul
          style={{
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'flex-start',
            listStyleType: 'none',
            flexShrink: 0,
          }}
        >
          {menus.map(menu => (
            <MenuLabelMT
              key={menu.name}
              style={{
                background: history.location.pathname.includes(menu.url) ? GREY_100 : undefined,
              }}
            >
              {menu.current ? (
                <>
                  <Link to={menu.url} style={{ color: 'inherit' }}>
                    <Typography variant="body2" color="textSecondary">
                      <FormattedMessage id={menu.name} />
                    </Typography>
                  </Link>
                </>
              ) : (
                <NewTabLink href={menu.url} style={{ color: 'inherit' }}>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id={menu.name} />
                  </Typography>
                </NewTabLink>
              )}
              {menu.new && (
                <Row
                  style={{
                    width: 32,
                    height: 16,
                    background: RED,
                    borderRadius: 12,
                    position: 'absolute',
                    top: -12,
                    right: 0,
                    justifyContent: 'center',
                  }}
                >
                  <Typography className="text-bold" style={{ color: 'white', fontSize: '75%' }}>
                    new
                  </Typography>
                </Row>
              )}
            </MenuLabelMT>
          ))}
        </ul>
        <div style={{ flex: 1 }} />
        {DEV && !isTablet && <LanguageSelect />}
        {!isTablet && (
          <Row
            style={{
              display: 'flex',
              margin: '8px 0px',
              minWidth: 280,
            }}
          >
            <Col
              style={{
                padding: 8,
                borderRadius: '4px',
                background: fade(PRIMARY, 0.2),
                color: 'black',
              }}
            >
              <Row style={{ height: 16, marginBottom: 2 }}>
                <img src={iconPhone} alt="" style={{ height: 20, width: 18, marginRight: 8 }} />
                <Typography variant="caption" style={{ fontSize: 10, lineHeight: '16px' }}>
                  <Typography
                    className="text-bold"
                    variant="caption"
                    style={{ fontSize: 10, lineHeight: '16px' }}
                    component="span"
                  >
                    Hà Nội
                  </Typography>
                  : 024 7109 9999
                </Typography>
              </Row>
              <Row style={{ height: 16, marginBottom: 2 }}>
                <Typography
                  className="text-bold"
                  variant="caption"
                  style={{ fontSize: 8, marginRight: 8, lineHeight: '16px' }}
                  component="span"
                >
                  24/7
                </Typography>
                <Typography variant="caption" style={{ fontSize: 10, lineHeight: '16px' }}>
                  <Typography
                    className="text-bold"
                    variant="caption"
                    style={{ fontSize: 10, lineHeight: '16px' }}
                    component="span"
                  >
                    TPHCM
                  </Typography>
                  : 028 7109 9998
                </Typography>
              </Row>
            </Col>

            <Col
              style={{
                alignItems: 'center',
                padding: 8,
                borderRadius: '4px',
                background: fade(PRIMARY, 0.2),
                marginLeft: 2,
                position: 'relative',
              }}
            >
              <div
                style={{
                  content: '',
                  position: 'absolute',
                  top: 3,
                  bottom: 3,
                  width: 1,
                  borderRight: '2px dashed #00adef4d',
                  left: -2,
                  zIndex: 1,
                }}
              />
              <Typography
                variant="caption"
                style={{ fontSize: 10, lineHeight: '16px', marginBottom: 2 }}
              >
                <FormattedMessage id="header.flightBookingSupport" />
              </Typography>
              <Typography
                className="text-bold"
                variant="caption"
                style={{ fontSize: 14, lineHeight: '16px', marginBottom: 2 }}
              >
                1900 2083
              </Typography>
            </Col>
          </Row>
        )}
        <UserInfoDropdown />
      </Container>
    </AppBar>
  );
};

export default MyTourHeader;
