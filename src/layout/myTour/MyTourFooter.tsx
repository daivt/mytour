import { Container, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';
import {
  FormattedHTMLMessage,
  FormattedMessage,
  injectIntl,
  WrappedComponentProps,
} from 'react-intl';
import styled from 'styled-components';
import {
  MYTOUR_BOOKING_POLICIES,
  MYTOUR_BOOKING_TOURING_POLICIES,
  MYTOUR_CLIENTS,
  MYTOUR_ABOUT,
} from './constants';
import { ReactComponent as Logo } from '../../svg/myTour/logo.svg';
import iconDisclaim from '../../svg/myTour/ic_business_disclaim.png';
import iconRegister from '../../svg/myTour/ic_business_register.png';
import iconAppstore from '../../svg/myTour/ic_download_appstore.png';
import iconGooglePlay from '../../svg/myTour/ic_download_google_play.png';
import iconFacebook from '../../svg/myTour/ic_facebook.svg';
import iconInstagram from '../../svg/myTour/ic_instagram.png';
import { Col, Row } from '../../modules/common/components/elements';
import { NewTabLink } from '../../modules/common/components/NewTabLink';

const FooterContainer = styled.div`
  background: #fafafa;
`;

const Line = styled(Typography)`
  padding-bottom: 8px;
`;

const Anchor = styled.a`
  text-decoration: none;
  color: unset;

  &:hover {
    color: #38c5f4;
  }
`;

interface Props extends WrappedComponentProps {
  compact?: boolean;
}

class MyTourFooter extends PureComponent<Props, {}> {
  renderAlphabetLinks = () => {
    const ALPHABET = Array(26)
      .fill(0)
      .map((_, y) => String.fromCharCode(y + 65));
    return (
      <Line variant="body2">
        {ALPHABET.map((value, index) => (
          <Anchor key={index} href={`https://mytour.vn/hotel-index/${value.toLowerCase()}.html`}>
            {value} -&nbsp;
          </Anchor>
        ))}
        <Anchor href="https://mytour.vn/area-index.html">
          <FormattedMessage id="footer.myTour.hotelsByLocation" />
        </Anchor>
      </Line>
    );
  };

  render() {
    return (
      <FooterContainer>
        <Container>
          <Logo
            style={{
              width: 160,
              margin: '0 0 12px -12px',
            }}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              flexWrap: 'wrap',
            }}
          >
            <Col style={{ flex: '1 0 380px', marginRight: 16 }}>
              <Line variant="subtitle2">
                <FormattedMessage id="footer.myTour.company" />
              </Line>
              <Line variant="body2">
                <FormattedMessage id="footer.myTour.phoneHanoi" />
              </Line>
              <Line variant="body2">
                <FormattedMessage id="footer.myTour.phoneSaigon" />
              </Line>
              <Line variant="body2">
                <FormattedMessage id="email" />
                :&nbsp;<a href="mailto:admin@mytour.vn">admin@mytour.vn</a>
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.hanoiOffice" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.saigonOffice" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.businessRegistration" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.businessRegistrationApproval" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.businessModal" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.domain" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.bankAccountNumber" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.bankAccountAddress" />
              </Line>
              <Line variant="body2">
                <FormattedHTMLMessage id="footer.myTour.bankAccountId" />
              </Line>
            </Col>

            <Col style={{ flex: '1 0 380px', marginRight: 16 }}>
              <Line variant="subtitle2">
                <FormattedMessage id="footer.myTour.bookingPolicies" />
              </Line>
              {MYTOUR_BOOKING_POLICIES.map(item => (
                <Line variant="body2" key={item.name}>
                  <Anchor href={item.url}>
                    <FormattedMessage id={item.name} />
                  </Anchor>
                </Line>
              ))}
              <NewTabLink href="http://online.gov.vn/Home/WebDetails/38507">
                <img src={iconDisclaim} alt="" style={{ width: 168, marginBottom: 20 }} />
              </NewTabLink>
              <Line variant="subtitle2">
                <FormattedMessage id="footer.myTour.bookingAndTouringPolicies" />
              </Line>
              {MYTOUR_BOOKING_TOURING_POLICIES.map(item => (
                <Line variant="body2" key={item.name}>
                  <Anchor href={item.url}>
                    <FormattedMessage id={item.name} />
                  </Anchor>
                </Line>
              ))}{' '}
              <NewTabLink href="http://online.gov.vn/Home/WebDetails/358">
                <img src={iconRegister} alt="" style={{ width: 168, marginBottom: 20 }} />
              </NewTabLink>
            </Col>

            <Col style={{ flex: '1 0 380px', marginRight: 16 }}>
              <Line variant="subtitle2">
                <FormattedMessage id="footer.myTour.hotels" />
              </Line>
              {this.renderAlphabetLinks()}

              <Line style={{ paddingTop: 6 }} variant="subtitle2">
                <FormattedMessage id="footer.myTour.client" />
              </Line>
              {MYTOUR_CLIENTS.map(item => (
                <Line variant="body2" key={item.name}>
                  <Anchor href={item.url}>
                    <FormattedMessage id={item.name} />
                  </Anchor>
                </Line>
              ))}

              <Line style={{ paddingTop: 6 }} variant="subtitle2">
                <FormattedMessage id="footer.myTour.partner" />
              </Line>
              <Line variant="body2">
                <Anchor href="https://hms.mytour.vn/auth/login">
                  <FormattedMessage id="footer.myTour.partnerSignIn" />
                </Anchor>
              </Line>

              <Line style={{ paddingTop: 6 }} variant="subtitle2">
                <FormattedMessage id="footer.myTour.about" />
              </Line>
              {MYTOUR_ABOUT.map(item => (
                <Line variant="body2" key={item.name}>
                  <Anchor href={item.url}>
                    <FormattedMessage id={item.name} />
                  </Anchor>
                </Line>
              ))}

              <Line style={{ paddingTop: 6 }} variant="subtitle2">
                <FormattedMessage id="footer.myTour.donwload" />
              </Line>
              <Row>
                <Anchor href="https://apps.apple.com/vn/app/mytour-vn-at-phong-khach-san/id1149730203">
                  <img src={iconAppstore} alt="Appstore" style={{ width: 148 }} />
                </Anchor>
                <Anchor href="https://play.google.com/store/apps/details?id=vn.mytour.apps.android">
                  <img src={iconGooglePlay} alt="Google Play" style={{ width: 148 }} />
                </Anchor>
              </Row>

              <Line style={{ paddingTop: 6 }} variant="subtitle2">
                <FormattedMessage id="footer.myTour.connect" />
              </Line>
              <Row>
                <Anchor href="https://www.facebook.com/mytour.vn/">
                  <img
                    src={iconFacebook}
                    alt="Facebook account"
                    style={{ width: 30, marginRight: 16 }}
                  />
                </Anchor>
                <Anchor href="https://www.instagram.com/mytour.vn/">
                  <img src={iconInstagram} alt="Instagram" style={{ width: 30 }} />
                </Anchor>
              </Row>
            </Col>
          </div>
        </Container>
      </FooterContainer>
    );
  }
}

export default injectIntl(MyTourFooter);
