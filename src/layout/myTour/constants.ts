export const MYTOUR_BOOKING_POLICIES = [
  {
    name: 'footer.myTour.termsAndPolicies',
    url: 'https://mytour.vn/news/135152-chinh-sach-va-quy-dinh-chung.html',
  },
  {
    name: 'footer.myTour.paymentPolicies',
    url: 'https://mytour.vn/news/135633-quy-dinh-ve-thanh-toan.html',
  },
  {
    name: 'footer.myTour.bookingConfirmationPolicies',
    url: 'https://mytour.vn/news/135634-quy-dinh-ve-xac-nhan-thong-tin-dat-phong.html',
  },
  {
    name: 'footer.myTour.bookingCancelPolicies',
    url: 'https://mytour.vn/news/135154-chinh-sach-huy-phong-va-hoan-tien.html',
  },
  {
    name: 'footer.myTour.privacyPolicies',
    url: 'https://mytour.vn/news/135636-chinh-sach-bao-mat-thong-tin-danh-cho-website-tmdt.html',
  },
];

export const MYTOUR_BOOKING_TOURING_POLICIES = [
  {
    name: 'footer.myTour.operationRegulation',
    url: 'https://mytour.vn/news/135155-quy-che-hoat-dong.html',
  },
  {
    name: 'footer.myTour.privacy',
    url: 'https://mytour.vn/news/135156-chinh-sach-bao-mat-thong-tin-danh-cho-san-gdtmdt.html',
  },
  {
    name: 'footer.myTour.handlingComplaints',
    url: 'https://mytour.vn/news/135420-quy-trinh-giai-quyet-tranh-chap-khieu-nai.html',
  },
];

export const MYTOUR_CLIENTS = [
  {
    name: 'footer.myTour.vPoint',
    url:
      'https://mytour.vn/promo-news/135699-chuong-trinh-diem-thuong-vpoint-danh-cho-thanh-vien-mytour.html',
  },
  {
    name: 'footer.myTour.signIn',
    url: 'https://hms.mytour.vn/auth/login',
  },
  {
    name: 'footer.myTour.register',
    url: 'https://mytour.vn/sign-up',
  },
];

export const MYTOUR_ABOUT = [
  {
    name: 'footer.myTour.introduction',
    url: 'https://mytour.vn/news/135150-gioi-thieu-ve-mytourvn.html',
  },
  {
    name: 'footer.myTour.news',
    url: 'https://mytour.vn/news',
  },
  {
    name: 'footer.myTour.recruitment',
    url: 'https://career.mytour.vn/',
  },
  {
    name: 'footer.myTour.contact',
    url: 'https://mytour.vn/help/30-lien-he.html',
  },
];
