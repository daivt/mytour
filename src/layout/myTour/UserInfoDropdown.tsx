import { Button, Collapse, Paper, Typography } from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import * as React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { BLUE_100, GREY_300, GREY_500, PRIMARY } from '../../configs/colors';
import { AppState } from '../../redux/reducers';
import { HEADER_HEIGHT } from '../constants';
import { TEST } from '../../constants';
import Link from '../../modules/common/components/Link';
import { ROUTES } from '../../configs/routes';

const RawLink = styled.a`
  text-decoration: none;
  color: unset;
`;
const Column = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px 20px;
  text-decoration: none;
  color: unset;
  &:hover {
    background: ${BLUE_100};
  }
  cursor: pointer;
  border-bottom: 0.5px solid ${GREY_300};
`;

interface Props {}

const UserInfoDropdown: React.FunctionComponent<Props> = props => {
  const userData = useSelector((state: AppState) => state.account.userData);
  const [open, setOpen] = React.useState(false);

  const onBlur = React.useCallback((e: React.FocusEvent<HTMLDivElement>) => {
    if (e.relatedTarget instanceof Element) {
      if (e.currentTarget.contains(e.relatedTarget as Element)) {
        return;
      }
    }

    setOpen(false);
  }, []);

  return (
    <>
      <div
        tabIndex={-1}
        onBlur={onBlur}
        style={{
          outline: 'none',
          marginLeft: 16,
        }}
      >
        <Button
          style={{
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'flex-end',
            cursor: 'pointer',
            color: GREY_500,
          }}
          onClick={() => {
            setOpen(!open);
          }}
        >
          <PermIdentityIcon color="inherit" />
          <ExpandLessIcon
            color="inherit"
            style={{
              transition: 'all 300ms',
              transform: open ? 'rotate(0deg)' : 'rotate(180deg)',
            }}
          />
        </Button>
        {userData ? (
          <Collapse
            in={open}
            style={{
              position: 'absolute',
              width: 170,
              color: 'black',
              zIndex: 110,
              top: HEADER_HEIGHT + 2,
              right: 24,
            }}
          >
            <Paper
              style={{
                overflow: 'hidden',
              }}
              variant="outlined"
            >
              <RawLink
                href={TEST ? 'https://ver3s.mytour.vn/sign-up' : 'https://mytour.vn/sign-up'}
              >
                <Column style={{ background: PRIMARY, color: 'white' }}>
                  <Typography variant="body2" color="inherit">
                    {userData.name}
                  </Typography>
                  <Typography variant="body2" color="inherit">
                    <FormattedMessage id="vPoint" />
                    &nbsp;
                    <FormattedNumber value={userData.credit} />
                  </Typography>
                </Column>
              </RawLink>
              <RawLink
                href={
                  TEST
                    ? 'https://ver3s.mytour.vn/account-info/order/hotel'
                    : 'https://mytour.vn/account-info/order/hotel'
                }
              >
                <Column>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id="managerHotel" />
                  </Typography>
                </Column>
              </RawLink>
              <Link to={{ pathname: ROUTES.order.flight.result }}>
                <Column>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id="managerFlight" />
                  </Typography>
                </Column>
              </Link>
              <RawLink
                href={
                  TEST
                    ? 'https://ver3s.mytour.vn/member/vpoint'
                    : 'https://mytour.vn/account-info/member/vpoint'
                }
              >
                <Column>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id="vPoint.bonus" />
                  </Typography>
                </Column>
              </RawLink>
              <RawLink
                href={
                  TEST
                    ? 'https://ver3s.mytour.vn/profile-info'
                    : 'https://mytour.vn/account-info/profile-info'
                }
              >
                <Column>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id="accountManager" />
                  </Typography>
                </Column>
              </RawLink>
            </Paper>
          </Collapse>
        ) : (
          <Collapse
            in={open}
            style={{
              position: 'absolute',
              width: 170,
              color: 'black',
              zIndex: 110,
              top: HEADER_HEIGHT + 2,
              right: 24,
            }}
          >
            <Paper
              style={{
                overflow: 'hidden',
              }}
              variant="outlined"
            >
              <RawLink
                href={TEST ? 'https://ver3s.mytour.vn/sign-up' : 'https://mytour.vn/sign-up'}
              >
                <Column>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id="login" />
                  </Typography>
                </Column>
              </RawLink>
              <RawLink
                href={TEST ? 'https://ver3s.mytour.vn/sign-up' : 'https://mytour.vn/sign-up'}
              >
                <Column>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id="signUp" />
                  </Typography>
                </Column>
              </RawLink>
              <RawLink
                href={
                  TEST
                    ? 'https://ver3s.mytour.vn/favorite-hotels'
                    : 'https://mytour.vn/favorite-hotels'
                }
              >
                <Column>
                  <Typography variant="body2" color="textSecondary">
                    <FormattedMessage id="favoriteHotel" />
                  </Typography>
                </Column>
              </RawLink>
            </Paper>
          </Collapse>
        )}
      </div>
    </>
  );
};

export default UserInfoDropdown;
