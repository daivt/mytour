import { Collapse, Tooltip, Typography } from '@material-ui/core';
import ArrowRightRounded from '@material-ui/icons/ArrowRightRounded';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { BLUE_200 } from '../../configs/colors';
import { some } from '../../constants';
import { RoutesTabType } from '../../models/permission';
import Link from '../../modules/common/components/Link';
import { AppState } from '../../redux/reducers';
import { ButtonRow } from './OrderAside';

const mapStateToProps = (state: AppState) => {
  return {
    router: state.router,
  };
};

interface Props extends ReturnType<typeof mapStateToProps> {
  data: RoutesTabType;
  pathname: string;
  open: boolean;
  listRouterActive: some[];
}

const OrderAsideItems: React.FC<Props> = (props: Props) => {
  const { data, pathname, open, router, listRouterActive } = props;
  const [openList, setOpen] = React.useState(false);

  const listRoutes = React.useMemo(() => {
    return data?.subMenu || [];
  }, [data]);

  const checkIsActive = React.useMemo(() => {
    let openTemp = false;
    listRouterActive?.forEach((item: some) => {
      if (item.name === data.name) {
        openTemp = true;
      }
    });
    return openTemp;
  }, [data.name, listRouterActive]);

  // const getOpenMenu = React.useMemo(() => {
  //   let tempOpen = true;
  //   if (!open) {
  //     tempOpen = false;
  //   }
  //   if (openList) {
  //     tempOpen = false;
  //   }
  //   return tempOpen;
  // }, [open, openList]);

  // const getOpenItem = React.useMemo(() => {
  //   let tempOpen = true;
  //   if (!open) {
  //     tempOpen = false;
  //   }
  //   if (checkIsActive) {
  //     tempOpen = false;
  //   }
  //   return tempOpen;
  // }, [checkIsActive, open]);

  React.useEffect(() => {
    setOpen(checkIsActive);
  }, [checkIsActive, pathname]);

  if (data.hidden) {
    return null;
  }

  if (data.ref) {
    return (
      <Tooltip
        title={data.title || data.name ? <FormattedMessage id={data.title || data.name} /> : ''}
        placement="right"
      >
        <a href={data.ref} style={{ textDecoration: 'none', color: 'unset' }}>
          <ButtonRow
            style={{
              backgroundColor: checkIsActive ? BLUE_200 : undefined,
              paddingLeft: data.isModule ? undefined : 24,
            }}
          >
            <Typography
              variant="body2"
              style={{
                flex: 1,
                fontWeight: data.isModule ? 500 : undefined,
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                width: 150,
              }}
            >
              {data.title ? (
                <FormattedMessage id={data.title} />
              ) : (
                data.name && <FormattedMessage id={data.name} />
              )}
            </Typography>
          </ButtonRow>
        </a>
      </Tooltip>
    );
  }
  return (
    <>
      {data.subMenu ? (
        <>
          <Tooltip
            title={data.title || data.name ? <FormattedMessage id={data.title || data.name} /> : ''}
            placement="right"
          >
            <ButtonRow
              style={{
                backgroundColor: openList ? BLUE_200 : undefined,
              }}
              onClick={() => setOpen(!openList)}
            >
              <Typography
                variant="subtitle2"
                style={{
                  flex: 1,
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap',
                  width: 150,
                }}
              >
                {data.title ? (
                  <FormattedMessage id={data.title} />
                ) : (
                  data.name && <FormattedMessage id={data.name} />
                )}
              </Typography>
              <ArrowRightRounded
                fontSize="large"
                style={{
                  transition: 'all 300ms',
                  transform: openList ? 'rotate(90deg)' : 'rotate(0deg)',
                  cursor: 'pointer',
                }}
              />
            </ButtonRow>
          </Tooltip>
          <Collapse in={openList && open}>
            {listRoutes.map((v: RoutesTabType, index: number) => (
              <OrderAsideItems
                key={index}
                open={open}
                listRouterActive={listRouterActive}
                data={v}
                pathname={pathname}
                router={router}
              />
            ))}
          </Collapse>
        </>
      ) : (
        <Link
          tooltip={data.title || data.name ? <FormattedMessage id={data.title || data.name} /> : ''}
          placement="right"
          to={{
            pathname: data.path,
            state: {
              [`${data.path}`]: true,
            },
          }}
          style={{ display: 'flex', flex: 1 }}
        >
          <ButtonRow
            style={{
              backgroundColor: checkIsActive ? BLUE_200 : undefined,
              paddingLeft: 24,
            }}
          >
            <Typography
              variant="body2"
              style={{
                flex: 1,
                fontWeight: data.isModule ? 500 : undefined,
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                width: 150,
              }}
            >
              {data.title ? (
                <FormattedMessage id={data.title} />
              ) : (
                data.name && <FormattedMessage id={data.name} />
              )}
            </Typography>
          </ButtonRow>
        </Link>
      )}
    </>
  );
};

export default connect(mapStateToProps)(OrderAsideItems);
