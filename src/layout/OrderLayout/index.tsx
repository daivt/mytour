import { Container, useMediaQuery, Typography } from '@material-ui/core';
import * as React from 'react';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { Redirect, Route, Switch, useLocation } from 'react-router';
import { FormattedMessage } from 'react-intl';
import { GREY_100 } from '../../configs/colors';
import { ROUTES, ROUTES_ORDER } from '../../configs/routes';
import { MUI_THEME } from '../../configs/setupTheme';
import { Col, PageWrapper, Row } from '../../modules/common/components/elements';
import LoadingIcon from '../../modules/common/components/LoadingIcon';
import BookingBreadcrumbs from '../BookingLayout/BookingBreadcrumbs';
import { HEADER_HEIGHT } from '../constants';
import MyTourHeader from '../myTour/MyTourHeader';
import { flatRoutes, getCurrentRoute } from '../utils';
import OrderAside from './OrderAside';

interface Props {}

const OrderLayout: React.FunctionComponent<Props> = props => {
  const location = useLocation();
  const { pathname } = location;
  const [openSideBar, setOpenSideBar] = React.useState(true);
  const getCurrent = React.useMemo(() => {
    return getCurrentRoute(pathname, ROUTES_ORDER);
  }, [pathname]);
  const matches = useMediaQuery(MUI_THEME.breakpoints.up('md'));

  React.useEffect(() => {
    setOpenSideBar(matches);
  }, [matches]);

  const listRoutes = React.useMemo(() => {
    return flatRoutes(ROUTES_ORDER);
  }, []);

  return (
    <>
      <PageWrapper style={{ background: GREY_100, flexDirection: 'row' }}>
        <Col
          style={{
            flex: 1,
            minHeight: '100vh',
          }}
        >
          <MyTourHeader />
          <Container
            style={{
              position: 'sticky',
              top: HEADER_HEIGHT,
              background: GREY_100,
              padding: 0,
              paddingLeft: 16,
              zIndex: 100,
            }}
          >
            <BookingBreadcrumbs disableNextStep />
          </Container>
          <Container style={{ padding: 0, paddingLeft: 16 }}>
            <Row style={{ alignItems: 'flex-start' }}>
              <OrderAside
                open={openSideBar}
                onClose={() => {
                  setOpenSideBar(!openSideBar);
                }}
              />
              <Col style={{ padding: '0px 16px 32px', flex: 1 }}>
                <Typography variant="h5">
                  {getCurrent?.title ||
                    (getCurrent?.name && (
                      <FormattedMessage id={getCurrent?.title || getCurrent?.name} />
                    ))}
                </Typography>
                <React.Suspense fallback={<LoadingIcon />}>
                  <Switch location={location}>
                    {listRoutes.map(
                      (route, index) =>
                        route.component && (
                          <Route
                            key={index}
                            exact={route.exact}
                            path={route.path}
                            component={route.component}
                          />
                        ),
                    )}
                    <Redirect to={ROUTES.order.notFound} />
                  </Switch>
                </React.Suspense>
              </Col>
            </Row>
          </Container>
        </Col>
      </PageWrapper>
    </>
  );
};

export default OrderLayout;
