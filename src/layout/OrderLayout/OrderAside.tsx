import { ButtonBase, Paper, withStyles } from '@material-ui/core';
import * as React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { connect } from 'react-redux';
import { BLUE_200 } from '../../configs/colors';
import { ROUTES_ORDER } from '../../configs/routes';
import { RoutesTabType } from '../../models/permission';
import { AppState } from '../../redux/reducers';
import '../../scss/svg.scss';
import { ASIDE_ITEM_HEIGHT, ASIDE_WIDTH, HEADER_HEIGHT } from '../constants';
import { getListRoutesContain } from '../utils';
import OrderAsideItems from './OrderAsideItems';

export const ButtonRow = withStyles(theme => ({
  root: {
    '&:hover': {
      background: BLUE_200,
    },
    height: ASIDE_ITEM_HEIGHT,
    display: 'flex',
    paddingLeft: 16,
    justifyContent: 'flex-start',
    minWidth: ASIDE_WIDTH,
    textAlign: 'start',
  },
}))(ButtonBase);

const mapStateToProps = (state: AppState) => {
  return { router: state.router };
};
interface Props extends ReturnType<typeof mapStateToProps> {
  open: boolean;
  onClose(): void;
}

const OrderAside: React.FunctionComponent<Props> = props => {
  const { router, open } = props;
  const { pathname } = router.location;
  const [hoverOpen, setOpen] = React.useState(false);

  const getListRouterActive = React.useMemo(() => {
    return getListRoutesContain(ROUTES_ORDER, router.location.pathname);
  }, [router.location.pathname]);

  return (
    <>
      <Paper
        elevation={2}
        style={{
          width: ASIDE_WIDTH,
          overflow: 'hidden',
          position: 'sticky',
          top: HEADER_HEIGHT + 67,
          transition: 'width 0.3s',
          // background: PRIMARY
          zIndex: 1200,
        }}
      >
        <PerfectScrollbar
          onMouseEnter={() => {
            setOpen(true);
          }}
          onMouseLeave={() => setOpen(false)}
        >
          <div
            style={{
              maxHeight: `calc(100%-64px)`,
              marginBottom: 16,
            }}
          >
            {ROUTES_ORDER.map((v: RoutesTabType, index: number) => (
              <OrderAsideItems
                key={index}
                open={open || hoverOpen}
                data={v}
                pathname={pathname}
                listRouterActive={getListRouterActive}
              />
            ))}
          </div>
        </PerfectScrollbar>
      </Paper>
    </>
  );
};

export default connect(mapStateToProps)(OrderAside);
