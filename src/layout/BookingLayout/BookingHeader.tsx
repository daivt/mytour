import * as React from 'react';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { CA_ID, MT_ID } from '../../constants';
import MyTourHeader from '../myTour/MyTourHeader';
import { HeaderProps } from '../utils';

interface Props extends HeaderProps {}

const BookingHeader: React.FunctionComponent<Props> = props => {
  if (CA_ID === MT_ID) {
    return <MyTourHeader {...props} />;
  }
  return null;
};

export default BookingHeader;
