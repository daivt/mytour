import { Container } from '@material-ui/core';
import * as React from 'react';
import Helmet from 'react-helmet';
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch, useLocation } from 'react-router';
import { ROUTES, ROUTES_BOOKING } from '../../configs/routes';
import { Col, PageWrapper } from '../../modules/common/components/elements';
import LoadingIcon from '../../modules/common/components/LoadingIcon';
import { AppState } from '../../redux/reducers';
import DefaultFooter from '../layout/DefaultFooter';
import { flatRoutes, getCurrentRoute, getListRoutesActivate } from '../utils';
import BookingBreadcrumbs from './BookingBreadcrumbs';
import BookingHeader from './BookingHeader';

interface Props {}

const BookingLayout: React.FunctionComponent<Props> = props => {
  const userData = useSelector((state: AppState) => state.account.userData);
  const location = useLocation();

  const listRoutes = React.useMemo(() => {
    return getListRoutesActivate(userData?.roleGroup?.role, flatRoutes(ROUTES_BOOKING));
  }, [userData]);

  const getRoute = React.useMemo(() => {
    return getCurrentRoute(location.pathname, ROUTES_BOOKING);
  }, [location.pathname]);

  return (
    <>
      <Helmet>
        <script async src="//mytour.api.useinsider.com/ins.js?id=10001599" />
      </Helmet>
      <PageWrapper>
        <BookingHeader noSticky={getRoute?.noStickyHeader} />
        <div
          style={{
            flex: 1,
          }}
        >
          <Container>
            <BookingBreadcrumbs />
          </Container>
          <Col>
            <React.Suspense fallback={<LoadingIcon />}>
              <Switch location={location}>
                {listRoutes.map(
                  (route, index) =>
                    route.component && (
                      <Route
                        key={index}
                        exact={route.exact}
                        path={route.path}
                        component={route.component}
                      />
                    ),
                )}
                <Redirect to={ROUTES.notFound} />
              </Switch>
            </React.Suspense>
          </Col>
        </div>
        <DefaultFooter />
      </PageWrapper>
    </>
  );
};

export default BookingLayout;
