import { PAGE_SIZE } from "../constants";

export interface Pagination {
  pageOffset?: number;
  totalResults?: number;
  pageSize?: number;
}

export interface PaginationFilter {
  pageOffset: number;
  pageSize: number;
}

export const defaultPagination: Pagination = {};

export const defaultPaginationFilter: PaginationFilter = {
  pageOffset: 0,
  pageSize: PAGE_SIZE,
};

export interface Pagination2 {
  page?: number;
  totalResults?: number;
  size?: number;
}

export interface PaginationFilter2 {
  page: number;
  size: number;
}

export const defaultPagination2: Pagination2 = {};

export const defaultPaginationFilter2: PaginationFilter2 = {
  page: 1,
  size: PAGE_SIZE,
};
