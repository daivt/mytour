export const validNumberRegex = /^[0-9]*$/;
export const validPhoneNumberRegex = /^[0-9+]*$/;
export const specialCharacterRegex = /[\]\\[!@#$%^&*(),.?":{}|<>+_;=~`'\\/-]/;
export const rawAlphabetRegex = /^[a-zA-Z ]*$/;
export const noSpecialText = /^[a-zA-Z0-9 ]*$/;
