import 'moment/locale/vi';
import React from 'react';
import ReactGa from 'react-ga';
import Helmet from 'react-helmet';
import { connect, useDispatch } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { BOOKING_URL, CHECKOUT_URL, ORDER_URL, PAYMENT_URL, ROUTES } from './configs/routes';
import { KEY_GOOGLE_ANALYTICS } from './constants';
import BookingLayout from './layout/BookingLayout';
import CheckoutLayout from './layout/CheckoutLayout';
import OrderLayout from './layout/OrderLayout';
import { validateAccessToken } from './modules/auth/redux/authThunks';
import Terms from './modules/booking/common/Terms';
import { scrollTo } from './modules/booking/utils';
import AuthProblemDialog from './modules/common/components/AuthProblemDialog';
import LoadingIcon from './modules/common/components/LoadingIcon';
import NetworkProblemDialog from './modules/common/components/NetworkProblemDialog';
import ProtectedRoute from './modules/common/components/ProtectedRoute';
import { fetchGeneralData } from './modules/common/redux/reducer';
import { AppState } from './redux/reducers';
import './scss/myTour.scss';

function mapStateToProps(state: AppState) {
  return {
    router: state.router,
    auth: state.auth.auth,
  };
}

interface Props extends ReturnType<typeof mapStateToProps> {}

const App: React.FC<Props> = props => {
  const { router, auth } = props;
  const dispatch = useDispatch<ThunkDispatch<AppState, null, Action<string>>>();

  React.useEffect(() => {
    dispatch(validateAccessToken());
    dispatch(fetchGeneralData());
  }, [dispatch]);

  React.useEffect(() => {
    setTimeout(() => {
      scrollTo('root', 0);
    }, 0);
  }, [router.location.pathname]);

  React.useEffect(() => {
    ReactGa.initialize(KEY_GOOGLE_ANALYTICS, {
      debug: false,
      titleCase: false,
    });
  }, []);

  return (
    <>
      <Helmet link={[{ rel: 'shortcut icon', href: '/static/mytour.ico' }]} />
      <NetworkProblemDialog />
      <AuthProblemDialog />
      <React.Suspense fallback={<LoadingIcon />}>
        <Switch>
          <ProtectedRoute auth={auth} path={ORDER_URL} component={OrderLayout} />
          <Route exact path={ROUTES.terms} component={Terms} />
          <Route path={PAYMENT_URL} component={CheckoutLayout} />
          <Route path={CHECKOUT_URL} component={CheckoutLayout} />
          <Route path={BOOKING_URL} component={BookingLayout} />
        </Switch>
      </React.Suspense>
    </>
  );
};

export default connect(mapStateToProps)(App);
