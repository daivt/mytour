const proxy = require('http-proxy-middleware');

const router = {
  '/api/general': 'https://dev-api.tripi.vn',
  '/api/hotel': 'https://dev-api.tripi.vn',
};
const router2 = {
  '/test': 'http://172.20.180.93:8080',
};

module.exports = function(app) {
  app.use(
    proxy('/api/', {
      target: 'https://dev-api.tripi.vn',
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        '^/api/general/': '/',
        '^/api/flight/': '/',
        '^/api/hotel/': '/',
      },
      router,
      logLevel: 'debug',
    }),
  );
  app.use(
    proxy('/test', {
      target: 'http://172.20.180.93:8080',
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        '^/test': '/',
      },
      router: router2,
      logLevel: 'debug',
    }),
  );
};
